"use strict";

class Map {


    constructor(mapId, options) {
        this.mapId      = mapId;
        this.options    = options;
        this.map        = new google.maps.Map(document.getElementById(this.mapId), this.options);
        this.markers    = [];
        this.stores     = [];
        this.colors     = [
            '2F4F4F',
            '4dad36',
            'e2cc2d',
            'bd0f0f'
        ];
        this.colors2    = [
            'ff00ff',
            '00ffff',
            'ff0000',
            '00ff00',
            '0000ff',
            'ffff00',
            '000000',
            '232489',
            'ff9966',
            'cc0099',
            '99ff99',
            '996633'
        ];
        this.colorIndex = 0;
        this.storeColor = [];
    }

    addMarker(id, options) {
        if (this.markers[id]) {
            this.markers[id].setMap(null);
            delete this.markers[id];
        }

        let marker       = new google.maps.Marker(options);
        this.markers[id] = marker;
        marker._id       = id;
        return marker;
    }

    /**
     * Custom functions
     */

    addStore(store, onClick) {

        if (this.stores[store]) {
            return;
        }

        let oReq = new XMLHttpRequest();
        oReq.open("GET", "/api/shop/store/" + store + '?parts=location');
        oReq.send();
        oReq.onload = () => {
            store = JSON.parse(oReq.response);

            onClick = onClick || (() => false);
            let infowindow = new google.maps.InfoWindow({
                content: store.name
            });

            if (!store.location) {
                return null;
            }
            let marker = this.addMarker(store.id, {
                position: new google.maps.LatLng(store.location.latitude, store.location.longitude),
                icon: 'https://www.google.com/maps/vt/icon/name=assets/icons/poi/tactile/pinlet_shadow-2-medium.png,assets/icons/poi/tactile/pinlet-2-medium.png,assets/icons/poi/quantum/pinlet/shoppingbag_pinlet-1-small.png&highlight=ffffffff,' + this.storeColor[store.id] + ',ffffff&color=ff000000?scale=1',
                map: this.map
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);

                //onClick(store, marker);
            });

            return marker;
        };

        this.stores[store] = true;


        /**/
    }

    addOrder(order, onClick) {
        onClick = onClick || (() => false);

        if (Math.abs(order.destination.latitude + order.destination.longitude) < 0.0001) {
            console.warn('order has no valid coordinates');
            return;
        }

        let now   = new Date();
        let max   = parseAtomDate(order.endTime);
        let diff  = max - now;
        let color = this.colors[3];

        if (diff < 0) {
            color = this.colors[0];
        } else if (diff < 3600000) {
            color = this.colors[1];
        } else if (diff < 7200000) {
            color = this.colors[2];
        }

        let infowindow = new google.maps.InfoWindow({
            content: order.destination.address
        });

        if (!this.storeColor[order.store.id]) {
            this.storeColor[order.store.id] = this.colors2[this.colorIndex % this.colors.length];
            this.colorIndex++;
            this.addStore(order.store.id);
        }

        let marker = this.addMarker(order.id, {
            position: new google.maps.LatLng(order.destination.latitude, order.destination.longitude),
            icon: 'https://www.google.com/maps/vt/icon/name=assets/icons/poi/tactile/pinlet_shadow-2-medium.png,assets/icons/poi/tactile/pinlet-2-medium.png,assets/icons/poi/quantum/pinlet/dot_pinlet-2-medium.png&highlight=ffffffff,' + color + ',' + this.storeColor[order.store.id] + '&color=ff000000?scale=1',
            scale: [20, 50],
            map: this.map
        });

        marker.addListener('click', () => {
            infowindow.open(map, marker);
            let divs = document.getElementsByClassName('order');
            for (let div of divs) {
                div.className = 'order hidden';
            }
            let divOrder       = document.getElementById(order.id);
            divOrder.className = 'order';
            onClick(order, marker);
        });

        return marker;
    }
}
