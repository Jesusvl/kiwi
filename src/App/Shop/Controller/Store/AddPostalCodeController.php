<?php

declare(strict_types=1);

namespace App\Shop\Controller\Store;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Store\Command\AddPostalCode;
use KiwiInfrastructure\Core\Http\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AddPostalCodeController.
 *
 * @Route("/api/shop/store")
 */
class AddPostalCodeController extends Controller
{
    /**
     * @Route("/{id}/add-postal-code")
     * @Method("POST")
     *
     * @param Request $request
     * @param Bus     $bus
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request, Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new AddPostalCode(
            $this->checkUser()->id(),
            $id,
            $request->check('postalCodes')
        ));

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
}
