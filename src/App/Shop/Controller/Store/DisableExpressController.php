<?php

declare(strict_types=1);

namespace App\Shop\Controller\Store;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Store\Command\DisableStoreExpress;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DisableExpressController.
 *
 * @Route("/api/shop")
 */
class DisableExpressController extends Controller
{
    /**
     * @Route("/store/{id}/disable-express")
     * @Method("POST")
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return JsonResponse
     */
    public function indexAction(Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new DisableStoreExpress($this->checkUser()->id(), $id));

        return new JsonResponse(null, JsonResponse::HTTP_ACCEPTED);
    }
}
