<?php

declare(strict_types=1);

namespace App\Shop\Controller\Store;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Store\Command\CloseTimespan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CloseTimespanController.
 *
 * @Route("/api/shop")
 */
class CloseTimespanController extends Controller
{
    /**
     * @Route("/timespan/{id}/close")
     * @Method("POST")
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return JsonResponse
     */
    public function indexAction(Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new CloseTimespan($this->checkUser()->id(), $id));

        return new JsonResponse(null, JsonResponse::HTTP_ACCEPTED);
    }
}
