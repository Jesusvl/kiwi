<?php

declare(strict_types=1);

namespace App\Shop\Controller\Store;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use KiwiInfrastructure\Core\Http\Request;
use Kiwi\Shop\Store\Command\AddStoreTimespan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AddTimespanController.
 *
 * @Route("/api/shop/store")
 */
class AddTimespanController extends Controller
{
    /**
     * @Route("/{id}/add-timespan")
     * @Method("POST")
     *
     * @param Request $request
     * @param Bus     $bus
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request, Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new AddStoreTimespan(
            $this->checkUser()->id(),
            $id,
            $request->checkDateTime('start_time'),
            $request->checkDateTime('end_time')
        ));

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
}
