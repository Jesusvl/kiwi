<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Query\FindUser;
use KiwiInfrastructure\Core\Security\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Controller.
 */
abstract class Controller extends BaseController
{
    /**
     * @var Bus
     */
    protected $bus;

    /**
     * Controller constructor.
     *
     * @param Bus $bus
     */
    public function __construct(Bus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @return User
     */
    protected function checkUser(): User
    {
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        return $user;
    }

    /**
     * @return string
     */
    public function merchantId(): string
    {
        $user = $this->checkUser();
        /** @var UserDocument $userDoc */
        $userDoc = $this->bus->dispatchQuery(new FindUser($user->id(), $user->id()));

        return $userDoc->merchantId();
    }
}
