<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController.
 */
class DefaultController extends Controller
{
    /**
     * @Route()
     * @Method("GET")
     *
     * @return RedirectResponse
     */
    public function indexAction(): RedirectResponse
    {
        return $this->redirect($this->generateUrl('app_core_security_login'));
    }
}
