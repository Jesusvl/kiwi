<?php

declare(strict_types=1);

namespace App\Shop\Controller\Client;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Client\Query\FindClients;
use KiwiInfrastructure\Core\Http\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ListController.
 *
 * @Route("/api/shop/client")
 */
class ListController extends Controller
{
    /**
     * @Route()
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request, Bus $bus): JsonResponse
    {
        return new JsonResponse($bus->dispatchQuery(new FindClients(
            $this->checkUser()->id(),
            $request->getInt('page', 1),
            $this->getParameter('web_paginator_size'),
            $request->getParts()
        ))->toScalar());
    }
}
