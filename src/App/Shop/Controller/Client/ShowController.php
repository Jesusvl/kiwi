<?php

declare(strict_types=1);

namespace App\Shop\Controller\Client;

use App\Shop\Controller\Controller;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Client\Query\FindClient;
use Kiwi\Shop\Client\Query\FindClients;
use KiwiInfrastructure\Core\Http\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ListController.
 *
 * @Route("/api/shop/client/{id}")
 */
class ShowController extends Controller
{
    /**
     * @Route()
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus     $bus
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request, Bus $bus, string $id): JsonResponse
    {
        return new JsonResponse($bus->dispatchQuery(new FindClient(
            $this->checkUser()->id(),
            $id,
            $request->getParts()
        ))->toScalar());
    }
}
