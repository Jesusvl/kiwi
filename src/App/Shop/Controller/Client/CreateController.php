<?php

declare(strict_types=1);

namespace App\Shop\Controller\Client;

use App\Shop\Controller\Controller;
use Kiwi\Shop\Client\Command\CreateClient;
use Kiwi\Shop\Client\Query\FindClient;
use Kiwi\Shop\Location\Command\CreateLocation;
use KiwiInfrastructure\Core\Http\Request;
use KiwiLib\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CreateController.
 */
class CreateController extends Controller
{
    /**
     * @Route("/api/shop/client")
     * @Method("POST")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request): JsonResponse
    {
        $user = $this->checkUser();

        $locationArray = $request->checkArray('location', [
            'address'         => 'string',
            'stair'           => 'string',
            'floor'           => 'string',
            'door'            => 'string',
            'city'            => 'string',
            'province'        => 'string',
            'postalCode'      => 'string',
            'hasElevator'     => 'boolean',
            'hasStepElevator' => 'boolean',
            'annotation'      => 'string',
            'latitude'        => 'float|opt|integer',
            'longitude'       => 'float|opt|integer'
        ]);

        $locationId = Uuid::new();
        $this->bus->dispatchCommand(new CreateLocation(
            $user->id(),
            $locationId,
            $locationArray['address'],
            $locationArray['stair'],
            $locationArray['floor'],
            $locationArray['door'],
            $locationArray['city'],
            $locationArray['province'],
            $locationArray['postalCode'],
            $locationArray['hasElevator'],
            $locationArray['hasStepElevator'],
            $locationArray['annotation'],
            $locationArray['latitude'] ?? 0,
            $locationArray['longitude'] ?? 0
        ));

        $id = Uuid::new();

        $this->bus->dispatchCommand(new CreateClient(
            $user->id(),
            $id,
            $request->check('code'),
            $request->check('name'),
            $request->check('phone'),
            $this->merchantId(),
            $locationId
        ));

        return new JsonResponse($this->bus->dispatchQuery(new FindClient(
            $this->checkUser()->id(),
            $id,
            ['location']
        ))->toScalar());
    }
}
