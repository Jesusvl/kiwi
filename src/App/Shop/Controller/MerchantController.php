<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use App\Shop\Form\MerchantType;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Merchant\Command\CreateMerchant;
use Kiwi\Shop\Merchant\Command\UpdateMerchant;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Query\FindMerchant;
use Kiwi\Shop\Merchant\Query\FindMerchants;
use KiwiInfrastructure\Core\Http\Request;
use KiwiLib\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Merchant controller.
 *
 * @Route("/admin/merchant")
 */
class MerchantController extends Controller
{
    /**
     * Lists all merchant entities.
     *
     * @Route()
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        return [
            'merchants' => $bus->dispatchQuery(new FindMerchants(
                $this->checkUser()->id(),
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size')
            ))
        ];
    }

    /**
     * Creates a new merchant entity.
     *
     * @Route("/new")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function newAction(): array
    {
        $form = $this->createForm(MerchantType::class);

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * Creates a new merchant entity.
     *
     * @Route("/new")
     * @Method("POST")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return RedirectResponse
     */
    public function saveAction(Request $request, Bus $bus): RedirectResponse
    {
        $id = $request->getUuid('id', Uuid::new());

        $bus->dispatchCommand(new CreateMerchant(
            $this->checkUser()->id(),
            $id,
            $request->check('name')
        ));

        return $this->redirectToRoute('app_shop_merchant_show', ['id' => $id]);
    }

    /**
     * Finds and displays a merchant entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     *
     * @param Merchant $id
     *
     * @return array
     */
    public function showAction(Merchant $id): array
    {
        $merchant   = $id;
        $deleteForm = $this->createDeleteForm($merchant);

        return [
            'merchant'    => $merchant,
            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to edit an existing merchant entity.
     *
     * @Route("/{id}/edit")
     * @Method("GET")
     * @Template()
     *
     * @param string $id
     * @param Bus    $bus
     *
     * @return array
     */
    public function editAction(
        string $id,
        Bus $bus
    ): array {
        $merchant = $bus->dispatchQuery(new FindMerchant($this->checkUser()->id(), $id));

        $deleteForm = $this->createDeleteForm($merchant);
        $editForm   = $this->createForm(MerchantType::class, $merchant);

        return [
            'merchant'    => $id,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to edit an existing merchant entity.
     *
     * @Route("/{id}/edit")
     * @Method("POST")
     *
     * @param Request $request
     * @param string  $id
     * @param Bus     $bus
     *
     * @return RedirectResponse
     */
    public function updateAction(Request $request, string $id, Bus $bus): RedirectResponse
    {
        $bus->dispatchCommand(new UpdateMerchant($this->checkUser()->id(), $id, $request->check('name')));

        return $this->redirectToRoute('app_shop_merchant_edit', ['id' => $id]);
    }

    /**
     * Deletes a merchant entity.
     *
     * @Route("/{id}")
     * @Method("DELETE")
     *
     * @param Request  $request
     * @param Merchant $merchant
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Merchant $merchant): RedirectResponse
    {
        $form = $this->createDeleteForm($merchant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($merchant);
            $manager->flush();
        }

        return $this->redirectToRoute('app_shop_merchant_index');
    }

    /**
     * @param Merchant $merchant
     *
     * @return FormInterface
     */
    private function createDeleteForm(Merchant $merchant): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_shop_merchant_delete', ['id' => $merchant->id()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
