<?php

declare(strict_types=1);

namespace App\Shop\Controller\Location;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Location\Query\FindLocations;
use KiwiInfrastructure\Core\Http\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ListController.
 */
class ListController extends Controller
{
    /**
     * @Route("/api/shop/location")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request, Bus $bus): JsonResponse
    {
        return new JsonResponse($bus->dispatchQuery(new FindLocations(
            $this->checkUser()->id(),
            $request->getInt('page', 1),
            $this->getParameter('web_paginator_size')
        ))->toScalar());
    }
}
