<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use Kiwi\Core\Infrastructure\Bus;
use KiwiInfrastructure\Core\Http\Request;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Query\FindClient;
use Kiwi\Shop\Client\Query\FindClients;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormInterface;

/**
 * Shopper controller.
 *
 * @Route("/client")
 */
class ClientController extends Controller
{
    /**
     * @Route()
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        return [
            'clients' => $bus->dispatchQuery(new FindClients(
                $this->checkUser()->id(),
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size')
            ))
        ];
    }

    /**
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     *
     * @param string $id
     * @param Bus    $bus
     *
     * @return array
     */
    public function showAction(string $id, Bus $bus): array
    {
        /** @var ClientDocument $client */
        $client = $bus->dispatchQuery(new FindClient($this->checkUser()->id(), $id));

        if ($client->isEmpty()) {
            throw $this->createNotFoundException();
        }

        $deleteForm = $this->createDeleteForm($client);

        return [
            'shopper'     => $client,
            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Deletes a shopper entity.
     *
     * @Route("/{id}")
     * @Method("DELETE")
     * @Template()
     *
     * @param Request $request
     * @param string  $id
     * @param Bus     $bus
     *
     * @return array
     */
    public function deleteAction(Request $request, string $id, Bus $bus): array
    {
        $client = $bus->dispatchQuery(new FindClient($this->checkUser()->id(), $id));
        $form   = $this->createDeleteForm($client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($client);
            $manager->flush();
        }

        return [];
    }

    /**
     * @param ClientDocument $client
     *
     * @return FormInterface
     */
    private function createDeleteForm(ClientDocument $client): FormInterface
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('app_shop_client_delete', ['id' => $client->id()]))
                    ->setMethod('DELETE')
                    ->getForm();
    }
}
