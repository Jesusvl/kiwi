<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use App\Shop\Form\StoreType;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Store\Command\CreateStore;
use Kiwi\Shop\Store\Command\UpdateStore;
use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\Store\Query\FindStore;
use Kiwi\Shop\Store\Query\FindStores;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Query\FindUser;
use KiwiInfrastructure\Core\Http\Request;
use KiwiLib\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Store controller.
 *
 * @Route("/admin/store")
 */
class StoreController extends Controller
{
    /**
     * Lists all store entities.
     *
     * @Route("/")
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        return [
            'stores' => $bus->dispatchQuery(new FindStores(
                $this->checkUser()->id(),
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size')
            ))
        ];
    }

    /**
     * Creates a new store entity.
     *
     * @Route("/new")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function newAction(): array
    {
        $form = $this->createForm(StoreType::class);

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/new")
     * @Method("POST")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return RedirectResponse
     */
    public function saveAction(Request $request, Bus $bus): RedirectResponse
    {
        $id = $request->getUuid('id', Uuid::new());

        /** @var UserDocument $user */
        $user = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $this->checkUser()->id()));

        $bus->dispatchCommand(new CreateStore(
            $this->checkUser()->id(),
            $id,
            $request->check('name'),
            $user->merchantId()
        ));

        return $this->redirectToRoute('app_shop_store_show', ['id' => $id]);
    }

    /**
     * Finds and displays a store entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     *
     * @param string $id
     * @param Bus    $bus
     *
     * @return array
     */
    public function showAction(string $id, Bus $bus): array
    {
        $deleteForm = $this->createDeleteForm($id);
        $store      = $bus->dispatchQuery(new FindStore($this->checkUser()->id(), $id, ['timespans', 'postalCodes']));

        return [
            'store'       => $store,
            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to edit an existing store entity.
     *
     * @Route("/{id}/edit")
     * @Method("GET")
     * @Template()
     *
     * @param string $id
     * @param Bus    $bus
     *
     * @return array
     */
    public function editAction(string $id, Bus $bus): array
    {
        $store = $bus->dispatchQuery(new FindStore($this->checkUser()->id(), $id, ['timespans', 'postalCodes']));

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(StoreType::class, $store);

        return [
            'store'       => $store,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to edit an existing store entity.
     *
     * @Route("/{id}/edit")
     * @Method("POST")
     *
     * @param Request $request
     * @param string  $id
     * @param Bus     $bus
     *
     * @return RedirectResponse
     */
    public function updateAction(Request $request, string $id, Bus $bus): RedirectResponse
    {
        $bus->dispatchCommand(new UpdateStore($this->checkUser()->id(), $id, $request->check('name')));

        return $this->redirectToRoute('app_shop_store_edit', ['id' => $id]);
    }

    /**
     * Deletes a store entity.
     *
     * @Route("/{id}")
     * @Method("DELETE")
     *
     * @param Store $store
     *
     * @return Response
     */
    public function deleteAction(Store $store): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($store);
        $manager->flush();

        return $this->redirectToRoute('app_shop_store_index');
    }

    /**
     * @param string $id
     *
     * @return FormInterface
     */
    private function createDeleteForm(string $id): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_shop_store_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
