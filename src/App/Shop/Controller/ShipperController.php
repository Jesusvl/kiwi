<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use Kiwi\Core\Exception\DomainException;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Order\Query\FindOrdersByDate;
use KiwiInfrastructure\Core\Http\Request;
use KiwiLib\DateTime\DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController.
 */
class ShipperController extends Controller
{
    /**
     * @Route("/ship")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return Response
     *
     * @throws DomainException
     */
    public function indexAction(Request $request, Bus $bus): Response
    {
        return $this->shipByDayAction($request, new DateTime(), $bus);
    }

    /**
     * @Route("/ship/{date}")
     * @Method("GET")
     *
     * @param Request  $request
     * @param DateTime $date
     * @param Bus      $bus
     *
     * @return Response
     *
     * @throws DomainException
     */
    public function shipByDayAction(Request $request, DateTime $date, Bus $bus): Response
    {
        $orders = $bus->dispatchQuery(
            new FindOrdersByDate(
                $this->checkUser()->id(),
                $date,
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size'),
                ['destination', 'store'],
                []
            )
        );

        return $this->render(
            'ship/index.html.twig',
            [
                'orders' => $orders,
            ]
        );
    }
}
