<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Order\Query\FindOrdersByDate;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Query\FindUser;
use KiwiInfrastructure\Core\Http\Request;
use KiwiLib\DateTime\DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController.
 *
 * @Route("/manager")
 */
class ManagerController extends Controller
{
    /**
     * @Route()
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        $this->checkUser();
        $date  = $request->getDateTime('date', new DateTime());
        $query = null;

        $orders = $bus->dispatchQuery(new FindOrdersByDate(
            $this->checkUser()->id(),
            $date,
            $request->getInt('page', 1),
            $this->getParameter('web_paginator_size'),
            $request->getParts()
        ));

        /** @var UserDocument $user */
        $user = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $this->checkUser()->id(), ['merchant']));

        return [
            'orders'   => $orders,
            'merchant' => $user->merchant()
        ];
    }
}
