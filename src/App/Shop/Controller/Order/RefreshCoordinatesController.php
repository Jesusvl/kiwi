<?php

declare(strict_types=1);

namespace App\Shop\Controller\Order;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Order\Command\RefreshOrderCoordinates;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RefreshCoordinatesController.
 *
 * @Route("/api/shop/order")
 */
class RefreshCoordinatesController extends Controller
{
    /**
     * @Route("/{id}/locate")
     * @Method("PATCH")
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return JsonResponse
     */
    public function indexAction(Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new RefreshOrderCoordinates($this->checkUser()->id(), $id));

        return new JsonResponse(null, JsonResponse::HTTP_ACCEPTED);
    }
}
