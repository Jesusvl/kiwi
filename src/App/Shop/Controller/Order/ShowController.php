<?php

declare(strict_types=1);

namespace App\Shop\Controller\Order;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Order\Document\OrderDocument;
use Kiwi\Shop\Order\Query\FindOrder;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Shop\Store\Query\FindStore;
use KiwiInfrastructure\Core\Http\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ShowController.
 *
 * @Route("/api/shop")
 */
class ShowController extends Controller
{
    /**
     * @Route("/order/{id}")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus     $bus
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request, Bus $bus, string $id): JsonResponse
    {
        /** @var OrderDocument $store */
        $order = $bus->dispatchQuery(new FindOrder($this->checkUser()->id(), $id, $request->getParts()));

        return new JsonResponse($order->toScalar(), JsonResponse::HTTP_ACCEPTED);
    }
}
