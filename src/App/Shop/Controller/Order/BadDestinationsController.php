<?php

declare(strict_types=1);

namespace App\Shop\Controller\Order;

use App\Shop\Controller\Controller;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Order\Query\FindBadDestinations;
use KiwiInfrastructure\Core\Http\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RefreshCoordinatesController.
 *
 * @Route("/api/shop/destinations/bad")
 */
class BadDestinationsController extends Controller
{
    /**
     * @Route("")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request, Bus $bus): JsonResponse
    {
        return $bus->dispatchQuery(new FindBadDestinations(
            $this->checkUser()->id(),
            $request->getInt('page', 1),
            $this->getParameter('web_paginator_size')
        ));
    }
}
