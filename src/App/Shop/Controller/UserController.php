<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use App\Shop\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\User\Command\CreateUser;
use Kiwi\Shop\User\Command\UpdateUser;
use Kiwi\Shop\User\Query\FindUser;
use Kiwi\Shop\User\Query\FindUsers;
use KiwiInfrastructure\Core\Http\Exception\InternalServerErrorHttpException;
use KiwiInfrastructure\Core\Http\Request;
use KiwiInfrastructure\Core\Security\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * User controller.
 *
 * @Route("/admin/user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/")
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        return [
            'users' => $bus->dispatchQuery(new FindUsers(
                $this->checkUser()->id(),
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size')
            ))
        ];
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function newAction(): array
    {
        $user = new User();
        $form = $this->createForm(UserType::class);

        return [
            'user' => $user,
            'form' => $form->createView()
        ];
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new")
     * @Method("POST")
     *
     * @param Request                      $request
     * @param Bus                          $bus
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return RedirectResponse
     */
    public function saveAction(Request $request, Bus $bus, UserPasswordEncoderInterface $encoder): RedirectResponse
    {
        $this->checkUser();

        $user = new User();
        $user->setUsername($request->check('username'));
        $user->setRole($request->check('role'));
        $user->setPassword($encoder->encodePassword($user, $request->check('password')));

        $bus->dispatchCommand(new CreateUser(
            $user->id(),
            $request->checkUuid('store'),
            $request->checkUuid('merchant'),
            $request->check('username'),
            $request->check('name'),
            $request->check('role'),
            $request->check('phone')
        ));

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute('app_shop_user_show', ['id' => $user->id()]);
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return array
     */
    public function showAction(Bus $bus, string $id): array
    {
        $deleteForm = $this->createDeleteForm($id);

        return [
            'user'        => $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $id)),
            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit")
     * @Method("GET")
     * @Template()
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return array
     */
    public function editAction(Bus $bus, string $id): array
    {
        $deleteForm = $this->createDeleteForm($id);
        $user       = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $id));
        $editForm   = $this->createForm(UserType::class, $user);

        return [
            'user'        => $user,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ];
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit")
     * @Method("POST")
     *
     * @param Request                $request
     * @param Bus                    $bus
     * @param string                 $id *
     * @param EntityManagerInterface $manager
     *
     * @return RedirectResponse
     */
    public function updateAction(
        Request $request,
        Bus $bus,
        string $id,
        EntityManagerInterface $manager
    ): RedirectResponse {
        $bus->dispatchCommand(new UpdateUser(
            $this->checkUser()->id(),
            $id,
            $request->checkUuid('store'),
            $request->checkUuid('merchant'),
            $request->check('username'),
            $request->check('name'),
            $request->check('role'),
            $request->check('phone')
        ));

        $entityUser = $manager->getRepository(User::class)->find($id);
        if (!$entityUser) {
            throw new InternalServerErrorHttpException('Internal Server Error');
        }
        $entityUser->setRole($request->check('role'));
        $entityUser->setUsername($request->check('username'));
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('app_shop_user_edit', ['id' => $entityUser->id()]);
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param Bus     $bus
     * @param string  $id
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Bus $bus, string $id): RedirectResponse
    {
        $user = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $id));
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($user);
            $manager->flush();
        }

        return $this->redirectToRoute('app_shop_user_index');
    }

    /**
     * @param string $userId
     *
     * @return FormInterface
     */
    private function createDeleteForm(string $userId): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_shop_user_delete', ['id' => $userId]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
