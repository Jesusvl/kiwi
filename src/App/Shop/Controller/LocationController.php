<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use App\Shop\Form\LocationType;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Location\Command\CreateLocation;
use Kiwi\Shop\Location\Command\UpdateLocation;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Query\FindInvalidLocations;
use Kiwi\Shop\Location\Query\FindLocation;
use Kiwi\Shop\Location\Query\FindLocations;
use KiwiInfrastructure\Core\Http\Request;
use KiwiLib\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Location controller.
 *
 * @Route("/location")
 */
class LocationController extends Controller
{
    /**
     * Lists all location entities.
     *
     * @Route()
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        return [
            'locations' => $bus->dispatchQuery(new FindLocations(
                $this->checkUser()->id(),
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size')
            ))
        ];
    }

    /**
     * @Route("/new")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function newAction(): array
    {
        return [
            'form' => $this->createForm(LocationType::class)->createView()
        ];
    }

    /**
     * @Route("/new")
     * @Method("POST")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return RedirectResponse
     */
    public function createAction(Request $request, Bus $bus): RedirectResponse
    {
        $id = Uuid::new();
        $bus->dispatchCommand(new CreateLocation(
            $this->checkUser()->id(),
            $id,
            $request->check('name'),
            $request->check('stairBuilding'),
            $request->check('floor'),
            $request->check('door'),
            $request->check('city'),
            $request->check('province'),
            $request->check('postalCode'),
            $request->getBool('elevator', false),
            $request->getBool('hasStepElevator', false),
            $request->get('annotation', ''),
            $request->getFloat('latitude', 0.0),
            $request->getFloat('longitude', 0.0)
        ));

        return $this->redirectToRoute('app_shop_location_show', ['id' => $id]);
    }

    /**
     * Finds and displays a location entity.
     *
     * @Route("/invalid")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return Response
     */
    public function listInvalidAction(Request $request, Bus $bus): Response
    {
        return $this->render('location/index.html.twig', [
            'locations' => $bus->dispatchQuery(new FindInvalidLocations(
                $this->checkUser()->id(),
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size')
            ))
        ]);
    }

    /**
     * Finds and displays a location entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     *
     * @param string $id
     * @param Bus    $bus
     *
     * @return array
     */
    public function showAction(String $id, Bus $bus): array
    {
        /** @var LocationDocument $location */
        $location = $bus->dispatchQuery(new FindLocation($this->checkUser()->id(), $id));

        if ($location->isEmpty()) {
            throw $this->createNotFoundException();
        }

        return [
            'location'    => $location
        ];
    }

    /**
     * Displays a form to edit an existing location entity.
     *
     * @Route("/{id}/edit")
     * @Method("GET")
     * @Template()
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return array
     */
    public function editAction(Bus $bus, string $id): array
    {
        /** @var LocationDocument $location */
        $location = $bus->dispatchQuery(new FindLocation($this->checkUser()->id(), $id));

        if ($location->isEmpty()) {
            throw $this->createNotFoundException();
        }

        $editForm   = $this->createForm(LocationType::class, $location);

        return [
            'location'    => $location,
            'edit_form'   => $editForm->createView()
        ];
    }

    /**
     * @Route("/{id}/edit")
     * @Method("POST")
     *
     * @param Request $request
     * @param Bus     $bus
     * @param string  $id
     *
     * @return RedirectResponse
     */
    public function updateAction(Request $request, Bus $bus, string $id): RedirectResponse
    {
        /** @var LocationDocument $location */
        $location = $bus->dispatchQuery(new FindLocation($this->checkUser()->id(), $id));

        if ($location->isEmpty()) {
            throw $this->createNotFoundException();
        }

        $bus->dispatchCommand(new UpdateLocation(
            $id,
            $request->get('name'),
            $request->get('stairBuilding'),
            $request->get('floor'),
            $request->get('door'),
            $request->get('city'),
            $request->get('province'),
            $request->get('postalCode'),
            $request->getBool('elevator'),
            $request->getBool('hasStepElevator'),
            $request->get('annotation'),
            $request->getFloat('latitude'),
            $request->getFloat('longitude')
        ));

        return $this->redirectToRoute('app_shop_location_edit', ['id' => $id]);
    }
}
