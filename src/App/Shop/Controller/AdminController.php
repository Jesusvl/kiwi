<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use Kiwi\Core\Infrastructure\Bus;
use KiwiInfrastructure\Core\Http\Request;
use Kiwi\Shop\Location\Query\FindInvalidLocations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController.
 *
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        return [
            'locations' => $bus->dispatchQuery(new FindInvalidLocations(
                $this->checkUser()->id(),
                $request->getInt('page', 1),
                $this->getParameter('web_paginator_size')
            ))
        ];
    }
}
