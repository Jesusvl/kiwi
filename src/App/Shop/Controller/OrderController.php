<?php

declare(strict_types=1);

namespace App\Shop\Controller;

use App\Shop\Form\OrderType;
use Exception;
use Kiwi\Core\Exception\DomainException;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Client\Command\CreateClient;
use Kiwi\Shop\Client\Command\SetLocationToClient;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Query\FindClientByPhoneAndMerchant;
use Kiwi\Shop\Location\Command\CreateLocation;
use Kiwi\Shop\Location\Command\UpdateLocation;
use Kiwi\Shop\Location\Query\FindLocation;
use Kiwi\Shop\Merchant\Query\FindMerchant;
use Kiwi\Shop\Order\Command\CreateOrder;
use Kiwi\Shop\Order\Command\PutOrderDeliveredStatus;
use Kiwi\Shop\Order\Command\PutOrderInsideStatus;
use Kiwi\Shop\Order\Command\PutOrderOnGoingStatus;
use Kiwi\Shop\Order\Query\FindOrder;
use Kiwi\Shop\Order\Query\FindOrdersByDate;
use Kiwi\Shop\Store\Query\FindStore;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Query\FindUser;
use KiwiInfrastructure\Core\Http\Exception\InternalServerErrorHttpException;
use KiwiInfrastructure\Core\Http\Request;
use KiwiLib\DateTime\DateTime;
use KiwiLib\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Order controller.
 */
class OrderController extends Controller
{
    /**
     * Lists all order entities.
     *
     * @Route("/order")
     * @Method("GET")
     * @Template()
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return array
     *
     * @throws DomainException
     */
    public function indexAction(Request $request, Bus $bus): array
    {
        /** @var UserDocument $user */
        $user = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $this->checkUser()->id(), ['merchant']));
        $date = $request->getDateTime('date', new DateTime());

        $orders = $bus->dispatchQuery(new FindOrdersByDate(
            $this->checkUser()->id(),
            $date,
            $request->getInt('page', 1),
            $this->getParameter('web_paginator_size'),
            ['destination', 'store', 'shipper'],
            $request->getFilters(),
            ['createdAt', 'DESC']
        ));

        return [
            'orders'   => $orders,
            'merchant' => $user->merchant()
        ];
    }

    /**
     * @Route("/api/shop/order/{id}/ongoing")
     * @Method("POST")
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return JsonResponse
     */
    public function ongoingAction(Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new PutOrderOnGoingStatus($this->checkUser()->id(), $this->checkUser()->id(), $id));

        return new JsonResponse([
            'order' => $bus->dispatchQuery(new FindOrder($this->checkUser()->id(), $id))->toScalar()
        ]);
    }

    /**
     * @Route("/api/shop/order/{id}/delivered")
     * @Method("POST")
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return JsonResponse
     */
    public function deliverOrderAction(Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new PutOrderDeliveredStatus($this->checkUser()->id(), $id));

        return new JsonResponse([
            'order' => $bus->dispatchQuery(new FindOrder($this->checkUser()->id(), $id))->toScalar()
        ]);
    }

    /**
     * @Route("/api/shop/order/{id}/inside")
     * @Method("POST")
     *
     * @param Bus    $bus
     * @param string $id
     *
     * @return JsonResponse
     */
    public function insideAction(Bus $bus, string $id): JsonResponse
    {
        $bus->dispatchCommand(new PutOrderInsideStatus($this->checkUser()->id(), $id));

        return new JsonResponse([
            'order' => $bus->dispatchQuery(new FindOrder($this->checkUser()->id(), $id))->toScalar()
        ]);
    }

    /**
     * @Route("/order/new")
     * @Method("GET")
     * @Template()
     *
     * @param Bus $bus
     *
     * @return array
     */
    public function newAction(Bus $bus): array
    {
        $form = $this->createForm(OrderType::class);

        /** @var UserDocument $user */
        $user     = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $this->checkUser()->id()));
        $merchant = $bus->dispatchQuery(new FindMerchant($this->checkUser()->id(), $user->merchantId()));
        $store    = $bus->dispatchQuery(
            new FindStore($this->checkUser()->id(), $user->storeId(), ['timespans', 'postalCodes'])
        );

        return [
            'store'    => $store,
            'merchant' => $merchant,
            'form'     => $form->createView()
        ];
    }

    /**
     * @Route("/order/new")
     * @Method("POST")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function saveAction(Request $request, Bus $bus): RedirectResponse
    {
        /** @var UserDocument $user */
        $user = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $this->checkUser()->id()));

        if ('express' === $request->get('shift')) {
            $start = new DateTime('now');
            $end   = clone $start;
            $end   = $end->modify('+1hour');
        } elseif ('morning' === $request->get('shift')) {
            $start = $this->getStartTime($request->check('morning'));
            $end   = $this->getEndTime($request->check('morning'));
        } elseif ('afternoon' === $request->get('shift')) {
            $start = $this->getStartTime($request->check('afternoon'));
            $end   = $this->getEndTime($request->check('afternoon'));
        } elseif ('tomorrow' === $request->get('shift')) {
            $start = $this->getStartTime($request->check('tomorrow'));
            $end   = $this->getEndTime($request->check('tomorrow'));
            $start = $start->modify('+1day');
            $end   = $end->modify('+1day');
        } else {
            throw new InternalServerErrorHttpException('Bad times');
        }

        $orderId = Uuid::new();

        $locationId = $request->getUuid('location_id', Uuid::new());
        $location   = $bus->dispatchQuery(new FindLocation($this->checkUser()->id(), $locationId));
        /** @var ClientDocument $client */
        $client = $bus->dispatchQuery(new FindClientByPhoneAndMerchant(
            $this->checkUser()->id(),
            $request->check('phone'),
            $user->merchantId()
        ));

        if ($location->isNotEmpty()) {
            $bus->dispatchCommand(
                new UpdateLocation(
                    $locationId,
                    "{$request->get('streetName')}, {$request->get('streetNumber', '')}",
                    $request->get('stairBuilding'),
                    $request->get('floor'),
                    $request->get('door'),
                    $request->get('city'),
                    $request->get('province'),
                    $request->get('postalCode'),
                    $request->getBool('elevator'),
                    $request->getBool('stepelevator'),
                    $request->get('annotation'),
                    $request->getFloat('latitude'),
                    $request->getFloat('longitude')
                )
            );
        } else {
            $bus->dispatchCommand(new CreateLocation(
                $this->checkUser()->id(),
                $locationId,
                "{$request->get('streetName', '')}, {$request->get('streetNumber', '')}",
                $request->get('stairBuilding', ''),
                $request->get('floor', ''),
                $request->get('door', ''),
                $request->check('city'),
                $request->get('province', ''),
                $request->check('postalCode'),
                $request->getBool('elevator', false),
                $request->getBool('stepelevator', false),
                $request->get('annotation', ''),
                $request->getFloat('latitude', 0.0),
                $request->getFloat('longitude', 0.0)
            ));
        }

        $orderLocationId = Uuid::new();
        $bus->dispatchCommand(new CreateLocation(
            $this->checkUser()->id(),
            $orderLocationId,
            "{$request->get('streetName')}, {$request->get('streetNumber', '')}",
            $request->get('stairBuilding', ''),
            $request->get('floor', ''),
            $request->get('door', ''),
            $request->check('city'),
            $request->get('province'),
            $request->check('postalCode'),
            $request->getBool('elevator', false),
            $request->getBool('stepelevator', false),
            $request->get('annotation', ''),
            $request->getFloat('latitude', 0.0),
            $request->getFloat('longitude', 0.0)
        ));

        if ($client->isNotEmpty()) {
            $clientId = $client->id();
            $bus->dispatchCommand(new SetLocationToClient($this->checkUser()->id(), $clientId, $orderLocationId));
        } else {
            $clientId = Uuid::new();
            $bus->dispatchCommand(new CreateClient(
                $this->checkUser()->id(),
                $clientId,
                $request->check('code'),
                "{$request->check('name')} {$request->check('surname')}",
                $request->check('phone'),
                $request->check('merchant_id'),
                $orderLocationId
            ));
        }

        $bus->dispatchCommand(new CreateOrder(
            $this->checkUser()->id(),
            $orderId,
            $clientId,
            $this->newOrderNumber($bus),
            $request->getInt('cooled', 0),
            $request->getInt('frozen', 0),
            $user->storeId(),
            $user->merchantId(),
            $start,
            $end,
            'express' === $request->get('shift'),
            $orderLocationId,
            $request->getInt('boxes', 0),
            $request->getBool('needs_payment', false)
        ));

        $this->addFlash(
            'notice',
            'Pedido guardado correctamente'
        );

        return $this->redirectToRoute('app_shop_order_new');
    }

    /**
     * @Route("/order/find_user_phone")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus     $bus
     *
     * @return JsonResponse
     */
    public function findUserByPhoneAction(Request $request, Bus $bus): JsonResponse
    {
        /** @var UserDocument $user */
        $user = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $this->checkUser()->id()));
        /** @var ClientDocument $client */
        $client = $bus->dispatchQuery(new FindClientByPhoneAndMerchant(
            $this->checkUser()->id(),
            $request->check('phone'),
            $user->merchantId(),
            $request->getParts()
        ));

        return new JsonResponse([
            'client' => $client->toScalar()
        ]);
    }

    /**
     * @param Bus $bus
     *
     * @return string
     */
    private function newOrderNumber(Bus $bus): string
    {
        /** @var UserDocument $user */
        $user = $bus->dispatchQuery(new FindUser($this->checkUser()->id(), $this->checkUser()->id()));

        $pre = \mb_substr(uniqid($user->username(), true), 0, 2);
        $now = new DateTime();

        return $pre . $now->format('ymdHi');
    }

    /**
     * @param string $timewindow
     *
     * @return DateTime
     *
     * @throws Exception
     */
    private function getStartTime(string $timewindow): DateTime
    {
        $dateTime  = new DateTime();
        $startHour = \mb_substr($timewindow, 0, 2);
        $startMin  = \mb_substr($timewindow, 3, 2);
        $dateTime  = $dateTime->setTime((int)$startHour, (int)$startMin);

        return new DateTime($dateTime->format(DATE_ATOM));
    }

    /**
     * @param string $timewindow
     *
     * @return DateTime
     *
     * @throws Exception
     */
    private function getEndTime(string $timewindow): DateTime
    {
        $dateTime = new DateTime();
        $endHour  = \mb_substr($timewindow, 6, 2);
        $endMin   = \mb_substr($timewindow, 9, 2);
        $dateTime = $dateTime->setTime((int)$endHour, (int)$endMin);

        return new DateTime($dateTime->format(DATE_ATOM));
    }
}
