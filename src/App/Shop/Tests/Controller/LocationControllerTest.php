<?php

declare(strict_types=1);

namespace App\Shop\Tests\Controller;

use App\Core\Test\KiwiTestCase;
use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LocationControllerTest.
 */
class LocationControllerTest extends KiwiTestCase
{
    /**
     * @test
     * @dataProvider roles
     *
     * @param string $role
     */
    public function itShouldListLocations(string $role): void
    {
        $I = self::startClient();
        $I->createUser($role, 'test', '1234', null, null);

        $I->login('test', '1234');
        $I->makeARequest('GET', '/location');
        $I->shouldSeeResponseStatus(Response::HTTP_OK);
    }

    /**
     * @test
     * @dataProvider roles
     *
     * @param string $role
     */
    public function itShouldLoadNewLocation(string $role): void
    {
        $I = self::startClient();
        $I->createUser($role, 'test', '1234', null, null);

        $I->login('test', '1234');
        $I->makeARequest('GET', '/location/new');
        $I->shouldSeeResponseStatus(Response::HTTP_OK);
    }

    /**
     * @test
     * @dataProvider roles
     *
     * @param string $role
     */
    public function itShouldCreateNewLocation(string $role): void
    {
        $I = self::startClient();
        $I->createUser($role, 'test', '1234', null, null);

        $I->login('test', '1234');
        $I->makeARequest('POST', '/location/new', [
            'name'            => Factory::create()->streetAddress,
            'stairBuilding'   => 'A',
            'floor'           => '1',
            'door'            => '1',
            'city'            => Factory::create()->city,
            'province'        => 'Barcelona',
            'postalCode'      => Factory::create()->postcode,
            'elevator'        => Factory::create()->boolean,
            'hasStepElevator' => Factory::create()->boolean,
            'annotation'      => Factory::create()->sentence,
            'latitude'        => Factory::create()->latitude,
            'longitude'       => Factory::create()->longitude
        ]);
        $I->shouldSeeResponseStatus(Response::HTTP_FOUND);
    }

    /**
     * @test
     * @dataProvider roles
     *
     * @param string $role
     */
    public function itShouldListInvalidLocations(string $role): void
    {
        $I = self::startClient();
        $I->createUser($role, 'test', '1234', null, null);

        $I->login('test', '1234');
        $I->makeARequest('GET', '/location/invalid');
        $I->shouldSeeResponseStatus(Response::HTTP_OK);
    }

    /**
     * @test
     * @dataProvider roles
     *
     * @param string $role
     */
    public function itShouldShowLocation(string $role): void
    {
        $I          = self::startClient();
        $userId     = $I->createUser($role, 'test', '1234', null, null);
        $locationId = $I->createLocation($userId, Factory::create()->latitude, Factory::create()->longitude);

        $I->login('test', '1234');
        $I->makeARequest('GET', "/location/$locationId");
        $I->shouldSeeResponseStatus(Response::HTTP_OK);
    }

    /**
     * @test
     * @dataProvider roles
     *
     * @param string $role
     */
    public function itShouldLoadEditLocation(string $role): void
    {
        $I          = self::startClient();
        $userId     = $I->createUser($role, 'test', '1234', null, null);
        $locationId = $I->createLocation($userId, Factory::create()->latitude, Factory::create()->longitude);

        $I->login('test', '1234');
        $I->makeARequest('GET', "/location/$locationId/edit");
        $I->shouldSeeResponseStatus(Response::HTTP_OK);
    }

    /**
     * @test
     * @dataProvider roles
     *
     * @param string $role
     */
    public function itShouldUpdateLocation(string $role): void
    {
        $I          = self::startClient();
        $userId     = $I->createUser($role, 'test', '1234', null, null);
        $locationId = $I->createLocation($userId, Factory::create()->latitude, Factory::create()->longitude);

        $I->login('test', '1234');
        $I->makeARequest('POST', "/location/$locationId/edit", [
            'name'            => Factory::create()->streetAddress,
            'stairBuilding'   => 'A',
            'floor'           => '1',
            'postalCode'      => Factory::create()->postcode,
            'elevator'        => Factory::create()->boolean,
            'latitude'        => Factory::create()->latitude,
            'longitude'       => Factory::create()->longitude
        ]);
        $I->shouldSeeResponseStatus(Response::HTTP_FOUND);
    }
}
