<?php

declare(strict_types=1);

namespace App\Shop\Form;

use Kiwi\Shop\Merchant\Domain\Merchant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MerchantType.
 */
class MerchantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Merchant::class
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): void
    {
    }
}
