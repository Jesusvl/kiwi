<?php

declare(strict_types=1);

namespace App\Shop\Form;

use Kiwi\Shop\Location\Document\LocationDocument;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LocationType.
 */
class LocationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name')
                ->add('stairBuilding')
                ->add('floor')
                ->add('door')
                ->add('postalCode')
                ->add('city')
                ->add('province')
                ->add('latitude')
                ->add('longitude')
                ->add('elevator')
                ->add('elevator', ChoiceType::class, [
                    'choices' => [
                        'Si' => true,
                        'No' => false
                    ],
                    'expanded' => true,
                    'multiple' => false
                ])
                ->add('hasStepElevator', ChoiceType::class, [
                    'choices' => [
                        'Si' => true,
                        'No' => false
                    ],
                    'expanded' => true,
                    'multiple' => false
                ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => LocationDocument::class
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): ?string
    {
        return null;
    }
}
