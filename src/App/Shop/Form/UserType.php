<?php

declare(strict_types=1);

namespace App\Shop\Form;

use Doctrine\ORM\EntityRepository;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\User\Document\BaseUserDocument;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType.
 */
class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('username')->add('name')->add('phone')
            ->add('role', ChoiceType::class, [
                'choices'  => [
                    'ROLE_ADMIN'   => 'ROLE_ADMIN',
                    'ROLE_SHIPPER' => 'ROLE_SHIPPER',
                    'ROLE_SAD'     => 'ROLE_SAD',
                    'ROLE_MANAGER' => 'ROLE_MANAGER',
                    'ROLE_MERCHANT_MANAGER' => 'ROLE_MERCHANT_MANAGER'
                ],
                'expanded' => true,
                'multiple' => false
            ])
            ->add('store', EntityType::class, [
                'class'         => Store::class,
                'query_builder' => function (EntityRepository $stor) {
                    return $stor->createQueryBuilder('m')
                        ->orderBy('m.name', 'ASC');
                },
                'choice_label'  => 'name'
            ])
            ->add('merchant', EntityType::class, [
                'class'         => Merchant::class,
                'query_builder' => function (EntityRepository $mer) {
                    return $mer->createQueryBuilder('m')
                        ->orderBy('m.name', 'ASC');
                },
                'choice_label'  => 'name'
            ]);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $product = $event->getData();
            $form    = $event->getForm();

            // check if the Product object is "new"
            // If no data is passed to the form, the data is "null".
            // This should be considered a new "Product"
            if (!$product || null === $product->id()) {
                $form->add('password');
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BaseUserDocument::class
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): void
    {
    }
}
