<?php

declare(strict_types=1);

namespace App\Shop\Topic;

use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Gos\Bundle\WebSocketBundle\Server\Exception\FirewallRejectionException;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;

/**
 * Class SelfTopic.
 */
class SelfTopic extends AbstractAdminTopic
{
    public const NAME = 'self.topic';

    /**
     * @param  ConnectionInterface $connection
     * @param  Topic               $topic
     * @param WampRequest          $request
     * @param                      $event
     * @param  array               $exclude
     * @param  array               $eligible
     */
    public function onPublish(
        ConnectionInterface $connection,
        Topic $topic,
        WampRequest $request,
        $event,
        array $exclude,
        array $eligible
    ): void {
        $topic->broadcast([
            'msg' => $event
        ]);
    }

    /**
     * @param Topic        $topic
     * @param WampRequest  $request
     * @param string|array $data
     * @param string       $provider
     */
    public function onPush(Topic $topic, WampRequest $request, $data, $provider): void
    {
        $topic->broadcast([
            'msg' => $data
        ]);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @param ConnectionInterface|null $connection
     * @param Topic                    $topic
     * @param WampRequest              $request
     * @param null                     $payload
     * @param null                     $exclude
     * @param null                     $eligible
     * @param null                     $provider
     *
     * @throws FirewallRejectionException
     */
    public function secure(
        ConnectionInterface $connection = null,
        Topic $topic,
        WampRequest $request,
        $payload = null,
        $exclude = null,
        $eligible = null,
        $provider = null
    ): void {
        parent::secure($connection, $topic, $request, $payload, $exclude, $eligible, $provider);

        if (!$connection) {
            throw new FirewallRejectionException('Access denied');
        }

        if ($request->getAttributes()->get('id') !== $this->getUser($connection)->id()) {
            throw new FirewallRejectionException('Access denied');
        }
    }
}
