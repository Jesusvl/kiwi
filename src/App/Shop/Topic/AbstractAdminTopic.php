<?php

declare(strict_types=1);

namespace App\Shop\Topic;

use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use KiwiInfrastructure\Core\Security\User;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;

/**
 * Class AbstractAdminTopic.
 */
abstract class AbstractAdminTopic implements TopicInterface, PushableTopicInterface
{
    /**
     * @var ClientManipulatorInterface
     */
    private $clientManipulator;

    /**
     * AbstractAdminTopic constructor.
     *
     * @param ClientManipulatorInterface $clientManipulator
     */
    public function __construct(ClientManipulatorInterface $clientManipulator)
    {
        $this->clientManipulator = $clientManipulator;
    }

    /**
     * @param ConnectionInterface $connection
     * @param Topic               $topic
     * @param WampRequest         $request
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request): void
    {
    }

    /**
     * @param ConnectionInterface $connection
     * @param Topic               $topic
     * @param WampRequest         $request
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request): void
    {
    }

    /**
     * @param Topic        $topic
     * @param WampRequest  $request
     * @param array|string $data
     * @param string       $provider
     */
    public function onPush(Topic $topic, WampRequest $request, $data, $provider): void
    {
    }

    /**
     * @param ConnectionInterface $connection
     * @param Topic               $topic
     * @param WampRequest         $request
     * @param                     $event
     * @param array               $exclude
     * @param array               $eligible
     */
    public function onPublish(
        ConnectionInterface $connection,
        Topic $topic,
        WampRequest $request,
        $event,
        array $exclude,
        array $eligible
    ): void {
    }

    /**
     * @param ConnectionInterface $connection
     *
     * @return User
     */
    public function getUser(ConnectionInterface $connection): User
    {
        /** @var User $user */
        $user = $this->clientManipulator->getClient($connection);

        if (!$user) {
            $connection->close();
        }

        if ($user->getRole() !== 'ROLE_ADMIN') {
            $connection->close();
        }

        return $user;
    }
}
