<?php
declare(strict_types=1);

namespace App\Billing\Command;

use Doctrine\ORM\EntityManagerInterface;
use Kiwi\Billing\Client\Domain\ClientCode;
use Kiwi\Billing\Client\Domain\ClientId;
use Kiwi\Billing\Client\Domain\ClientName;
use Kiwi\Billing\Client\Domain\ClientPhone;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Merchant\Domain\MerchantName;
use Kiwi\Billing\Order\Domain\OrderBoxesQuantity;
use Kiwi\Billing\Order\Domain\OrderClientName;
use Kiwi\Billing\Order\Domain\OrderClientPhone;
use Kiwi\Billing\Order\Domain\OrderFrozenQuantity;
use Kiwi\Billing\Order\Domain\OrderId;
use Kiwi\Billing\Order\Domain\OrderNumber;
use Kiwi\Billing\Order\Domain\OrderRefrigeratedQuantity;
use Kiwi\Billing\Order\Domain\OrderStatus;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreName;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserName;
use Kiwi\Billing\User\Domain\UserPhone;
use Kiwi\Billing\User\Domain\UserRole;
use Kiwi\Billing\User\Domain\UserUsername;
use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Billing\Client\Domain\Client as BillingClient;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Billing\Merchant\Domain\Merchant as  BillingMerchant;
use Kiwi\Shop\Order\Domain\Order;
use Kiwi\Billing\Order\Domain\Order as BillingOrder;
use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Billing\Store\Domain\Store as BillingStore;
use Kiwi\Shop\User\Domain\User;
use Kiwi\Billing\User\Domain\User as BillingUser;
use KiwiLib\DateTime\DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * DumpShopDataIntoBillingCommand
 *
 * @author Jose Beteta <1993jbetetag@gmail.com>
 * @copyright 2006-2018 KIWI S.L.
 */
class DumpShopDataIntoBillingCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * DavidCommand constructor.
     *
     * @param EntityManagerInterface $manager
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct();
        $this->manager = $manager;
    }

    protected function configure(): void
    {
        $this->setName('kiwi:billing:dump');
        $this->setDescription('Dumps data from shop to billing context');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->manager->beginTransaction();

            $this->dumpMerchants($output);
            $this->dumpUsers($output);
            $this->dumpClients($output);
            $this->dumpStores($output);
            $this->dumpOrders($output);

            $this->manager->commit();
        } catch (\Exception $exception) {
            $this->manager->rollback();
            throw $exception;
        }
    }

    /**
     * Dump users into billing db.
     * @param OutputInterface $output
     */
    private function dumpUsers(OutputInterface $output)
    {
        $output->write("Dumping users \n");

        /** @var User $user */
        foreach ($this->manager->getRepository(User::class)->findAll() as $user) {
            $billingUser = new BillingUser(
              new UserId($user->id()->id()),
              $user-> storeId() ? new StoreId($user->storeId()->id()) : null,
              $user->merchantId() ? new MerchantId($user->merchantId()->id()) : null,
              new UserUserName($user->username()->name()),
              new UserName($user->name()->name()),
              new UserRole($user->role()->name()),
              new UserPhone($user->phone()->number())
            );
            $this->manager->persist($billingUser);
            $output->write('.');
        }
        $output->write("\n");
        $this->manager->flush();

        $this->manager->clear();
    }

    /**
     * Dump merchants into billing db.
     * @param OutputInterface $output
     */
    private function dumpMerchants(OutputInterface $output)
    {
        $output->write("Dumping merchants \n");

        /** @var Merchant $merchant */
        foreach ($this->manager->getRepository(Merchant::class)->findAll() as $merchant) {
            $billingMerchant = new BillingMerchant(
              new MerchantId($merchant->id()->id()),
              new MerchantName($merchant->name()->name())
            );
            $this->manager->persist($billingMerchant);
            $output->write('.');

        }
        $output->write("\n");

        $this->manager->flush();

        $this->manager->clear();
    }

    /**
     * Dump clients into billing db.
     * @param OutputInterface $output
     */
    private function dumpClients(OutputInterface $output)
    {
        $output->write("Dumping clients \n");
        foreach ($this->manager->getRepository(Client::class)->findAll() as $client) {
            $billingClient = new BillingClient(
              new ClientId($client->id()->id()),
              new ClientCode($client->code()->code()),
              new ClientName($client->fullName()->name()),
              new ClientPhone($client->phone()->phone()),
              $client->merchantId() ? new MerchantId($client->merchantId()->id()) : null
            );
            $this->manager->persist($billingClient);
            $output->write('.');
        }
        $output->write("\n");
        $this->manager->flush();

        $this->manager->clear();
    }

    /**
     * Dump stores into billing.
     * @param OutputInterface $output
     */
    private function dumpStores(OutputInterface $output)
    {
        $output->write("Dumping stores \n");
        foreach ($this->manager->getRepository(Store::class)->findAll() as $store) {
            $billingStore = new BillingStore(
                new StoreId($store->id()->id()),
                $store->name() ? new StoreName($store->name()->name()) : null,
                $store->merchantId() ? new MerchantId($store->merchantId()->id()) : null,
                $store->userId() ? new UserId($store->userId()->id()) : null
            );
            $this->manager->persist($billingStore);
            $output->write('.');
        }
        $output->write("\n");
        $this->manager->flush();

        $this->manager->clear();
    }

    /**
     * Dump orders into billing db.
     * @param OutputInterface $output
     */
    private function dumpOrders(OutputInterface $output)
    {
        $output->write("Dumping orders \n");

        foreach ($this->manager->getRepository(Order::class)->findAll() as $order) {

            $billingOrder = new BillingOrder(
                new OrderId($order->id()->id()),
                new ClientId($order->clientId()->id()),
                new OrderClientName($order->clientName()->name()),
                new OrderClientPhone($order->clientPhone()->phone()),
                new OrderStatus($order->status()->name()),
                new OrderNumber($order->number()->code()),
                new OrderRefrigeratedQuantity($order->cooled()->quantity()),
                new OrderFrozenQuantity($order->frozen()->quantity()),
                $order->storeId() ? new StoreId($order->storeId()->id()) : null,
                new DateTime($order->startTime()->format(DATE_ATOM)),
                new DateTime($order->endTime()->format(DATE_ATOM)),
                $order->isExpress(),
                new OrderBoxesQuantity($order->boxes()->quantity())
            );
            $this->manager->persist($billingOrder);

            $output->write('.');
        }
        $this->manager->flush();

        $output->write("\n");

        $this->manager->clear();
    }
}
