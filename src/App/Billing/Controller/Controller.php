<?php

declare(strict_types=1);

namespace App\Billing\Controller;

use KiwiInfrastructure\Core\Security\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Controller.
 */
abstract class Controller extends BaseController
{
    /**
     * @return User
     */
    protected function checkUser(): User
    {
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedHttpException();
        }

        return $user;
    }
}
