<?php
declare(strict_types=1);

namespace App\Billing\Controller;

use Kiwi\Billing\Order\Document\OrderByDaysDocument;
use Kiwi\Billing\Order\Document\OrderDocumentCollection;
use Kiwi\Billing\Order\Query\FindOrdersByTimeAndStoreAndMerchant;
use Kiwi\Billing\Order\Query\GetOrderDaysAverage;
use Kiwi\Core\Infrastructure\Bus;
use KiwiInfrastructure\Core\Http\Request;
use League\Csv\Writer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SplTempFileObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * OrderController
 *
 * @author Jose Beteta <jose@nektria.com>
 * @copyright 2006-2018 Kiwi S.L.
 *
 * @Route("/api/billing_orders")
 */
class OrderController extends Controller
{
    /**
     * Lists all order entities.
     *
     * @Route("/")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus $bus
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws \KiwiInfrastructure\Core\Http\Exception\MissingParamHttpException
     */
    public function getOrdersAction(Request $request, Bus $bus): JsonResponse
    {
        $user = $this->checkUser();

        $storeId = $request->check('store_id');
        $dateStart = $request->check('date_start');
        $dateEnd = $request->check('date_end');
        $merchantId = $request->check('merchant_id');

        if (!$user->isAdmin()) {
            throw new AccessDeniedHttpException();
        }

        /** @var OrderDocumentCollection $orderDocumentCollection */
        $orderDocumentCollection = $bus->dispatchQuery(new FindOrdersByTimeAndStoreAndMerchant(
            new \DateTime($dateStart),
            new \DateTime($dateEnd),
            $storeId,
            $merchantId
        ));

        return new JsonResponse($orderDocumentCollection->toArray());
    }

    /**
     * Lists all order entities.
     *
     * @Route("/xls")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus $bus
     *
     * @return JsonResponse
     * @throws \League\Csv\CannotInsertRecord
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws \KiwiInfrastructure\Core\Http\Exception\MissingParamHttpException
     * @throws \League\Csv\Exception
     */
    public function getOrdersXlsAction(Request $request, Bus $bus): JsonResponse
    {
        $user = $this->checkUser();

        $storeId = $request->check('store_id');
        $dateStart = $request->check('date_start');
        $dateEnd = $request->check('date_end');
        $merchantId = $request->check('merchant_id');

        if (!$user->isAdmin()) {
            throw new AccessDeniedHttpException();
        }

        /** @var OrderDocumentCollection $orderDocumentCollection */
        $orderDocumentCollection = $bus->dispatchQuery(new FindOrdersByTimeAndStoreAndMerchant(
            new \DateTime($dateStart),
            new \DateTime($dateEnd),
            $storeId,
            $merchantId
        ));


        $csv = Writer::createFromFileObject(new SplTempFileObject());

        $csv->setDelimiter(';');

        $csv->insertOne([
            'total_deliveries' ,
            'total_deliveries_express',
            'total_deliveries_with_extra_boxes',
        ]);

        $csv->insertOne([
            $orderDocumentCollection->toArray()['stats']['total_deliveries'],
            $orderDocumentCollection->toArray()['stats']['total_deliveries_express'],
            $orderDocumentCollection->toArray()['stats']['total_deliveries_with_extra_boxes'],
        ]);

        $csv->insertOne([
            'id' ,
            'status',
            'number',
            'cooled',
            'frozen',
            'isExpress',
            'startTime',
            'endTime',
            'boxes',
            'createdAt',
            'insideAt',
            'ongoingAt',
            'clientName',
            'clientPhone'
        ]);

        if (isset($orderDocumentCollection->toArray()['orders'])) {
            foreach ($orderDocumentCollection->toArray()['orders'] as $order) {
                $csv->insertOne([
                    $order['id'],
                    $order['status'],
                    $order['number'],
                    $order['cooled'],
                    $order['frozen'],
                    $order['isExpress'] ? 'true' : 'false',
                    $order['startTime'],
                    $order['endTime'],
                    $order['boxes'],
                    $order['createdAt'],
                    $order['insideAt'],
                    $order['ongoingAt'],
                    $order['clientName'],
                    $order['clientPhone'],
                ]);
            }
        }

        $csv->output('orders.csv');

        return new JsonResponse(true);
    }

    /**
     * Lists all order entities.
     *
     * @Route("/by_days")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus $bus
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws \KiwiInfrastructure\Core\Http\Exception\MissingParamHttpException
     */
    public function getOrdersAverageByDaysAction(Request $request, Bus $bus) : JsonResponse
    {
        $user = $this->checkUser();

        $storeId = $request->check('store_id');
        $dateStart = $request->check('date_start');
        $dateEnd = $request->check('date_end');
        $merchantId = $request->check('merchant_id');

        if (!$user->isAdmin()) {
            throw new AccessDeniedHttpException();
        }

        /** @var OrderByDaysDocument $orderByDaysDocument */
        $orderByDaysDocument = $bus->dispatchQuery(new GetOrderDaysAverage(
            new \DateTime($dateStart),
            new \DateTime($dateEnd),
            $storeId,
            $merchantId
        ));

        return new JsonResponse($orderByDaysDocument->toScalar());
    }


    /**
     * Lists all order entities.
     *
     * @Route("/by_days/xls")
     * @Method("GET")
     *
     * @param Request $request
     * @param Bus $bus
     *
     * @return JsonResponse
     * @throws \League\Csv\CannotInsertRecord
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws \KiwiInfrastructure\Core\Http\Exception\MissingParamHttpException
     * @throws \League\Csv\Exception
     * @throws \TypeError
     */
    public function getOrdersAverageByDaysXlsAction(Request $request, Bus $bus) : JsonResponse
    {
        $user = $this->checkUser();

        $storeId = $request->check('store_id');
        $dateStart = $request->check('date_start');
        $dateEnd = $request->check('date_end');
        $merchantId = $request->check('merchant_id');

        if (!$user->isAdmin()) {
            throw new AccessDeniedHttpException();
        }

        /** @var OrderByDaysDocument $orderByDaysDocument */
        $orderByDaysDocument = $bus->dispatchQuery(new GetOrderDaysAverage(
            new \DateTime($dateStart),
            new \DateTime($dateEnd),
            $storeId,
            $merchantId
        ));

        $csv = Writer::createFromFileObject(new SplTempFileObject());

        $csv->setDelimiter(';');

        $this->createCsv($csv, $orderByDaysDocument);

        $csv->output('order_percentages.csv');

        return new JsonResponse(true);
    }

    /**
     * TODO i know is not readable but time is running off
     * @param $csv
     * @param $orderByDaysDocument
     * @throws \League\Csv\CannotInsertRecord
     */
    private function createCsv(Writer $csv, OrderByDaysDocument $orderByDaysDocument): void
    {
        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        $csv->insertOne($days);

        $daysPercentage = [];
        foreach ($days as $day) {
            $daysPercentage[] = 'Day percentage - ' . round($orderByDaysDocument->toScalar()[$day]['day_percentage'], 2) . '%';
        }

        $csv->insertOne($daysPercentage);

        $dayTimeWindowsPercentage = $orderByDaysDocument->toScalar();

        $maxTimeWindows = 0;
        foreach ($days as $day) {
            if (count($dayTimeWindowsPercentage[$day]['day_time_windows_percentage']) > $maxTimeWindows) {
                $maxTimeWindows = count($dayTimeWindowsPercentage[$day]['day_time_windows_percentage']);
            }
        }

        for ($i = 0; $i < $maxTimeWindows; $i++) {
            $timeWindowPercentages = [];
            foreach ($days as $day) {
                if (count($dayTimeWindowsPercentage[$day]['day_time_windows_percentage']) > 0) {
                    $firstTimeWindow = reset($dayTimeWindowsPercentage[$day]['day_time_windows_percentage']);
                    $timeWindowPercentages[] = $firstTimeWindow['start_time']
                        . ' - '
                        . $firstTimeWindow['end_time']
                        . ' | Percentage:'
                        . round($firstTimeWindow['percentage'], 2)
                        . '%';
                    array_shift($dayTimeWindowsPercentage[$day]['day_time_windows_percentage']);
                } else {
                    $timeWindowPercentages[] = '';
                }
            }

            $csv->insertOne($timeWindowPercentages);
        }
    }
}
