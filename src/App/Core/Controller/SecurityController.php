<?php

declare(strict_types=1);

namespace App\Core\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController.
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     *
     * @return Response
     */
    public function loginAction(): Response
    {
        if (\is_object($this->getUser())) {
            $redirect = $this->redirectByRole($this->getUser()->getRole());

            return $this->redirect($this->generateUrl($redirect));
        }

        $authenticationUtils = $this->get('security.authentication_utils');
        $error               = $authenticationUtils->getLastAuthenticationError();
        $lastUsername        = $authenticationUtils->getLastUsername();

        return $this->render('default/index.html.twig', [
            'lastUsername' => $lastUsername,
            'error'        => $error
        ]);
    }

    /**
     * @param $role
     *
     * @return string
     */
    public function redirectByRole($role): string
    {
        if ('ROLE_ADMIN' === $role) {
            return 'app_shop_admin_index';
        }
        if ('ROLE_SAD' === $role) {
            return 'app_shop_order_new';
        }
        if ('ROLE_MANAGER' === $role) {
            return 'app_shop_order_index';
        }
        if ('ROLE_MERCHANT_MANAGER' === $role) {
            return 'app_shop_order_index';
        }
        if ('ROLE_SHIPPER' === $role) {
            return 'app_shop_shipper_index';
        }

        return 'app_core_security_login';
    }

    /**
     * @Route("/logout")
     */
    public function logoutAction(): void
    {
    }
}
