<?php

declare(strict_types=1);

namespace App\Core\Controller\User;

use Doctrine\ORM\EntityManagerInterface;
use KiwiInfrastructure\Core\Http\Request;
use KiwiInfrastructure\Core\Security\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class LoginController.
 */
class LoginController extends AbstractController
{
    /**
     * @Route("/api/login")
     * @Method("POST")
     *
     * @param Request                      $request
     * @param EntityManagerInterface       $manager
     * @param SessionInterface             $session
     * @param TokenStorageInterface        $tokenStorage
     * @param UserPasswordEncoderInterface $encoder
     * @param EventDispatcherInterface     $eventDispatcher
     *
     * @return Response
     */
    public function indexAction(
        Request $request,
        EntityManagerInterface $manager,
        SessionInterface $session,
        TokenStorageInterface $tokenStorage,
        UserPasswordEncoderInterface $encoder,
        EventDispatcherInterface $eventDispatcher
    ): Response {
        //$tokenStorage->setToken(null);

        $username = $request->check('username');
        $password = $request->check('password');

        $user = $manager->getRepository(User::class)->findOneBy(['username' => $username]);

        // TODO validate user
        if (!$user) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $isValid = $encoder->isPasswordValid($user, $password);

        if (!$isValid) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $tokenStorage->setToken($token);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
