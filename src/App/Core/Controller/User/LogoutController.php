<?php

declare(strict_types=1);

namespace App\Core\Controller\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class LogoutController.
 */
class LogoutController extends AbstractController
{
    /**
     * @Route("/api/logout")
     *
     * @param TokenStorageInterface $tokenStorage
     *
     * @return JsonResponse
     */
    public function indexAction(TokenStorageInterface $tokenStorage): JsonResponse
    {
        $tokenStorage->setToken(null);

        return new JsonResponse();
    }
}
