<?php

declare(strict_types=1);

namespace App\Core\Test;

use Kiwi\Shop\Location\Test\Dummy\LocationDummy;
use Kiwi\Shop\User\Domain\UserId;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class KiwiTestCase.
 */
class KiwiTestCase extends WebTestCase
{
    /**
     * @param Client $I
     * @param string $id
     */
    public function createLocation(Client $I, string $id): void
    {
        $location = LocationDummy::random($id);
        $I->makeARequest('POST', '/location/new', [
            'id'              => $location->id()->id(),
            'name'            => $location->name()->name(),
            'stairBuilding'   => $location->stairBuilding()->name(),
            'floor'           => $location->floor()->floor(),
            'door'            => $location->door()->door(),
            'city'            => $location->city()->city(),
            'province'        => $location->province()->province(),
            'postalCode'      => $location->postalCode()->postalCode(),
            'elevator'        => $location->hasElevator(),
            'hasStepElevator' => $location->hasStepElevator(),
            'annotation'      => $location->annotation()->message(),
            'latitude'        => $location->latitude()->value(),
            'longitude'       => $location->longitude()->value()
        ]);
    }

    /**
     * @return Client
     */
    protected static function startClient(): Client
    {
        return self::createClient();
    }

    /**
     * @return array
     */
    public function roles(): array
    {
        return [['ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_SHIPPER', 'ROLE_SAD']];
    }
}
