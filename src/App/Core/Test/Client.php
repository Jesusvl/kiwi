<?php

declare(strict_types=1);

namespace App\Core\Test;

use Faker\Factory;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Location\Command\CreateLocation;
use Kiwi\Shop\User\Command\CreateUser;
use KiwiInfrastructure\Core\Http\Request;
use Doctrine\Common\Persistence\ObjectManager;
use KiwiInfrastructure\Core\Security\User;
use Symfony\Bundle\FrameworkBundle\Client as BaseClient;
use Symfony\Bundle\SecurityBundle\Command\UserPasswordEncoderCommand;
use Symfony\Component\BrowserKit\CookieJar;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\BrowserKit\Request as DomRequest;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

/**
 * Class Client.
 *
 * @method Response getResponse()
 */
class Client extends BaseClient
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var Bus
     */
    private $bus;

    /**
     * @var UserPasswordEncoder
     */
    private $encoder;

    /**
     * Client constructor.
     *
     * @param KernelInterface $kernel
     * @param array           $server
     * @param History|null    $history
     * @param CookieJar|null  $cookieJar
     * @param Bus             $bus
     */
    public function __construct(
        KernelInterface $kernel,
        array $server = [],
        History $history = null,
        CookieJar $cookieJar = null,
        Bus $bus
    ) {
        parent::__construct($kernel, $server, $history, $cookieJar);
        $this->manager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->encoder = $kernel->getContainer()->get('security.password_encoder');
        $this->bus     = $bus;
    }

    /**
     * @param string $username
     * @param string $password
     */
    public function login(string $username, string $password): void
    {
        $this->request('POST', '/login', [
            '_username' => $username,
            '_password' => $password
        ]);
    }

    public function logAsAdmin(): void
    {
        $this->login('admin', 'admin');
    }

    /***********
     * REQUEST *
     ***********/

    /**
     * @param string      $method
     * @param string      $uri
     * @param array       $parameters
     * @param array       $files
     * @param array       $server
     * @param string|null $content
     * @param bool        $changeHistory
     *
     * @return Crawler
     */
    public function makeARequest(
        string $method,
        string $uri,
        array $parameters = [],
        array $files = [],
        array $server = [],
        ?string $content = null,
        bool $changeHistory = true
    ): Crawler {
        return $this->request(
            $method,
            $uri,
            $parameters,
            $files,
            $server,
            $content,
            $changeHistory
        );
    }

    /**
     * @return string
     */
    public function grabResponse(): string
    {
        return $this->getResponse()->getContent();
    }

    /**
     * @param int $status
     */
    public function shouldSeeResponseStatus(int $status): void
    {
        KiwiTestCase::assertNotNull($this->getResponse(), 'You must make a request first');
        KiwiTestCase::assertEquals($status, $this->getResponse()->getStatusCode());
    }

    /************
     * DOCTRINE *
     ************/

    /**
     * @param string $class
     */
    public function shouldSeeOneInDataBase(string $class, array $data): void
    {
        $data = $this->manager->getRepository($class)->findBy($data);
        KiwiTestCase::assertCount(1, $data, 'Cannot be seen in database');
    }

    /**
     * @param string $class
     * @param array  $data
     */
    public function shouldNotSeeInDataBase(string $class, array $data): void
    {
        $data = $this->manager->getRepository($class)->findBy($data);
        KiwiTestCase::assertCount(0, $data, 'Cannot be seen in database');
    }

    /**
     * Converts the BrowserKit request to a HttpKernel request.
     *
     * @param DomRequest $request
     *
     * @return Request A Request instance
     */
    protected function filterRequest(DomRequest $request): Request
    {
        $httpRequest = Request::create(
            $request->getUri(),
            $request->getMethod(),
            $request->getParameters(),
            $request->getCookies(),
            $request->getFiles(),
            $request->getServer(),
            $request->getContent()
        );

        foreach ($this->filterFiles($httpRequest->files->all()) as $key => $value) {
            $httpRequest->files->set($key, $value);
        }

        return $httpRequest;
    }

    /********
     * USER *
     ********/

    /**
     * @param string      $role
     * @param string      $username
     * @param string      $password
     * @param null|string $merchant
     * @param null|string $store
     *
     * @return string
     */
    public function createUser(
        string $role,
        string $username,
        string $password,
        ?string $merchant,
        ?string $store
    ): string {
        $id   = Factory::create()->uuid;
        $user = new User();
        $user->setId($id);
        $user->setRole($role);
        $user->setUsername($username);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $this->manager->persist($user);
        $this->manager->flush();

        $this->bus->dispatchCommand(new CreateUser(
            $id,
            $store,
            $merchant,
            $username,
            Factory::create()->word,
            $role,
            Factory::create()->phoneNumber
        ));

        return $id;
    }

    /************
     * LOCATION *
     ************/

    /**
     * @param string $userId
     * @param float  $latitude
     * @param float  $longitude
     *
     * @return string
     */
    public function createLocation(
        string $userId,
        float $latitude,
        float $longitude
    ): string {
        $locationId = Factory::create()->uuid;
        $this->bus->dispatchCommand(new CreateLocation(
            $userId,
            $locationId,
            Factory::create()->streetName,
            'A',
            '1',
            '1',
            Factory::create()->city,
            Factory::create()->country,
            Factory::create()->postcode,
            Factory::create()->boolean,
            Factory::create()->boolean,
            Factory::create()->sentence,
            $latitude,
            $longitude
        ));

        return $locationId;
    }
}
