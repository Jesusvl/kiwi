<?php

declare(strict_types=1);

namespace App\Core\Tests\Controller;

use App\Core\Test\KiwiTestCase;
use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SecurityControllerTest.
 */
class SecurityControllerTest extends KiwiTestCase
{
    /**
     * @test
     */
    public function itShouldLogin(): void
    {
        $I = self::startClient();

        $I->createUser(
            Factory::create()->uuid,
            'ROLE_ADMIN',
            'test',
            '1234',
            null,
            null,
            'test',
            '123-456-789'
        );
        $I->makeARequest('GET', '', [
            '_username' => 'test',
            '_password' => '1234'
        ]);
        $I->shouldSeeResponseStatus(Response::HTTP_FOUND);
    }
}
