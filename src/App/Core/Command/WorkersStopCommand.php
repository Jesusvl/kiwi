<?php

declare(strict_types=1);

namespace App\Core\Command;

use KiwiLib\Uuid\Uuid;
use Kiwi\Core\Infrastructure\Worker;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WorkersStopCommand.
 */
class WorkersStopCommand extends KiwiCommand
{
    protected function configure(): void
    {
        $this
            ->setName('workers:stop')
            ->addArgument('name')
            ->addArgument('boundary');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $command = 'rabbitmqctl list_connections pid port state user vhost recv_cnt send_cnt send_pend name | sed -n \'1!p\' | awk \'{print "rabbitmqctl close_connection \"" $1 "\" \"manually closing idle connection\"" | "/bin/bash" }\'';
        $return  = 0;
        for ($i = 0; $i < 6; $i++) {
            system($command, $return);
        }

        return 0;
    }
}
