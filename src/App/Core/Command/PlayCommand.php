<?php

declare(strict_types=1);

namespace App\Core\Command;

use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Store\Command\OpenTimespan;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AddMissingTimespanCommand.
 */
class PlayCommand extends KiwiCommand
{
    /**
     * @var Bus
     */
    private $bus;

    /**
     * PlayCommand constructor.
     *
     * @param Bus $bus
     */
    public function __construct(Bus $bus)
    {
        parent::__construct();
        $this->bus = $bus;
    }

    protected function configure(): void
    {
        $this->setName('play');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->login();

        $this->bus->dispatchCommand(new OpenTimespan('7ba7f7ac-db7d-45cf-b1f5-e7fdb2150e67'));
    }
}
