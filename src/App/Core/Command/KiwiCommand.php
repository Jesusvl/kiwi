<?php

declare(strict_types=1);

namespace App\Core\Command;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class KiwiCommand.
 */
abstract class KiwiCommand extends ContainerAwareCommand
{
    protected function login(): void
    {
        $repo = $this->getContainer()->get('doctrine')->getRepository(User::class);
        /** @var User $user */
        $user  = $repo->findOneBy(['username' => 'david']);
        $token = new UsernamePasswordToken(
            $user,
            null,
            'main',
            $user->getRoles()
        );
        // give it to the security context
        $this->getContainer()->get('security.token_storage')->setToken($token);
    }
}
