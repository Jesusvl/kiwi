<?php

declare(strict_types=1);

namespace App\Core\Command;

use Doctrine\ORM\EntityManagerInterface;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Merchant\Command\CreateMerchant;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Merchant\Domain\MerchantName;
use Kiwi\Shop\Store\Command\CreateStore;
use Kiwi\Shop\User\Command\CreateUser;
use KiwiInfrastructure\Core\Security\User;
use KiwiLib\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class DavidCommand.
 */
class DavidCommand extends ContainerAwareCommand
{
    /**
     * @var Bus
     */
    private $bus;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * DavidCommand constructor.
     *
     * @param Bus                          $bus
     * @param EntityManagerInterface       $manager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(Bus $bus, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();
        $this->bus     = $bus;
        $this->manager = $manager;
        $this->encoder = $encoder;
    }

    protected function configure(): void
    {
        $this->setName('david');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId          = Uuid::new();
        $merchantId      = Uuid::new();
        $davidMerchantId = Uuid::new();
        $davidStoreId    = Uuid::new();

        $output->write('Creating Merchants .......... ');
        $this->manager->persist(new Merchant(new MerchantId($davidMerchantId), new MerchantName('David')));
        $this->manager->persist(new Merchant(new MerchantId($merchantId), new MerchantName('CondisLife')));
        $this->manager->flush();
        $output->writeln('<info>✔</info>');

        $output->write('Creating Users .............. ');
        $user = new User();
        $user->setId($userId);
        $user->setUsername('david');
        $user->setRole('ROLE_ADMIN');
        $user->setPassword($this->encoder->encodePassword($user, 'david'));
        $this->manager->persist($user);
        $this->manager->flush();

        $this->bus->dispatchCommand(new CreateUser(
            $userId,
            $davidStoreId,
            $davidMerchantId,
            'david',
            'David',
            'ROLE_ADMIN',
            '123 456 789'
        ));
        $output->writeln('<info>✔</info>');

        $output->write('Creating Stores ............. ');
        $this->bus->dispatchCommand(new CreateStore($userId, $davidStoreId, 'David', $davidMerchantId));
        $output->writeln('<info>✔</info>');
    }
}
