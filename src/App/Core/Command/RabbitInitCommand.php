<?php

declare(strict_types=1);

namespace App\Core\Command;

use Kiwi\Core\Infrastructure\Worker;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RabbitInitCommand.
 */
class RabbitInitCommand extends KiwiCommand
{
    /**
     * @var Worker
     */
    private $worker;

    /**
     * RabbitInitCommand constructor.
     *
     * @param Worker $worker
     *
    public function __construct(Worker $worker)
    {
        parent::__construct();
        $th*is->worker = $worker;
    }*/

    protected function configure(): void
    {
        $this->setName('rabbit:start')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addArgument('boundary', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("<info>RabbitMQ started with id {$input->getArgument('name')}</info>");
        $this->worker->consume($input->getArgument('name'), $input->getArgument('boundary'));

        return 0;
    }
}
