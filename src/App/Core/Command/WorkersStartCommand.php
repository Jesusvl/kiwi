<?php

declare(strict_types=1);

namespace App\Core\Command;

use KiwiLib\Uuid\Uuid;
use Kiwi\Core\Infrastructure\Worker;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WorkersStartCommand.
 */
class WorkersStartCommand extends KiwiCommand
{
    protected function configure(): void
    {
        $this
            ->setName('workers:start')
            ->addArgument('name')
            ->addArgument('boundary');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name     = $input->getArgument('name');
        $boundary = $input->getArgument('boundary');

        while (true) {
            \passthru("bin/console rabbit:start $name $boundary -vvv");
        }
    }
}
