<?php

declare(strict_types=1);

namespace App\Core\Command;

use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Map\Map\Query\FetchCoordinates;
use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Client\Query\FindClients;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Query\FindMerchantByName;
use Kiwi\Shop\Order\Command\CreateOrder;
use Kiwi\Shop\Store\Command\AddStoreTimespan;
use Kiwi\Shop\Store\Command\Handler\AddStoreTimespanHandler;
use Kiwi\Shop\Store\Document\BaseStoreDocument;
use Kiwi\Shop\Store\Query\FindStoreByMerchantAndName;
use KiwiLib\DateTime\DateTime;
use KiwiLib\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TestAddressCommand.
 */
class TestAddressCommand extends ContainerAwareCommand
{
    /**
     * @var Bus
     */
    private $bus;

    /**
     * DemoCommand constructor.
     *
     * @param Bus $bus
     */
    public function __construct(Bus $bus)
    {
        parent::__construct();
        $this->bus = $bus;
    }

    protected function configure(): void
    {
        $this->setName('test:address')
            ->addArgument('address', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        dump($this->bus->dispatchQuery(new FetchCoordinates($input->getArgument('address'))));

        return 0;
    }
}
