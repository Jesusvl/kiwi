<?php

declare(strict_types=1);

namespace App\Core\Command;

use Doctrine\ORM\EntityManagerInterface;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Client\Command\CreateClient;
use Kiwi\Shop\Client\Exception\DuplicatedClientCodeException;
use Kiwi\Shop\Client\Query\FindClient;
use Kiwi\Shop\Location\Command\CreateLocation;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Query\FindMerchantByName;
use Kiwi\Shop\User\Domain\User;
use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ExcelCommand.
 */
class ExcelCommand extends ContainerAwareCommand
{
    /**
     * @var Bus
     */
    private $bus;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * ExcelCommand constructor.
     *
     * @param Bus                    $bus
     * @param EntityManagerInterface $manager
     */
    public function __construct(Bus $bus, EntityManagerInterface $manager)
    {
        parent::__construct();
        $this->bus     = $bus;
        $this->manager = $manager;
    }

    protected function configure(): void
    {
        $this->setName('csv')
            ->addArgument('file', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        DoctrineWriteModel::disableCache();
        $output->write('Importing clients ........... ');

        $data = \explode("\r", \file_get_contents($input->getArgument('file')));
        \array_pop($data);

        $output->writeln('');
        $i = 0;

        /** @var User $user */
        $user = $this->manager->getRepository(User::class)->findOneBy(['username' => 'david']);
        if (!$user) {
            return 1;
        }
        /** @var MerchantDocument $merchant */
        $merchant = $this->bus->dispatchQuery(new FindMerchantByName($user->id()->id(), 'CondisLife'));

        foreach ($data as $row) {
            ++$i;
            $info       = \str_getcsv($row);
            $clientId   = Uuid::uuid4()->toString();
            $locationId = Uuid::uuid4()->toString();

            if ($this->bus->dispatchQuery(new FindClient($user->id()->id(), $clientId, []))->isEmpty()) {
                try {
                    $this->bus->dispatchCommand(new CreateLocation(
                        $user->id()->id(),
                        $locationId,
                        $info[3],
                        $info[4],
                        $info[5],
                        $info[6],
                        $info[7],
                        $info[8],
                        '0' . $info[9],
                        false,
                        false,
                        '',
                        0.0,
                        0.0
                    ));

                    $this->bus->dispatchCommand(new CreateClient(
                        $user->id()->id(),
                        $clientId,
                        $info[1],
                        $info[2],
                        $info[1],
                        $merchant->id(),
                        $locationId
                    ));

                    $output->write('.');
                } catch (DuplicatedClientCodeException $e) {
                    $output->write(':');
                }
            } else {
                $output->write(':');
            }
        }

        return 0;
    }
}
