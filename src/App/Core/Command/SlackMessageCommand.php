<?php

declare(strict_types=1);

namespace App\Core\Command;

use Kiwi\Error\Error\Infrastructure\ErrorClient;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SlackMessageCommand.
 */
class SlackMessageCommand extends ContainerAwareCommand
{
    /**
     * @var ErrorClient
     */
    private $client;

    /**
     * SlackMessageCommand constructor.
     *
     * @param ErrorClient $client
     */
    public function __construct(ErrorClient $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    protected function configure(): void
    {
        $this->setName('test:slack')
             ->addArgument('message', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->client->sendMessage($input->getArgument('message'));

        return 0;
    }
}
