<?php

declare(strict_types=1);

namespace App\Core\Command;

use Doctrine\ORM\EntityManagerInterface;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Client\Query\FindClients;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Query\FindMerchantByName;
use Kiwi\Shop\Order\Command\CreateOrder;
use Kiwi\Shop\Store\Command\AddStoreTimespan;
use Kiwi\Shop\Store\Document\BaseStoreDocument;
use Kiwi\Shop\Store\Query\FindStoreByMerchantAndName;
use Kiwi\Shop\User\Domain\User;
use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use KiwiLib\DateTime\DateTime;
use KiwiLib\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DemoCommand.
 */
class DemoCommand extends ContainerAwareCommand
{
    /**
     * @var Bus
     */
    private $bus;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * DemoCommand constructor.
     *
     * @param Bus                    $bus
     * @param EntityManagerInterface $manager
     */
    public function __construct(Bus $bus, EntityManagerInterface $manager)
    {
        parent::__construct();
        $this->bus     = $bus;
        $this->manager = $manager;
    }

    protected function configure(): void
    {
        $this->setName('demo');
    }

    /**
     * @param int $pos
     *
     * @return DateTime
     */
    public function getDateTimeStart(int $pos): DateTime
    {
        $list = [
            new DateTime('20:00'),
            new DateTime('19:30'),
            new DateTime('19:00'),
            new DateTime('18:30'),
            new DateTime('18:00'),
            new DateTime('17:30'),
            new DateTime('17:00'),
            new DateTime('16:30'),
            new DateTime('16:00'),
            new DateTime('15:30'),
            new DateTime('15:00'),
            new DateTime('14:30'),
            new DateTime('14:00'),
            new DateTime('13:30'),
            new DateTime('13:00'),
            new DateTime('12:30'),
            new DateTime('12:00'),
            new DateTime('11:30'),
            new DateTime('11:00'),
            new DateTime('10:30'),
            new DateTime('10:00')
        ];

        return $list[$pos];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        DoctrineWriteModel::disableCache();
        /** @var User $user */
        /** @var MerchantDocument $merchant */
        /** @var BaseStoreDocument $store */
        /** @var Client[] $clients */
        $user = $this->manager->getRepository(User::class)->findOneBy(['username' => 'david']);
        if (!$user) {
            return 1;
        }
        $merchant = $this->bus->dispatchQuery(new FindMerchantByName($user->id()->id(), 'David'));
        $store    = $this->bus->dispatchQuery(
            new FindStoreByMerchantAndName($user->id()->id(), $merchant->id(), 'David', [])
        );
        /** @var ClientDocument[] $clients */
        $clients  = $this->bus->dispatchQuery(new FindClients($user->id()->id(), 1, 300))->list();

        for ($i = 0; $i < 21; $i++) {
            $this->bus->dispatchCommand(new AddStoreTimespan(
                $user->id()->id(),
                $store->id(),
                $this->getDateTimeStart($i),
                $this->getDateTimeStart($i)->modify('+1hour')
            ));
        }
        for ($i = 0; $i < 300; ++$i) {
            $pos       = \random_int(0, 20);
            $clientPos = \array_rand($clients);
            $this->bus->dispatchCommand(new CreateOrder(
                $user->id()->id(),
                Uuid::new(),
                $clients[$clientPos]->id(),
                \uniqid('', true),
                \random_int(0, 10),
                \random_int(0, 10),
                $store->id(),
                $merchant->id(),
                $this->getDateTimeStart($pos),
                $this->getDateTimeStart($pos)->modify('+1hour'),
                1 === \random_int(0, 1),
                $clients[$clientPos]->locationId(),
                \random_int(1, 5),
                false
            ));
        }

        return 0;
    }
}
