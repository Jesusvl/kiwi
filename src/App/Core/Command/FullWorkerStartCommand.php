<?php

declare(strict_types=1);

namespace App\Core\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FullWorkerStartCommand.
 */
class FullWorkerStartCommand extends KiwiCommand
{
    protected function configure(): void
    {
        $this
            ->setName('workers:serve');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        \passthru('
            bin/console workers:start Billing0 Kiwi.Billing -vvv &
            bin/console workers:start Map0 Kiwi.Map -vvv &
            bin/console workers:start Shop0 Kiwi.Shop -vvv & 
        ');

        return 0;
    }
}
