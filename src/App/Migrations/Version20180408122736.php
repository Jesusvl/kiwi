<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * phpcs:ignoreFile
 *
 * Class Version20180408122736.
 */
class Version20180408122736 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE _store_available_postal_code (id UUID NOT NULL, store_id UUID NOT NULL, postal_code VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN _store_available_postal_code.id IS \'(DC2Type:store_timespan_id)\'');
        $this->addSql('COMMENT ON COLUMN _store_available_postal_code.store_id IS \'(DC2Type:store_id)\'');
        $this->addSql('COMMENT ON COLUMN _store_available_postal_code.postal_code IS \'(DC2Type:postal_code)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE sessions (sess_id VARCHAR(128) NOT NULL, sess_data BYTEA NOT NULL, sess_time INT NOT NULL, sess_lifetime INT NOT NULL, PRIMARY KEY(sess_id))');
    }
}
