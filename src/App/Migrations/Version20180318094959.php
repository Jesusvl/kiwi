<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180318094959.
 */
class Version20180318094959 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER INDEX uniq_3e77e3d677153099 RENAME TO UNIQ_3E77E3D677153098');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER INDEX uniq_3e77e3d677153098 RENAME TO uniq_3e77e3d677153099');
    }
}
