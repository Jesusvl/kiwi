<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180304123620.
 */
class Version20180304123620 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _timespan.start_time IS \'(DC2Type:kiwi_time)\'');
        $this->addSql('COMMENT ON COLUMN _timespan.end_time IS \'(DC2Type:kiwi_time)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _timespan.start_time IS NULL');
        $this->addSql('COMMENT ON COLUMN _timespan.end_time IS NULL');
    }
}
