<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180304153050.
 *
 * phpcs:ignoreFile
 */
class Version20180304153050 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _order ADD destination_id UUID;');
        $this->addSql('ALTER TABLE _destination ADD location_id UUID;');
        $this->addSql('COMMENT ON COLUMN _order.id IS \'(DC2Type:order_id)\'');
        $this->addSql('COMMENT ON COLUMN _order.status IS \'(DC2Type:order_status)\'');
        $this->addSql('COMMENT ON COLUMN _order.number IS \'(DC2Type:order_number)\'');
        $this->addSql('COMMENT ON COLUMN _order.start_time IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _order.end_time IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _order.client_id IS \'(DC2Type:client_id)\'');
        $this->addSql('COMMENT ON COLUMN _order.store_id IS \'(DC2Type:store_id)\'');
        $this->addSql('COMMENT ON COLUMN _order.delivered_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _order.created_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _order.missing_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _order.boxes IS \'(DC2Type:order_boxes_quantity)\'');
        $this->addSql('COMMENT ON COLUMN _order.cooled IS \'(DC2Type:order_refrigerated_quantity)\'');
        $this->addSql('COMMENT ON COLUMN _order.frozen IS \'(DC2Type:order_frozen_quantity)\'');
        $this->addSql('COMMENT ON COLUMN _order.ongoing_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _order.inside_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _order.destination_id IS \'(DC2Type:destination_id)\'');
        $this->addSql('COMMENT ON COLUMN _destination.location_id IS \'(DC2Type:location_id)\'');
        $this->addSql('UPDATE _order SET destination_id=_order.id');
        $this->addSql('
            INSERT INTO _destination(id, address, annotation, latitude, longitude, has_elevator, has_step_elevator, location_id) 
            SELECT o.id, 
                CONCAT(l.name, \', \', l.stair_building, \' \', l.floor, \' \', l.door, \', \', l.postal_code , \' \', l.city, \' (\', l.province, \')\'), 
                l.annotation,
                l.latitude,
                l.longitude,
                l.elevator,
                l.has_step_elevator,
                o.location_id
            FROM _order o LEFT JOIN _location l ON l.id = o.location_id;
        ');
        $this->addSql('
          UPDATE _destination 
          SET location_id = (select _order.location_id from _order where _destination.id=_order.id)
          ');
        $this->addSql('
          UPDATE _destination 
          SET location_id = (select _order.location_id from _order where _destination.id=_order.id)
          ');
        $this->addSql('ALTER TABLE _order ALTER destination_id SET NOT NULL');
        $this->addSql('ALTER TABLE _destination ALTER location_id SET NOT NULL');
        $this->addSql('ALTER TABLE _order DROP location_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _order ADD location_id UUID;');
        $this->addSql('ALTER TABLE _order DROP destination_id');
        $this->addSql('ALTER TABLE _destination DROP location_id');
        $this->addSql('COMMENT ON COLUMN _order.id IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.status IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.number IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.start_time IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.end_time IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.client_id IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.store_id IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.delivered_at IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.created_at IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.missing_at IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.boxes IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.cooled IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.frozen IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.ongoing_at IS NULL');
        $this->addSql('COMMENT ON COLUMN _order.inside_at IS NULL');
        $this->addSql('UPDATE _order SET location_id=_order.id');
        $this->addSql('ALTER TABLE _order ALTER location_id SET NOT NULL');
    }
}
