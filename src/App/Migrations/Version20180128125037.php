<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180128125037.
 *
 * phpcs:ignoreFile
 */
class Version20180128125037 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE _timespan (id UUID NOT NULL, start_time TIME(0) WITHOUT TIME ZONE NOT NULL, end_time TIME(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX _timespan_unique_idx ON _timespan (start_time, end_time)');
        $this->addSql('COMMENT ON COLUMN _timespan.id IS \'(DC2Type:timespanId)\'');
        $this->addSql('COMMENT ON COLUMN _timespan.start_time IS \'(DC2Type:time_immutable)\'');
        $this->addSql('COMMENT ON COLUMN _timespan.end_time IS \'(DC2Type:time_immutable)\'');
        $this->addSql('CREATE TABLE _store_timespan (id UUID NOT NULL, store_id UUID NOT NULL, timespan_id UUID NOT NULL, is_closed BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX _timespan_store_unique_idx ON _store_timespan (store_id, timespan_id)');
        $this->addSql('COMMENT ON COLUMN _store_timespan.id IS \'(DC2Type:storeTimespanId)\'');
        $this->addSql('COMMENT ON COLUMN _store_timespan.timespan_id IS \'(DC2Type:timespanId)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE _timespan');
        $this->addSql('DROP TABLE _store_timespan');
    }
}
