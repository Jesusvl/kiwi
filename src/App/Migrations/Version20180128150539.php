<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180128150539.
 *
 * phpcs:ignoreFile
 */
class Version20180128150539 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _store ADD is_express_enabled BOOLEAN DEFAULT \'true\' NOT NULL');
        $this->addSql('ALTER TABLE _store_timespan ALTER is_closed SET DEFAULT \'false\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _store DROP is_express_enabled');
        $this->addSql('ALTER TABLE _store_timespan ALTER is_closed SET DEFAULT \'true\'');
    }
}
