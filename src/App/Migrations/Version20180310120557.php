<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * phpcs:ignoreFile
 *
 * Class Version20180310120557.
 */
class Version20180310120557 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _user DROP CONSTRAINT fk_d0b6a652b092a811');
        $this->addSql('ALTER TABLE _user DROP CONSTRAINT fk_d0b6a6526796d554');
        $this->addSql('DROP INDEX idx_d0b6a6526796d554');
        $this->addSql('DROP INDEX idx_d0b6a652b092a811');
        $this->addSql('ALTER TABLE _user DROP store_id');
        $this->addSql('ALTER TABLE _user DROP merchant_id');
        $this->addSql('ALTER TABLE _user DROP name');
        $this->addSql('ALTER TABLE _user DROP active');
        $this->addSql('ALTER TABLE _user DROP phone');
        $this->addSql('ALTER TABLE _user ALTER role SET NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _user ADD store_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE _user ADD merchant_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE _user ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE _user ADD active BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE _user ADD phone VARCHAR(32) NOT NULL');
        $this->addSql('ALTER TABLE _user ALTER role DROP NOT NULL');
        $this->addSql('COMMENT ON COLUMN _user.store_id IS \'(DC2Type:store_id)\'');
        $this->addSql('COMMENT ON COLUMN _user.merchant_id IS \'(DC2Type:merchant_id)\'');
        $this->addSql('ALTER TABLE _user ADD CONSTRAINT fk_d0b6a652b092a811 FOREIGN KEY (store_id) REFERENCES _store (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE _user ADD CONSTRAINT fk_d0b6a6526796d554 FOREIGN KEY (merchant_id) REFERENCES _merchant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d0b6a6526796d554 ON _user (merchant_id)');
        $this->addSql('CREATE INDEX idx_d0b6a652b092a811 ON _user (store_id)');
    }
}
