<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180303231213.
 */
class Version20180303231213 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _timespan.id IS \'(DC2Type:timespan_id)\'');
        $this->addSql('COMMENT ON COLUMN _store_timespan.id IS \'(DC2Type:store_timespan_id)\'');
        $this->addSql('COMMENT ON COLUMN _store_timespan.timespan_id IS \'(DC2Type:timespan_id)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _timespan.id IS \'(DC2Type:timespanId)\'');
        $this->addSql('COMMENT ON COLUMN _store_timespan.id IS \'(DC2Type:storeTimespanId)\'');
        $this->addSql('COMMENT ON COLUMN _store_timespan.timespan_id IS \'(DC2Type:timespanId)\'');
    }
}
