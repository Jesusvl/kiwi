<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * phpcs:ignoreFile
 *
 * Class Version20180328184914.
 */
class Version20180328184914 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE _billing_merchant (id UUID NOT NULL, name VARCHAR(16) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C5B639A95E237E06 ON _billing_merchant (name)');
        $this->addSql('COMMENT ON COLUMN _billing_merchant.id IS \'(DC2Type:billing_merchant_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_merchant.name IS \'(DC2Type:billing_merchant_name)\'');
        $this->addSql('CREATE TABLE _billing_client (id UUID NOT NULL, code VARCHAR(64) NOT NULL, full_name VARCHAR(255) NOT NULL, phone VARCHAR(32) NOT NULL, merchant_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5EE77A8A77153098 ON _billing_client (code)');
        $this->addSql('CREATE UNIQUE INDEX _billing_client_phone_merchantId_idx ON _billing_client (phone, merchant_id)');
        $this->addSql('COMMENT ON COLUMN _billing_client.id IS \'(DC2Type:client_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_client.code IS \'(DC2Type:billing_client_code)\'');
        $this->addSql('COMMENT ON COLUMN _billing_client.full_name IS \'(DC2Type:billing_client_name)\'');
        $this->addSql('COMMENT ON COLUMN _billing_client.phone IS \'(DC2Type:billing_client_phone)\'');
        $this->addSql('COMMENT ON COLUMN _billing_client.merchant_id IS \'(DC2Type:billing_merchant_id)\'');
        $this->addSql('CREATE TABLE _billing_timespan (id UUID NOT NULL, start_time TIME(0) WITHOUT TIME ZONE NOT NULL, end_time TIME(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX _billing_timespan_unique_idx ON _billing_timespan (start_time, end_time)');
        $this->addSql('COMMENT ON COLUMN _billing_timespan.id IS \'(DC2Type:billing_timespan_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_timespan.start_time IS \'(DC2Type:kiwi_time)\'');
        $this->addSql('COMMENT ON COLUMN _billing_timespan.end_time IS \'(DC2Type:kiwi_time)\'');
        $this->addSql('CREATE TABLE _billing_store (id UUID NOT NULL, name VARCHAR(64) NOT NULL, merchant_id UUID NOT NULL, shipper UUID DEFAULT NULL, is_express_enabled BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN _billing_store.id IS \'(DC2Type:store_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_store.name IS \'(DC2Type:billing_store_name)\'');
        $this->addSql('COMMENT ON COLUMN _billing_store.merchant_id IS \'(DC2Type:billing_merchant_id)\'');
        $this->addSql('CREATE TABLE _billing_store_timespan (id UUID NOT NULL, store_id UUID NOT NULL, timespan_id UUID NOT NULL, is_closed BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN _billing_store_timespan.id IS \'(DC2Type:billing_store_timespan_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_store_timespan.store_id IS \'(DC2Type:billing_store_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_store_timespan.timespan_id IS \'(DC2Type:billing_timespan_id)\'');
        $this->addSql('CREATE TABLE _billing_order (id UUID NOT NULL, status VARCHAR(32) NOT NULL, number VARCHAR(32) NOT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, client_id UUID NOT NULL, shipper_id UUID DEFAULT NULL, store_id UUID NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, missing_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, ongoing_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, inside_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, boxes INT NOT NULL, cooled INT NOT NULL, frozen INT NOT NULL, is_express BOOLEAN NOT NULL, client_name VARCHAR(255) DEFAULT \'\' NOT NULL, client_phone VARCHAR(255) DEFAULT \'\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN _billing_order.id IS \'(DC2Type:billing_order_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.status IS \'(DC2Type:billing_order_status)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.number IS \'(DC2Type:billing_order_number)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.start_time IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.end_time IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.client_id IS \'(DC2Type:billing_client_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.store_id IS \'(DC2Type:billing_store_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.delivered_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.created_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.missing_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.ongoing_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.inside_at IS \'(DC2Type:kiwi_datetime)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.boxes IS \'(DC2Type:billing_order_boxes_quantity)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.cooled IS \'(DC2Type:billing_order_refrigerated_quantity)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.frozen IS \'(DC2Type:billing_order_frozen_quantity)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.client_name IS \'(DC2Type:billing_order_client_name)\'');
        $this->addSql('COMMENT ON COLUMN _billing_order.client_phone IS \'(DC2Type:billing_order_client_phone)\'');
        $this->addSql('CREATE TABLE _billing_kiwi_user (id UUID NOT NULL, name VARCHAR(255) NOT NULL, username VARCHAR(25) NOT NULL, phone VARCHAR(32) NOT NULL, role VARCHAR(20) NOT NULL, merchant_id UUID DEFAULT NULL, store_id UUID DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9F8D8581F85E0677 ON _billing_kiwi_user (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9F8D8581444F97DD ON _billing_kiwi_user (phone)');
        $this->addSql('COMMENT ON COLUMN _billing_kiwi_user.id IS \'(DC2Type:billing_kiwiuser_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_kiwi_user.name IS \'(DC2Type:billing_kiwiuser_name)\'');
        $this->addSql('COMMENT ON COLUMN _billing_kiwi_user.username IS \'(DC2Type:billing_kiwiuser_username)\'');
        $this->addSql('COMMENT ON COLUMN _billing_kiwi_user.phone IS \'(DC2Type:billing_kiwiuser_phone)\'');
        $this->addSql('COMMENT ON COLUMN _billing_kiwi_user.role IS \'(DC2Type:billing_kiwiuser_role)\'');
        $this->addSql('COMMENT ON COLUMN _billing_kiwi_user.merchant_id IS \'(DC2Type:billing_merchant_id)\'');
        $this->addSql('COMMENT ON COLUMN _billing_kiwi_user.store_id IS \'(DC2Type:billing_store_id)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE _billing_merchant');
        $this->addSql('DROP TABLE _billing_client');
        $this->addSql('DROP TABLE _billing_timespan');
        $this->addSql('DROP TABLE _billing_store');
        $this->addSql('DROP TABLE _billing_store_timespan');
        $this->addSql('DROP TABLE _billing_order');
        $this->addSql('DROP TABLE _billing_kiwi_user');
    }
}
