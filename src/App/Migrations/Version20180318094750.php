<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180318094750.
 */
class Version20180318094750 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _order.shipper_id IS \'(DC2Type:kiwiuser_id)\';');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _order.shipper_id IS NULL;');
    }
}
