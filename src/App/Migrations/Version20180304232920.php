<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180304232920.
 */
class Version20180304232920 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX _timespan_store_unique_idx;');
        $this->addSql('ALTER TABLE _order ADD client_name VARCHAR(255) DEFAULT \'\' NOT NULL;');
        $this->addSql('ALTER TABLE _order ADD client_phone VARCHAR(255) DEFAULT \'\' NOT NULL;');
        $this->addSql('COMMENT ON COLUMN _order.client_name IS \'(DC2Type:order_client_name)\';');
        $this->addSql('COMMENT ON COLUMN _order.client_phone IS \'(DC2Type:order_client_phone)\';');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX _timespan_store_unique_idx ON _store_timespan (store_id, timespan_id)');
        $this->addSql('ALTER TABLE _order DROP client_name');
        $this->addSql('ALTER TABLE _order DROP client_phone');
    }
}
