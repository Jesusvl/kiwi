<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180304150753.
 *
 * phpcs:ignoreFile
 */
class Version20180304150753 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE _destination (id UUID NOT NULL, address VARCHAR(255) NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, has_elevator BOOLEAN NOT NULL, has_step_elevator BOOLEAN NOT NULL, annotation TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN _destination.id IS \'(DC2Type:destination_id)\'');
        $this->addSql('COMMENT ON COLUMN _destination.address IS \'(DC2Type:destination_address)\'');
        $this->addSql('COMMENT ON COLUMN _destination.latitude IS \'(DC2Type:destination_latitude)\'');
        $this->addSql('COMMENT ON COLUMN _destination.longitude IS \'(DC2Type:destination_longitude)\'');
        $this->addSql('COMMENT ON COLUMN _destination.annotation IS \'(DC2Type:destination_annotation)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE _destination');
        $this->addSql('CREATE UNIQUE INDEX _timespan_store_unique_idx ON _store_timespan (store_id, timespan_id)');
    }
}
