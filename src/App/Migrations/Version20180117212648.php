<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180117212648.
 *
 * phpcs:ignoreFile
 */
class Version20180117212648 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
        $this->addSql("SET timezone TO 'Europe/Madrid'");
        $this->addSql('CREATE TABLE _location (id UUID NOT NULL, name VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, province VARCHAR(64) NOT NULL, postal_code VARCHAR(16) NOT NULL, latitude NUMERIC(11, 7) NOT NULL, longitude NUMERIC(11, 7) NOT NULL, door VARCHAR(16) NOT NULL, floor VARCHAR(16) NOT NULL, stair_building VARCHAR(16) NOT NULL, elevator BOOLEAN NOT NULL, has_step_elevator BOOLEAN NOT NULL, annotation VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE _cleanlocation (id UUID NOT NULL, location_id UUID NOT NULL, name VARCHAR(255) NOT NULL, number VARCHAR(16) NOT NULL, city VARCHAR(255) NOT NULL, province VARCHAR(64) NOT NULL, administrative VARCHAR(64) NOT NULL, country VARCHAR(32) NOT NULL, country_code VARCHAR(4) NOT NULL, postal_code VARCHAR(16) NOT NULL, address VARCHAR(255) NOT NULL, latitude NUMERIC(11, 7) NOT NULL, longitude NUMERIC(11, 7) NOT NULL, precision INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX location_idx ON _cleanlocation (location_id)');
        $this->addSql('CREATE TABLE _shopper (id UUID NOT NULL, code VARCHAR(64) NOT NULL, full_name VARCHAR(255) NOT NULL, phone VARCHAR(32) NOT NULL, location_id UUID NOT NULL, merchant_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE _store (id UUID NOT NULL, name VARCHAR(64) NOT NULL, merchant_id UUID NOT NULL, shipper UUID DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE _merchant (id UUID NOT NULL, name VARCHAR(16) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5A6B60815E237E06 ON _merchant (name)');
        $this->addSql('CREATE TABLE _order (id UUID NOT NULL, status VARCHAR(32) NOT NULL, number VARCHAR(32) NOT NULL, location_id UUID NOT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, client_id UUID NOT NULL, shipper_id UUID DEFAULT NULL, store_id UUID NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, missing_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, boxes INT NOT NULL, cooled INT NOT NULL, frozen INT NOT NULL, is_express BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN _order.start_time IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN _order.end_time IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN _order.delivered_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN _order.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN _order.missing_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE _user (id UUID NOT NULL, store_id UUID DEFAULT NULL, merchant_id UUID DEFAULT NULL, username VARCHAR(25) NOT NULL, name VARCHAR(255) NOT NULL, role VARCHAR(20) DEFAULT NULL, password VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, phone VARCHAR(32) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D0B6A652F85E0677 ON _user (username)');
        $this->addSql('CREATE INDEX IDX_D0B6A652B092A811 ON _user (store_id)');
        $this->addSql('CREATE INDEX IDX_D0B6A6526796D554 ON _user (merchant_id)');
        $this->addSql('ALTER TABLE _user ADD CONSTRAINT FK_D0B6A652B092A811 FOREIGN KEY (store_id) REFERENCES _store (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE _user ADD CONSTRAINT FK_D0B6A6526796D554 FOREIGN KEY (merchant_id) REFERENCES _merchant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _user DROP CONSTRAINT FK_D0B6A652B092A811');
        $this->addSql('ALTER TABLE _user DROP CONSTRAINT FK_D0B6A6526796D554');
        $this->addSql('DROP TABLE _location');
        $this->addSql('DROP TABLE _cleanlocation');
        $this->addSql('DROP TABLE _shopper');
        $this->addSql('DROP TABLE _store');
        $this->addSql('DROP TABLE _merchant');
        $this->addSql('DROP TABLE _order');
        $this->addSql('DROP TABLE _user');
    }
}
