<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180303231555.
 */
class Version20180303231555 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _store.id IS \'(DC2Type:store_id)\'');
        $this->addSql('COMMENT ON COLUMN _store.location_id IS \'(DC2Type:location_id)\'');
        $this->addSql('COMMENT ON COLUMN _store.name IS \'(DC2Type:store_name)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _store.id IS NULL');
        $this->addSql('COMMENT ON COLUMN _store.location_id IS NULL');
        $this->addSql('COMMENT ON COLUMN _store.name IS NULL');
    }
}
