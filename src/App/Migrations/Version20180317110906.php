<?php declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180317110906.
 *
 * phpcs:ignoreFile
 */
class Version20180317110906 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE _cleanlocation');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.id IS \'(DC2Type:kiwiuser_id)\';');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE _cleanlocation (id UUID NOT NULL, location_id UUID NOT NULL, name VARCHAR(255) NOT NULL, number VARCHAR(16) NOT NULL, city VARCHAR(255) NOT NULL, province VARCHAR(64) NOT NULL, administrative VARCHAR(64) NOT NULL, country VARCHAR(32) NOT NULL, country_code VARCHAR(4) NOT NULL, postal_code VARCHAR(16) NOT NULL, address VARCHAR(255) NOT NULL, latitude NUMERIC(11, 7) NOT NULL, longitude NUMERIC(11, 7) NOT NULL, "precision" INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX location_idx ON _cleanlocation (location_id)');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.id IS NULL;');
    }
}
