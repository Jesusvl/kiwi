<?php

declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180701174433.
 */
class Version20180701174433 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX uniq_3e77e3d677153098');
        $this->addSql('CREATE UNIQUE INDEX client_code_merchantId_idx ON _client (code, merchant_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX client_code_merchantId_idx');
        $this->addSql('CREATE UNIQUE INDEX uniq_3e77e3d677153098 ON _client (code)');
    }
}
