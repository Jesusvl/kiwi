<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180303121013.
 */
class Version20180303121013 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _shopper.id IS \'(DC2Type:client_id)\';');
        $this->addSql('COMMENT ON COLUMN _shopper.code IS \'(DC2Type:client_code)\';');
        $this->addSql('COMMENT ON COLUMN _shopper.full_name IS \'(DC2Type:client_name)\';');
        $this->addSql('COMMENT ON COLUMN _shopper.phone IS \'(DC2Type:client_phone)\';');
        $this->addSql('COMMENT ON COLUMN _shopper.location_id IS \'(DC2Type:location_id)\';');
        $this->addSql('COMMENT ON COLUMN _location.id IS \'(DC2Type:location_id)\';');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _shopper.id IS NULL;');
        $this->addSql('COMMENT ON COLUMN _shopper.code IS NULL;');
        $this->addSql('COMMENT ON COLUMN _shopper.full_name IS NULL;');
        $this->addSql('COMMENT ON COLUMN _shopper.phone IS NULL;');
        $this->addSql('COMMENT ON COLUMN _shopper.location_id IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.id IS \'(DC2Type:locationId)\';');
    }
}
