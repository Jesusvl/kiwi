<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180303180153.
 */
class Version20180303180153 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _location.name IS \'(DC2Type:location_street)\';');
        $this->addSql('COMMENT ON COLUMN _location.city IS \'(DC2Type:location_city)\';');
        $this->addSql('COMMENT ON COLUMN _location.province IS \'(DC2Type:location_province)\';');
        $this->addSql('COMMENT ON COLUMN _location.postal_code IS \'(DC2Type:location_postal_code)\';');
        $this->addSql('COMMENT ON COLUMN _location.latitude IS \'(DC2Type:location_latitude)\';');
        $this->addSql('COMMENT ON COLUMN _location.longitude IS \'(DC2Type:location_longitude)\';');
        $this->addSql('COMMENT ON COLUMN _location.door IS \'(DC2Type:location_door)\';');
        $this->addSql('COMMENT ON COLUMN _location.floor IS \'(DC2Type:location_floor)\';');
        $this->addSql('COMMENT ON COLUMN _location.stair_building IS \'(DC2Type:location_stair)\';');
        $this->addSql('COMMENT ON COLUMN _location.annotation IS \'(DC2Type:location_annotation)\';');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3E77E3D677153099 ON _client (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3E77E3D6444F97DD ON _client (phone)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3E77E3D664D218E ON _client (location_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _location.name IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.city IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.province IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.postal_code IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.latitude IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.longitude IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.door IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.floor IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.stair_building IS NULL;');
        $this->addSql('COMMENT ON COLUMN _location.annotation IS NULL;');
        $this->addSql('DROP INDEX UNIQ_3E77E3D677153099');
        $this->addSql('DROP INDEX UNIQ_3E77E3D6444F97DD');
        $this->addSql('DROP INDEX UNIQ_3E77E3D664D218E');
    }
}
