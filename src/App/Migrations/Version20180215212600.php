<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180215212600.
 */
class Version20180215212600 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _order ADD needs_payment BOOLEAN DEFAULT \'false\' NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _order DROP needs_payment');
    }
}
