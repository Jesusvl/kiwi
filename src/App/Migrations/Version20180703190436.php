<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180703190436.
 */
class Version20180703190436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _kiwi_user ALTER role TYPE VARCHAR(32)');
        $this->addSql('ALTER TABLE _user ALTER role TYPE VARCHAR(32)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _user ALTER role TYPE VARCHAR(20)');
        $this->addSql('ALTER TABLE _kiwi_user ALTER role TYPE VARCHAR(20)');
    }
}
