<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * phpcs:ignoreFile
 *
 * Class Version20180310115611.
 */
class Version20180310115611 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE _kiwi_user (id UUID NOT NULL, name VARCHAR(255) NOT NULL, username VARCHAR(25) NOT NULL, phone VARCHAR(32) NOT NULL, role VARCHAR(20) NOT NULL, merchant_id UUID DEFAULT NULL, store_id UUID DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AAA7F022F85E0677 ON _kiwi_user (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AAA7F022444F97DD ON _kiwi_user (phone)');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.id IS \'(DC2Type:client_id)\'');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.name IS \'(DC2Type:kiwiuser_name)\'');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.username IS \'(DC2Type:kiwiuser_username)\'');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.phone IS \'(DC2Type:kiwiuser_phone)\'');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.role IS \'(DC2Type:kiwiuser_role)\'');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.merchant_id IS \'(DC2Type:merchant_id)\'');
        $this->addSql('COMMENT ON COLUMN _kiwi_user.store_id IS \'(DC2Type:store_id)\'');

        $this->addSql('
            INSERT INTO _kiwi_user(id, name, username, phone, role, merchant_id, store_id) 
            SELECT 
                u.id, 
                u.name,
                u.username,
                u.phone,
                u.role,
                u.merchant_id,
                u.store_id
            FROM _user u;
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE _kiwi_user');
    }
}
