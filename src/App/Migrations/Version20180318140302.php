<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use KiwiLib\Exception\TODOException;

/**
 * Class Version20180318140302.
 */
class Version20180318140302 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _order ADD merchant_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN _order.merchant_id IS \'(DC2Type:merchant_id)\'');

        $this->addSql('
          UPDATE _order 
          SET merchant_id = (select _store.merchant_id from _store where _store.id=_order.store_id)
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _order DROP merchant_id');
    }
}
