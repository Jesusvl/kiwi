<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180321192531.
 */
class Version20180321192531 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _destination ADD floor VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE _destination ADD door VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE _destination ADD stair VARCHAR(255) DEFAULT \'\' NOT NULL');
        $this->addSql('COMMENT ON COLUMN _destination.floor IS \'(DC2Type:destination_floor)\'');
        $this->addSql('COMMENT ON COLUMN _destination.door IS \'(DC2Type:destination_door)\'');
        $this->addSql('COMMENT ON COLUMN _destination.stair IS \'(DC2Type:destination_stair)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE _destination DROP floor');
        $this->addSql('ALTER TABLE _destination DROP door');
        $this->addSql('ALTER TABLE _destination DROP stair');
    }
}
