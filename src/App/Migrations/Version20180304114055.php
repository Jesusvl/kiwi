<?php declare(strict_types = 1);

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180304114055.
 */
class Version20180304114055 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _user.store_id IS \'(DC2Type:store_id)\'');
        $this->addSql('COMMENT ON COLUMN _user.merchant_id IS \'(DC2Type:merchant_id)\'');
        $this->addSql('COMMENT ON COLUMN _merchant.id IS \'(DC2Type:merchant_id)\'');
        $this->addSql('COMMENT ON COLUMN _merchant.name IS \'(DC2Type:merchant_name)\'');
        $this->addSql('COMMENT ON COLUMN _client.merchant_id IS \'(DC2Type:merchant_id)\'');
        $this->addSql('COMMENT ON COLUMN _store.merchant_id IS \'(DC2Type:merchant_id)\'');
        $this->addSql('COMMENT ON COLUMN _store_timespan.store_id IS \'(DC2Type:store_id)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql('COMMENT ON COLUMN _user.store_id IS NULL');
        $this->addSql('COMMENT ON COLUMN _user.merchant_id IS NULL');
        $this->addSql('COMMENT ON COLUMN _merchant.id IS NULL');
        $this->addSql('COMMENT ON COLUMN _merchant.name IS NULL');
        $this->addSql('COMMENT ON COLUMN _client.merchant_id IS NULL');
        $this->addSql('COMMENT ON COLUMN _store.merchant_id IS NULL');
        $this->addSql('COMMENT ON COLUMN _store_timespan.store_id IS NULL');
    }
}
