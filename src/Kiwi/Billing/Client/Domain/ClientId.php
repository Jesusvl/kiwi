<?php

declare(strict_types=1);

namespace Kiwi\Billing\Client\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class ClientId.
 */
class ClientId extends Uuid
{
}
