<?php

declare(strict_types=1);

namespace Kiwi\Billing\Client\Domain;

use Kiwi\Billing\Merchant\Domain\MerchantId;

/**
 * Class Client.
 */
class Client
{
    /**
     * @var ClientId
     */
    private $id;

    /**
     * @var ClientCode
     */
    private $code;

    /**
     * @var ClientName
     */
    private $fullName;

    /**
     * @var ClientPhone
     */
    private $phone;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * Client constructor.
     *
     * @param ClientId    $id
     * @param ClientCode  $code
     * @param ClientName  $fullName
     * @param ClientPhone $phone
     * @param MerchantId  $merchantId
     */
    public function __construct(
        ClientId $id,
        ClientCode $code,
        ClientName $fullName,
        ClientPhone $phone,
        MerchantId $merchantId
    ) {
        $this->id         = $id;
        $this->code       = $code;
        $this->fullName   = $fullName;
        $this->phone      = $phone;
        $this->merchantId = $merchantId;
    }

    /**
     * @return ClientId
     */
    public function id(): ClientId
    {
        return $this->id;
    }

    /**
     * @return ClientCode
     */
    public function code(): ClientCode
    {
        return $this->code;
    }

    /**
     * @return ClientName
     */
    public function fullName(): ClientName
    {
        return $this->fullName;
    }

    /**
     * @return ClientPhone
     */
    public function phone(): ClientPhone
    {
        return $this->phone;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }
}
