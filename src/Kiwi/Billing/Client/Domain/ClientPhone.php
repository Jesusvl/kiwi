<?php

declare(strict_types=1);

namespace Kiwi\Billing\Client\Domain;

/**
 * Class ClientPhone.
 */
class ClientPhone
{
    /**
     * @var string
     */
    private $phone;

    /**
     * ClientPhone constructor.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->phone = $code;
    }

    /**
     * @return string
     */
    public function phone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->phone();
    }
}
