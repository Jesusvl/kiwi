<?php

declare(strict_types=1);

namespace Kiwi\Billing\Client\Document;

use Kiwi\Billing\Client\Domain\Client;
use Kiwi\Core\Document\Document;

/**
 * Class ClientDocument.
 */
class BaseClientDocument extends Document
{
    /**
     * @var Client|null
     */
    private $client;

    /**
     * BaseClientDocument constructor.
     *
     * @param Client|null $client
     */
    public function __construct(?Client $client)
    {
        parent::__construct($client);
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->client->id()->id();
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->client->code()->code();
    }

    /**
     * @return string
     */
    public function fullName(): string
    {
        return $this->client->fullName()->name();
    }

    /**
     * @return string
     */
    public function phone(): string
    {
        return $this->client->phone()->phone();
    }

    /**
     * @return string
     */
    public function merchantId(): string
    {
        return $this->client->merchantId()->id();
    }

    /**
     * @return array
     */
    public function toScalar(): ?array
    {
        return $this->client ? [
            'id'          => $this->id(),
            'code'        => $this->code(),
            'fullname'    => $this->fullName(),
            'phone'       => $this->phone(),
            'merchant_id' => $this->merchantId(),
        ] : null;
    }
}
