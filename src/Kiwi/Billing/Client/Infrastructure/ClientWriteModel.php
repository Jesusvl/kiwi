<?php

declare(strict_types=1);

namespace Kiwi\Billing\Client\Infrastructure;

use Kiwi\Billing\Client\Domain\Client;
use Kiwi\Billing\Client\Domain\ClientId;

/**
 * Interface ClientWriteModel.
 */
interface ClientWriteModel
{
    /**
     * @param ClientId $id
     *
     * @return Client|null
     */
    public function find(ClientId $id): ?Client;

    /**
     * @param Client $client
     */
    public function save(Client $client): void;

    /**
     * @param ClientId $id
     */
    public function delete(ClientId $id): void;
}
