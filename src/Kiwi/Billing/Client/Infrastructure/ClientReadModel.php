<?php

declare(strict_types=1);

namespace Kiwi\Billing\Client\Infrastructure;

/**
 * Interface ClientReadModel.
 */
interface ClientReadModel
{
}
