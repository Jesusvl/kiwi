<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Billing\Store\Command\CreateStore;
use Kiwi\Billing\Store\Domain\Store;

/**
 * Class CreateStoreHandler.
 */
class CreateStoreHandler extends StoreCommandHandler
{
    /**
     * @param CreateStore $command
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(CreateStore $command): void
    {
        $store = new Store(
            $command->id(),
            $command->name(),
            $command->merchantId(),
            null,
            null
        );

        $this->service()->checkUserCanWriteStore($command->userId(), $store);
        $this->service()->save($store);
    }
}
