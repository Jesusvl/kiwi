<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Billing\Store\Service\StoreService;

/**
 * Class StoreCommandHandler.
 */
abstract class StoreCommandHandler implements CommandHandler
{
    /**
     * @var StoreService
     */
    private $service;

    /**
     * StoreCommandHandler constructor.
     *
     * @param StoreService    $service
     */
    public function __construct(StoreService $service)
    {
        $this->service         = $service;
    }

    /**
     * @return StoreService
     */
    protected function service(): StoreService
    {
        return $this->service;
    }
}
