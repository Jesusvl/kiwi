<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Billing\Store\Document\StoreDocument;
use Kiwi\Billing\Store\Query\FindStore;

/**
 * Class FindStoreHandler.
 */
class FindStoreHandler extends StoreQueryHandler
{
    /**
     * @param FindStore $query
     *
     * @return StoreDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindStore $query): StoreDocument
    {
        $store = $this->readModel()->find($query->id(), $query->parts());
        $this->service()->checkUserCanReadStore($query->userId(), $store);

        return $store;
    }
}
