<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreParts;
use Kiwi\Billing\User\Domain\UserId;

/**
 * Class FindStore.
 */
class FindStore implements Query
{
    /**
     * @var StoreId
     */
    private $id;

    /**
     * @var StoreParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindStore constructor.
     *
     * @param string $userId
     * @param string $id
     * @param array  $parts
     */
    public function __construct(string $userId, string $id, array $parts = [])
    {
        $this->id     = new StoreId($id);
        $this->parts  = new StoreParts($parts);
        $this->userId = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoreId
     */
    public function id(): StoreId
    {
        return $this->id;
    }

    /**
     * @return StoreParts
     */
    public function parts(): StoreParts
    {
        return $this->parts;
    }
}
