<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Document;

use Kiwi\Core\Document\Document;
use Kiwi\Billing\Store\Domain\Timespan;
use KiwiLib\DateTime\DateTime;

/**
 * Class BaseTimespanDocument.
 */
class BaseTimespanDocument extends Document
{
    /**
     * @var Timespan|null
     */
    private $timespan;

    /**
     * BaseTimespanDocument constructor.
     *
     * @param Timespan|null $timespan
     */
    public function __construct(?Timespan $timespan)
    {
        parent::__construct($timespan);
        $this->timespan = $timespan;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->timespan->id()->id();
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->timespan->startTime();
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->timespan->endTime();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->timespan ? [
            'startTime' => $this->startTime()->toAtomString(),
            'endTime'   => $this->endTime()->toAtomString()
        ] : null;
    }
}
