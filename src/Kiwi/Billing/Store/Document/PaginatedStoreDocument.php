<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Document;

use Kiwi\Core\Document\PaginatedDocument;

/**
 * Class PaginatedStoreDocument.
 */
class PaginatedStoreDocument extends PaginatedDocument
{
    /**
     * @return StoreDocument[]
     */
    public function stores(): array
    {
        return $this->list();
    }
}
