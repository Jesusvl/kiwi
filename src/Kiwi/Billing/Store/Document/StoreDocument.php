<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Document;

use Kiwi\Billing\Store\Domain\Store;
use Kiwi\Shop\Location\Document\LocationDocument;

/**
 * Class StoreDocument.
 */
final class StoreDocument extends BaseStoreDocument
{
    /**
     * @var LocationDocument
     */
    private $location;

    /**
     * @var StoreTimespanDocumentBook
     */
    private $timespans;

    /**
     * @return StoreDocument
     */
    public static function empty(): StoreDocument
    {
        return new StoreDocument(null, new LocationDocument(null), new StoreTimespanDocumentBook());
    }

    /**
     * StoreDocument constructor.
     *
     * @param Store|null                $store
     * @param LocationDocument      $location
     * @param StoreTimespanDocumentBook $timespans
     */
    public function __construct(?Store $store, LocationDocument $location, StoreTimespanDocumentBook $timespans)
    {
        parent::__construct($store);
        $this->location  = $location;
        $this->timespans = $timespans;
    }

    /**
     * @return LocationDocument
     */
    public function location(): LocationDocument
    {
        return $this->location;
    }

    /**
     * @return StoreTimespanDocumentBook
     */
    public function timespans(): StoreTimespanDocumentBook
    {
        return $this->timespans;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->mergeDocumentArray(
            parent::toScalar(),
            [
                'location'  => $this->location()->toScalar(),
                'timespans' => $this->timespans->toScalar()
            ]
        );
    }
}
