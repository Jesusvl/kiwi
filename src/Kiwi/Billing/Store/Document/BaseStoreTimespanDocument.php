<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Document;

use KiwiLib\DateTime\DateTime;
use Kiwi\Core\Document\Document;
use Kiwi\Billing\Store\Domain\StoreTimespan;
use Kiwi\Billing\Store\Domain\StoreTimespanId;

/**
 * Class BaseStoreTimespanDocument.
 */
class BaseStoreTimespanDocument extends Document
{
    /**
     * @var BaseTimespanDocument
     */
    private $timespan;

    /**
     * @var StoreTimespan|null
     */
    private $storeTimespan;

    /**
     * BaseStoreTimespanDocument constructor.
     *
     * @param StoreTimespan|null   $storeTimespan
     * @param BaseTimespanDocument $timespan
     */
    public function __construct(?StoreTimespan $storeTimespan, BaseTimespanDocument $timespan)
    {
        parent::__construct($storeTimespan);
        $this->timespan      = $timespan;
        $this->storeTimespan = $storeTimespan;
    }

    /**
     * @return StoreTimespanId
     */
    public function id(): StoreTimespanId
    {
        return $this->storeTimespan->id();
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->storeTimespan->isClosed();
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->timespan->startTime();
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->timespan->endTime();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return [
            'id'        => $this->id()->id(),
            'isClosed'  => $this->isClosed(),
            'startTime' => $this->startTime()->format(DATE_ATOM),
            'endTime'   => $this->endTime()->format(DATE_ATOM)
        ];
    }
}
