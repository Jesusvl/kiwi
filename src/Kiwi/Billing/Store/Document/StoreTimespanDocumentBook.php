<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Document;

use Kiwi\Core\Document\DocumentBook;

/**
 * Class StoreTimespanDocumentBook.
 */
class StoreTimespanDocumentBook extends DocumentBook
{
}
