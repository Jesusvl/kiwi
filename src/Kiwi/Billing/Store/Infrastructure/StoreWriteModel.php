<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Infrastructure;

use Kiwi\Billing\Store\Domain\Store;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreTimespanId;
use Kiwi\Billing\Store\Domain\TimespanId;

/**
 * Interface StoreWriteModel.
 */
interface StoreWriteModel
{
    /**
     * @param StoreId $storeId
     *
     * @return Store|null
     */
    public function find(StoreId $storeId): ?Store;

    /**
     * @param Store $store
     */
    public function save(Store $store): void;

    /**
     * @param Store $store
     */
    public function update(Store $store): void;

    /**
     * @param StoreId $id
     */
    public function delete(StoreId $id): void;
}
