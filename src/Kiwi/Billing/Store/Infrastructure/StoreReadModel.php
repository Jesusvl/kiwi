<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Infrastructure;

use Kiwi\Core\Domain\Pagination;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Store\Document\PaginatedStoreDocument;
use Kiwi\Billing\Store\Document\StoreDocument;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreName;
use Kiwi\Billing\Store\Domain\StoreParts;
use Kiwi\Billing\Store\Domain\TimespanId;

/**
 * Interface StoreReadModel.
 */
interface StoreReadModel
{
    /**
     * @param StoreId         $id
     * @param StoreParts|null $parts
     *
     * @return StoreDocument
     */
    public function find(StoreId $id, ?StoreParts $parts = null): StoreDocument;

    /**
     * @param Pagination      $filter
     * @param StoreParts|null $parts
     *
     * @return PaginatedStoreDocument
     */
    public function findAll(Pagination $filter, ?StoreParts $parts = null): PaginatedStoreDocument;

    /**
     * @param StoreId         $id
     * @param TimespanId      $timespanId
     * @param StoreParts|null $parts
     *
     * @return StoreDocument
     */
    public function findByIdAndTimespan(
        StoreId $id,
        TimespanId $timespanId,
        ?StoreParts $parts = null
    ): StoreDocument;

    /**
     * @param StoreName  $storeName
     * @param MerchantId $merchantId
     * @param StoreParts $parts
     *
     * @return StoreDocument
     */
    public function findByNameAndMerchant(
        StoreName $storeName,
        MerchantId $merchantId,
        StoreParts $parts
    ): StoreDocument;
}
