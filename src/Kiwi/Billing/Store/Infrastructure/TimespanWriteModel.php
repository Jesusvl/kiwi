<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Infrastructure;

use Kiwi\Billing\Store\Domain\Timespan;

/**
 * Interface TimespanWriteModel.
 */
interface TimespanWriteModel
{
    /**
     * @param Timespan $timespan
     */
    public function save(Timespan $timespan): void;
}
