<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Infrastructure;

use Kiwi\Billing\Store\Document\BaseTimespanDocument;
use KiwiLib\DateTime\DateTime;

/**
 * Interface TimespanReadModel.
 */
interface TimespanReadModel
{
    /**
     * @param DateTime $startTime
     * @param DateTime $endTime
     *
     * @return BaseTimespanDocument
     */
    public function find(DateTime $startTime, DateTime $endTime): BaseTimespanDocument;
}
