<?php
declare(strict_types=1);

namespace Kiwi\Billing\Store\Listener;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreName;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Billing\Store\Domain\Store as BillingStore;
use KiwiInfrastructure\Billing\Store\WriteModel\StoreWriteModel;
use KiwiInfrastructure\Shop\Store\ReadModel\StoreReadModel;

/**
 * OnStoreCreatedListener
 *
 * @author Jose Beteta <jose@gmail.com>
 * @copyright 2006-2018 KIWI S.L.
 */
class OnStoreCreatedListener implements AsyncListener
{
    /**
     * @var StoreReadModel
     */
    private $storeReadModel;
    /**
     * @var StoreWriteModel
     */
    private $storeWriteModel;

    /**
     * OnStoreCreatedListener constructor.
     *
     * @param StoreReadModel $storeReadModel
     * @param StoreWriteModel $storeWriteModel
     */
    public function __construct(StoreReadModel $storeReadModel, StoreWriteModel $storeWriteModel)
    {
        $this->storeReadModel = $storeReadModel;
        $this->storeWriteModel = $storeWriteModel;
    }

    /**
     * @param Event $event\OnStoreCreatedEvent
     */
    public function handle(Event $event): void
    {
        /** @var StoreDocument $store */
        $store = $this->storeReadModel->find($event->id());
        $billingStore = new BillingStore(
            new StoreId($store->id()),
            new StoreName($store->name()),
            new MerchantId($store->merchantId()),
            $store->userId() ? new UserId($store->userId()) : null
        );

        $this->storeWriteModel->save($billingStore);
    }

    /**
     * @return string
     */
    public function listensTo(): string
    {
        return 'todo me falta el evento';
    }
}
