<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Service;

use Kiwi\Billing\Store\Document\BaseTimespanDocument;
use Kiwi\Billing\Store\Domain\Timespan;
use Kiwi\Billing\Store\Infrastructure\TimespanReadModel;
use Kiwi\Billing\Store\Infrastructure\TimespanWriteModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class TimespanService.
 */
class TimespanService
{
    /**
     * @var TimespanWriteModel
     */
    private $timespanWriteModel;

    /**
     * @var TimespanReadModel
     */
    private $timespanReadModel;

    /**
     * TimespanService constructor.
     *
     * @param TimespanWriteModel $timespanWriteModel
     * @param TimespanReadModel  $timespanReadModel
     */
    public function __construct(
        TimespanWriteModel $timespanWriteModel,
        TimespanReadModel $timespanReadModel
    ) {
        $this->timespanWriteModel = $timespanWriteModel;
        $this->timespanReadModel  = $timespanReadModel;
    }

    /**
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return BaseTimespanDocument
     */
    public function fetch(DateTime $start, DateTime $end): BaseTimespanDocument
    {
        $timespan = $this->timespanReadModel->find($start, $end);

        if ($timespan->isEmpty()) {
            $timespanDomain = new Timespan($start, $end);
            $this->timespanWriteModel->save($timespanDomain);
            $timespan = new BaseTimespanDocument($timespanDomain);
        }

        return $timespan;
    }
}
