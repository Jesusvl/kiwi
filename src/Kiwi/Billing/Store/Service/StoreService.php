<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Service;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Billing\Store\Document\BaseStoreDocument;
use Kiwi\Billing\Store\Document\BaseTimespanDocument;
use Kiwi\Billing\Store\Domain\Store;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreTimespanId;
use Kiwi\Billing\Store\Domain\TimespanId;
use Kiwi\Billing\Store\Exception\ShopAlreadyHasTimestampException;
use Kiwi\Billing\Store\Exception\StoreNotFoundException;
use Kiwi\Billing\Store\Infrastructure\StoreReadModel;
use Kiwi\Billing\Store\Infrastructure\StoreWriteModel;
use Kiwi\Billing\Store\Query\ObtainTimespan;
use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Billing\User\Infrastructure\UserReadModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class StoreService.
 */
class StoreService
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var StoreWriteModel
     */
    private $storeWriteModel;

    /**
     * @var StoreReadModel
     */
    private $storeReadModel;

    /**
     * @var Bus
     */
    private $bus;

    /**
     * StoreService constructor.
     *
     * @param StoreWriteModel $storeWriteModel
     * @param StoreReadModel  $storeReadModel
     * @param UserReadModel   $userReadModel
     * @param Bus             $bus
     */
    public function __construct(
        StoreWriteModel $storeWriteModel,
        StoreReadModel $storeReadModel,
        UserReadModel $userReadModel,
        Bus $bus
    ) {
        $this->storeWriteModel = $storeWriteModel;
        $this->storeReadModel  = $storeReadModel;
        $this->userReadModel   = $userReadModel;
        $this->bus             = $bus;
    }

    /**
     * @param StoreId $id
     *
     * @return Store
     *
     * @throws StoreNotFoundException
     */
    public function checkDomain(StoreId $id): Store
    {
        $store = $this->storeWriteModel->find($id);

        if ($store) {
            return $store;
        }

        throw new StoreNotFoundException();
    }

    /**
     * @param StoreId $id
     *
     * @return BaseStoreDocument
     *
     * @throws StoreNotFoundException
     */
    public function checkDocument(StoreId $id): BaseStoreDocument
    {
        $store = $this->storeReadModel->find($id);

        if ($store->isNotEmpty()) {
            return $store;
        }

        throw new StoreNotFoundException();
    }

    /**
     * @param Store $store
     */
    public function save(Store $store): void
    {
        $this->storeWriteModel->save($store);
    }

    /**
     * @param Store $store
     */
    public function update(Store $store): void
    {
        $this->storeWriteModel->update($store);
    }

    /**
     * @param Store    $store
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function addTimespan(Store $store, DateTime $startTime, DateTime $endTime): void
    {
        /** @var BaseTimespanDocument $timespan */
        $timespan = $this->bus->dispatchQuery(new ObtainTimespan($startTime, $endTime));

        if ($this->storeReadModel->findByIdAndTimespan(
            $store->id(),
            new TimespanId($timespan->id())
        )->isNotEmpty()) {
            throw new ShopAlreadyHasTimestampException();
        }

        $this->storeWriteModel->addTimestamp($store->id(), new TimespanId($timespan->id()));
    }

    /**
     * @param UserId $userId
     * @param Store  $store
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteStore(UserId $userId, Store $store): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || ($user->isManager() && $user->merchantId() === $store->merchantId()->id())) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId            $userId
     * @param BaseStoreDocument $store
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadStore(UserId $userId, BaseStoreDocument $store): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || ($user->isManager() && $user->merchantId() === $store->merchantId())) {
            return;
        }

        if ($user->isSad() && $store->merchantId() === $user->merchantId()) {
            return;
        }

        if ($user->isShipper() && $store->merchantId() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }

    /**
     * @todo improve
     *
     * @param StoreTimespanId $timespanId
     */
    public function closeTimestamp(StoreTimespanId $timespanId): void
    {
        $this->storeWriteModel->closeTimestamp($timespanId);
    }

    /**
     * @todo improve
     *
     * @param StoreTimespanId $timespanId
     */
    public function openTimestamp(StoreTimespanId $timespanId): void
    {
        $this->storeWriteModel->openTimestamp($timespanId);
    }
}
