<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Domain;

use Kiwi\Core\Domain\Parts;

/**
 * Class StoreParts.
 */
class StoreParts extends Parts
{
    public const LOCATION  = 'location';
    public const TIMESPANS = 'timespans';

    /**
     * StoreParts constructor.
     *
     * @param array $inputFields
     */
    public function __construct($inputFields = [])
    {
        parent::__construct([self::LOCATION, self::TIMESPANS], $inputFields);
    }
}
