<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Domain;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\User\Domain\UserId;

/**
 * Class Store.
 */
class Store
{
    /**
     * @var StoreId
     */
    private $id;

    /**
     * @var StoreName
     */
    private $name;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var UserId|null
     */
    private $userId;

    /**
     * @var bool
     */
    private $isExpressEnabled;

    /**
     * Store constructor.
     *
     * @param StoreId         $id
     * @param StoreName       $name
     * @param MerchantId      $merchantId
     * @param null|UserId     $userId
     */
    public function __construct(
        StoreId $id,
        StoreName $name,
        MerchantId $merchantId,
        ?UserId $userId
    ) {
        $this->id               = $id;
        $this->name             = $name;
        $this->merchantId       = $merchantId;
        $this->userId           = $userId;
        $this->isExpressEnabled = true;
    }

    /**
     * @return StoreId
     */
    public function id(): StoreId
    {
        return $this->id;
    }

    /**
     * @return StoreName
     */
    public function name(): StoreName
    {
        return $this->name;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return UserId|null
     */
    public function userId(): ?UserId
    {
        return $this->userId;
    }

    /**
     * @return bool
     */
    public function isExpressEnabled(): bool
    {
        return $this->isExpressEnabled;
    }

    /**
     * @param StoreName $name
     */
    public function changeName(StoreName $name): void
    {
        $this->name = $name;
    }

    public function disableExpress(): void
    {
        $this->isExpressEnabled = false;
    }

    public function enableExpress(): void
    {
        $this->isExpressEnabled = true;
    }
}
