<?php

declare(strict_types=1);

namespace Kiwi\Billing\Store\Domain;

/**
 * Class StoreName.
 */
class StoreName
{
    /**
     * @var string
     */
    private $name;

    /**
     * StoreName constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }
}
