<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Service;

use Kiwi\Billing\Client\Exception\DuplicatedMerchantIdException;
use Kiwi\Billing\Merchant\Document\BaseMerchantDocument;
use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Merchant\Exception\MerchantNotFoundException;
use Kiwi\Billing\Merchant\Infrastructure\MerchantReadModel;
use Kiwi\Billing\Merchant\Infrastructure\MerchantWriteModel;
use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Billing\User\Infrastructure\UserReadModel;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class MerchantService.
 */
class MerchantService
{
    /**
     * @var MerchantReadModel
     */
    private $merchantReadModel;

    /**
     * @var MerchantWriteModel
     */
    private $merchantWriteModel;

    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * MerchantService constructor.
     *
     * @param MerchantReadModel  $merchantReadModel
     * @param MerchantWriteModel $merchantWriteModel
     * @param UserReadModel      $userReadModel
     */
    public function __construct(
        MerchantReadModel $merchantReadModel,
        MerchantWriteModel $merchantWriteModel,
        UserReadModel $userReadModel
    ) {
        $this->merchantReadModel  = $merchantReadModel;
        $this->merchantWriteModel = $merchantWriteModel;
        $this->userReadModel      = $userReadModel;
    }

    /**
     * @param MerchantId $id
     *
     * @return Merchant
     *
     * @throws MerchantNotFoundException
     */
    public function checkDomain(MerchantId $id): Merchant
    {
        $merchant = $this->merchantWriteModel->find($id);

        if ($merchant) {
            return $merchant;
        }

        throw new MerchantNotFoundException();
    }

    /**
     * @param MerchantId $id
     *
     * @return BaseMerchantDocument
     *
     * @throws MerchantNotFoundException
     */
    public function checkDocument(MerchantId $id): BaseMerchantDocument
    {
        $merchant = $this->merchantReadModel->find($id);

        if ($merchant->isNotEmpty()) {
            return $merchant;
        }

        throw new MerchantNotFoundException();
    }

    /**
     * @param Merchant $merchant
     *
     * @throws DuplicatedMerchantIdException
     */
    public function save(Merchant $merchant): void
    {
        if ($this->merchantReadModel->find($merchant->id())) {
            throw new DuplicatedMerchantIdException();
        }
        $this->merchantWriteModel->save($merchant);
    }

    /**
     * @param Merchant $merchant
     */
    public function update(Merchant $merchant): void
    {
        $this->merchantWriteModel->save($merchant);
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }

    /**
     * @param UserId   $userId
     * @param Merchant $merchant
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteMerchant(UserId $userId, Merchant $merchant): void
    {
        $user = $this->checkUser($userId);

        if ($user->isAdmin()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId               $userId
     * @param BaseMerchantDocument $merchant
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadMerchant(UserId $userId, BaseMerchantDocument $merchant): void
    {
        $user = $this->checkUser($userId);
        if ($user->isAdmin() || ($user->isManager() && $user->merchantId() === $merchant->id())) {
            return;
        }

        if ($user->isSad() && $merchant->id() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }
}
