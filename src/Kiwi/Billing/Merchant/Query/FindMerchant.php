<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Query;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Core\Infrastructure\Query;

/**
 * Class FindMerchant.
 */
class FindMerchant implements Query
{
    /**
     * @var MerchantId
     */
    private $id;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindMerchant constructor.
     *
     * @param string $userId
     * @param string $id
     */
    public function __construct(string $userId, string $id)
    {
        $this->id     = new MerchantId($id);
        $this->userId = new UserId($userId);
    }

    /**
     * @return MerchantId
     */
    public function id(): MerchantId
    {
        return $this->id;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
