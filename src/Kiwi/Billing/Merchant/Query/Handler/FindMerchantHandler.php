<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Query\Handler;

use Kiwi\Billing\Merchant\Document\BaseMerchantDocument;
use Kiwi\Billing\Merchant\Query\FindMerchant;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class FindMerchantHandler.
 */
class FindMerchantHandler extends MerchantQueryHandler
{
    /**
     * @param FindMerchant $query
     *
     * @return BaseMerchantDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindMerchant $query): BaseMerchantDocument
    {
        $merchant = $this->readModel()->find($query->id());
        $this->service()->checkUserCanReadMerchant($query->userId(), $merchant);

        return $merchant;
    }
}
