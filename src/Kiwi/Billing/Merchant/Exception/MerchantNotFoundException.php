<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Exception;

use Kiwi\Core\Exception\DataNotFoundDomainException;

/**
 * Class MerchantNotFoundException.
 */
class MerchantNotFoundException extends DataNotFoundDomainException
{
    public function __construct()
    {
        parent::__construct('Merchant not found.');
    }
}
