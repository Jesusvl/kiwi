<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Infrastructure;

use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Merchant\Domain\MerchantName;

/**
 * Interface MerchantWriteModel.
 */
interface MerchantWriteModel
{
    /**
     * @param MerchantId $id
     *
     * @return Merchant|null
     */
    public function find(MerchantId $id): ?Merchant;

    /**
     * @param Merchant $merchant
     */
    public function save(Merchant $merchant): void;

    /**
     * @param MerchantId   $id
     * @param MerchantName $name
     */
    public function update(MerchantId $id, MerchantName $name): void;
}
