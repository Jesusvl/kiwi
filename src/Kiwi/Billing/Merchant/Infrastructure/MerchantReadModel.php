<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Infrastructure;

use Kiwi\Billing\Merchant\Document\BaseMerchantDocument;
use Kiwi\Billing\Merchant\Document\PaginatedMerchantDocument;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Merchant\Domain\MerchantName;
use Kiwi\Core\Domain\Pagination;

/**
 * Interface MerchantReadModel.
 */
interface MerchantReadModel
{
    /**
     * @param MerchantId $id
     *
     * @return BaseMerchantDocument
     */
    public function find(MerchantId $id): BaseMerchantDocument;

    /**
     * @param MerchantName $name
     *
     * @return BaseMerchantDocument|null
     */
    public function findByName(MerchantName $name): ?BaseMerchantDocument;

    /**
     * @param Pagination $filter
     *
     * @return PaginatedMerchantDocument
     */
    public function findAll(Pagination $filter): PaginatedMerchantDocument;
}
