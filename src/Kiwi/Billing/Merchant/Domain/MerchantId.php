<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class MerchantId.
 */
class MerchantId extends Uuid
{
}
