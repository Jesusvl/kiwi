<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Domain;

/**
 * Class Merchant.
 */
class Merchant
{
    /**
     * @var MerchantId
     */
    private $id;

    /**
     * @var MerchantName
     */
    private $name;

    /**
     * Merchant constructor.
     *
     * @param MerchantId   $id
     * @param MerchantName $name
     */
    public function __construct(MerchantId $id, MerchantName $name)
    {
        $this->id   = $id;
        $this->name = $name;
    }

    /**
     * @return MerchantId
     */
    public function id(): MerchantId
    {
        return $this->id;
    }

    /**
     * @return MerchantName
     */
    public function name(): MerchantName
    {
        return $this->name;
    }

    /**
     * @param MerchantName $name
     */
    public function rename(MerchantName $name): void
    {
        $this->name = $name;
    }
}
