<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Document;

use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Core\Document\Document;

/**
 * Class BaseMerchantDocument.
 */
class BaseMerchantDocument extends Document
{
    /**
     * @var Merchant|null
     */
    private $merchant;

    /**
     * BaseMerchantDocument constructor.
     *
     * @param Merchant|null $merchant
     */
    public function __construct(?Merchant $merchant)
    {
        parent::__construct($merchant);
        $this->merchant = $merchant;
    }

    /**
     * @return BaseMerchantDocument
     */
    public static function empty(): self
    {
        return new self(null);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->merchant->id()->id();
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->merchant->name()->name();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->merchant
            ? [
                'id'   => $this->id(),
                'name' => $this->name()
            ] : null;
    }
}
