<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Document;

use Kiwi\Core\Document\PaginatedDocument;

/**
 * Class PaginatedMerchantDocument.
 */
class PaginatedMerchantDocument extends PaginatedDocument
{
    /**
     * @return BaseMerchantDocument[]
     */
    public function merchants(): array
    {
        return $this->list();
    }
}
