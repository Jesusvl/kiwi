<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Command;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Merchant\Domain\MerchantName;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Core\Infrastructure\Command;

/**
 * Class CreateMerchant.
 */
class CreateMerchant implements Command
{
    /**
     * @var MerchantId
     */
    private $id;

    /**
     * @var MerchantName
     */
    private $name;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * Merchant constructor.
     *
     * @param string $userId
     * @param string $id
     * @param string $name
     */
    public function __construct(string $userId, string $id, string $name)
    {
        $this->id     = new MerchantId($id);
        $this->name   = new MerchantName($name);
        $this->userId = new UserId($userId);
    }

    /**
     * @return MerchantId
     */
    public function id(): MerchantId
    {
        return $this->id;
    }

    /**
     * @return MerchantName
     */
    public function name(): MerchantName
    {
        return $this->name;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
