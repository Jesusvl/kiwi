<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Command\Handler;

use Kiwi\Billing\Client\Exception\DuplicatedMerchantIdException;
use Kiwi\Billing\Merchant\Command\CreateMerchant;
use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class CreateMerchantHandler.
 */
class CreateMerchantHandler extends MerchantCommandHandler
{
    /**
     * @param CreateMerchant $command
     *
     * @throws DuplicatedMerchantIdException
     * @throws InsufficientPermissionsException
     */
    public function __invoke(CreateMerchant $command): void
    {
        // TODO this does not check for duplicated merchants

        $merchant = new Merchant($command->id(), $command->name());
        $this->service()->checkUserCanWriteMerchant($command->userId(), $merchant);
        $this->service()->save($merchant);
    }
}
