<?php

declare(strict_types=1);

namespace Kiwi\Billing\Merchant\Command\Handler;

use Kiwi\Billing\Merchant\Service\MerchantService;
use Kiwi\Core\Infrastructure\CommandHandler;

/**
 * Class MerchantCommandHandler.
 */
abstract class MerchantCommandHandler implements CommandHandler
{
    /**
     * @var MerchantService
     */
    private $service;

    /**
     * MerchantCommandHandler constructor.
     *
     * @param MerchantService $service
     */
    public function __construct(MerchantService $service)
    {
        $this->service = $service;
    }

    /**
     * @return MerchantService
     */
    protected function service(): MerchantService
    {
        return $this->service;
    }
}
