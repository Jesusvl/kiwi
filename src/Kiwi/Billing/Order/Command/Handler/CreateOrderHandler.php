<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;

/**
 * Class CreateOrderHandler.
 */
class CreateOrderHandler implements CommandHandler
{
    //    /**
    //     * @var OrderWriteModel
    //     */
    //    private $repository;
    //
    //    /**
    //     * @var EventManager
    //     */
    //    private $eventManager;
    //
    //    /**
    //     * @var ClientReadModel
    //     */
    //    private $clientReadModel;
    //
    //    /**
    //     * @var LocationReadModel
    //     */
    //    private $locationReadModel;
    //
    //    /**
    //     * @var DestinationWriteModel
    //     */
    //    private $destinationWriteModel;
    //
    //    /**
    //     * CreateOrderHandler constructor.
    //     *
    //     * @param OrderWriteModel       $repository
    //     * @param ClientReadModel       $clientReadModel
    //     * @param LocationReadModel     $locationReadModel
    //     * @param DestinationWriteModel $destinationWriteModel
    //     * @param EventManager          $eventManager
    //     */
    //    public function __construct(
    //        OrderWriteModel $repository,
    //        ClientReadModel $clientReadModel,
    //        LocationReadModel $locationReadModel,
    //        DestinationWriteModel $destinationWriteModel,
    //        EventManager $eventManager
    //    ) {
    //        $this->repository            = $repository;
    //        $this->eventManager          = $eventManager;
    //        $this->clientReadModel       = $clientReadModel;
    //        $this->locationReadModel     = $locationReadModel;
    //        $this->destinationWriteModel = $destinationWriteModel;
    //    }
    //
    //    /**
    //     * @param CreateOrder $command
    //     */
    //    public function __invoke(CreateOrder $command): void
    //    {
    //        $client   = $this->clientReadModel->find($command->clientId());
    //        $location = $this->locationReadModel->find($command->locationId());
    //
    //        $address = "{$location->name()}, {$location->stairBuilding()} {$location->floor()} {$location->door()}, ";
    //        $address .= "{$location->postalCode()} {$location->city()} ({$location->province()})";
    //
    //        $destination = new Destination(
    //            new DestinationId(),
    //            new DestinationAddress($address),
    //            new DestinationAnnotation($location->annotation()),
    //            new DestinationLatitude($location->latitude()),
    //            new DestinationLongitude($location->longitude()),
    //            $location->hasElevator(),
    //            $location->hasStepElevator(),
    //            new LocationId($location->id())
    //        );
    //        $this->destinationWriteModel->save($destination);
    //
    //        $this->repository->save(new Order(
    //            $command->id(),
    //            $command->clientId(),
    //            new OrderClientName($client->fullName()),
    //            new OrderClientPhone($client->phone()),
    //            OrderStatus::delivered(),
    //            $command->number(),
    //            $command->cooled(),
    //            $command->frozen(),
    //            $command->storeId(),
    //            $command->startTime(),
    //            $command->endTime(),
    //            $command->isExpress(),
    //            $destination->id(),
    //            $command->boxes()
    //        ));
    //
    //        $this->eventManager->dispatch(new OnCreateOrderEvent($command->id()));
    //    }
}
