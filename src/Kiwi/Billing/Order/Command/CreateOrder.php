<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Command;

use Kiwi\Billing\Order\Domain\ClientId;
use Kiwi\Billing\Order\Domain\LocationId;
use Kiwi\Billing\Order\Domain\OrderBoxesQuantity;
use Kiwi\Billing\Order\Domain\OrderFrozenQuantity;
use Kiwi\Billing\Order\Domain\OrderId;
use Kiwi\Billing\Order\Domain\OrderNumber;
use Kiwi\Billing\Order\Domain\OrderRefrigeratedQuantity;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Core\Infrastructure\Command;
use KiwiLib\DateTime\DateTime;

/**
 * Class CreateOrder.
 */
class CreateOrder implements Command
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var OrderNumber
     */
    private $number;

    /**
     * @var OrderRefrigeratedQuantity
     */
    private $cooled;

    /**
     * @var OrderFrozenQuantity
     */
    private $frozen;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * @var bool
     */
    private $isExpress;

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * @var OrderBoxesQuantity
     */
    private $boxes;

    /**
     * @var ClientId
     */
    private $clientId;

    /**
     * CreateOrder constructor.
     *
     * @param string   $id
     * @param string   $clientId
     * @param string   $number
     * @param int      $cooled
     * @param int      $frozen
     * @param string   $storeId
     * @param DateTime $startTime
     * @param DateTime $endTime
     * @param bool     $isExpress
     * @param string   $locationId
     * @param int      $boxes
     */
    public function __construct(
        string $id,
        string $clientId,
        string $number,
        int $cooled,
        int $frozen,
        string $storeId,
        DateTime $startTime,
        DateTime $endTime,
        bool $isExpress,
        string $locationId,
        int $boxes
    ) {
        $this->id         = new OrderId($id);
        $this->number     = new OrderNumber($number);
        $this->cooled     = new OrderRefrigeratedQuantity($cooled);
        $this->frozen     = new OrderFrozenQuantity($frozen);
        $this->storeId    = new StoreId($storeId);
        $this->startTime  = $startTime;
        $this->endTime    = $endTime;
        $this->isExpress  = $isExpress;
        $this->locationId = new LocationId($locationId);
        $this->boxes      = new OrderBoxesQuantity($boxes);
        $this->clientId   = new ClientId($clientId);
    }

    /**
     * @return OrderId
     */
    public function id(): OrderId
    {
        return $this->id;
    }

    /**
     * @return OrderNumber
     */
    public function number(): OrderNumber
    {
        return $this->number;
    }

    /**
     * @return OrderRefrigeratedQuantity
     */
    public function cooled(): OrderRefrigeratedQuantity
    {
        return $this->cooled;
    }

    /**
     * @return OrderFrozenQuantity
     */
    public function frozen(): OrderFrozenQuantity
    {
        return $this->frozen;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return bool
     */
    public function isExpress(): bool
    {
        return $this->isExpress;
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }

    /**
     * @return OrderBoxesQuantity
     */
    public function boxes(): OrderBoxesQuantity
    {
        return $this->boxes;
    }

    /**
     * @return ClientId
     */
    public function clientId(): ClientId
    {
        return $this->clientId;
    }
}
