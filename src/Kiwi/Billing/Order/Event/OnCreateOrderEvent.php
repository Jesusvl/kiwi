<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Event;

use Kiwi\Billing\Order\Domain\OrderId;
use Kiwi\Core\Infrastructure\Event;

/**
 * Class OnCreateOrderEvent.
 */
class OnCreateOrderEvent implements Event
{
    public const NAME = 'OnCreateOrder';

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * OnCreateOrderEvent constructor.
     *
     * @param OrderId $orderId
     */
    public function __construct(OrderId $orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return OrderId
     */
    public function orderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public static function name(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return \serialize([
            $this->orderId->id()
        ]);
    }

    /**
     * @param string $message
     *
     * @return OnCreateOrderEvent
     */
    public static function deserialize(string $message): self
    {
        system('echo pene >> /Users/jose/projects/kiwi/out.txt');
        [
            $orderId
        ] = unserialize($message, ['allowed_classes' => false]);

        return new self(new OrderId($orderId));
    }
}
