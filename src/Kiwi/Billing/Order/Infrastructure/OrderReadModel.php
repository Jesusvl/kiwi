<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Infrastructure;

use Kiwi\Billing\Order\Document\BaseOrderDocument;
use Kiwi\Billing\Order\Document\OrderDocumentCollection;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Order\Domain\OrderId;
use Kiwi\Billing\Order\Domain\OrderParts;
use Kiwi\Billing\Store\Domain\StoreId;

/**
 * Interface OrderReadModel.
 */
interface OrderReadModel
{
    /**
     * @param OrderId    $id
     * @param OrderParts $parts
     *
     * @return BaseOrderDocument
     */
    public function find(OrderId $id, ?OrderParts $parts = null): BaseOrderDocument;

    /**
     * @param $start
     * @param $end
     * @param $merchantId
     * @param $storeId
     *
     * @return OrderDocumentCollection
     */
    public function findOrdersByTimeAndStoreAndMerchant(
        \DateTime $start,
        \DateTime $end,
        MerchantId $merchantId,
        StoreId $storeId
    ) : OrderDocumentCollection;
}
