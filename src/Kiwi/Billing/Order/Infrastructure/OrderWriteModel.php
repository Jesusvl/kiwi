<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Infrastructure;

use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Billing\Order\Domain\OrderId;

/**
 * Interface OrderWriteModel.
 */
interface OrderWriteModel
{
    /**
     * @param OrderId $id
     *
     * @return Order|null
     */
    public function find(OrderId $id): ?Order;

    /**
     * @param Order $order
     */
    public function save(Order $order): void;

    /**
     * @param Order $order
     */
    public function update(Order $order): void;
}
