<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Query;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Core\Infrastructure\Query;

/**
 * GetOrderDaysAverage
 *
 * @author    Jose Beteta <jose@gmail.com>
 * @copyright 2006-2018 Kiwi S.L.
 */
class GetOrderDaysAverage implements Query
{
    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * FindOrdersByTimeAndStoreAndMerchant constructor.
     *
     * @param \DateTime $start
     * @param \DateTime $end
     * @param string    $storeId
     * @param string    $merchantId
     */
    public function __construct(\DateTime $start, \DateTime $end, string $storeId, string $merchantId)
    {
        $this->start      = $start;
        $this->end        = $end;
        $this->storeId    = new StoreId($storeId);
        $this->merchantId = new MerchantId($merchantId);
    }

    /**
     * @return \DateTime
     */
    public function start(): \DateTime
    {
        return $this->start;
    }

    /**
     * @return \DateTime
     */
    public function end(): \DateTime
    {
        return $this->end;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }
}
