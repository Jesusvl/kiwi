<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Query\Handler;

use Kiwi\Billing\Order\Document\BaseOrderDocument;
use Kiwi\Billing\Order\Document\OrderByDaysDocument;
use Kiwi\Billing\Order\Document\OrderDocumentCollection;
use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Billing\Order\Domain\Week\Day;
use Kiwi\Billing\Order\Domain\Week\Timespan;
use Kiwi\Billing\Order\Domain\Week\TimeWindow;
use Kiwi\Billing\Order\Domain\Week\TimeWindowCollection;
use Kiwi\Billing\Order\Domain\Week\Week;
use Kiwi\Billing\Order\Exception\InvalidWeekDayException;
use Kiwi\Billing\Order\Query\FindOrdersByTimeAndStoreAndMerchant;
use Kiwi\Billing\Order\Query\GetOrderDaysAverage;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Core\Infrastructure\CommandHandler;

/**
 * Class GetOrderDaysAverageHandler.
 */
class GetOrderDaysAverageHandler implements CommandHandler
{
    /**
     * @var Bus
     */
    private $bus;

    /**
     * FindStoreHandler constructor.
     *
     * @param Bus $bus
     */
    public function __construct(Bus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param GetOrderDaysAverage $query
     *
     * @return OrderByDaysDocument
     * @throws InvalidWeekDayException
     */
    public function __invoke(GetOrderDaysAverage $query): OrderByDaysDocument
    {
        /** @var OrderDocumentCollection $orderDocumentCollection */
        $orderDocumentCollection = $this->bus->dispatchQuery(new FindOrdersByTimeAndStoreAndMerchant(
            $query->start(),
            $query->end(),
            $query->storeId()->id(),
            $query->merchantId()->id()
        ));

        $week = $this->getWeek($orderDocumentCollection);

        return new OrderByDaysDocument($week);
    }

    /**
     * @param OrderDocumentCollection $orderDocumentCollection
     *
     * @return Week
     * @throws InvalidWeekDayException
     */
    private function getWeek(OrderDocumentCollection $orderDocumentCollection): Week
    {
        $timeWindowsPerday = $this->getTimeWindowsPerDay($orderDocumentCollection);
        $days              = $this->getDays($timeWindowsPerday);

        return new Week($days);
    }

    /**
     * @param OrderDocumentCollection $orderDocumentCollection
     *
     * @return array
     */
    private function getTimeWindowsPerDay(OrderDocumentCollection $orderDocumentCollection): array
    {
        /*$timeWindowsPerDay = [];
        foreach (Week::ALL as $dayNumber) {
            $orders = 0;
            /** @var BaseOrderDocument $orderDocument *
            foreach ($orderDocumentCollection as $orderDocument) {
                if ($orderDocument->endTime()->dayOfWeek === $dayNumber) {
                    if (isset($timeWindowsPerDay[$dayNumber][$orderDocument->endTime()->format(DATE_ATOM)])) {
                        $timeWindowsPerDay[$dayNumber][$orderDocument->endTime()->format(DATE_ATOM)]
                            ->increaseOrderCount();
                    } else {
                        $orders++;
                        $timeWindowsPerDay[$dayNumber][$orderDocument->endTime()->format(DATE_ATOM)] =
                            new TimeWindow(
                                $orders,
                                new Timespan($orderDocument->startTime(), $orderDocument->endTime())
                            );
                    }
                }
            }*
        }

        return $timeWindowsPerDay;*/

        return [];
    }

    /**
     * @param array $timeWindowsPerDay
     *
     * @return array
     */
    private function getDays(array $timeWindowsPerDay): array
    {
        /*
        $days = [];
        foreach ($timeWindowsPerDay as $key => $day) {
            $timeWindows = [];
            foreach ($day as $timeWindow) {
                $timeWindows[] = $timeWindow;
            }
            $days[] = new Day($key, new TimeWindowCollection($timeWindows));
        }

        return $days;*/
        return [];
    }
}
