<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Query\Handler;

use Kiwi\Billing\Order\Document\OrderDocumentCollection;
use Kiwi\Billing\Order\Infrastructure\OrderReadModel;
use Kiwi\Billing\Order\Query\FindOrdersByTimeAndStoreAndMerchant;
use Kiwi\Core\Infrastructure\CommandHandler;

/**
 * Class FindOrdersByTimeAndStoreAndMerchantHandler.
 */
class FindOrdersByTimeAndStoreAndMerchantHandler implements CommandHandler
{
    /**
     * @var OrderReadModel
     */
    private $readModel;

    /**
     * FindStoreHandler constructor.
     *
     * @param OrderReadModel $readModel
     */
    public function __construct(OrderReadModel $readModel)
    {
        $this->readModel = $readModel;
    }

    /**
     * @param FindOrdersByTimeAndStoreAndMerchant $query
     *
     * @return OrderDocumentCollection
     */
    public function __invoke(FindOrdersByTimeAndStoreAndMerchant $query): OrderDocumentCollection
    {
        return $this->readModel->findOrdersByTimeAndStoreAndMerchant(
            $query->start(),
            $query->end(),
            $query->merchantId(),
            $query->storeId()
        );
    }
}
