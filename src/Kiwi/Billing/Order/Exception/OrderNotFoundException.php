<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class OrderNotFoundException.
 */
class OrderNotFoundException extends DomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Order not found.');
    }
}
