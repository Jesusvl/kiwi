<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class InvalidItemClassException.
 */
class InvalidItemClassException extends DomainException
{
    /**
     * InvalidItemClassException constructor.
     *
     * @param string $class
     */
    public function __construct(string $class)
    {
        parent::__construct("Invalid item class {$class} on array");
    }
}
