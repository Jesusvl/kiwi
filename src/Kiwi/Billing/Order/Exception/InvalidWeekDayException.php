<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class InvalidWeekDayException
 */
class InvalidWeekDayException extends DomainException
{
    /**
     * InvalidWeekDayException constructor.
     *
     * @param int             $day
     * @param null|\Throwable $throwable
     */
    public function __construct(int $day, ?\Throwable $throwable = null)
    {
        parent::__construct("WeekDay out of range ({$day})", $throwable);
    }
}
