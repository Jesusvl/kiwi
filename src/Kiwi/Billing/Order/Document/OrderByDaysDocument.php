<?php
declare(strict_types=1);

namespace Kiwi\Billing\Order\Document;

use Kiwi\Billing\Order\Domain\Week\Day;
use Kiwi\Billing\Order\Domain\Week\Week;
use Kiwi\Core\Document\Document;

/**
 * Class OrderByDaysDocument.
 */
class OrderByDaysDocument extends Document
{
    /**
     * @var Week
     */
    private $week;

    /**
     * OrderByDaysDocument constructor.
     *
     * @param Week $week
     */
    public function __construct(Week $week)
    {
        // TODO I don't understand thid class
        parent::__construct($week);
        $this->week = $week;
    }

    /**
     * @return array
     */
    public function mondayAverage(): array
    {
        return $this->getDayAverage(Week::MONDAY);
    }

    /**
     * @return array
     */
    public function tuesdayAverage(): array
    {
        return $this->getDayAverage(Week::TUESDAY);
    }

    /**
     * @return array
     */
    public function wednesdayAverage(): array
    {

        return $this->getDayAverage(Week::WEDNESDAY);
    }

    /**
     * @return array
     */
    public function thursdayAverage(): array
    {
        return $this->getDayAverage(Week::THURSDAY);
    }

    /**
     * @return array
     */
    public function fridayAverage(): array
    {
        return $this->getDayAverage(Week::FRIDAY);
    }

    /**
     * @return array
     */
    public function saturdayAverage(): array
    {
        return $this->getDayAverage(Week::SATURDAY);
    }

    /**
     * @return array
     */
    public function sundayAverage(): array
    {
        return $this->getDayAverage(Week::SUNDAY);
    }

    /**
     * @param int $dayOfWeek
     *
     * @return array
     */
    private function getDayAverage(int $dayOfWeek): array
    {
        $totalOrders              = 0;
        $ordersByDay              = 0;
        $dayTimeWindowsPercentage = [];
        /** @var Day $day */
        foreach ($this->week as $day) {
            if ($day->dayNumber() === $dayOfWeek) {
                $ordersByDay              = $day->getTotalOrdersCount();
                $dayTimeWindowsPercentage = $day->getTimeWindowsPercentage();
            }
            $totalOrders += $day->getTotalOrdersCount();
        }

        return [
            'day_percentage'              => $totalOrders === 0 ? 0 : $ordersByDay * 100 / $totalOrders,
            'day_time_windows_percentage' => $dayTimeWindowsPercentage
        ];
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return [
            'monday'    => $this->mondayAverage(),
            'tuesday'   => $this->tuesdayAverage(),
            'wednesday' => $this->wednesdayAverage(),
            'thursday'  => $this->thursdayAverage(),
            'friday'    => $this->fridayAverage(),
            'saturday'  => $this->saturdayAverage(),
            'sunday'    => $this->sundayAverage()
        ];
    }
}
