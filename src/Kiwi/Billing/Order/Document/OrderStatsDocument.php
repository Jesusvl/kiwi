<?php
declare(strict_types=1);

namespace Kiwi\Billing\Order\Document;

use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Billing\Order\Domain\OrderStatus;
use Kiwi\Core\Document\Document;

/**
 * Class OrderStatsDocument.
 */
class OrderStatsDocument extends Document
{
    private const BOXES_EXTRA = 6;

    /**
     * @var OrderDocumentCollection
     */
    private $orders;

    /**
     * AdditionalOrderStats constructor.
     *
     * @param array $orders
     */
    public function __construct(array $orders)
    {
        parent::__construct($orders);

        $this->orders = $orders;
    }

    /**
     * @return int
     */
    public function totalDeliveries(): int
    {
        $count = 0;
        /** @var Order $order */
        foreach ($this->orders as $order) {
            if ($order->status()->name() === OrderStatus::DELIVERED) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return int
     */
    public function totalDeliveriesExpress(): int
    {
        $count = 0;
        /** @var Order $order */
        foreach ($this->orders as $order) {
            if ($order->isExpress()) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return int
     */
    public function totalDeliveriesWithExtraBoxes(): int
    {
        $count = 0;
        /** @var Order $order */
        foreach ($this->orders as $order) {
            if ($order->boxes()->quantity() >= self::BOXES_EXTRA) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return [
            'total_deliveries'                  => $this->totalDeliveries(),
            'total_deliveries_express'          => $this->totalDeliveriesExpress(),
            'total_deliveries_with_extra_boxes' => $this->totalDeliveriesWithExtraBoxes()
        ];
    }
}
