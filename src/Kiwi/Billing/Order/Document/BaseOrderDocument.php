<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Document;

use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Core\Document\Document;
use KiwiLib\DateTime\DateTime;

/**
 * Class BaseOrderDocument.
 */
class BaseOrderDocument extends Document
{
    /**
     * @var Order|null
     */
    protected $order;

    /**
     * BaseOrderDocument constructor.
     *
     * @param Order|null $order
     */
    public function __construct(?Order $order)
    {
        parent::__construct($order);
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->order->id()->id();
    }

    /**
     * @return string
     */
    public function status(): string
    {
        return $this->order->status()->name();
    }

    /**
     * @return string
     */
    public function number(): string
    {
        return $this->order->number()->code();
    }

    /**
     * @return int
     */
    public function cooled(): int
    {
        return $this->order->cooled()->quantity();
    }

    /**
     * @return string
     */
    public function storeId(): string
    {
        return $this->order->storeId()->id();
    }

    /**
     * @return int
     */
    public function frozen(): int
    {
        return $this->order->frozen()->quantity();
    }

    /**
     * @return bool
     */
    public function isExpress(): bool
    {
        return $this->order->isExpress();
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->order->startTime();
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->order->endTime();
    }

    /**
     * @return int
     */
    public function boxes(): int
    {
        return $this->order->boxes()->quantity();
    }

    /**
     * @return DateTime
     */
    public function createdAt(): DateTime
    {
        return $this->order->createdAt();
    }

    /**
     * @return DateTime
     */
    public function insideAt(): ?DateTime
    {
        return $this->order->insideAt();
    }

    /**
     * @return DateTime
     */
    public function ongoingAt(): ?DateTime
    {
        return $this->order->ongoingAt();
    }

    /**
     * @return string
     */
    public function clientName(): string
    {
        return $this->order->clientName()->name();
    }

    /**
     * @return string
     */
    public function clientPhone(): string
    {
        return $this->order->clientPhone()->phone();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->order ? [
            'id'          => $this->id(),
            'status'      => $this->status(),
            'number'      => $this->number(),
            'cooled'      => $this->cooled(),
            'frozen'      => $this->frozen(),
            'isExpress'   => $this->isExpress(),
            'startTime'   => $this->startTime()->toAtomString(),
            'endTime'     => $this->endTime()->toAtomString(),
            'boxes'       => $this->boxes(),
            'createdAt'   => $this->createdAt()->toAtomString(),
            'insideAt'    => $this->insideAt() ? $this->insideAt()->toAtomString() : null,
            'ongoingAt'   => $this->ongoingAt() ? $this->ongoingAt()->toAtomString() : null,
            'clientName'  => $this->clientName(),
            'clientPhone' => $this->clientPhone()
        ] : null;
    }
}
