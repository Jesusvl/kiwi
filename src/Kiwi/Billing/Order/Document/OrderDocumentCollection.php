<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Billing\Order\Exception\InvalidItemClassException;

/**
 * Class PaginatedOrderDocument.
 */
class OrderDocumentCollection extends ArrayCollection
{
    /**
     * @var OrderStatsDocument $orderStats
     */
    private $orderStats;

    /**
     * OrderDocumentCollection constructor.
     *
     * @param array $elements
     *
     * @throws InvalidItemClassException
     */
    public function __construct(array $elements = [])
    {
        // TODO I don't understand thid class
        // TODO Why is there a doctrine class here?
        $orderDocuments = [];
        foreach ($elements as $order) {
            if (!$order instanceof Order) {
                throw new InvalidItemClassException(Order::class);
            }
            $orderDocuments [] = new BaseOrderDocument($order);
        }

        $this->orderStats = new OrderStatsDocument($elements);

        parent::__construct($orderDocuments);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = [];
        /** @var BaseOrderDocument $item */
        foreach ($this->getIterator() as $item) {
            $array['orders'][] = $item->toScalar();
        }

        $array['stats'] = $this->orderStats->toScalar();

        return $array;
    }
}
