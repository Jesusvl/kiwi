<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Service;

use Kiwi\Billing\Order\Document\BaseOrderDocument;
use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Billing\Order\Domain\OrderId;
use Kiwi\Billing\Order\Exception\OrderNotFoundException;
use Kiwi\Billing\Order\Infrastructure\OrderReadModel;
use Kiwi\Billing\Order\Infrastructure\OrderWriteModel;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Exception\StoreNotFoundException;
use Kiwi\Billing\Store\Infrastructure\StoreReadModel;
use Kiwi\Billing\Store\Service\StoreService;
use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Billing\User\Infrastructure\UserReadModel;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Core\Infrastructure\Bus;

/**
 * Class OrderService.
 */
class OrderService
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var OrderWriteModel
     */
    private $orderWriteModel;

    /**
     * @var OrderReadModel
     */
    private $orderReadModel;

    /**
     * @var StoreService
     */
    private $storeService;

    /**
     * OrderService constructor.
     *
     * @param StoreService $storeService
     * @param OrderWriteModel $orderWriteModel
     * @param OrderReadModel $orderReadModel
     * @param UserReadModel $userReadModel
     * @param StoreReadModel $storeReadModel
     * @param Bus $bus
     */
    public function __construct(
        StoreService $storeService,
        OrderWriteModel $orderWriteModel,
        OrderReadModel $orderReadModel,
        UserReadModel $userReadModel,
        StoreReadModel $storeReadModel,
        Bus $bus
    ) {
        $this->orderWriteModel       = $orderWriteModel;
        $this->orderReadModel        = $orderReadModel;
        $this->userReadModel         = $userReadModel;
        $this->storeService          = $storeService;
    }

    /**
     * @param OrderId $id
     *
     * @return Order
     *
     * @throws OrderNotFoundException
     */
    public function checkDomain(OrderId $id): Order
    {
        $order = $this->orderWriteModel->find($id);

        if ($order) {
            return $order;
        }

        throw new OrderNotFoundException();
    }

    /**
     * @param OrderId $id
     *
     * @return BaseOrderDocument
     *
     * @throws OrderNotFoundException
     */
    public function checkDocument(OrderId $id): BaseOrderDocument
    {
        $order = $this->orderReadModel->find($id);

        if ($order) {
            return $order;
        }

        throw new OrderNotFoundException();
    }

    /**
     * @param Order       $order
     */
    public function save(Order $order): void
    {
        $this->orderWriteModel->save($order);
    }

    /**
     * @param Order $order
     */
    public function update(Order $order): void
    {
        $this->orderWriteModel->update($order);
    }

    /**
     * @param UserId $userId
     * @param Order  $order
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteOrder(UserId $userId, Order $order): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || ($user->isManager() && $user->storeId() === $order->storeId()->id())) {
            return;
        }

        if ($user->isShipper()) {
            return;
        }

        if ($user->isSad() && $user->storeId() === $order->storeId()->id()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId            $userId
     * @param BaseOrderDocument $order
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     */
    public function checkUserCanReadOrder(UserId $userId, BaseOrderDocument $order): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        $store = $this->storeService->checkDocument(new StoreId($order->storeId()));

        if ($user->isAdmin() || ($user->isManager() && $user->merchantId() === $store->merchantId())) {
            return;
        }

        if ($user->isSad() && $user->storeId() === $order->storeId()) {
            return;
        }

        if ($user->isShipper()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }
}
