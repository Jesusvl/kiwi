<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class OrderId.
 */
class OrderId extends Uuid
{
}
