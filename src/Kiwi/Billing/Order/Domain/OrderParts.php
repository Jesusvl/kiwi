<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain;

use Kiwi\Core\Domain\Parts;

/**
 * Class OrderParts.
 */
class OrderParts extends Parts
{
    public const DESTINATION = 'destination';

    /**
     * OrderParts constructor.
     *
     * @param array $inputFields
     */
    public function __construct($inputFields = [])
    {
        parent::__construct([self::DESTINATION], $inputFields);
    }
}
