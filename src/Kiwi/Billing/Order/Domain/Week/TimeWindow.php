<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain\Week;

/**
 * Class TimeWindow.
 */
class TimeWindow
{
    /**
     * @var int
     */
    private $orderCount;

    /**
     * @var Timespan
     */
    private $timespan;

    /**
     * TimeWindow constructor.
     *
     * @param int      $orderCount
     * @param Timespan $timespan
     */
    public function __construct(int $orderCount, Timespan $timespan)
    {
        $this->orderCount = $orderCount;
        $this->timespan   = $timespan;
    }

    public function increaseOrderCount(): void
    {
        $this->orderCount++;
    }

    /**
     * @return Timespan
     */
    public function timespan(): Timespan
    {
        return $this->timespan;
    }

    /**
     * @return int
     */
    public function orderCount(): int
    {
        return $this->orderCount;
    }
}
