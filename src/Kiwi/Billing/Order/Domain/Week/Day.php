<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain\Week;

use Kiwi\Billing\Order\Exception\InvalidWeekDayException;

/**
 * Class Day.
 */
class Day
{
    public const SUNDAY    = 0;
    public const MONDAY    = 1;
    public const TUESDAY   = 2;
    public const WEDNESDAY = 3;
    public const THURSDAY  = 4;
    public const FRIDAY    = 5;
    public const SATURDAY  = 6;
    public const ALL       = [
        self::SUNDAY,
        self::MONDAY,
        self::TUESDAY,
        self::WEDNESDAY,
        self::THURSDAY,
        self::FRIDAY,
        self::SATURDAY
    ];

    /**
     * @var int
     */
    private $dayNumber;

    /**
     * @var TimeWindowCollection
     */
    private $timeWindowCollection;

    /**
     * Day constructor.
     *
     * @param int                  $dayNumber
     * @param TimeWindowCollection $timeWindowCollection
     *
     * @throws InvalidWeekDayException
     */
    public function __construct(int $dayNumber, TimeWindowCollection $timeWindowCollection)
    {
        if (!\in_array($dayNumber, self::ALL, true)) {
            throw new InvalidWeekDayException($dayNumber);
        }

        $this->dayNumber            = $dayNumber;
        $this->timeWindowCollection = TimeWindowCollection::sortByDate($timeWindowCollection);
    }

    /**
     * @return TimeWindowCollection
     */
    public function timeWindowCollection(): TimeWindowCollection
    {
        return $this->timeWindowCollection;
    }

    /**
     * @return int
     */
    public function getTotalOrdersCount(): int
    {
        $total = 0;
        /** @var TimeWindow $timeWindow */
        foreach ($this->timeWindowCollection as $timeWindow) {
            $total += $timeWindow->orderCount();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function dayNumber(): int
    {
        return $this->dayNumber;
    }

    /**
     * @return array
     */
    public function getTimeWindowsPercentage(): array
    {
        $timeWindows = [];
        /** @var TimeWindow $timeWindow */
        foreach ($this->timeWindowCollection as $timeWindow) {
            $timeWindows [] = [
                'start_time' => $timeWindow->timespan()->startTime()->format(DATE_ATOM),
                'end_time'   => $timeWindow->timespan()->endTime()->format(DATE_ATOM),
                'percentage' => $timeWindow->orderCount() * 100 / $this->getTotalOrdersCount()
            ];
        }

        return $timeWindows;
    }
}
