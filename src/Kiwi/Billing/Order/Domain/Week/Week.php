<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain\Week;

use Doctrine\Common\Collections\ArrayCollection;
use Kiwi\Billing\Order\Exception\InvalidWeekDayException;

/**
 * Class Week.
 */
class Week extends ArrayCollection
{
    public const SUNDAY    = 0;
    public const MONDAY    = 1;
    public const TUESDAY   = 2;
    public const WEDNESDAY = 3;
    public const THURSDAY  = 4;
    public const FRIDAY    = 5;
    public const SATURDAY  = 6;
    public const ALL       = [
        self::SUNDAY,
        self::MONDAY,
        self::TUESDAY,
        self::WEDNESDAY,
        self::THURSDAY,
        self::FRIDAY,
        self::SATURDAY
    ];

    /**
     * WeekDay constructor.
     *
     * @param array $days
     *
     * @throws InvalidWeekDayException
     */
    public function __construct(array $days)
    {
        /** @var Day $day */
        foreach ($days as $day) {
            $dayNumber = $day->dayNumber();
            if ($dayNumber < self::SUNDAY || $dayNumber > self::SATURDAY) {
                throw new InvalidWeekDayException($dayNumber);
            }
        }

        parent::__construct($days);
    }
}
