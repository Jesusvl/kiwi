<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain\Week;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class TimeWindowCollection.
 */
class TimeWindowCollection extends ArrayCollection
{
    /**
     * @param TimeWindowCollection $timeWindowCollection
     * @return TimeWindowCollection
     */
    public static function sortByDate(TimeWindowCollection $timeWindowCollection) : TimeWindowCollection
    {
        $iterator = $timeWindowCollection->getIterator();

        $iterator->uasort(function (TimeWindow $first, TimeWindow $second) {
            return (int) $first->timespan()->startTime()->format('U.u') >
                   (int) $second->timespan()->startTime()->format('U.u') ? 1 : -1;
        });

        $timeWindowsSorted = [];
        /** @var TimeWindow $window */
        foreach ($iterator as $window) {
            $timeWindowsSorted[] = $window;
        }

        return new self($timeWindowsSorted);
    }
}
