<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain\Week;

use KiwiLib\DateTime\DateTime;

/**
 * Class TimeSpan.
 */
class Timespan
{
    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * TimeSpan constructor.
     *
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function __construct(DateTime $startTime, DateTime $endTime)
    {
        $this->startTime = $startTime;
        $this->endTime   = $endTime;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }
}
