<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain;

use Kiwi\Billing\Client\Domain\ClientId;
use Kiwi\Billing\Store\Domain\StoreId;
use KiwiLib\DateTime\DateTime;

/**
 * Class Order.
 */
class Order
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var OrderNumber
     */
    private $number;

    /**
     * @var OrderRefrigeratedQuantity
     */
    private $cooled;

    /**
     * @var OrderFrozenQuantity
     */
    private $frozen;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * @var bool
     */
    private $isExpress;

    /**
     * @var OrderBoxesQuantity
     */
    private $boxes;

    /**
     * @var OrderStatus
     */
    private $status;

    /**
     * @var ClientId
     */
    private $clientId;

    /**
     * @var string|null
     */
    private $shipperId;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var DateTime|null
     */
    private $missingAt;

    /**
     * @var DateTime|null
     */
    private $deliveredAt;

    /**
     * @var DateTime|null
     */
    private $ongoingAt;

    /**
     * @var DateTime|null
     */
    private $insideAt;

    /**
     * @var OrderClientName
     */
    private $clientName;

    /**
     * @var OrderClientPhone
     */
    private $clientPhone;

    /**
     * Order constructor.
     *
     * @param OrderId                   $id
     * @param ClientId                  $clientId
     * @param OrderClientName           $clientName
     * @param OrderClientPhone          $clientPhone
     * @param OrderStatus               $status
     * @param OrderNumber               $number
     * @param OrderRefrigeratedQuantity $cooled
     * @param OrderFrozenQuantity       $frozen
     * @param StoreId                   $storeId
     * @param DateTime                  $startTime
     * @param DateTime                  $endTime
     * @param bool                      $isExpress
     * @param OrderBoxesQuantity        $boxes
     */
    public function __construct(
        OrderId $id,
        ClientId $clientId,
        OrderClientName $clientName,
        OrderClientPhone $clientPhone,
        OrderStatus $status,
        OrderNumber $number,
        OrderRefrigeratedQuantity $cooled,
        OrderFrozenQuantity $frozen,
        StoreId $storeId,
        DateTime $startTime,
        DateTime $endTime,
        bool $isExpress,
        OrderBoxesQuantity $boxes
    ) {
        $this->createdAt   = new DateTime();
        $this->id          = $id;
        $this->clientId    = $clientId;
        $this->clientName  = $clientName;
        $this->clientPhone = $clientPhone;
        $this->status      = $status;
        $this->number      = $number;
        $this->cooled      = $cooled;
        $this->frozen      = $frozen;
        $this->storeId     = $storeId;
        $this->startTime   = $startTime;
        $this->endTime     = $endTime;
        $this->isExpress   = $isExpress;
        $this->boxes       = $boxes;
        $this->status      = OrderStatus::PENDING;
    }

    /**
     * @return OrderId
     */
    public function id(): OrderId
    {
        return $this->id;
    }

    /**
     * @return OrderNumber
     */
    public function number(): OrderNumber
    {
        return $this->number;
    }

    /**
     * @return OrderRefrigeratedQuantity
     */
    public function cooled(): OrderRefrigeratedQuantity
    {
        return $this->cooled;
    }

    /**
     * @return OrderFrozenQuantity
     */
    public function frozen(): OrderFrozenQuantity
    {
        return $this->frozen;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return bool
     */
    public function isExpress(): bool
    {
        return $this->isExpress;
    }

    /**
     * @return OrderBoxesQuantity
     */
    public function boxes(): OrderBoxesQuantity
    {
        return $this->boxes;
    }

    /**
     * @return OrderStatus
     */
    public function status(): OrderStatus
    {
        return $this->status;
    }

    /**
     * @return ClientId
     */
    public function clientId(): ClientId
    {
        return $this->clientId;
    }

    /**
     * @return null|string
     */
    public function shipperId(): ?string
    {
        return $this->shipperId;
    }

    /**
     * @return DateTime
     */
    public function createdAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime|null
     */
    public function missingAt(): ?DateTime
    {
        return $this->missingAt;
    }

    /**
     * @return DateTime|null
     */
    public function deliveredAt(): ?DateTime
    {
        return $this->deliveredAt;
    }

    /**
     * @return DateTime|null
     */
    public function ongoingAt(): ?DateTime
    {
        return $this->ongoingAt;
    }

    /**
     * @return DateTime|null
     */
    public function insideAt(): ?DateTime
    {
        return $this->insideAt;
    }

    /**
     * @return OrderClientName
     */
    public function clientName(): OrderClientName
    {
        return $this->clientName;
    }

    /**
     * @return OrderClientPhone
     */
    public function clientPhone(): OrderClientPhone
    {
        return $this->clientPhone;
    }

    public function deliver(): void
    {
        $this->status      = OrderStatus::delivered();
        $this->deliveredAt = new DateTime();
    }

    /**
     * @param string $userId
     */
    public function ongoing(string $userId): void
    {
        $this->shipperId = $userId;
        $this->status    = OrderStatus::inside();
        $this->ongoingAt = new DateTime();
    }

    public function inside(): void
    {
        $this->status    = OrderStatus::ongoing();
        $this->ongoingAt = new DateTime();
    }
}
