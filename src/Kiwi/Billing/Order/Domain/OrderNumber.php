<?php

declare(strict_types=1);

namespace Kiwi\Billing\Order\Domain;

/**
 * Class OrderNumber.
 */
class OrderNumber
{
    /**
     * @var string
     */
    private $code;

    /**
     * LocationStreet constructor.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->code();
    }
}
