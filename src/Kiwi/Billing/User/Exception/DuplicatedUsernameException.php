<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class DuplicatedUsernameException.
 */
class DuplicatedUsernameException extends DomainException
{
    public function __construct()
    {
        parent::__construct('User with this username already exists.');
    }
}
