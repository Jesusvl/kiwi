<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Infrastructure;

use Kiwi\Billing\User\Domain\User;
use Kiwi\Billing\User\Domain\UserId;

/**
 * Interface UserWriteModel.
 */
interface UserWriteModel
{
    /**
     * @param UserId $id
     *
     * @return User|null
     */
    public function find(UserId $id): ?User;

    /**
     * @param User $user
     */
    public function save(User $user): void;

    /**
     * @param User $user
     */
    public function update(User $user): void;
}
