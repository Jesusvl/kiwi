<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Infrastructure;

use Kiwi\Billing\User\Document\PaginatedUserDocument;
use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Billing\User\Domain\UserPhone;
use Kiwi\Billing\User\Domain\UserUsername;
use Kiwi\Core\Domain\Pagination;

/**
 * Interface UserReadModel.
 */
interface UserReadModel
{
    /**
     * @param UserId         $id
     * @param UserParts|null $parts
     *
     * @return UserDocument
     */
    public function find(UserId $id, ?UserParts $parts = null): UserDocument;

    /**
     * @param UserUsername $username
     * @param UserParts    $parts
     *
     * @return UserDocument
     */
    public function findByUsername(UserUsername $username, ?UserParts $parts = null): UserDocument;

    /**
     * @param UserPhone      $phone
     * @param UserParts|null $parts
     *
     * @return UserDocument
     */
    public function findByPhone(UserPhone $phone, ?UserParts $parts = null): UserDocument;

    /**
     * @param Pagination     $filter
     * @param UserParts|null $parts
     *
     * @return PaginatedUserDocument
     */
    public function findAll(Pagination $filter, ?UserParts $parts = null): PaginatedUserDocument;
}
