<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Document;

use Kiwi\Billing\User\Domain\User;
use Kiwi\Core\Document\Document;

/**
 * Class UserDocument.
 */
class BaseUserDocument extends Document
{
    /**
     * @var User|null
     */
    private $user;

    /**
     * UserDocument constructor.
     *
     * @param User|null $user
     */
    public function __construct(?User $user)
    {
        parent::__construct($user);
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->user->id()->id();
    }

    /**
     * @return string
     */
    public function merchantId(): ?string
    {
        return $this->user->merchantId() ? $this->user->merchantId()->id() : null;
    }

    /**
     * @return string
     */
    public function storeId(): ?string
    {
        return $this->user->storeId() ? $this->user->storeId()->id() : null;
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->user->username()->name();
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->user->name()->name();
    }

    /**
     * @return string
     */
    public function role(): string
    {
        return $this->user->role()->name();
    }

    /**
     * @return string
     */
    public function phone(): string
    {
        return $this->user->phone()->number();
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->user->isAdmin();
    }

    /**
     * @return bool
     */
    public function isShipper(): bool
    {
        return $this->user->isShipper();
    }

    /**
     * @return bool
     */
    public function isSad(): bool
    {
        return $this->user->isSad();
    }

    /**
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->user->isManager();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->user ? [
            'id'         => $this->id(),
            'storeId'    => $this->storeId(),
            'merchantId' => $this->merchantId(),
            'username'   => $this->username(),
            'name'       => $this->name(),
            'role'       => $this->role(),
            'phone'      => $this->phone(),
            'isAdmin'    => $this->isAdmin(),
            'isManager'  => $this->isManager()
        ] : null;
    }
}
