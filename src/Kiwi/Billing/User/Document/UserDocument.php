<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Document;

use Kiwi\Billing\Merchant\Document\BaseMerchantDocument;
use Kiwi\Billing\Store\Document\BaseStoreDocument;
use Kiwi\Billing\User\Domain\User;

/**
 * Class UserDocument.
 */
class UserDocument extends BaseUserDocument
{
    /**
     * @var BaseStoreDocument
     */
    private $store;

    /**
     * @var BaseMerchantDocument
     */
    private $merchant;

    /**
     * UserWithStoreDocument constructor.
     *
     * @param User|null            $user
     * @param BaseStoreDocument    $store
     * @param BaseMerchantDocument $merchant
     */
    public function __construct(?User $user, BaseStoreDocument $store, BaseMerchantDocument $merchant)
    {
        parent::__construct($user);
        $this->store    = $store;
        $this->merchant = $merchant;
    }

    /**
     * @return UserDocument
     */
    public static function empty(): self
    {
        return new self(null, BaseStoreDocument::empty(), BaseMerchantDocument::empty());
    }

    /**
     * @return BaseStoreDocument
     */
    public function store(): BaseStoreDocument
    {
        return $this->store;
    }

    /**
     * @return BaseMerchantDocument
     */
    public function merchant(): BaseMerchantDocument
    {
        return $this->merchant;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->mergeDocumentArray(parent::toScalar(), [
            'store'    => $this->store()->toScalar(),
            'merchant' => $this->merchant()->toScalar()
        ]);
    }
}
