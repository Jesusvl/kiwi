<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Document;

use Kiwi\Core\Document\PaginatedDocument;

/**
 * Class UserPaginatedDocument.
 */
class PaginatedUserDocument extends PaginatedDocument
{
    /**
     * @return BaseUserDocument[]
     */
    public function users(): array
    {
        return $this->list();
    }
}
