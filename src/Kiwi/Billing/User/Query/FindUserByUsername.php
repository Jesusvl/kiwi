<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Query;

use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Billing\User\Domain\UserUsername;
use Kiwi\Core\Infrastructure\Query;

/**
 * Class FindUserByUsername.
 */
class FindUserByUsername implements Query
{
    /**
     * @var UserUsername
     */
    private $username;

    /**
     * @var UserParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUser constructor.
     *
     * @param string   $userId
     * @param string   $username
     * @param string[] $fields
     */
    public function __construct(string $userId, string $username, array $fields = [])
    {
        $this->parts    = new UserParts($fields);
        $this->username = new UserUsername($username);
        $this->userId   = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return UserUsername
     */
    public function username(): UserUsername
    {
        return $this->username;
    }

    /**
     * @return UserParts
     */
    public function parts(): UserParts
    {
        return $this->parts;
    }
}
