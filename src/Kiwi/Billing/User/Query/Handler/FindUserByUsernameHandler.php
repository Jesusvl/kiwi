<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Query\Handler;

use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Query\FindUserByUsername;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class FindUserByUsernameHandler.
 */
class FindUserByUsernameHandler extends UserQueryHandler
{
    /**
     * @param FindUserByUsername $query
     *
     * @return UserDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindUserByUsername $query): UserDocument
    {
        $user = $this->readModel()->findByUsername($query->username(), $query->parts());

        $this->service()->checkUserCanReadUser($query->userId(), $user);

        return $user;
    }
}
