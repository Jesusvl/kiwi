<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Query\Handler;

use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Query\FindUser;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class FindUserHandler.
 */
class FindUserHandler extends UserQueryHandler
{
    /**
     * @param FindUser $query
     *
     * @return UserDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindUser $query): UserDocument
    {
        $user = $this->readModel()->find($query->id(), $query->parts());
        $this->service()->checkUserCanReadUser($query->userId(), $user);

        return $user;
    }
}
