<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Query\Handler;

use Kiwi\Billing\User\Document\PaginatedUserDocument;
use Kiwi\Billing\User\Query\FindUsers;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class FindUsersHandler.
 */
class FindUsersHandler extends UserQueryHandler
{
    /**
     * @param FindUsers $query
     *
     * @return PaginatedUserDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindUsers $query): PaginatedUserDocument
    {
        $user = $this->service()->checkUser($query->userId());
        if (!$user->isAdmin()) {
            throw new InsufficientPermissionsException();
        }

        return $this->readModel()->findAll($query->filter(), $query->parts());
    }
}
