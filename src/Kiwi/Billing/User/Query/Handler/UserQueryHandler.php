<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Query\Handler;

use Kiwi\Core\Infrastructure\QueryHandler;
use Kiwi\Billing\User\Infrastructure\UserReadModel;
use Kiwi\Billing\User\Service\UserService;

/**
 * Class UserQueryHandler.
 */
abstract class UserQueryHandler implements QueryHandler
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var UserService
     */
    private $service;

    /**
     * UserQueryHandler constructor.
     *
     * @param UserService   $service
     * @param UserReadModel $userReadModel
     */
    public function __construct(UserService $service, UserReadModel $userReadModel)
    {
        $this->userReadModel = $userReadModel;
        $this->service       = $service;
    }

    /**
     * @return UserReadModel
     */
    protected function readModel(): UserReadModel
    {
        return $this->userReadModel;
    }

    /**
     * @return UserService
     */
    protected function service(): UserService
    {
        return $this->service;
    }
}
