<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Query;

use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Core\Infrastructure\Query;

/**
 * Class FindUser.
 */
class FindUser implements Query
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var UserParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUser constructor.
     *
     * @param string   $userId
     * @param string   $id
     * @param string[] $fields
     */
    public function __construct(string $userId, string $id, array $fields = [])
    {
        $this->parts  = new UserParts($fields);
        $this->id     = new UserId($id);
        $this->userId = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function id(): UserId
    {
        return $this->id;
    }

    /**
     * @return UserParts
     */
    public function parts(): UserParts
    {
        return $this->parts;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
