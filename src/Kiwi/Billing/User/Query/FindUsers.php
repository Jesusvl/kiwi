<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Query;

use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Infrastructure\Query;

/**
 * Class FindUsers.
 */
class FindUsers implements Query
{
    /**
     * @var Pagination
     */
    private $filter;

    /**
     * @var UserParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUsers constructor.
     *
     * @param string   $userId
     * @param int      $page
     * @param int      $pageSize
     * @param string[] $parts
     */
    public function __construct(string $userId, int $page, int $pageSize, array $parts = [])
    {
        $this->parts  = new UserParts($parts);
        $this->filter = new Pagination($page, $pageSize);
        $this->userId = new UserId($userId);
    }

    /**
     * @return Pagination
     */
    public function filter(): Pagination
    {
        return $this->filter;
    }

    /**
     * @return UserParts
     */
    public function parts(): UserParts
    {
        return $this->parts;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
