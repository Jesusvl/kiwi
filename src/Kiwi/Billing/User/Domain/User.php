<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Domain;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Store\Domain\StoreId;

/**
 * Class User.
 */
class User
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var null|StoreId
     */
    private $storeId;

    /**
     * @var null|MerchantId
     */
    private $merchantId;

    /**
     * @var UserUsername
     */
    private $username;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserRole
     */
    private $role;

    /**
     * @var UserPhone
     */
    private $phone;

    /**
     * User constructor.
     *
     * @param UserId          $id
     * @param StoreId|null    $storeId
     * @param MerchantId|null $merchantId
     * @param UserUsername    $username
     * @param UserName        $name
     * @param UserRole        $role
     * @param UserPhone       $phone
     */
    public function __construct(
        UserId $id,
        ?StoreId $storeId,
        ?MerchantId $merchantId,
        UserUsername $username,
        UserName $name,
        UserRole $role,
        UserPhone $phone
    ) {
        $this->id         = $id;
        $this->storeId    = $storeId;
        $this->merchantId = $merchantId;
        $this->username   = $username;
        $this->name       = $name;
        $this->role       = $role;
        $this->phone      = $phone;
    }

    /**
     * @return UserId
     */
    public function id(): UserId
    {
        return $this->id;
    }

    /**
     * @return MerchantId|null
     */
    public function merchantId(): ?MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return UserName
     */
    public function name(): UserName
    {
        return $this->name;
    }

    /**
     * @return StoreId|null
     */
    public function storeId(): ?StoreId
    {
        return $this->storeId;
    }

    /**
     * @return UserPhone
     */
    public function phone(): UserPhone
    {
        return $this->phone;
    }

    /**
     * @return UserRole
     */
    public function role(): UserRole
    {
        return $this->role;
    }

    /**
     * @return UserUsername
     */
    public function username(): UserUsername
    {
        return $this->username;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->role->isAdmin();
    }

    /**
     * @return bool
     */
    public function isShipper(): bool
    {
        return $this->role->isShipper();
    }

    /**
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->role->isManager();
    }

    /**
     * @return bool
     */
    public function isSad(): bool
    {
        return $this->role->isSad();
    }

    /**
     * @param MerchantId $id
     */
    public function assignMerchant(MerchantId $id): void
    {
        $this->merchantId = $id;
    }

    /**
     * @param StoreId $id
     */
    public function assignStore(StoreId $id): void
    {
        $this->storeId = $id;
    }

    /**
     * @param UserName $name
     */
    public function updateName(UserName $name): void
    {
        $this->name = $name;
    }
}
