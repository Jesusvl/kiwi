<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Domain;

/**
 * Class UserRole.
 */
class UserRole
{
    /**
     * @var string
     */
    private $name;

    /**
     * StoreName constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->name === 'ROLE_ADMIN';
    }

    /**
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->name === 'ROLE_MANAGER';
    }

    /**
     * @return bool
     */
    public function isSad(): bool
    {
        return $this->name === 'ROLE_SAD';
    }

    /**
     * @return bool
     */
    public function isShipper(): bool
    {
        return $this->name === 'ROLE_SHIPPER';
    }
}
