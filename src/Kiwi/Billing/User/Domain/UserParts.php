<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Domain;

use Kiwi\Core\Domain\Parts;

/**
 * Class UserParts.
 */
class UserParts extends Parts
{
    public const MERCHANT = 'merchant';
    public const STORE    = 'store';

    /**
     * UserParts constructor.
     *
     * @param array $inputFields
     */
    public function __construct($inputFields = [])
    {
        parent::__construct([self::MERCHANT, self::STORE], $inputFields);
    }
}
