<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Domain;

/**
 * Class UserPhone.
 */
class UserPhone
{
    /**
     * @var string
     */
    private $number;

    /**
     * StoreName constructor.
     *
     * @param string $phone
     */
    public function __construct(string $phone)
    {
        $this->number = $phone;
    }

    /**
     * @return string
     */
    public function number(): string
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->number();
    }
}
