<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Service;

use Kiwi\Billing\User\Document\BaseUserDocument;
use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Domain\User;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Billing\User\Exception\DuplicatedPhoneException;
use Kiwi\Billing\User\Exception\DuplicatedUsernameException;
use Kiwi\Billing\User\Exception\UserNotFoundException;
use Kiwi\Billing\User\Infrastructure\UserReadModel;
use Kiwi\Billing\User\Infrastructure\UserWriteModel;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class UserService.
 */
class UserService
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var UserWriteModel
     */
    private $userWriteModel;

    /**
     * UserService constructor.
     *
     * @param UserWriteModel $userWriteModel
     * @param UserReadModel  $userReadModel
     */
    public function __construct(
        UserWriteModel $userWriteModel,
        UserReadModel $userReadModel
    ) {
        $this->userWriteModel = $userWriteModel;
        $this->userReadModel  = $userReadModel;
    }

    /**
     * @param UserId $id
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function checkDomain(UserId $id): User
    {
        $user = $this->userWriteModel->find($id);

        if ($user) {
            return $user;
        }

        throw new UserNotFoundException();
    }

    /**
     * @param UserId $id
     *
     * @return BaseUserDocument
     *
     * @throws UserNotFoundException
     */
    public function checkDocument(UserId $id): BaseUserDocument
    {
        $user = $this->userReadModel->find($id, new UserParts());

        if ($user->isNotEmpty()) {
            return $user;
        }

        throw new UserNotFoundException();
    }

    /**
     * @param User $user
     *
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function save(User $user): void
    {
        if ($this->userReadModel->findByUsername($user->username())->isNotEmpty()) {
            throw new DuplicatedUsernameException();
        }

        if ($this->userReadModel->findByPhone($user->phone())->isNotEmpty()) {
            throw new DuplicatedPhoneException();
        }

        $this->userWriteModel->save($user);
    }

    /**
     * @param User $user
     *
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     * @throws UserNotFoundException
     */
    public function update(User $user): void
    {
        $tmpUser = $this->userReadModel->findByUsername($user->username());
        if ($tmpUser->isEmpty()) {
            throw new UserNotFoundException();
        }

        if ($tmpUser->id() !== $user->id()->id()) {
            throw new DuplicatedUsernameException();
        }

        $tmpUser = $this->userReadModel->findByPhone($user->phone());
        if ($tmpUser->isEmpty()) {
            throw new UserNotFoundException();
        }

        if ($tmpUser->id() !== $user->id()->id()) {
            throw new DuplicatedPhoneException();
        }

        $this->userWriteModel->update($user);
    }

    /**
     * @param UserId $userId
     * @param User   $userTarget
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteUser(UserId $userId, User $userTarget): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || $user->id() === $userTarget->id()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId           $userId
     * @param BaseUserDocument $userTarget
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadUser(UserId $userId, BaseUserDocument $userTarget): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || $user->id() === $userTarget->id()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }
}
