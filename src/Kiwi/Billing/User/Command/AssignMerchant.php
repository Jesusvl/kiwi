<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Command;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Core\Infrastructure\Command;

/**
 * Class AssignMerchant.
 */
class AssignMerchant implements Command
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * AssignMerchant constructor.
     *
     * @param string $userId the user that wants to update the user
     * @param string $id
     * @param string $merchantId
     */
    public function __construct(string $userId, string $id, string $merchantId)
    {
        $this->id         = new UserId($id);
        $this->merchantId = new MerchantId($merchantId);
        $this->userId     = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function id(): UserId
    {
        return $this->id;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
