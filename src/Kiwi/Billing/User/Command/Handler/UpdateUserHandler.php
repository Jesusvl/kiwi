<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Command\Handler;

use Kiwi\Billing\User\Command\UpdateUser;
use Kiwi\Billing\User\Domain\User;
use Kiwi\Billing\User\Exception\DuplicatedPhoneException;
use Kiwi\Billing\User\Exception\DuplicatedUsernameException;
use Kiwi\Billing\User\Exception\UserNotFoundException;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class UpdateUserHandler.
 */
class UpdateUserHandler extends UserCommandHandler
{
    /**
     * @param UpdateUser $command
     *
     * @throws InsufficientPermissionsException
     * @throws UserNotFoundException
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function __invoke(UpdateUser $command): void
    {
        $user = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteUser($command->userId(), $user);

        $this->service()->update(new User(
            $command->id(),
            $command->storeId(),
            $command->merchantId(),
            $command->username(),
            $command->name(),
            $command->role(),
            $command->phone()
        ));
    }
}
