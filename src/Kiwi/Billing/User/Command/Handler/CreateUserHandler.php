<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Command\Handler;

use Kiwi\Billing\User\Command\CreateUser;
use Kiwi\Billing\User\Domain\User;
use Kiwi\Billing\User\Exception\DuplicatedPhoneException;
use Kiwi\Billing\User\Exception\DuplicatedUsernameException;

/**
 * Class CreateUserHandler.
 */
class CreateUserHandler extends UserCommandHandler
{
    /**
     * @param CreateUser $user
     *
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function __invoke(CreateUser $user): void
    {
        $this->service()->save(new User(
            $user->id(),
            $user->storeId(),
            $user->merchantId(),
            $user->username(),
            $user->name(),
            $user->role(),
            $user->phone()
        ));
    }
}
