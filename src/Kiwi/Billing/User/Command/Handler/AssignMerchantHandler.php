<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Command\Handler;

use Kiwi\Billing\User\Command\AssignMerchant;
use Kiwi\Billing\User\Exception\DuplicatedPhoneException;
use Kiwi\Billing\User\Exception\DuplicatedUsernameException;
use Kiwi\Billing\User\Exception\UserNotFoundException;
use Kiwi\Core\Exception\InsufficientPermissionsException;

/**
 * Class AssignMerchantHandler.
 */
class AssignMerchantHandler extends UserCommandHandler
{
    /**
     * @param AssignMerchant $command
     *
     * @throws InsufficientPermissionsException
     * @throws UserNotFoundException
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function __invoke(AssignMerchant $command): void
    {
        $user = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteUser($command->userId(), $user);
        $user->assignMerchant($command->merchantId());
        $this->service()->update($user);
    }
}
