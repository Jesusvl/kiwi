<?php

declare(strict_types=1);

namespace Kiwi\Billing\User\Command;

use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserName;
use Kiwi\Billing\User\Domain\UserPhone;
use Kiwi\Billing\User\Domain\UserRole;
use Kiwi\Billing\User\Domain\UserUsername;
use Kiwi\Core\Infrastructure\Command;

/**
 * Class CreateUser.
 */
class CreateUser implements Command
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var null|StoreId
     */
    private $storeId;

    /**
     * @var null|MerchantId
     */
    private $merchantId;

    /**
     * @var UserUsername
     */
    private $username;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserRole
     */
    private $role;

    /**
     * @var UserPhone
     */
    private $phone;

    /**
     * CreateUser constructor.
     *
     * @param string      $id
     * @param null|string $storeId
     * @param null|string $merchantId
     * @param string      $username
     * @param string      $name
     * @param string      $role
     * @param string      $phone
     */
    public function __construct(
        string $id,
        ?string $storeId,
        ?string $merchantId,
        string $username,
        string $name,
        string $role,
        string $phone
    ) {
        $this->id         = new UserId($id);
        $this->storeId    = new StoreId($storeId);
        $this->merchantId = new MerchantId($merchantId);
        $this->username   = new UserUsername($username);
        $this->name       = new UserName($name);
        $this->role       = new UserRole($role);
        $this->phone      = new UserPhone($phone);
    }

    /**
     * @return UserId
     */
    public function id(): UserId
    {
        return $this->id;
    }

    /**
     * @return StoreId|null
     */
    public function storeId(): ?StoreId
    {
        return $this->storeId;
    }

    /**
     * @return MerchantId|null
     */
    public function merchantId(): ?MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return UserUsername
     */
    public function username(): UserUsername
    {
        return $this->username;
    }

    /**
     * @return UserName
     */
    public function name(): UserName
    {
        return $this->name;
    }

    /**
     * @return UserRole
     */
    public function role(): UserRole
    {
        return $this->role;
    }

    /**
     * @return UserPhone
     */
    public function phone(): UserPhone
    {
        return $this->phone;
    }
}
