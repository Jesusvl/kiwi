<?php

declare(strict_types=1);

namespace Kiwi\Core\Document;

/**
 * Class PaginatedDocument.
 */
final class PaginatedDocument extends Document
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $totalPages;

    /**
     * @var array|Document[]
     */
    private $list;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var bool
     */
    private $hasPrevious;

    /**
     * @var bool
     */
    private $hasNext;

    /**
     * @var int
     */
    private $totalDocuments;

    /**
     * @var int
     */
    private $currentPageSize;

    /**
     * PaginatedDocument constructor.
     *
     * @param Document[] $list
     * @param int        $page
     * @param int        $pageSize
     * @param int        $totalDocuments
     */
    public function __construct(array $list, int $page, int $pageSize, int $totalDocuments)
    {
        parent::__construct($list);
        $this->page            = $page;
        $this->totalPages      = (int) \ceil($totalDocuments / 50);
        $this->list            = $list;
        $this->pageSize        = $pageSize;
        $this->currentPageSize = \count($list);
        $this->hasNext         = $this->page < $this->totalPages;
        $this->hasPrevious     = $this->page > 1;
        $this->totalDocuments  = $totalDocuments;
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function totalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @return Document[]
     */
    public function list(): array
    {
        return $this->list;
    }

    /**
     * @return int
     */
    public function pageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return bool
     */
    public function hasPrevious(): bool
    {
        return $this->hasPrevious;
    }

    /**
     * @return bool
     */
    public function hasNext(): bool
    {
        return $this->hasNext;
    }

    /**
     * @return int
     */
    public function totalDocuments(): int
    {
        return $this->totalDocuments;
    }

    /**
     * @return int
     */
    public function currentPageSize(): int
    {
        return $this->currentPageSize;
    }

    /**
     * @return array
     */
    public function toScalar(): array
    {
        return [
            'list' => \array_map(function (Document $document): array {
                return $document->toScalar();
            }, $this->list()),
            'page'            => $this->page(),
            'currentPageSize' => $this->currentPageSize(),
            'totalPages'      => $this->totalPages(),
            'totalDocuments'  => $this->totalDocuments(),
            'pageSize'        => $this->pageSize(),
            'hasPrevious'     => $this->hasPrevious(),
            'hasNext'         => $this->hasNext()
        ];
    }
}
