<?php

declare(strict_types=1);

namespace Kiwi\Core\Document;

/**
 * Class Document.
 */
abstract class Document
{
    /**
     * @var bool
     */
    protected $isNull;

    /**
     * Document constructor.
     *
     * @param mixed|null $week
     */
    public function __construct($week)
    {
        $this->isNull = null === $week;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->isNull;
    }

    /**
     * @return bool
     */
    public function isNotEmpty(): bool
    {
        return !$this->isEmpty();
    }

    /**
     * @return array|null
     */
    abstract public function toScalar(): ?array;

    /**
     * @param array|null $parentData
     * @param array|null $data
     *
     * @return array|null
     */
    protected function mergeDocumentArray(?array $parentData, ?array $data): ?array
    {
        if (null === $data) {
            return $parentData;
        }

        if (null === $parentData) {
            return null;
        }

        $newData = [];
        foreach ($data as $key => $element) {
            if ($element !== null) {
                $newData[$key] = $element;
            }
        }

        return \array_merge($parentData, $newData);
    }
}
