<?php

declare(strict_types=1);

namespace Kiwi\Core\Document;

/**
 * Class DocumentBook.
 */
final class DocumentBook extends Document
{
    /**
     * @var array
     */
    private $list;

    /**
     * DocumentList constructor.
     *
     * @param array $list
     */
    public function __construct(array $list = [])
    {
        parent::__construct($list);
        $this->list = $list;
    }

    /**
     * @return array
     */
    public function list(): array
    {
        return $this->list;
    }

    /**
     * @param int $index
     *
     * @return Document
     */
    public function get(int $index): Document
    {
        return $this->list[$index];
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->list);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->list);
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return \array_map(function (Document $item): array {
            return $item->toScalar();
        }, $this->list);
    }
}
