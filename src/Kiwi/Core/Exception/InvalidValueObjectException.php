<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

/**
 * Class InvalidValueObjectException.
 */
abstract class InvalidValueObjectException extends DomainException
{

}
