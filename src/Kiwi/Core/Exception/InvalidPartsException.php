<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

/**
 * Class InvalidPartsException.
 */
class InvalidPartsException extends InvalidValueObjectException
{
    /**
     * InvalidPartsException constructor.
     *
     * @param string $field
     */
    public function __construct(string $field)
    {
        parent::__construct("Part '{$field}' is not accepted.");
    }
}
