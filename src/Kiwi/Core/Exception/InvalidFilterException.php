<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

/**
 * Class InvalidFilterException.
 */
class InvalidFilterException extends InvalidValueObjectException
{
    /**
     * InvalidFilterException constructor.
     *
     * @param string $name
     * @param string $comparator
     * @param string $value
     */
    public function __construct(string $name, string $comparator, string $value)
    {
        parent::__construct("Filter '{$name},{$comparator},{$value}' is not accepted.");
    }
}
