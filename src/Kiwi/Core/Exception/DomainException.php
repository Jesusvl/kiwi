<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

use Exception;
use Throwable;

/**
 * Class DomainException.
 */
class DomainException extends Exception
{
    /**
     * DomainException constructor.
     *
     * @param string    $message
     * @param Throwable $previous
     */
    public function __construct(string $message, ?Throwable $previous = null)
    {
        parent::__construct($message, 8, $previous);
    }
}
