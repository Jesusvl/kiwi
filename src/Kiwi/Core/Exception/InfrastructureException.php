<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

use Exception;
use Throwable;

/**
 * Class InfrastructureException.
 */
class InfrastructureException extends Exception
{
    /**
     * InfrastructureException constructor.
     *
     * @param Throwable $previous
     */
    public function __construct(Throwable $previous)
    {
        parent::__construct($previous->getMessage(), 16, $previous);
    }
}
