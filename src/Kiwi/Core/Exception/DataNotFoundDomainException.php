<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

/**
 * Class DataNotFoundDomainException.
 */
class DataNotFoundDomainException extends DomainException
{
}
