<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

/**
 * Class InvalidSortArgumentException.
 */
class InvalidSortArgumentException extends DomainException
{
    /**
     * InvalidPartsException constructor.
     *
     * @param string $field
     */
    public function __construct(string $field)
    {
        parent::__construct("Entity cannot be sorted by {$field}");
    }
}
