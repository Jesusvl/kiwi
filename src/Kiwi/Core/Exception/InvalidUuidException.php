<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

use Kiwi\Core\Domain\Uuid;

/**
 * Class InvalidUuidException.
 */
class InvalidUuidException extends InvalidValueObjectException
{
    /**
     * InvalidUuidException constructor.
     *
     * @param Uuid $uuid
     */
    public function __construct(Uuid $uuid)
    {
        parent::__construct("Uuid '{$uuid}' is not valid.");
    }
}
