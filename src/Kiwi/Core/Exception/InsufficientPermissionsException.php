<?php

declare(strict_types=1);

namespace Kiwi\Core\Exception;

/**
 * Class InsufficientPermissionsException.
 */
class InsufficientPermissionsException extends DomainException
{
    /**
     * InsufficientPermissionsException constructor.
     */
    public function __construct()
    {
        parent::__construct('Current user has not sufficient permissions');
    }
}
