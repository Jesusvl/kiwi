<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

/**
 * Class Event.
 */
interface Event extends Serializable
{
    /**
     * @return string
     */
    public static function name(): string;
}
