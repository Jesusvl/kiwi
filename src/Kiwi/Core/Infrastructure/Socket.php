<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

use Kiwi\Core\Domain\Topic;
use Kiwi\Core\Domain\UserId;

/**
 * Interface Socket.
 */
interface Socket
{
    /**
     * @param Topic       $topic
     * @param array       $message
     * @param UserId|null $userId
     */
    public function push(Topic $topic, array $message, ?UserId $userId = null): void;
}
