<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

/**
 * Interface Worker.
 */
interface Worker
{
    /**
     * @param Event  $event
     * @param string $boundary
     */
    public function publish(Event $event, string $boundary): void;

    /**
     * @param string $name
     * @param string $boundary
     */
    public function consume(string $name, string $boundary): void;

    /**
     * @param EventManager $eventManager
     */
    public function setEventManager(EventManager $eventManager): void;
}
