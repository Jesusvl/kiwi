<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

/**
 * Interface Serializable.
 */
interface Serializable
{
    /**
     * @return string
     */
    public function serialize(): string;

    /**
     * @param string $message
     */
    public static function deserialize(string $message);
}
