<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

/**
 * Interface Listener.
 */
interface EventManager
{
    /**
     * @param Event $event
     */
    public function dispatch(Event $event): void;

    /**
     * @param Event $event
     */
    public function dispatchSync(Event $event): void;

    /**
     * @param Event $event
     */
    public function dispatchAsync(Event $event): void;

    /**
     * @param Listener $listener
     */
    public function subscribe(Listener $listener): void;

    /**
     * @param AsyncListener $listener
     */
    public function subscribeForAsync(AsyncListener $listener): void;
}
