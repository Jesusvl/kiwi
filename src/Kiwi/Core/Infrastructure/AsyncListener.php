<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

/**
 * Interface AsyncListener.
 */
interface AsyncListener
{
    /**
     * @param Event $event
     */
    public function handle(Event $event): void;

    /**
     * @return string
     */
    public function listensTo(): string;
}
