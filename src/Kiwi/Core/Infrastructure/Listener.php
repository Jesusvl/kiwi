<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

/**
 * Interface Listener.
 */
interface Listener
{
    /**
     * @param Event $data
     */
    public function handle(Event $data): void;

    /**
     * @return string
     */
    public function listensTo(): string;
}
