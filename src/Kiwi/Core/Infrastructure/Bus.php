<?php

declare(strict_types=1);

namespace Kiwi\Core\Infrastructure;

use Kiwi\Core\Document\Document;

/**
 * Interface Bus.
 */
interface Bus
{
    /**
     * @param Query $query
     *
     * @return mixed|Document
     */
    public function dispatchQuery(Query $query);

    /**
     * @param Command $command
     */
    public function dispatchCommand(Command $command): void;
}
