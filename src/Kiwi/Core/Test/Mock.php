<?php

declare(strict_types=1);

namespace Kiwi\Core\Test;

use Hamcrest\Matchers;
use Mockery\MockInterface;

/**
 * Class Mock.
 */
abstract class Mock
{
    /**
     * @var MockInterface
     */
    protected $mock;

    /**
     * @var string
     */
    private $class;

    /**
     * @var array
     */
    private static $cache = [];

    /**
     * Mock constructor.
     *
     * @param string $class
     */
    public function __construct(string $class)
    {
        $this->class         = $class;
        $this->mock          = \Mockery::mock($class);
        self::$cache[$class] = self::$cache[$class] ?? [];
    }

    /**
     * @param string $name
     * @param array  $arguments
     */
    public function __call(string $name, array $arguments): void
    {
        $info   = self::getInfo($name, $this->class);
        $return = \array_pop($arguments);

        $list = [];
        foreach ($arguments as $argument) {
            $list[] = Matchers::equalTo($argument);
        }

        $this->mock
            ->shouldReceive($info['name'])
            ->once()
            ->withArgs($list)
            ->andReturn($return);
    }

    /**
     * @return MockInterface|mixed
     */
    public function mock()
    {
        return $this->mock;
    }

    /**
     * @param string $name
     * @param string $class
     *
     * @return array
     */
    private static function getInfo(string $name, string $class): array
    {
        if (self::$cache[$class][$name] ?? false) {
            return self::$cache[$class][$name];
        }

        $methodName = \lcfirst(\mb_substr($name, 6));

        self::$cache[$class][$name]         = self::$cache[$class][$name] ?? [];
        self::$cache[$class][$name]['name'] = $methodName;

        return self::$cache[$class][$name];
    }
}
