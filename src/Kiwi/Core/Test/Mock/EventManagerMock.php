<?php

declare(strict_types=1);

namespace Kiwi\Core\Test\Mock;

use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Core\Test\Mock;

/**
 * Class EventManagerMock.
 *
 * @method shouldDispatch(Event $event): void
 */
class EventManagerMock extends Mock
{
    /**
     * LocationReadModelMock constructor.
     */
    public function __construct()
    {
        parent::__construct(EventManager::class);
    }
}
