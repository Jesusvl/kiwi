<?php

declare(strict_types=1);

namespace Kiwi\Core\Domain;

/**
 * Class Pagination.
 */
class Pagination
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * Pagination constructor.
     *
     * @param int $page
     * @param int $pageSize
     */
    public function __construct(int $page, int $pageSize)
    {
        $this->page     = $page;
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function pageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return int
     */
    public function firstResult(): int
    {
        return $this->pageSize() * ($this->page() - 1);
    }
}
