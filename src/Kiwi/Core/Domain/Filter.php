<?php

declare(strict_types=1);

namespace Kiwi\Core\Domain;

use Kiwi\Core\Exception\InvalidFilterException;

/**
 * Class Filter.
 */
abstract class Filter
{
    public const EQ = 'eq';
    public const NEQ = 'neq';
    public const LT = 'lt';
    public const LTE = 'lte';
    public const GT = 'gt';
    public const GTE = 'gte';

    private const VALID_OPERATORS = [self::EQ, self::NEQ, self::LT, self::LTE, self::GT, self::GTE];

    /**
     * @var string[]
     */
    private $validFields;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $comparator;

    /**
     * Filter constructor.
     *
     * @param array  $validFields
     * @param string $name
     * @param string $comparator
     * @param string $value
     *
     * @throws InvalidFilterException
     */
    public function __construct(array $validFields, string $name, string $comparator, string $value)
    {
        $this->validFields = $validFields;

        $this->name = $name;
        $this->comparator = $comparator;
        $this->value = $value;

        $this->validate();
    }

    /**
     * @throws InvalidFilterException
     */
    private function validate(): void
    {
        if (!\in_array($this->name, $this->validFields, true)) {
            throw new InvalidFilterException($this->name, $this->comparator, $this->value);
        }

        if (!\in_array($this->comparator, self::VALID_OPERATORS, true)) {
            throw new InvalidFilterException($this->name, $this->comparator, $this->value);
        }
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function comparator(): string
    {
        return $this->comparator;
    }

    public function comparatorSymbol(): string
    {
        return [
            'lte' => '<=',
            'lt' => '<',
            'eq' => '=',
            'neq' => '!=',
            'gt' => '>',
            'gte' => '>='
        ][$this->comparator()];
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return string[]
     */
    public function validFields(): array
    {
        return $this->validFields;
    }
}
