<?php

declare(strict_types=1);

namespace Kiwi\Core\Domain;

use Kiwi\Core\Exception\InvalidPartsException;

/**
 * Class Parts.
 */
abstract class Parts
{
    /**
     * @var string[]
     */
    private $validFields;

    /**
     * @var string[]
     */
    private $inputFields;

    /**
     * Parts constructor.
     *
     * @param string[] $validFields
     * @param string[] $inputFields
     *
     * @throws InvalidPartsException
     */
    public function __construct(array $validFields = [], array $inputFields = [])
    {
        $this->validFields = $validFields;
        $this->inputFields = $inputFields;

        $this->validate();
    }

    /**
     * @throws InvalidPartsException
     */
    private function validate(): void
    {
        foreach ($this->inputFields as $field) {
            if (!\in_array($field, $this->validFields, true)) {
                throw new InvalidPartsException($field);
            }
        }
    }

    /**
     * @return string[]
     */
    public function inputFields(): array
    {
        return $this->inputFields;
    }

    /**
     * @param string $field
     *
     * @return bool
     */
    public function contains(string $field): bool
    {
        return \in_array($field, $this->inputFields, true);
    }
}
