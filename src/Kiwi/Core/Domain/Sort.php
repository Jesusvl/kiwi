<?php

declare(strict_types=1);

namespace Kiwi\Core\Domain;

use Kiwi\Core\Exception\InvalidSortArgumentException;

/**
 * Class Sort.
 */
class Sort
{
    /**
     * @var string
     */
    private $attribute;

    /**
     * @var string
     */
    private $type;

    /**
     * Sort constructor.
     *
     * @param array  $attributes
     * @param string $attribute
     * @param string $type
     *
     * @throws InvalidSortArgumentException
     */
    public function __construct(array $attributes, string $attribute, string $type)
    {
        $this->attribute  = $attribute;
        $this->type       = $type;

        if (!in_array($attribute, $attributes, true)) {
            throw new InvalidSortArgumentException($attribute);
        }
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function attribute(): string
    {
        return $this->attribute;
    }
}
