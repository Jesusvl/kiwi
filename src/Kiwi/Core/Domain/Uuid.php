<?php

declare(strict_types=1);

namespace Kiwi\Core\Domain;

use Kiwi\Core\Exception\InvalidUuidException;
use Kiwi\Core\Utils\Uuid as UuidGenerator;

/**
 * Class Uuid.
 */
abstract class Uuid
{
    /**
     * @var string
     */
    private $id;

    /**
     * LocationId constructor.
     *
     * @param string $id
     */
    public function __construct(string $id = null)
    {
        $this->id = $id ?? UuidGenerator::new();
        $this->validate();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id();
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    private function validate(): void
    {
        if (!UuidGenerator::isValid($this->id)) {
            throw new InvalidUuidException($this);
        }
    }
}
