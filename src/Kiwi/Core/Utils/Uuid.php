<?php

declare(strict_types=1);

namespace Kiwi\Core\Utils;

/**
 * Class Uuid.
 */
class Uuid
{
    /**
     * @return string
     */
    public static function new(): string
    {
        return \Ramsey\Uuid\Uuid::uuid4()->toString();
    }

    /**
     * @param string $uuid
     *
     * @return bool
     */
    public static function isValid(string $uuid): bool
    {
        return \Ramsey\Uuid\Uuid::isValid($uuid);
    }
}
