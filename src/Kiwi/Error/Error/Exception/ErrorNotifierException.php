<?php

declare(strict_types=1);

namespace Kiwi\Error\Error\Exception;

use Kiwi\Core\Exception\InfrastructureException;

/**
 * Class ErrorNotifierException.
 */
class ErrorNotifierException extends InfrastructureException
{

}
