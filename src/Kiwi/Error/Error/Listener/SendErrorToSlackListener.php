<?php

declare(strict_types=1);

namespace Kiwi\Error\Error\Listener;

use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Error\Error\Event\OnErrorEvent;
use Kiwi\Error\Error\Exception\ErrorNotifierException;
use Kiwi\Error\Error\Infrastructure\ErrorClient;

/**
 * Class SendErrorToSlackListener.
 */
class SendErrorToSlackListener implements AsyncListener
{
    /**
     * @var ErrorClient
     */
    private $client;

    /**
     * SendErrorToSlack constructor.
     *
     * @param ErrorClient $client
     */
    public function __construct(ErrorClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param Event|OnErrorEvent $event
     */
    public function handle(Event $event): void
    {
        if (!is_a($event->exceptionClass(), ErrorNotifierException::class)) {
            $this->client->sendMessage("{$event->message()}:\n
                Exception *{$event->exceptionClass()}*\n
                File *{$event->file()}*:*{$event->line()}*\n
                At *{$event->on()->format(DATE_ATOM)}*\n
            ");
        }
    }

    /**
     * @return string
     */
    public function listensTo(): string
    {
        return OnErrorEvent::NAME;
    }
}
