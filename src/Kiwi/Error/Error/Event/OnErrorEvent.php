<?php

declare(strict_types=1);

namespace Kiwi\Error\Error\Event;

use KiwiLib\DateTime\DateTime;
use Kiwi\Core\Infrastructure\Event;

/**
 * Class OnErrorEvent.
 */
class OnErrorEvent implements Event
{
    public const NAME = 'OnError';

    /**
     * @var string
     */
    private $exceptionClass;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $file;

    /**
     * @var int
     */
    private $line;

    /**
     * @var DateTime
     */
    private $on;

    /**
     * OnErrorEvent constructor.
     *
     * @param string            $exceptionClass
     * @param string            $message
     * @param string            $file
     * @param int               $line
     * @param DateTime $on
     */
    public function __construct(string $exceptionClass, string $message, string $file, int $line, DateTime $on)
    {
        $this->exceptionClass = $exceptionClass;
        $this->message        = $message;
        $this->file           = $file;
        $this->line           = $line;
        $this->on             = $on;
    }

    /**
     * @return string
     */
    public static function name(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function exceptionClass(): string
    {
        return $this->exceptionClass;
    }

    /**
     * @return string
     */
    public function file(): string
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function line(): int
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }

    /**
     * @return DateTime
     */
    public function on(): DateTime
    {
        return $this->on;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->exceptionClass,
            $this->message,
            $this->file,
            $this->line,
            $this->on->format(DATE_ATOM)
        ]);
    }

    /**
     * @param string $message
     *
     * @return self
     */
    public static function deserialize(string $message): self
    {
        [
            $exceptionClass,
            $message,
            $file,
            $line,
            $on
        ] = unserialize($message, ['allowed_classes' => true]);

        return new self($exceptionClass, $message, $file, $line, new DateTime($on));
    }
}
