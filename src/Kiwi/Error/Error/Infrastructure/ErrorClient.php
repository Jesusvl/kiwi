<?php

declare(strict_types=1);

namespace Kiwi\Error\Error\Infrastructure;

/**
 * Interface ErrorClient.
 */
interface ErrorClient
{
    /**
     * @param string $message
     */
    public function sendMessage(string $message): void;
}
