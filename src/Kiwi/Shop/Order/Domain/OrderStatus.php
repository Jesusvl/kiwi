<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class OrderStatus.
 */
class OrderStatus
{
    public const INSIDE = 'inside';
    public const PENDING = 'pending';
    public const DELIVERED = 'delivered';
    public const ONGOING = 'ongoing';
    public const CANCELED = 'canceled';

    /**
     * @var string
     */
    private $name;

    /**
     * LocationStreet constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }

    /**
     * @return OrderStatus
     */
    public static function pending(): self
    {
        return new self(self::PENDING);
    }

    /**
     * @return OrderStatus
     */
    public static function delivered(): self
    {
        return new self(self::DELIVERED);
    }

    /**
     * @return OrderStatus
     */
    public static function inside(): self
    {
        return new self(self::INSIDE);
    }

    /**
     * @return OrderStatus
     */
    public static function canceled(): self
    {
        return new self(self::CANCELED);
    }

    /**
     * @return OrderStatus
     */
    public static function ongoing(): self
    {
        return new self(self::ONGOING);
    }
}
