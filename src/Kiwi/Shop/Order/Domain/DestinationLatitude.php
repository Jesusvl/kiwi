<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class DestinationLatitude.
 */
class DestinationLatitude
{
    /**
     * @var float
     */
    private $latitude;

    /**
     * Latitude constructor.
     *
     * @param float $latitude
     */
    public function __construct(float $latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function value(): float
    {
        return $this->latitude;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->latitude;
    }
}
