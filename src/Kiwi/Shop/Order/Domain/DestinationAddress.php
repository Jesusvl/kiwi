<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class DestinationAddress.
 */
class DestinationAddress
{
    /**
     * @var string
     */
    private $name;

    /**
     * LocationStreet constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }
}
