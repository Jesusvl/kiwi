<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class DestinationDoor.
 */
class DestinationDoor
{
    /**
     * @var string
     */
    private $door;

    /**
     * DestinationDoor constructor.
     *
     * @param string $door
     */
    public function __construct(string $door)
    {
        $this->door = $door;
    }

    /**
     * @return string
     */
    public function door(): string
    {
        return $this->door;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->door();
    }
}
