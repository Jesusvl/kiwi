<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class OrderClientPhone.
 */
class OrderClientPhone
{
    /**
     * @var string
     */
    private $phone;

    /**
     * LocationStreet constructor.
     *
     * @param string $phone
     */
    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function phone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->phone();
    }
}
