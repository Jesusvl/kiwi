<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class OrderRefrigeratedQuantity.
 */
class OrderRefrigeratedQuantity
{
    /**
     * @var int
     */
    private $quantity;

    /**
     * OrderBoxesQuantity constructor.
     *
     * @param int $quantity
     */
    public function __construct(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function quantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->quantity;
    }
}
