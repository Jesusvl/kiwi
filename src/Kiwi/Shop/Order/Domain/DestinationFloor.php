<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class DestinationFloor.
 */
class DestinationFloor
{
    /**
     * @var string
     */
    private $floor;

    /**
     * DestinationFloor constructor.
     *
     * @param string $floor
     */
    public function __construct(string $floor)
    {
        $this->floor = $floor;
    }

    /**
     * @return string
     */
    public function floor(): string
    {
        return $this->floor;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->floor();
    }
}
