<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class OrderDestinationId.
 */
class DestinationId extends Uuid
{
}
