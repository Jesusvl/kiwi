<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Exception\InvalidFilterException;

/**
 * Class OrderFilter.
 */
class OrderFilter extends Filter
{
    /**
     * OrderFilter constructor.
     *
     * @param string $name
     * @param string $comparator
     * @param string $value
     *
     * @throws InvalidFilterException
     */
    public function __construct(string $name, string $comparator, string $value)
    {
        parent::__construct(['startTime', 'endTime', 'storeId'], $name, $comparator, $value);
    }
}
