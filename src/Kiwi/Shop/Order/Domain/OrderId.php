<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class OrderId.
 */
class OrderId extends Uuid
{
}
