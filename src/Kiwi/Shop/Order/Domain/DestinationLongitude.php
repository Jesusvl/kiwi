<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class DestinationLongitude.
 */
class DestinationLongitude
{
    /**
     * @var float
     */
    private $longitude;

    /**
     * Longitude constructor.
     *
     * @param float $longitude
     */
    public function __construct(float $longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function value(): float
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->longitude;
    }
}
