<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

use Kiwi\Core\Domain\Sort;
use Kiwi\Core\Exception\InvalidSortArgumentException;

/**
 * Class OrderSort.
 */
class OrderSort extends Sort
{
    /**
     * OrderSort constructor.
     *
     * @param string $attribute
     * @param string $type
     *
     * @throws InvalidSortArgumentException
     */
    public function __construct(string $attribute, string $type)
    {
        parent::__construct(['createdAt', 'startTime'], $attribute, $type);
    }
}
