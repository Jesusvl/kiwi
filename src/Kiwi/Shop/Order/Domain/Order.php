<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Order\Exception\OrderIsNotInsideException;
use Kiwi\Shop\Order\Exception\OrderIsNotOngoingException;
use Kiwi\Shop\Order\Exception\OrderIsNotPendingException;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;
use KiwiLib\DateTime\DateTime;

/**
 * Class Order.
 */
class Order
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var OrderNumber
     */
    private $number;

    /**
     * @var OrderRefrigeratedQuantity
     */
    private $cooled;

    /**
     * @var OrderFrozenQuantity
     */
    private $frozen;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * @var bool
     */
    private $isExpress;

    /**
     * @var OrderBoxesQuantity
     */
    private $boxes;

    /**
     * @var OrderStatus
     */
    private $status;

    /**
     * @var ClientId
     */
    private $clientId;

    /**
     * @var UserId|null
     */
    private $shipperId;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var DateTime|null
     */
    private $missingAt;

    /**
     * @var DateTime|null
     */
    private $deliveredAt;

    /**
     * @var DateTime|null
     */
    private $ongoingAt;

    /**
     * @var DateTime|null
     */
    private $insideAt;

    /**
     * @var OrderClientName
     */
    private $clientName;

    /**
     * @var OrderClientPhone
     */
    private $clientPhone;

    /**
     * @var DestinationId
     */
    private $destinationId;

    /**
     * @var bool
     */
    private $needsPayment;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * Order constructor.
     *
     * @param OrderId                   $id
     * @param ClientId                  $clientId
     * @param OrderClientName           $clientName
     * @param OrderClientPhone          $clientPhone
     * @param OrderStatus               $status
     * @param OrderNumber               $number
     * @param OrderRefrigeratedQuantity $cooled
     * @param OrderFrozenQuantity       $frozen
     * @param StoreId                   $storeId
     * @param MerchantId                $merchantId
     * @param DateTime                  $startTime
     * @param DateTime                  $endTime
     * @param bool                      $isExpress
     * @param DestinationId             $destinationId
     * @param OrderBoxesQuantity        $boxes
     * @param bool                      $needsPayment
     */
    public function __construct(
        OrderId $id,
        ClientId $clientId,
        OrderClientName $clientName,
        OrderClientPhone $clientPhone,
        OrderStatus $status,
        OrderNumber $number,
        OrderRefrigeratedQuantity $cooled,
        OrderFrozenQuantity $frozen,
        StoreId $storeId,
        MerchantId $merchantId,
        DateTime $startTime,
        DateTime $endTime,
        bool $isExpress,
        DestinationId $destinationId,
        OrderBoxesQuantity $boxes,
        bool $needsPayment
    ) {
        $this->createdAt     = new DateTime();
        $this->id            = $id;
        $this->clientId      = $clientId;
        $this->clientName    = $clientName;
        $this->clientPhone   = $clientPhone;
        $this->status        = $status;
        $this->number        = $number;
        $this->cooled        = $cooled;
        $this->frozen        = $frozen;
        $this->storeId       = $storeId;
        $this->merchantId    = $merchantId;
        $this->startTime     = $startTime;
        $this->endTime       = $endTime;
        $this->isExpress     = $isExpress;
        $this->destinationId = $destinationId;
        $this->boxes         = $boxes;
        $this->status        = OrderStatus::PENDING;
        $this->needsPayment  = $needsPayment;
    }

    /**
     * @return OrderId
     */
    public function id(): OrderId
    {
        return $this->id;
    }

    /**
     * @return OrderNumber
     */
    public function number(): OrderNumber
    {
        return $this->number;
    }

    /**
     * @return OrderRefrigeratedQuantity
     */
    public function cooled(): OrderRefrigeratedQuantity
    {
        return $this->cooled;
    }

    /**
     * @return OrderFrozenQuantity
     */
    public function frozen(): OrderFrozenQuantity
    {
        return $this->frozen;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return bool
     */
    public function isExpress(): bool
    {
        return $this->isExpress;
    }

    /**
     * @return OrderBoxesQuantity
     */
    public function boxes(): OrderBoxesQuantity
    {
        return $this->boxes;
    }

    /**
     * @return OrderStatus
     */
    public function status(): OrderStatus
    {
        return $this->status;
    }

    /**
     * @return ClientId
     */
    public function clientId(): ClientId
    {
        return $this->clientId;
    }

    /**
     * @return null|UserId
     */
    public function shipperId(): ?UserId
    {
        return $this->shipperId;
    }

    /**
     * @return DateTime
     */
    public function createdAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime|null
     */
    public function missingAt(): ?DateTime
    {
        return $this->missingAt;
    }

    /**
     * @return DateTime|null
     */
    public function deliveredAt(): ?DateTime
    {
        return $this->deliveredAt;
    }

    /**
     * @return DateTime|null
     */
    public function ongoingAt(): ?DateTime
    {
        return $this->ongoingAt;
    }

    /**
     * @return DateTime|null
     */
    public function insideAt(): ?DateTime
    {
        return $this->insideAt;
    }

    /**
     * @return OrderClientName
     */
    public function clientName(): OrderClientName
    {
        return $this->clientName;
    }

    /**
     * @return OrderClientPhone
     */
    public function clientPhone(): OrderClientPhone
    {
        return $this->clientPhone;
    }

    /**
     * @return DestinationId
     */
    public function destinationId(): DestinationId
    {
        return $this->destinationId;
    }

    /**
     * @throws OrderIsNotInsideException
     */
    public function deliver(): void
    {
        if ($this->status->name() !== OrderStatus::INSIDE) {
            throw new OrderIsNotInsideException();
        }

        $this->status      = OrderStatus::delivered();
        $this->deliveredAt = new DateTime();
    }

    /**
     * @param UserId $userId
     *
     * @throws OrderIsNotPendingException
     */
    public function ongoing(UserId $userId): void
    {
        if ($this->status->name() !== OrderStatus::PENDING) {
            throw new OrderIsNotPendingException();
        }

        $this->shipperId = $userId;
        $this->status    = OrderStatus::ongoing();
        $this->ongoingAt = new DateTime();
    }

    /**
     * @throws OrderIsNotOngoingException
     */
    public function inside(): void
    {
        if ($this->status->name() !== OrderStatus::ONGOING) {
            throw new OrderIsNotOngoingException();
        }

        $this->status    = OrderStatus::inside();
        $this->insideAt = new DateTime();
    }

    /**
     * @return bool
     */
    public function needsPayment(): bool
    {
        return $this->needsPayment;
    }
}
