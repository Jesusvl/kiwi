<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

use Kiwi\Core\Domain\Parts;

/**
 * Class OrderParts.
 */
class OrderParts extends Parts
{
    public const DESTINATION = 'destination';
    public const STORE       = 'store';
    public const SHIPPER     = 'shipper';
    public const MERCHANT    = 'merchant';

    /**
     * OrderParts constructor.
     *
     * @param array $inputFields
     */
    public function __construct($inputFields = [])
    {
        parent::__construct([self::DESTINATION, self::STORE, self::SHIPPER, self::MERCHANT], $inputFields);
    }
}
