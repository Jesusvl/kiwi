<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

/**
 * Class DestinationStair.
 */
class DestinationStair
{
    /**
     * @var string
     */
    private $name;

    /**
     * DestinationStair constructor.
     *
     * @param string $stair
     */
    public function __construct(string $stair)
    {
        $this->name = $stair;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }
}
