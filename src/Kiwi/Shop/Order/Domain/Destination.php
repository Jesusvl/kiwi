<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Domain;

use Kiwi\Shop\Location\Domain\LocationId;

/**
 * Class Destination.
 */
class Destination
{
    /**
     * @var DestinationId
     */
    private $id;

    /**
     * @var DestinationAddress
     */
    private $address;

    /**
     * @var DestinationAnnotation
     */
    private $annotation;

    /**
     * @var DestinationLatitude
     */
    private $latitude;

    /**
     * @var DestinationLongitude
     */
    private $longitude;

    /**
     * @var bool
     */
    private $hasElevator;

    /**
     * @var bool
     */
    private $hasStepElevator;

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * @var DestinationFloor
     */
    private $floor;

    /**
     * @var DestinationDoor
     */
    private $door;

    /**
     * @var DestinationStair
     */
    private $stair;

    /**
     * Destination constructor.
     *
     * @param DestinationId         $id
     * @param DestinationAddress    $address
     * @param DestinationAnnotation $annotation
     * @param DestinationLatitude   $latitude
     * @param DestinationLongitude  $longitude
     * @param DestinationStair      $destinationStair
     * @param DestinationDoor       $destinationDoor
     * @param DestinationFloor      $destinationFloor
     * @param bool                  $hasElevator
     * @param bool                  $hasStepElevator
     * @param LocationId            $locationId
     */
    public function __construct(
        DestinationId $id,
        DestinationAddress $address,
        DestinationAnnotation $annotation,
        DestinationLatitude $latitude,
        DestinationLongitude $longitude,
        DestinationStair $destinationStair,
        DestinationFloor $destinationFloor,
        DestinationDoor $destinationDoor,
        bool $hasElevator,
        bool $hasStepElevator,
        LocationId $locationId
    ) {
        $this->id              = $id;
        $this->address         = $address;
        $this->annotation      = $annotation;
        $this->latitude        = $latitude;
        $this->longitude       = $longitude;
        $this->hasElevator     = $hasElevator;
        $this->hasStepElevator = $hasStepElevator;
        $this->locationId      = $locationId;
        $this->stair           = $destinationStair;
        $this->door            = $destinationDoor;
        $this->floor           = $destinationFloor;
    }

    /**
     * @return DestinationId
     */
    public function id(): DestinationId
    {
        return $this->id;
    }

    /**
     * @return DestinationAddress
     */
    public function address(): DestinationAddress
    {
        return $this->address;
    }

    /**
     * @return DestinationAnnotation
     */
    public function annotation(): DestinationAnnotation
    {
        return $this->annotation;
    }

    /**
     * @return DestinationLatitude
     */
    public function latitude(): DestinationLatitude
    {
        return $this->latitude;
    }

    /**
     * @return DestinationLongitude
     */
    public function longitude(): DestinationLongitude
    {
        return $this->longitude;
    }

    /**
     * @return bool
     */
    public function hasElevator(): bool
    {
        return $this->hasElevator;
    }

    /**
     * @return bool
     */
    public function hasStepElevator(): bool
    {
        return $this->hasStepElevator;
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }

    /**
     * @return DestinationDoor
     */
    public function door(): DestinationDoor
    {
        return $this->door;
    }

    /**
     * @return DestinationFloor
     */
    public function floor(): DestinationFloor
    {
        return $this->floor;
    }

    /**
     * @return DestinationStair
     */
    public function stair(): DestinationStair
    {
        return $this->stair;
    }

    /**
     * @param DestinationLatitude  $latitude
     * @param DestinationLongitude $longitude
     */
    public function changeCoordinates(DestinationLatitude $latitude, DestinationLongitude $longitude): void
    {
        $this->longitude = $longitude;
        $this->latitude  = $latitude;
    }
}
