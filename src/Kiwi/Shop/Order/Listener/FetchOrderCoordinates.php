<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Listener;

use Kiwi\Core\Exception\DataNotFoundDomainException;
use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Map\Map\Document\CoordinatesDocument;
use Kiwi\Map\Map\Query\FetchCoordinates;
use Kiwi\Shop\Order\Domain\DestinationId;
use Kiwi\Shop\Order\Domain\DestinationLatitude;
use Kiwi\Shop\Order\Domain\DestinationLongitude;
use Kiwi\Shop\Order\Event\OnCreateDestinationEvent;
use Kiwi\Shop\Order\Infrastructure\DestinationWriteModel;
use Kiwi\Shop\Order\Infrastructure\OrderReadModel;

/**
 * Class FetchDestinationCoordinates.
 */
class FetchOrderCoordinates implements AsyncListener
{
    /**
     * @var OrderReadModel
     */
    private $readModel;

    /**
     * @var DestinationWriteModel
     */
    private $writeModel;

    /**
     * @var Bus
     */
    private $bus;

    /**
     * FetchOrderCoordinates constructor.
     *
     * @param Bus                   $bus
     * @param OrderReadModel        $readModel
     * @param DestinationWriteModel $writeModel
     */
    public function __construct(Bus $bus, OrderReadModel $readModel, DestinationWriteModel $writeModel)
    {
        $this->readModel  = $readModel;
        $this->writeModel = $writeModel;
        $this->bus        = $bus;
    }

    /**
     * @param Event|OnCreateDestinationEvent $event
     *
     * @throws DataNotFoundDomainException
     */
    public function handle(Event $event): void
    {
        $order = $this->readModel->find($event->orderId());
        if ($order->isEmpty()) {
            throw new DataNotFoundDomainException('Order not found'. $event->orderId()->id());
        }

        /** @var CoordinatesDocument $coordinates */
        $coordinates = $this->bus->dispatchQuery(new FetchCoordinates($event->address()->name()));
        $destination = $this->writeModel->find(new DestinationId($order->destinationId()));

        if (!$destination) {
            throw new DataNotFoundDomainException('Destination not found');
        }

        $destination->changeCoordinates(
            new DestinationLatitude($coordinates->latitude()),
            new DestinationLongitude($coordinates->longitude())
        );

        $this->writeModel->update($destination);
    }

    /**
     * @return string
     */
    public function listensTo(): string
    {
        return OnCreateDestinationEvent::NAME;
    }
}
