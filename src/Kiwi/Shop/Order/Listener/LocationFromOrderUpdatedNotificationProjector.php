<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Listener;

use Kiwi\Core\Domain\Topic;
use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Core\Infrastructure\Socket;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Event\OnUpdateLocationEvent;
use Kiwi\Shop\Order\Infrastructure\OrderReadModel;
use Psr\Log\LoggerInterface;

/**
 * Class LocationFromOrderUpdatedNotificationProjector.
 */
class LocationFromOrderUpdatedNotificationProjector implements AsyncListener
{
    /**
     * @var Socket
     */
    private $socket;

    /**
     * @var OrderReadModel
     */
    private $readModel;

    /**
     * LocationFromOrderUpdatedNotificationProjector constructor.
     *
     * @param Socket         $socket
     * @param OrderReadModel $readModel
     */
    public function __construct()
    {
        return;
        $this->socket    = $socket;
        $this->readModel = $readModel;
    }

    /**
     * @todo fix that
     *
     * @param Event|OnUpdateLocationEvent $event
     */
    public function handle(Event $event): void
    {
        return;
        $document = $this->readModel->findByLocation(new LocationId($event->locationId()));

        foreach ($document->orders() as $order) {
            if ($order->isNotEmpty()) {
                $this->socket->push(
                    new Topic('admin_order_topic', 'update'),
                    $order->toScalar()
                );
            }
        }
    }

    /**
     * @return string
     */
    public function listensTo(): string
    {
        return OnUpdateLocationEvent::NAME;
    }
}
