<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Listener;

use Kiwi\Core\Domain\Topic;
use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Core\Infrastructure\Socket;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\Order\Event\OnCreateOrderEvent;
use Kiwi\Shop\Order\Infrastructure\OrderReadModel;
use Psr\Log\LoggerInterface;

/**
 * Class SendNewOrderNotificationProjector.
 */
class SendNewOrderNotificationProjector implements AsyncListener
{
    /**
     * @var Socket
     */
    private $socket;

    /**
     * @var OrderReadModel
     */
    private $readModel;

    /**
     * SendNewOrderNotificationProjector constructor.
     *
     * @param Socket          $socket
     * @param OrderReadModel  $readModel
     * @param LoggerInterface $logger
     */
    public function __construct()
    {
        return;
        $this->socket    = $socket;
        $this->readModel = $readModel;
    }

    /**
     * @param Event|OnCreateOrderEvent $event
     */
    public function handle(Event $event): void
    {
        return;
        $order = $this->readModel->find(new OrderId($event->orderId()));

        if ($order->isNotEmpty()) {
            $this->socket->push(
                new Topic('admin_order_topic', 'create'),
                $order->toScalar()
            );
        }
    }

    /**
     * @return string
     */
    public function listensTo(): string
    {
        return OnCreateOrderEvent::NAME;
    }
}
