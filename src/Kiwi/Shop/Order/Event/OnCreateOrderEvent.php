<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Event;

use Kiwi\Core\Infrastructure\Event;
use Kiwi\Shop\Order\Domain\OrderId;

/**
 * Class OnCreateOrderEvent.
 */
class OnCreateOrderEvent implements Event
{
    public const NAME = 'OnCreateOrder';

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * OnCreateOrderEvent constructor.
     *
     * @param OrderId $orderId
     */
    public function __construct(OrderId $orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function orderId(): string
    {
        return $this->orderId->id();
    }

    /**
     * @return string
     */
    public static function name(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return \serialize([
            $this->orderId->id()
        ]);
    }

    /**
     * @param string $message
     *
     * @return OnCreateOrderEvent
     */
    public static function deserialize(string $message): self
    {
        [
            $orderId
        ] = unserialize($message, ['allowed_classes' => false]);

        return new self(new OrderId($orderId));
    }
}
