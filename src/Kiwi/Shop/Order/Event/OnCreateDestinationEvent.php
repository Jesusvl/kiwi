<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Event;

use Kiwi\Core\Infrastructure\Event;
use Kiwi\Shop\Order\Domain\DestinationAddress;
use Kiwi\Shop\Order\Domain\OrderId;

/**
 * Class OnCreateDestinationEvent.
 */
class OnCreateDestinationEvent implements Event
{
    public const NAME = 'OnCreateDestination';

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var DestinationAddress
     */
    private $address;

    /**
     * OnCreateDestinationEvent constructor.
     *
     * @param OrderId            $orderId
     * @param DestinationAddress $address
     */
    public function __construct(OrderId $orderId, DestinationAddress $address)
    {
        $this->orderId = $orderId;
        $this->address = $address;
    }

    /**
     * @return OrderId
     */
    public function orderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return DestinationAddress
     */
    public function address(): DestinationAddress
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public static function name(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return \serialize([
            $this->orderId->id(),
            $this->address->name()
        ]);
    }

    /**
     * @param string $message
     *
     * @return OnCreateDestinationEvent
     */
    public static function deserialize(string $message): self
    {
        [
            $orderId,
            $address
        ] = unserialize($message, ['allowed_classes' => false]);

        return new self(new OrderId($orderId), new DestinationAddress($address));
    }
}
