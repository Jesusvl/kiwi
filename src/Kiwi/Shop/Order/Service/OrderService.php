<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Service;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Map\Map\Query\FetchCoordinates;
use Kiwi\Shop\Order\Document\OrderDocument;
use Kiwi\Shop\Order\Domain\Destination;
use Kiwi\Shop\Order\Domain\DestinationLatitude;
use Kiwi\Shop\Order\Domain\DestinationLongitude;
use Kiwi\Shop\Order\Domain\Order;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\Order\Exception\DestinationNotFoundException;
use Kiwi\Shop\Order\Exception\OrderNotFoundException;
use Kiwi\Shop\Order\Exception\TimeNoEligibleException;
use Kiwi\Shop\Order\Infrastructure\DestinationWriteModel;
use Kiwi\Shop\Order\Infrastructure\OrderReadModel;
use Kiwi\Shop\Order\Infrastructure\OrderWriteModel;
use Kiwi\Shop\Store\Document\BaseStoreTimespanDocument;
use Kiwi\Shop\Store\Document\StorePostalCodeDocument;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\TimespanId;
use Kiwi\Shop\Store\Exception\StoreDoesNotContainsPostalCodeException;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;
use Kiwi\Shop\Store\Infrastructure\StoreReadModel;
use Kiwi\Shop\Store\Query\ObtainTimespan;
use Kiwi\Shop\Store\Service\StoreService;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Infrastructure\UserReadModel;

/**
 * Class OrderService.
 */
class OrderService
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var OrderWriteModel
     */
    private $orderWriteModel;

    /**
     * @var OrderReadModel
     */
    private $orderReadModel;

    /**
     * @var StoreService
     */
    private $storeService;

    /**
     * @var DestinationWriteModel
     */
    private $destinationWriteModel;

    /**
     * @var Bus
     */
    private $bus;

    /**
     * @var StoreReadModel
     */
    private $storeReadModel;

    /**
     * OrderService constructor.
     *
     * @param StoreService          $storeService
     * @param OrderWriteModel       $orderWriteModel
     * @param DestinationWriteModel $destinationWriteModel
     * @param OrderReadModel        $orderReadModel
     * @param UserReadModel         $userReadModel
     * @param StoreReadModel        $storeReadModel
     * @param Bus                   $bus
     */
    public function __construct(
        StoreService $storeService,
        OrderWriteModel $orderWriteModel,
        DestinationWriteModel $destinationWriteModel,
        OrderReadModel $orderReadModel,
        UserReadModel $userReadModel,
        StoreReadModel $storeReadModel,
        Bus $bus
    ) {
        $this->orderWriteModel       = $orderWriteModel;
        $this->orderReadModel        = $orderReadModel;
        $this->userReadModel         = $userReadModel;
        $this->storeService          = $storeService;
        $this->destinationWriteModel = $destinationWriteModel;
        $this->bus                   = $bus;
        $this->storeReadModel        = $storeReadModel;
    }

    /**
     * @param OrderId $id
     *
     * @return Order
     *
     * @throws OrderNotFoundException
     */
    public function checkDomain(OrderId $id): Order
    {
        $order = $this->orderWriteModel->find($id);

        if ($order) {
            return $order;
        }

        throw new OrderNotFoundException();
    }

    /**
     * @param OrderId $id
     *
     * @return OrderDocument
     *
     * @throws OrderNotFoundException
     */
    public function checkDocument(OrderId $id): OrderDocument
    {
        $order = $this->orderReadModel->find($id);

        if ($order->isNotEmpty()) {
            return $order;
        }

        throw new OrderNotFoundException();
    }

    /**
     * @param Order       $order
     * @param Destination $destination
     *
     * @throws DestinationNotFoundException
     * @throws TimeNoEligibleException
     * @throws StoreDoesNotContainsPostalCodeException
     */
    public function save(Order $order, Destination $destination): void
    {
        if (!$order->isExpress()) {
            $timespan = $this->bus->dispatchQuery(new ObtainTimespan($order->startTime(), $order->endTime()));

            $storeDoc = $this->storeReadModel->findByIdAndTimespan(
                $order->storeId(),
                new TimespanId($timespan->id())
            );
            if ($storeDoc->isEmpty()) {
                throw new TimeNoEligibleException();
            }

            /** @var BaseStoreTimespanDocument $timeSpanDoc */
            $timeSpanDoc = $storeDoc->timespans()->get(0);
            if ($timeSpanDoc->isClosed()) {
                throw new TimeNoEligibleException();
            }
        }

        $storeDoc    = $this->storeReadModel->find($order->storeId());
        $postalCodes = $storeDoc->postalCodes();
        $found       = false;

        if ($order->destinationId()->id() !== $destination->id()->id()) {
            throw new DestinationNotFoundException();
        }

        /** @var StorePostalCodeDocument $postalCode */
        foreach ($postalCodes as $postalCode) {
            if (strpos($destination->address()->name(), $postalCode->postalCode())) {
                $found = true;
                break;
            }
        }

        /*if (!$found) {
            throw new StoreDoesNotContainsPostalCodeException();
        }*/

        $this->destinationWriteModel->save($destination);
        $this->orderWriteModel->save($order);
    }

    /**
     * @param Order $order
     */
    public function update(Order $order): void
    {
        $this->orderWriteModel->update($order);
    }

    /**
     * @param UserId $userId
     * @param Order  $order
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteOrder(UserId $userId, Order $order): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || ($user->isManager() && $user->storeId() === $order->storeId()->id())) {
            return;
        }

        if ($user->isShipper()) {
            return;
        }

        if ($user->isSad() && $user->storeId() === $order->storeId()->id()) {
            return;
        }

        if ($user->isMerchantAdmin() && $order->merchantId()->id() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId            $userId
     * @param OrderDocument $order
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     */
    public function checkUserCanReadOrder(UserId $userId, OrderDocument $order): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        $store = $this->storeService->checkDocument(new StoreId($order->storeId()));

        if ($user->isAdmin()) {
            return;
        }

        if ($user->isManager() && $user->merchant()->id() === $store->merchantId()) {
            return;
        }

        if ($user->isSad() && $user->storeId() === $order->storeId()) {
            return;
        }

        if ($user->isShipper()) {
            return;
        }

        if ($user->isMerchantAdmin() && $order->merchantId() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }

    /**
     * @param Order $order
     *
     * @throws DestinationNotFoundException
     */
    public function refreshCoordinates(Order $order): void
    {
        $destination = $this->destinationWriteModel->find($order->destinationId());
        if (!$destination) {
            throw new DestinationNotFoundException();
        }
        $coordinates = $this->bus->dispatchQuery(new FetchCoordinates($destination->address()->name()));
        if ($coordinates->isNotEmpty()) {
            $destination->changeCoordinates(
                new DestinationLatitude($coordinates->latitude()),
                new DestinationLongitude($coordinates->longitude())
            );
        }
        $this->destinationWriteModel->update($destination);
    }
}
