<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Order\Command\PutOrderDeliveredStatus;
use Kiwi\Shop\Order\Exception\OrderIsNotInsideException;
use Kiwi\Shop\Order\Exception\OrderNotFoundException;

/**
 * Class PutOrderDeliveredStatusHandler.
 */
class PutOrderDeliveredStatusHandler extends OrderCommandHandler
{
    /**
     * @param PutOrderDeliveredStatus $command
     *
     * @throws InsufficientPermissionsException
     * @throws OrderNotFoundException
     * @throws OrderIsNotInsideException
     */
    public function __invoke(PutOrderDeliveredStatus $command): void
    {
        $order = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteOrder($command->userId(), $order);

        $order->deliver();
        $this->service()->update($order);
    }
}
