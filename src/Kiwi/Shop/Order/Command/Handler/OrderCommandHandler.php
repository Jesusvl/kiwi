<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command\Handler;

use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Shop\Client\Service\ClientService;
use Kiwi\Shop\Location\Service\LocationService;
use Kiwi\Shop\Order\Service\OrderService;

/**
 * Class OrderCommandHandler.
 */
abstract class OrderCommandHandler
{
    /**
     * @var OrderService
     */
    private $service;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var ClientService
     */
    private $clientService;

    /**
     * @var LocationService
     */
    private $locationService;

    /**
     * OrderCommandHandler constructor.
     *
     * @param OrderService    $service
     * @param ClientService   $clientService
     * @param LocationService $locationService
     * @param EventManager    $eventManager
     */
    public function __construct(
        OrderService $service,
        ClientService $clientService,
        LocationService $locationService,
        EventManager $eventManager
    ) {
        $this->service         = $service;
        $this->eventManager    = $eventManager;
        $this->clientService   = $clientService;
        $this->locationService = $locationService;
    }

    /**
     * @return OrderService
     */
    protected function service(): OrderService
    {
        return $this->service;
    }

    /**
     * @return ClientService
     */
    public function clientService(): ClientService
    {
        return $this->clientService;
    }

    /**
     * @return LocationService
     */
    public function locationService(): LocationService
    {
        return $this->locationService;
    }

    /**
     * @return EventManager
     */
    protected function eventManager(): EventManager
    {
        return $this->eventManager;
    }
}
