<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command\Handler;

use Kiwi\Core\Exception\DataNotFoundDomainException;
use Kiwi\Core\Exception\DomainException;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Order\Command\CreateOrder;
use Kiwi\Shop\Order\Domain\Destination;
use Kiwi\Shop\Order\Domain\DestinationAddress;
use Kiwi\Shop\Order\Domain\DestinationAnnotation;
use Kiwi\Shop\Order\Domain\DestinationDoor;
use Kiwi\Shop\Order\Domain\DestinationFloor;
use Kiwi\Shop\Order\Domain\DestinationId;
use Kiwi\Shop\Order\Domain\DestinationLatitude;
use Kiwi\Shop\Order\Domain\DestinationLongitude;
use Kiwi\Shop\Order\Domain\DestinationStair;
use Kiwi\Shop\Order\Domain\Order;
use Kiwi\Shop\Order\Domain\OrderClientName;
use Kiwi\Shop\Order\Domain\OrderClientPhone;
use Kiwi\Shop\Order\Domain\OrderStatus;
use Kiwi\Shop\Order\Event\OnCreateDestinationEvent;
use Kiwi\Shop\Order\Event\OnCreateOrderEvent;

/**
 * Class CreateOrderHandler.
 */
class CreateOrderHandler extends OrderCommandHandler
{
    /**
     * @param CreateOrder $command
     *
     * @throws DataNotFoundDomainException
     * @throws DomainException
     */
    public function __invoke(CreateOrder $command): void
    {
        $client = $this->clientService()->checkDocument($command->clientId());
        $this->clientService()->checkUserCanReadClient($command->userId(), $client);

        $location = $this->locationService()->checkDocument($command->locationId());
        $this->locationService()->checkUserCanReadLocation($command->userId(), $location);

        $address = "{$location->name()}, {$location->postalCode()} {$location->city()} ({$location->province()})";

        $destination = new Destination(
            new DestinationId(),
            new DestinationAddress($address),
            new DestinationAnnotation($location->annotation()),
            new DestinationLatitude($location->latitude()),
            new DestinationLongitude($location->longitude()),
            new DestinationStair($location->stairBuilding()),
            new DestinationFloor($location->floor()),
            new DestinationDoor($location->door()),
            $location->hasElevator(),
            $location->hasStepElevator(),
            new LocationId($location->id())
        );

        $order = new Order(
            $command->id(),
            $command->clientId(),
            new OrderClientName($client->fullName()),
            new OrderClientPhone($client->phone()),
            OrderStatus::delivered(),
            $command->number(),
            $command->cooled(),
            $command->frozen(),
            $command->storeId(),
            $command->merchantId(),
            $command->startTime(),
            $command->endTime(),
            $command->isExpress(),
            $destination->id(),
            $command->boxes(),
            $command->needsPayment()
        );

        $this->service()->save($order, $destination);
        $this->eventManager()->dispatch(new OnCreateOrderEvent($command->id()));
        $this->eventManager()->dispatch(new OnCreateDestinationEvent($command->id(), $destination->address()));
    }
}
