<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Order\Command\PutOrderInsideStatus;
use Kiwi\Shop\Order\Exception\OrderIsNotOngoingException;
use Kiwi\Shop\Order\Exception\OrderNotFoundException;

/**
 * Class PutOrderInsideStatusHandler.
 */
class PutOrderInsideStatusHandler extends OrderCommandHandler
{
    /**
     * @param PutOrderInsideStatus $command
     *
     * @throws InsufficientPermissionsException
     * @throws OrderNotFoundException
     * @throws OrderIsNotOngoingException
     */
    public function __invoke(PutOrderInsideStatus $command): void
    {
        $order = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteOrder($command->userId(), $order);

        $order->inside();
        $this->service()->update($order);
    }
}
