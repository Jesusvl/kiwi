<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Order\Command\RefreshOrderCoordinates;
use Kiwi\Shop\Order\Exception\DestinationNotFoundException;
use Kiwi\Shop\Order\Exception\OrderNotFoundException;

/**
 * Class RefreshOrderCoordinatesHandler.
 */
class RefreshOrderCoordinatesHandler extends OrderCommandHandler
{
    /**
     * @param RefreshOrderCoordinates $command
     *
     * @throws InsufficientPermissionsException
     * @throws OrderNotFoundException
     * @throws DestinationNotFoundException
     */
    public function __invoke(RefreshOrderCoordinates $command): void
    {
        $order = $this->service()->checkDomain($command->orderId());
        $this->service()->checkUserCanWriteOrder($command->userId(), $order);
        $this->service()->refreshCoordinates($order);
    }
}
