<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Order\Command\PutOrderOnGoingStatus;
use Kiwi\Shop\Order\Exception\OrderIsNotPendingException;
use Kiwi\Shop\Order\Exception\OrderNotFoundException;

/**
 * Class PutOrderOnGoingHandler.
 */
class PutOrderOnGoingStatusHandler extends OrderCommandHandler
{
    /**
     * @param PutOrderOnGoingStatus $command
     *
     * @throws InsufficientPermissionsException
     * @throws OrderNotFoundException
     * @throws OrderIsNotPendingException
     */
    public function __invoke(PutOrderOnGoingStatus $command): void
    {
        $order = $this->service()->checkDomain($command->orderId());
        $this->service()->checkUserCanWriteOrder($command->userId(), $order);
        $this->service()->checkUserCanWriteOrder($command->shipperId(), $order);

        $order->ongoing($command->userId());
        $this->service()->update($order);
    }
}
