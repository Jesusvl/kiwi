<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class PutOrderInsideStatus.
 */
class PutOrderInsideStatus implements Command
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * PutOrderOnGoing constructor.
     *
     * @param string $userId
     * @param string $id
     */
    public function __construct(string $userId, string $id)
    {
        $this->id     = new OrderId($id);
        $this->userId = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return OrderId
     */
    public function id(): OrderId
    {
        return $this->id;
    }
}
