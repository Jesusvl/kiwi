<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class RefreshOrderCoordinates.
 */
class RefreshOrderCoordinates implements Command
{
    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * RefreshOrderCoordinates constructor.
     *
     * @param string $userId
     * @param string $orderId
     */
    public function __construct(string $userId, string $orderId)
    {
        $this->orderId = new OrderId($orderId);
        $this->userId  = new UserId($userId);
    }

    /**
     * @return OrderId
     */
    public function orderId(): OrderId
    {
        return $this->orderId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
