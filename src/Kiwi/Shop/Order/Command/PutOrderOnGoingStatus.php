<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class PutOrderOnGoing.
 */
class PutOrderOnGoingStatus implements Command
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var UserId
     */
    private $shipperId;

    /**
     * PutOrderOnGoingStatus constructor.
     *
     * @param string $userId
     * @param string $shipperId
     * @param string $orderId
     */
    public function __construct(string $userId, string $shipperId, string $orderId)
    {
        $this->orderId   = new OrderId($orderId);
        $this->userId    = new UserId($userId);
        $this->shipperId = new UserId($shipperId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return UserId
     */
    public function shipperId(): UserId
    {
        return $this->shipperId;
    }

    /**
     * @return OrderId
     */
    public function orderId(): OrderId
    {
        return $this->orderId;
    }
}
