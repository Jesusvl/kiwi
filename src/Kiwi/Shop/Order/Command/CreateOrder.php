<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Order\Domain\OrderBoxesQuantity;
use Kiwi\Shop\Order\Domain\OrderFrozenQuantity;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\Order\Domain\OrderNumber;
use Kiwi\Shop\Order\Domain\OrderRefrigeratedQuantity;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;
use KiwiLib\DateTime\DateTime;

/**
 * Class CreateOrder.
 */
class CreateOrder implements Command
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var OrderNumber
     */
    private $number;

    /**
     * @var OrderRefrigeratedQuantity
     */
    private $cooled;

    /**
     * @var OrderFrozenQuantity
     */
    private $frozen;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * @var bool
     */
    private $isExpress;

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * @var OrderBoxesQuantity
     */
    private $boxes;

    /**
     * @var ClientId
     */
    private $clientId;

    /**
     * @var bool
     */
    private $needsPayment;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * CreateOrder constructor.
     *
     * @param string   $userId
     * @param string   $id
     * @param string   $clientId
     * @param string   $number
     * @param int      $cooled
     * @param int      $frozen
     * @param string   $storeId
     * @param string   $merchantId
     * @param DateTime $startTime
     * @param DateTime $endTime
     * @param bool     $isExpress
     * @param string   $locationId
     * @param int      $boxes
     * @param bool     $needsPayment
     */
    public function __construct(
        string $userId,
        string $id,
        string $clientId,
        string $number,
        int $cooled,
        int $frozen,
        string $storeId,
        string $merchantId,
        DateTime $startTime,
        DateTime $endTime,
        bool $isExpress,
        string $locationId,
        int $boxes,
        bool $needsPayment
    ) {
        $this->id           = new OrderId($id);
        $this->number       = new OrderNumber($number);
        $this->cooled       = new OrderRefrigeratedQuantity($cooled);
        $this->frozen       = new OrderFrozenQuantity($frozen);
        $this->storeId      = new StoreId($storeId);
        $this->merchantId   = new MerchantId($merchantId);
        $this->startTime    = $startTime;
        $this->endTime      = $endTime;
        $this->isExpress    = $isExpress;
        $this->locationId   = new LocationId($locationId);
        $this->boxes        = new OrderBoxesQuantity($boxes);
        $this->clientId     = new ClientId($clientId);
        $this->needsPayment = $needsPayment;
        $this->userId       = new UserId($userId);
    }

    /**
     * @return OrderId
     */
    public function id(): OrderId
    {
        return $this->id;
    }

    /**
     * @return OrderNumber
     */
    public function number(): OrderNumber
    {
        return $this->number;
    }

    /**
     * @return OrderRefrigeratedQuantity
     */
    public function cooled(): OrderRefrigeratedQuantity
    {
        return $this->cooled;
    }

    /**
     * @return OrderFrozenQuantity
     */
    public function frozen(): OrderFrozenQuantity
    {
        return $this->frozen;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return bool
     */
    public function isExpress(): bool
    {
        return $this->isExpress;
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }

    /**
     * @return OrderBoxesQuantity
     */
    public function boxes(): OrderBoxesQuantity
    {
        return $this->boxes;
    }

    /**
     * @return ClientId
     */
    public function clientId(): ClientId
    {
        return $this->clientId;
    }

    /**
     * @return bool
     */
    public function needsPayment(): bool
    {
        return $this->needsPayment;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
