<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Document;

use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Order\Domain\Order;
use Kiwi\Shop\Store\Document\BaseStoreDocument;
use Kiwi\Shop\User\Document\BaseUserDocument;

/**
 * Class OrderDocument.
 */
class OrderDocument extends BaseOrderDocument
{
    /**
     * @var LocationDocument
     */
    private $destination;

    /**
     * @var BaseStoreDocument
     */
    private $store;

    /**
     * @var BaseUserDocument
     */
    private $shipper;

    /**
     * @var MerchantDocument
     */
    private $merchant;

    /**
     * OrderDocument constructor.
     *
     * @param Order|null               $order
     * @param OrderDestinationDocument $destination
     * @param BaseStoreDocument        $store
     * @param BaseUserDocument         $shipper
     * @param MerchantDocument         $merchant
     */
    public function __construct(
        ?Order $order,
        OrderDestinationDocument $destination,
        BaseStoreDocument $store,
        BaseUserDocument $shipper,
        MerchantDocument $merchant
    ) {
        parent::__construct($order);
        $this->order       = $order;
        $this->destination = $destination;
        $this->store       = $store;
        $this->shipper     = $shipper;
        $this->merchant    = $merchant;
    }

    /**
     * @return OrderDocument
     */
    public static function empty(): self
    {
        return new self(
            null,
            OrderDestinationDocument::empty(),
            BaseStoreDocument::empty(),
            BaseUserDocument::empty(),
            MerchantDocument::empty()
        );
    }

    /**
     * @return OrderDestinationDocument
     */
    public function destination(): OrderDestinationDocument
    {
        return $this->destination;
    }

    /**
     * @return BaseStoreDocument
     */
    public function store(): BaseStoreDocument
    {
        return $this->store;
    }

    /**
     * @return MerchantDocument
     */
    public function merchant(): MerchantDocument
    {
        return $this->merchant;
    }

    /**
     * @return BaseUserDocument
     */
    public function shipper(): BaseUserDocument
    {
        return $this->shipper;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->mergeDocumentArray(
            parent::toScalar(),
            [
                'destination' => $this->destination()->toScalar(),
                'store'       => $this->store()->toScalar(),
                'shipper'     => $this->shipper()->toScalar(),
                'merchant'    => $this->merchant()->toScalar()
            ]
        );
    }
}
