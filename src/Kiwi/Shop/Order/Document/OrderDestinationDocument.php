<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Document;

use Kiwi\Core\Document\Document;
use Kiwi\Shop\Order\Domain\Destination;

/**
 * Class OrderDestinationDocument.
 */
class OrderDestinationDocument extends Document
{
    /**
     * @var Destination|null
     */
    protected $destination;

    /**
     * OrderDestinationDocument constructor.
     *
     * @param Destination|null $destination
     */
    public function __construct(?Destination $destination)
    {
        parent::__construct($destination);
        $this->destination = $destination;
    }

    /**
     * @return OrderDestinationDocument
     */
    public static function empty(): self
    {
        return new self(null);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->destination->id()->id();
    }

    /**
     * @return string
     */
    public function address(): string
    {
        return $this->destination->address()->name();
    }

    /**
     * @return string
     */
    public function annotation(): string
    {
        return $this->destination->annotation()->message();
    }

    /**
     * @return float
     */
    public function latitude(): float
    {
        return $this->destination->latitude()->value();
    }

    /**
     * @return float
     */
    public function longitude(): float
    {
        return $this->destination->longitude()->value();
    }

    /**
     * @return bool
     */
    public function hasElevator(): bool
    {
        return $this->destination->hasElevator();
    }

    /**
     * @return bool
     */
    public function hasStepElevator(): bool
    {
        return $this->destination->hasStepElevator();
    }

    /**
     * @return string
     */
    public function floor(): string
    {
        return $this->destination->floor()->floor();
    }

    /**
     * @return string
     */
    public function stair(): string
    {
        return $this->destination->stair()->name();
    }

    /**
     * @return string
     */
    public function door(): string
    {
        return $this->destination->door()->door();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->destination ? [
            'id'              => $this->id(),
            'address'         => $this->address(),
            'annotaion'       => $this->annotation(),
            'latitude'        => $this->latitude(),
            'longitude'       => $this->longitude(),
            'hasElevator'     => $this->hasElevator(),
            'hasStepElevator' => $this->hasStepElevator(),
            'stair'           => $this->stair(),
            'floor'           => $this->floor(),
            'door'            => $this->door()
        ] : null;
    }
}
