<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Order\Query\FindOrdersByDate;
use Kiwi\Shop\Store\Domain\StoreId;

/**
 * Class FindOrdersByDateHandler.
 */
class FindOrdersByDateHandler extends OrderQueryHandler
{
    /**
     * @param FindOrdersByDate $query
     *
     * @return PaginatedDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindOrdersByDate $query): PaginatedDocument
    {
        $user = $this->service()->checkUser($query->userId());

        if ($user->isShipper()) {
            return $this->readModel()->findNonDeliveredByDateAndMerchant(
                $query->date(),
                new MerchantId($user->merchantId()),
                $query->pagination(),
                $query->filters(),
                $query->parts(),
                $query->sort()
            );
        }

        if ($user->isAdmin()) {
            return $this->readModel()->findByDate(
                $query->date(),
                $query->pagination(),
                $query->filters(),
                $query->parts(),
                $query->sort()
            );
        }

        if ($user->isManager() || $user->isSad() ) {
            return $this->readModel()->findByDateAndStore(
                $query->date(),
                new StoreId($user->storeId()),
                $query->pagination(),
                $query->filters(),
                $query->parts(),
                $query->sort()
            );
        }

        if ($user->isMerchantAdmin()) {
            return $this->readModel()->findByDateAndMerchant(
                $query->date(),
                new MerchantId($user->merchantId()),
                $query->pagination(),
                $query->filters(),
                $query->parts(),
                $query->sort()
            );
        }

        throw new InsufficientPermissionsException();
    }
}
