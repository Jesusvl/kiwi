<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Order\Document\OrderDocument;
use Kiwi\Shop\Order\Query\FindOrder;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class FindOrderHandler.
 */
class FindOrderHandler extends OrderQueryHandler
{
    /**
     * @param FindOrder $query
     *
     * @return OrderDocument
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     */
    public function __invoke(FindOrder $query): OrderDocument
    {
        $order = $this->readModel()->find($query->id(), $query->parts());
        $this->service()->checkUserCanReadOrder($query->userId(), $order);

        return $order;
    }
}
