<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Shop\Order\Query\FindBadDestinations;

/**
 * Class FindBadDestinationsQueryHandler.
 */
class FindBadDestinationsHandler extends DestinationQueryHandler
{
    /**
     * @param FindBadDestinations $query
     *
     * @return PaginatedDocument
     */
    public function __invoke(FindBadDestinations $query): PaginatedDocument
    {
        return $this->readModel()->findBadDestinations($query->filter());
    }
}
