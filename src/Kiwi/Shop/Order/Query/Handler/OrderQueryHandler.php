<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query\Handler;

use Kiwi\Core\Infrastructure\QueryHandler;
use Kiwi\Shop\Order\Infrastructure\OrderReadModel;
use Kiwi\Shop\Order\Service\OrderService;

/**
 * Class OrderQueryHandler.
 */
abstract class OrderQueryHandler implements QueryHandler
{
    /**
     * @var OrderService
     */
    private $service;

    /**
     * @var OrderReadModel
     */
    private $readModel;

    /**
     * OrderQueryHandler constructor.
     *
     * @param OrderService   $service
     * @param OrderReadModel $readModel
     */
    public function __construct(OrderService $service, OrderReadModel $readModel)
    {
        $this->service   = $service;
        $this->readModel = $readModel;
    }

    /**
     * @return OrderService
     */
    protected function service(): OrderService
    {
        return $this->service;
    }

    /**
     * @return OrderReadModel
     */
    protected function readModel(): OrderReadModel
    {
        return $this->readModel;
    }
}
