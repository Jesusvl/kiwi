<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query\Handler;

use Kiwi\Core\Infrastructure\QueryHandler;
use Kiwi\Shop\Order\Infrastructure\DestinationReadModel;

/**
 * Class DestinationQueryHandler.
 */
abstract class DestinationQueryHandler implements QueryHandler
{
    /**
     * @var DestinationReadModel
     */
    private $readModel;

    /**
     * DestinationQueryHandler constructor.
     *
     * @param DestinationReadModel $readModel
     */
    public function __construct(DestinationReadModel $readModel)
    {
        $this->readModel = $readModel;
    }

    /**
     * @return DestinationReadModel
     */
    protected function readModel(): DestinationReadModel
    {
        return $this->readModel;
    }
}
