<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\Order\Domain\OrderParts;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindOrder.
 */
class FindOrder implements Query
{
    /**
     * @var OrderId
     */
    private $id;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var OrderParts
     */
    private $parts;

    /**
     * FindOrder constructor.
     *
     * @param string $userId
     * @param string $id
     * @param array  $parts
     */
    public function __construct(string $userId, string $id, array $parts = [])
    {
        $this->id = new OrderId($id);
        $this->userId = new UserId($userId);
        $this->parts = new OrderParts($parts);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return OrderId
     */
    public function id(): OrderId
    {
        return $this->id;
    }

    /**
     * @return OrderParts
     */
    public function parts(): OrderParts
    {
        return $this->parts;
    }
}
