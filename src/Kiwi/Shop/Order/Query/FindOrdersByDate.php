<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query;

use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Exception\InvalidFilterException;
use Kiwi\Core\Exception\InvalidSortArgumentException;
use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Order\Domain\OrderFilter;
use Kiwi\Shop\Order\Domain\OrderParts;
use Kiwi\Shop\Order\Domain\OrderSort;
use Kiwi\Shop\User\Domain\UserId;
use KiwiLib\DateTime\DateTime;

/**
 * Class FindOrdersByDate.
 */
class FindOrdersByDate implements Query
{
    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var Pagination
     */
    private $filter;

    /**
     * @var OrderParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var OrderFilter[]
     */
    private $filters;

    /**
     * @var OrderSort
     */
    private $sort;

    /**
     * FindOrdersByDate constructor.
     *
     * @param string   $userId
     * @param DateTime $date
     * @param int      $page
     * @param int      $pageSize
     * @param array    $parts
     * @param array    $filters
     * @param array    $sort
     *
     * @throws InvalidSortArgumentException
     * @throws InvalidFilterException
     */
    public function __construct(
        string $userId,
        DateTime $date,
        int $page,
        int $pageSize,
        array $parts,
        array $filters,
        array $sort = []
    ) {
        $this->date = $date;
        $this->filter = new Pagination($page, $pageSize);
        $this->parts = new OrderParts($parts);
        $this->userId = new UserId($userId);
        $this->sort = new OrderSort($sort[0] ?? 'startTime', $sort[1] ?? 'ASC');
        $this->filters = [];

        foreach ($filters as $filter) {
            $this->filters[] = new OrderFilter($filter[0], $filter[1], $filter[2]);
        }
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return DateTime
     */
    public function date(): DateTime
    {
        return $this->date;
    }

    /**
     * @return OrderParts
     */
    public function parts(): OrderParts
    {
        return $this->parts;
    }

    /**
     * @return Pagination
     */
    public function pagination(): Pagination
    {
        return $this->filter;
    }

    /**
     * @return OrderSort
     */
    public function sort(): OrderSort
    {
        return $this->sort;
    }

    /**
     * @return OrderFilter[]
     */
    public function filters(): array
    {
        return $this->filters;
    }
}
