<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Query;

use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindBadDestinations.
 */
class FindBadDestinations implements Query
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var Pagination
     */
    private $filter;

    /**
     * FindBadDestinations constructor.
     *
     * @param string $userId
     * @param int    $page
     * @param int    $pageSize
     */
    public function __construct(string $userId, int $page, int $pageSize)
    {
        $this->userId = new UserId($userId);
        $this->filter = new Pagination($page, $pageSize);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return Pagination
     */
    public function filter(): Pagination
    {
        return $this->filter;
    }
}
