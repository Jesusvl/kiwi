<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Infrastructure;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;

/**
 * Interface DestinationReadModel.
 */
interface DestinationReadModel
{
    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findBadDestinations(Pagination $filter): PaginatedDocument;
}
