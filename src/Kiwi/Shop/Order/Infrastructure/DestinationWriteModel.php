<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Infrastructure;

use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Order\Domain\Destination;
use Kiwi\Shop\Order\Domain\DestinationId;

/**
 * Interface DestinationWriteModel.
 */
interface DestinationWriteModel
{
    /**
     * @param DestinationId $id
     *
     * @return Destination|null
     */
    public function find(DestinationId $id): ?Destination;

    /**
     * @param Destination $destination
     */
    public function save(Destination $destination): void;

    /**
     * @param Destination $destination
     */
    public function update(Destination $destination): void;

    /**
     * @param LocationId $locationId
     *
     * @return Destination[]
     */
    public function findByLocation(LocationId $locationId): array;
}
