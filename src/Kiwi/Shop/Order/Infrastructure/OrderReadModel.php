<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Infrastructure;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Order\Document\OrderDocument;
use Kiwi\Shop\Order\Domain\OrderFilter;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\Order\Domain\OrderParts;
use Kiwi\Shop\Order\Domain\OrderSort;
use Kiwi\Shop\Store\Domain\StoreId;
use KiwiLib\DateTime\DateTime;

/**
 * Interface OrderReadModel.
 */
interface OrderReadModel
{
    /**
     * @param OrderId    $id
     * @param OrderParts $parts
     *
     * @return OrderDocument
     */
    public function find(OrderId $id, ?OrderParts $parts = null): OrderDocument;

    /**
     * @param DateTime        $date
     * @param Pagination      $pagination
     * @param OrderFilter[]   $filters
     * @param OrderParts|null $parts
     * @param OrderSort|null  $sortBy
     *
     * @return PaginatedDocument
     */
    public function findByDate(
        DateTime $date,
        Pagination $pagination,
        array $filters,
        ?OrderParts $parts = null,
        ?OrderSort $sortBy = null
    ): PaginatedDocument;

    /**
     * @param DateTime        $date
     * @param StoreId         $store
     * @param Pagination      $pagination
     * @param Filter[]        $filters
     * @param OrderParts|null $parts
     * @param OrderSort|null  $sortBy
     *
     * @return PaginatedDocument
     */
    public function findByDateAndStore(
        DateTime $date,
        StoreId $store,
        Pagination $pagination,
        array $filters,
        ?OrderParts $parts = null,
        ?OrderSort $sortBy = null
    ): PaginatedDocument;

    /**
     * @param DateTime        $date
     * @param MerchantId      $store
     * @param Pagination      $pagination
     * @param Filter[]        $filtes
     * @param OrderParts|null $parts
     * @param OrderSort|null  $sortBy
     *
     * @return PaginatedDocument
     */
    public function findByDateAndMerchant(
        DateTime $date,
        MerchantId $store,
        Pagination $pagination,
        array $filtes,
        ?OrderParts $parts = null,
        ?OrderSort $sortBy = null
    ): PaginatedDocument;

    /**
     * @param DateTime       $date
     * @param MerchantId     $store
     * @param Pagination     $pagination
     * @param Filter[]       $filters
     * @param OrderParts     $parts
     * @param OrderSort|null $sortBy
     *
     * @return PaginatedDocument
     */
    public function findNonDeliveredByDateAndMerchant(
        DateTime $date,
        MerchantId $store,
        Pagination $pagination,
        array $filters,
        OrderParts $parts,
        ?OrderSort $sortBy = null
    ): PaginatedDocument;
}
