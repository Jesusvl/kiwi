<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Infrastructure;

use Kiwi\Shop\Order\Domain\Order;
use Kiwi\Shop\Order\Domain\OrderId;

/**
 * Interface OrderWriteModel.
 */
interface OrderWriteModel
{
    /**
     * @param OrderId $id
     *
     * @return Order|null
     */
    public function find(OrderId $id): ?Order;

    /**
     * @param Order $order
     */
    public function save(Order $order): void;

    /**
     * @param Order $order
     */
    public function update(Order $order): void;
}
