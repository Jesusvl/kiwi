<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Exception;

use Kiwi\Core\Exception\DataNotFoundDomainException;

/**
 * Class OrderNotFoundException.
 */
class OrderNotFoundException extends DataNotFoundDomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Order not found.');
    }
}
