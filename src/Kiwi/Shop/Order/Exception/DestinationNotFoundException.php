<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Exception;

use Kiwi\Core\Exception\DataNotFoundDomainException;

/**
 * Class DestinationNotFoundException.
 */
class DestinationNotFoundException extends DataNotFoundDomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Destination not found.');
    }
}
