<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class OrderIsNotOngoingException.
 */
class OrderIsNotOngoingException extends DomainException
{
    public function __construct()
    {
        parent::__construct('Order must be ongoing to be able to set inside');
    }
}
