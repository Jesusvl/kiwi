<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class OrderIsNotPendingException.
 */
class OrderIsNotPendingException extends DomainException
{
    public function __construct()
    {
        parent::__construct('Order must be pending to be able to set ongoing');
    }
}
