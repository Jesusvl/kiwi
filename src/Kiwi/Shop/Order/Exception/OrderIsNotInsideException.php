<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class OrderIsNotInsideException.
 */
class OrderIsNotInsideException extends DomainException
{
    public function __construct()
    {
        parent::__construct('Order must be inside to be able to set delivered');
    }
}
