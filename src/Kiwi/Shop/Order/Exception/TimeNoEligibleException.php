<?php

declare(strict_types=1);

namespace Kiwi\Shop\Order\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class TimeNoEligibleException.
 */
class TimeNoEligibleException extends DomainException
{
    public function __construct()
    {
        parent::__construct('Order cannot be delivered in this timespan');
    }
}
