<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Document;

use Kiwi\Core\Document\Document;
use Kiwi\Shop\Store\Domain\Store;

/**
 * Class BaseStoreDocument.
 */
class BaseStoreDocument extends Document
{
    /**
     * @var Store
     */
    protected $store;

    /**
     * StoreDocument constructor.
     *
     * @param Store|null $store
     */
    public function __construct(?Store $store)
    {
        parent::__construct($store);
        $this->store = $store;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->store->id()->id();
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->store->name()->name();
    }

    /**
     * @return string
     */
    public function merchantId(): string
    {
        return $this->store->merchantId()->id();
    }

    /**
     * @return string|null
     */
    public function userId(): ?string
    {
        return $this->store->userId() ? $this->store->userId()->id() : null;
    }

    /**
     * @return string|null
     */
    public function locationId(): ?string
    {
        return $this->store->locationId()
            ? $this->store->locationId()->id()
            : null;
    }

    /**
     * @return bool
     */
    public function isExpressEnabled(): bool
    {
        return $this->store->isExpressEnabled();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->store ? [
            'id'               => $this->id(),
            'name'             => $this->name(),
            'location'         => $this->locationId(),
            'user'             => $this->userId(),
            'merchant'         => $this->merchantId(),
            'isExpressEnabled' => $this->isExpressEnabled()
        ] : null;
    }
}
