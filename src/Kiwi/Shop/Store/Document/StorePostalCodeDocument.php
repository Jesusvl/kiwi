<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Document;

use Kiwi\Core\Document\Document;
use Kiwi\Shop\Store\Domain\StorePostalCode;

/**
 * Class StorePostalCodeDocument.
 */
class StorePostalCodeDocument extends Document
{
    /**
     * @var StorePostalCode|null
     */
    private $postalCode;

    /**
     * StorePostalCodeDocument constructor.
     *
     * @param StorePostalCode|null $postalCode
     */
    public function __construct(?StorePostalCode $postalCode)
    {
        parent::__construct($postalCode);
        $this->postalCode = $postalCode;
    }

    /**
     * @return StorePostalCodeDocument
     */
    public static function empty(): self
    {
        return new self(null);
    }

    /**
     * @return string
     */
    public function postalCode(): string
    {
        return $this->postalCode->postalCode()->postalCode();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->postalCode ? [
            'postalCode' => $this->postalCode()
        ] : null;
    }
}
