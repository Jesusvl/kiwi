<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Document;

use Kiwi\Core\Document\Document;
use Kiwi\Shop\Store\Domain\StoreTimespan;
use Kiwi\Shop\Store\Domain\StoreTimespanId;
use KiwiLib\DateTime\DateTime;

/**
 * Class BaseStoreTimespanDocument.
 */
class BaseStoreTimespanDocument extends Document
{
    /**
     * @var BaseTimespanDocument
     */
    private $timespan;

    /**
     * @var StoreTimespan|null
     */
    private $storeTimespan;

    /**
     * BaseStoreTimespanDocument constructor.
     *
     * @param StoreTimespan|null   $storeTimespan
     * @param BaseTimespanDocument $timespan
     */
    public function __construct(?StoreTimespan $storeTimespan, BaseTimespanDocument $timespan)
    {
        parent::__construct($storeTimespan);
        $this->timespan      = $timespan;
        $this->storeTimespan = $storeTimespan;
    }

    /**
     * @return BaseStoreTimespanDocument
     */
    public static function empty(): self
    {
        return new self(null, new BaseTimespanDocument(null));
    }

    /**
     * @return StoreTimespanId
     */
    public function id(): StoreTimespanId
    {
        return $this->storeTimespan->id();
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->storeTimespan->isClosed();
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->timespan->startTime();
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->timespan->endTime();
    }

    /**
     * @param BaseStoreTimespanDocument $other
     *
     * @return int
     */
    public function compareTo(BaseStoreTimespanDocument $other): int
    {
        if ($this->startTime() > $other->startTime()) {
            return 1;
        }

        if ($this->startTime() < $other->startTime()) {
            return -1;
        }

        if ($this->endTime() > $other->endTime()) {
            return 1;
        }

        if ($this->endTime() < $other->endTime()) {
            return -1;
        }

        return 0;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return [
            'id'        => $this->id()->id(),
            'isClosed'  => $this->isClosed(),
            'startTime' => $this->startTime()->format(DATE_ATOM),
            'endTime'   => $this->endTime()->format(DATE_ATOM)
        ];
    }
}
