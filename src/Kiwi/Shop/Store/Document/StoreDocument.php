<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Document;

use Kiwi\Core\Document\DocumentBook;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Store\Domain\Store;

/**
 * Class StoreDocument.
 */
final class StoreDocument extends BaseStoreDocument
{
    /**
     * @var LocationDocument
     */
    private $location;

    /**
     * @var DocumentBook
     */
    private $timespans;

    /**
     * @var DocumentBook
     */
    private $postalCodes;

    /**
     * @return StoreDocument
     */
    public static function empty(): StoreDocument
    {
        return new StoreDocument(
            null,
            LocationDocument::empty(),
            new DocumentBook(),
            new DocumentBook()
        );
    }

    /**
     * StoreDocument constructor.
     *
     * @param Store|null       $store
     * @param LocationDocument $location
     * @param DocumentBook     $timespans
     * @param DocumentBook     $postalCodes
     */
    public function __construct(
        ?Store $store,
        LocationDocument $location,
        DocumentBook $timespans,
        DocumentBook $postalCodes
    ) {
        parent::__construct($store);
        $this->location    = $location;
        $this->timespans   = $timespans;
        $this->postalCodes = $postalCodes;
    }

    /**
     * @return LocationDocument
     */
    public function location(): LocationDocument
    {
        return $this->location;
    }

    /**
     * @return DocumentBook
     */
    public function timespans(): DocumentBook
    {
        return $this->timespans;
    }

    /**
     * @return DocumentBook
     */
    public function postalCodes(): DocumentBook
    {
        return $this->postalCodes;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->mergeDocumentArray(
            parent::toScalar(),
            [
                'location'    => $this->location()->toScalar(),
                'timespans'   => $this->timespans()->toScalar(),
                'postalCodes' => $this->postalCodes()->toScalar()
            ]
        );
    }
}
