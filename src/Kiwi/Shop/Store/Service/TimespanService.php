<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Service;

use Kiwi\Shop\Store\Document\BaseTimespanDocument;
use Kiwi\Shop\Store\Domain\Timespan;
use Kiwi\Shop\Store\Infrastructure\TimespanReadModel;
use Kiwi\Shop\Store\Infrastructure\TimespanWriteModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class TimespanService.
 */
class TimespanService
{
    /**
     * @var TimespanWriteModel
     */
    private $timespanWriteModel;

    /**
     * @var TimespanReadModel
     */
    private $timespanReadModel;

    /**
     * TimespanService constructor.
     *
     * @param TimespanWriteModel $timespanWriteModel
     * @param TimespanReadModel  $timespanReadModel
     */
    public function __construct(
        TimespanWriteModel $timespanWriteModel,
        TimespanReadModel $timespanReadModel
    ) {
        $this->timespanWriteModel = $timespanWriteModel;
        $this->timespanReadModel  = $timespanReadModel;
    }

    /**
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return BaseTimespanDocument
     */
    public function fetch(DateTime $start, DateTime $end): BaseTimespanDocument
    {
        $timespan = $this->timespanReadModel->find($start, $end);

        if ($timespan->isEmpty()) {
            $timespanDomain = new Timespan($start, $end);
            $this->timespanWriteModel->save($timespanDomain);
            $timespan = new BaseTimespanDocument($timespanDomain);
        }

        return $timespan;
    }
}
