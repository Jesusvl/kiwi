<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Service;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Shop\Store\Document\BaseTimespanDocument;
use Kiwi\Shop\Store\Domain\PostalCode;
use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StoreParts;
use Kiwi\Shop\Store\Domain\StorePostalCode;
use Kiwi\Shop\Store\Domain\StoreTimespanId;
use Kiwi\Shop\Store\Domain\TimespanId;
use Kiwi\Shop\Store\Exception\ShopAlreadyHasTimestampException;
use Kiwi\Shop\Store\Exception\StoreAlreadyContainsPostalCodeException;
use Kiwi\Shop\Store\Exception\StoreDoesNotContainsPostalCodeException;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;
use Kiwi\Shop\Store\Infrastructure\StoreReadModel;
use Kiwi\Shop\Store\Infrastructure\StoreWriteModel;
use Kiwi\Shop\Store\Query\ObtainTimespan;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Infrastructure\UserReadModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class StoreService.
 */
class StoreService
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var StoreWriteModel
     */
    private $storeWriteModel;

    /**
     * @var StoreReadModel
     */
    private $storeReadModel;

    /**
     * @var Bus
     */
    private $bus;

    /**
     * StoreService constructor.
     *
     * @param StoreWriteModel $storeWriteModel
     * @param StoreReadModel  $storeReadModel
     * @param UserReadModel   $userReadModel
     * @param Bus             $bus
     */
    public function __construct(
        StoreWriteModel $storeWriteModel,
        StoreReadModel $storeReadModel,
        UserReadModel $userReadModel,
        Bus $bus
    ) {
        $this->storeWriteModel = $storeWriteModel;
        $this->storeReadModel  = $storeReadModel;
        $this->userReadModel   = $userReadModel;
        $this->bus             = $bus;
    }

    /**
     * @param StoreId $id
     *
     * @return Store
     *
     * @throws StoreNotFoundException
     */
    public function checkDomain(StoreId $id): Store
    {
        $store = $this->storeWriteModel->find($id);

        if ($store) {
            return $store;
        }

        throw new StoreNotFoundException();
    }

    /**
     * @param StoreId         $id
     * @param StoreParts|null $parts
     *
     * @return StoreDocument
     *
     * @throws StoreNotFoundException
     */
    public function checkDocument(StoreId $id, ?StoreParts $parts = null): StoreDocument
    {
        $parts = $parts ?? new StoreParts();
        $store = $this->storeReadModel->find($id, $parts);

        if ($store->isNotEmpty()) {
            return $store;
        }

        throw new StoreNotFoundException();
    }

    /**
     * @param Store $store
     */
    public function save(Store $store): void
    {
        $this->storeWriteModel->save($store);
    }

    /**
     * @param Store $store
     */
    public function update(Store $store): void
    {
        $this->storeWriteModel->update($store);
    }

    /**
     * @param Store    $store
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function addTimespan(Store $store, DateTime $startTime, DateTime $endTime): void
    {
        /** @var BaseTimespanDocument $timespan */
        $timespan = $this->bus->dispatchQuery(new ObtainTimespan($startTime, $endTime));

        if ($this->storeReadModel->findByIdAndTimespan(
            $store->id(),
            new TimespanId($timespan->id())
        )->isNotEmpty()) {
            throw new ShopAlreadyHasTimestampException();
        }

        $this->storeWriteModel->addTimestamp($store->id(), new TimespanId($timespan->id()));
    }

    /**
     * @param UserId $userId
     * @param Store  $store
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteStore(UserId $userId, Store $store): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin()) {
            return;
        }

        if ($user->isManager() && $user->storeId() === $store->id()->id()) {
            return;
        }

        if ($user->isMerchantAdmin() && $store->merchantId()->id() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId        $userId
     * @param StoreDocument $store
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadStore(UserId $userId, StoreDocument $store): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin()) {
            return;
        }

        if ($user->isManager() && $user->storeId() === $store->id()) {
            return;
        }

        if ($user->isSad() && $store->merchantId() === $user->merchantId()) {
            return;
        }

        if ($user->isShipper() && $store->merchantId() === $user->merchantId()) {
            return;
        }

        if ($user->isMerchantAdmin() && $store->merchantId() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }

    /**
     * @todo improve
     *
     * @param StoreTimespanId $timespanId
     */
    public function closeTimestamp(StoreTimespanId $timespanId): void
    {
        $this->storeWriteModel->closeTimestamp($timespanId);
    }

    /**
     * @todo improve
     *
     * @param StoreTimespanId $timespanId
     */
    public function openTimestamp(StoreTimespanId $timespanId): void
    {
        $this->storeWriteModel->openTimestamp($timespanId);
    }

    /**
     * @param StoreId    $storeId
     * @param PostalCode $postalCode
     *
     * @throws StoreNotFoundException
     * @throws StoreAlreadyContainsPostalCodeException
     */
    public function addPostalCode(StoreId $storeId, PostalCode $postalCode): void
    {
        $store = $this->checkDocument($storeId, new StoreParts([StoreParts::POSTAL_CODES]));

        if (in_array($postalCode->postalCode(), $store->postalCodes()->toScalar(), true)) {
            throw new StoreAlreadyContainsPostalCodeException();
        }

        $storePostalCode = new StorePostalCode($storeId, $postalCode);
        $this->storeWriteModel->addPostalCode($storePostalCode);
    }

    /**
     * @param StoreId    $storeId
     * @param PostalCode $postalCode
     *
     * @throws StoreNotFoundException
     * @throws StoreDoesNotContainsPostalCodeException
     */
    public function removePostalCode(StoreId $storeId, PostalCode $postalCode): void
    {
        $store = $this->checkDocument($storeId, new StoreParts([StoreParts::POSTAL_CODES]));

        if (!in_array($postalCode->postalCode(), $store->postalCodes()->toScalar(), true)) {
            throw new StoreDoesNotContainsPostalCodeException();
        }

        $storePostalCode = new StorePostalCode($storeId, $postalCode);
        $this->storeWriteModel->removePostalCode($storePostalCode);
    }
}
