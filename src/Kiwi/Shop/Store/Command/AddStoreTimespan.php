<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;
use KiwiLib\DateTime\DateTime;

/**
 * Class AddStoreTimespan.
 */
class AddStoreTimespan implements Command
{
    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * EnableTimeSpan constructor.
     *
     * @param string   $storeId
     * @param string   $userId
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function __construct(
        string $userId,
        string $storeId,
        DateTime $startTime,
        DateTime $endTime
    ) {
        $this->storeId   = new StoreId($storeId);
        $this->startTime = $startTime;
        $this->endTime   = $endTime;
        $this->userId    = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }
}
