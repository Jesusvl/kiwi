<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StoreName;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class UpdateStore.
 */
class UpdateStore implements Command
{
    /**
     * @var StoreId
     */
    private $id;

    /**
     * @var StoreName
     */
    private $name;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * UpdateStore constructor.
     *
     * @param string $userId
     * @param string $id
     * @param string $name
     */
    public function __construct(string $userId, string $id, string $name)
    {
        $this->userId = new UserId($userId);
        $this->id     = new StoreId($id);
        $this->name   = new StoreName($name);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoreId
     */
    public function id(): StoreId
    {
        return $this->id;
    }

    /**
     * @return StoreName
     */
    public function name(): StoreName
    {
        return $this->name;
    }
}
