<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Store\Domain\StoreTimespanId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class EnableTimeSpan.
 */
class OpenTimespan implements Command
{
    /**
     * @var StoreTimespanId
     */
    private $timespanId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * OpenTimespan constructor.
     *
     * @param string $userId
     * @param string $storeTimespanId
     */
    public function __construct(string $userId, string $storeTimespanId)
    {
        $this->timespanId = new StoreTimespanId($storeTimespanId);
        $this->userId     = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoreTimespanId
     */
    public function timespanId(): StoreTimespanId
    {
        return $this->timespanId;
    }
}
