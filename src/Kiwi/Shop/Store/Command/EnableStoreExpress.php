<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class EnableStoreExpress.
 */
class EnableStoreExpress implements Command
{
    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * EnableStoreExpress constructor.
     *
     * @param string $userId
     * @param string $storeId
     */
    public function __construct(string $userId, string $storeId)
    {
        $this->storeId = new StoreId($storeId);
        $this->userId  = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }
}
