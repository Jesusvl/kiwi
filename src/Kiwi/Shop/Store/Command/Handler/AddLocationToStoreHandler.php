<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Location\Exception\LocationNotFoundException;
use Kiwi\Shop\Store\Command\AddLocationToStore;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class AddLocationToStoreHandler.
 */
class AddLocationToStoreHandler extends StoreCommandHandler
{
    /**
     * @param AddLocationToStore $command
     *
     * @throws InsufficientPermissionsException
     * @throws LocationNotFoundException
     * @throws StoreNotFoundException
     */
    public function __invoke(AddLocationToStore $command): void
    {
        $location = $this->locationService()->checkDocument($command->locationId());
        $this->locationService()->checkUserCanReadLocation($command->userId(), $location);

        $store = $this->service()->checkDomain($command->storeId());
        $this->service()->checkUserCanWriteStore($command->userId(), $store);

        $store->addLocation($command->locationId());
        $this->service()->update($store);
    }
}
