<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\AddPostalCode;
use Kiwi\Shop\Store\Exception\StoreDoesNotContainsPostalCodeException;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class RemovePostalCodeHandler.
 */
class RemovePostalCodeHandler extends StoreCommandHandler
{
    /**
     * @param AddPostalCode $command
     *
     * @throws InsufficientPermissionsException
     * @throws StoreDoesNotContainsPostalCodeException
     * @throws StoreNotFoundException
     */
    public function __invoke(AddPostalCode $command)
    {
        $this->service()->checkUser($command->userId());
        $store = $this->service()->checkDomain($command->storeId());

        $this->service()->checkUserCanWriteStore($command->userId(), $store);
        $this->service()->removePostalCode($store->id(), $command->postalCode());
    }
}
