<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\CloseTimespan;

/**
 * Class CloseTimespanHandler.
 */
class CloseTimespanHandler extends StoreCommandHandler
{
    /**
     * @param CloseTimespan $query
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(CloseTimespan $query): void
    {
        $this->service()->checkUser($query->userId());
        $this->service()->closeTimestamp($query->timespanId());
    }
}
