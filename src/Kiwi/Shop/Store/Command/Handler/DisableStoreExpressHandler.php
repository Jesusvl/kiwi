<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\DisableStoreExpress;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class DisableStoreExpressHandler.
 */
class DisableStoreExpressHandler extends StoreCommandHandler
{
    /**
     * @param DisableStoreExpress $query
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     */
    public function __invoke(DisableStoreExpress $query): void
    {
        $store = $this->service()->checkDomain($query->storeId());
        $this->service()->checkUserCanWriteStore($query->userId(), $store);
        $store->disableExpress();
        $this->service()->update($store);
    }
}
