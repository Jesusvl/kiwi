<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\OpenTimespan;

/**
 * Class OpenTimespanHandler.
 */
class OpenTimespanHandler extends StoreCommandHandler
{
    /**
     * @param OpenTimespan $query
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(OpenTimespan $query): void
    {
        $this->service()->checkUser($query->userId());
        $this->service()->openTimestamp($query->timespanId());
    }
}
