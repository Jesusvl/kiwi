<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\AddPostalCode;
use Kiwi\Shop\Store\Exception\StoreAlreadyContainsPostalCodeException;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class AddPostalCodeHandler.
 */
class AddPostalCodeHandler extends StoreCommandHandler
{
    /**
     * @param AddPostalCode $command
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     * @throws StoreAlreadyContainsPostalCodeException
     */
    public function __invoke(AddPostalCode $command)
    {
        $this->service()->checkUser($command->userId());
        $store = $this->service()->checkDomain($command->storeId());

        $this->service()->checkUserCanWriteStore($command->userId(), $store);
        $this->service()->addPostalCode($store->id(), $command->postalCode());
    }
}
