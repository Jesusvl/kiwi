<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\UpdateStore;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class UpdateStoreHandler.
 */
class UpdateStoreHandler extends StoreCommandHandler
{
    /**
     * @param UpdateStore $command
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     */
    public function __invoke(UpdateStore $command): void
    {
        $store = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteStore($command->userId(), $store);

        $store->changeName($command->name());
        $this->service()->update($store);
    }
}
