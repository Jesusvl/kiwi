<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\AddStoreTimespan;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class AddTimeStampHandler.
 */
class AddStoreTimespanHandler extends StoreCommandHandler
{
    /**
     * @param AddStoreTimespan $query
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     */
    public function __invoke(AddStoreTimespan $query): void
    {
        $store = $this->service()->checkDomain($query->storeId());
        $this->service()->checkUserCanWriteStore($query->userId(), $store);
        $this->service()->addTimespan($store, $query->startTime(), $query->endTime());
    }
}
