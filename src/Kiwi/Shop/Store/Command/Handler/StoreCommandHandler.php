<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Shop\Location\Service\LocationService;
use Kiwi\Shop\Store\Service\StoreService;

/**
 * Class StoreCommandHandler.
 */
abstract class StoreCommandHandler implements CommandHandler
{
    /**
     * @var StoreService
     */
    private $service;

    /**
     * @var LocationService
     */
    private $locationService;

    /**
     * StoreCommandHandler constructor.
     *
     * @param StoreService    $service
     * @param LocationService $locationService
     */
    public function __construct(StoreService $service, LocationService $locationService)
    {
        $this->service         = $service;
        $this->locationService = $locationService;
    }

    /**
     * @return StoreService
     */
    protected function service(): StoreService
    {
        return $this->service;
    }

    /**
     * @return LocationService
     */
    protected function locationService(): LocationService
    {
        return $this->locationService;
    }
}
