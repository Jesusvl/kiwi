<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Command\EnableStoreExpress;
use Kiwi\Shop\Store\Exception\StoreNotFoundException;

/**
 * Class EnableStoreExpressHandler.
 */
class EnableStoreExpressHandler extends StoreCommandHandler
{
    /**
     * @param EnableStoreExpress $query
     *
     * @throws InsufficientPermissionsException
     * @throws StoreNotFoundException
     */
    public function __invoke(EnableStoreExpress $query): void
    {
        $store = $this->service()->checkDomain($query->storeId());
        $this->service()->checkUserCanWriteStore($query->userId(), $store);
        $store->enableExpress();
        $this->service()->update($store);
    }
}
