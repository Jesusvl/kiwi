<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class AddLocationToStore.
 */
class AddLocationToStore implements Command
{
    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * AddLocationToStore constructor.
     *
     * @param string $userId
     * @param string $storeId
     * @param string $locationId
     */
    public function __construct(string $userId, string $storeId, string $locationId)
    {
        $this->storeId    = new StoreId($storeId);
        $this->locationId = new LocationId($locationId);
        $this->userId     = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }
}
