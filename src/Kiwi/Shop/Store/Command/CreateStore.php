<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StoreName;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class CreateStore.
 */
class CreateStore implements Command
{
    /**
     * @var StoreId
     */
    private $id;

    /**
     * @var StoreName
     */
    private $name;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * CreateStore constructor.
     *
     * @param string $userId
     * @param string $id
     * @param string $name
     * @param string $merchantId
     */
    public function __construct(
        string $userId,
        string $id,
        string $name,
        string $merchantId
    ) {
        $this->id         = new StoreId($id);
        $this->name       = new StoreName($name);
        $this->merchantId = new MerchantId($merchantId);
        $this->userId     = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoreId
     */
    public function id(): StoreId
    {
        return $this->id;
    }

    /**
     * @return StoreName
     */
    public function name(): StoreName
    {
        return $this->name;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }
}
