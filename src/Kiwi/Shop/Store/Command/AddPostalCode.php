<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Store\Domain\PostalCode;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class AddPostalCode.
 */
class AddPostalCode implements Command
{
    /**
     * @var PostalCode
     */
    private $postalCode;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * AddPostalCode constructor.
     *
     * @param string $userId
     * @param string $storeId
     * @param string $postalCode
     */
    public function __construct(string $userId, string $storeId, string $postalCode)
    {
        $this->userId     = new UserId($userId);
        $this->storeId    = new StoreId($storeId);
        $this->postalCode = new PostalCode($postalCode);
    }

    /**
     * @return PostalCode
     */
    public function postalCode(): PostalCode
    {
        return $this->postalCode;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }
}
