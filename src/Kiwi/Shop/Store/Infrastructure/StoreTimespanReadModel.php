<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Infrastructure;

use Kiwi\Shop\Store\Document\BaseStoreTimespanDocument;
use Kiwi\Shop\Store\Domain\StoreId;
use KiwiLib\DateTime\DateTime;

/**
 * Interface StoreTimespanReadModel.
 */
interface StoreTimespanReadModel
{
    /**
     * @param StoreId  $storeId
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return BaseStoreTimespanDocument
     */
    public function findByStoreAndTimeRange(
        StoreId $storeId,
        DateTime $start,
        DateTime $end
    ): BaseStoreTimespanDocument;
}
