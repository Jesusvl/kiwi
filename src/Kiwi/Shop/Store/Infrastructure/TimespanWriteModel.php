<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Infrastructure;

use Kiwi\Shop\Store\Domain\Timespan;

/**
 * Interface TimespanWriteModel.
 */
interface TimespanWriteModel
{
    /**
     * @param Timespan $timespan
     */
    public function save(Timespan $timespan): void;
}
