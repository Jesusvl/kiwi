<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Infrastructure;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StoreName;
use Kiwi\Shop\Store\Domain\StoreParts;
use Kiwi\Shop\Store\Domain\TimespanId;

/**
 * Interface StoreReadModel.
 */
interface StoreReadModel
{
    /**
     * @param StoreId         $id
     * @param StoreParts|null $parts
     *
     * @return StoreDocument
     */
    public function find(StoreId $id, ?StoreParts $parts = null): StoreDocument;

    /**
     * @param Pagination      $filter
     * @param StoreParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter, ?StoreParts $parts = null): PaginatedDocument;

    /**
     * @param StoreId         $id
     * @param TimespanId      $timespanId
     * @param StoreParts|null $parts
     *
     * @return StoreDocument
     */
    public function findByIdAndTimespan(
        StoreId $id,
        TimespanId $timespanId,
        ?StoreParts $parts = null
    ): StoreDocument;

    /**
     * @param StoreName  $name
     * @param MerchantId $merchant
     * @param StoreParts $parts
     *
     * @return StoreDocument
     */
    public function findByNameAndMerchant(
        StoreName $name,
        MerchantId $merchant,
        StoreParts $parts
    ): StoreDocument;
}
