<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Infrastructure;

use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StorePostalCode;
use Kiwi\Shop\Store\Domain\StoreTimespanId;
use Kiwi\Shop\Store\Domain\TimespanId;

/**
 * Interface StoreWriteModel.
 */
interface StoreWriteModel
{
    /**
     * @param StoreId $storeId
     *
     * @return Store|null
     */
    public function find(StoreId $storeId): ?Store;

    /**
     * @param Store $store
     */
    public function save(Store $store): void;

    /**
     * @param Store $store
     */
    public function update(Store $store): void;

    /**
     * @param StoreId $id
     */
    public function delete(StoreId $id): void;

    /**
     * @param StoreId    $storeId
     * @param TimespanId $timespanId
     */
    public function addTimestamp(StoreId $storeId, TimespanId $timespanId): void;

    /**
     * @param StoreTimespanId $timespanId
     */
    public function openTimestamp(StoreTimespanId $timespanId): void;

    /**
     * @param StoreTimespanId $timespanId
     */
    public function closeTimestamp(StoreTimespanId $timespanId): void;

    /**
     * @param StoreId $id
     */
    public function enableExpress(StoreId $id): void;

    /**
     * @param StoreId $id
     */
    public function disableExpress(StoreId $id): void;

    /**
     * @param StorePostalCode $storePostalCode
     */
    public function addPostalCode(StorePostalCode $storePostalCode): void;

    /**
     * @param StorePostalCode $storePostalCode
     */
    public function removePostalCode(StorePostalCode $storePostalCode): void;
}
