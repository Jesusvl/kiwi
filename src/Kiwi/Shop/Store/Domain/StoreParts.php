<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

use Kiwi\Core\Domain\Parts;

/**
 * Class StoreParts.
 */
class StoreParts extends Parts
{
    public const LOCATION     = 'location';
    public const TIMESPANS    = 'timespans';
    public const POSTAL_CODES = 'postalCodes';

    /**
     * StoreParts constructor.
     *
     * @param array $inputFields
     */
    public function __construct($inputFields = [])
    {
        parent::__construct([self::LOCATION, self::TIMESPANS, self::POSTAL_CODES], $inputFields);
    }
}
