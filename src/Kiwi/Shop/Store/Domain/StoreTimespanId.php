<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class StoreTimeSpanId.
 */
class StoreTimespanId extends Uuid
{
}
