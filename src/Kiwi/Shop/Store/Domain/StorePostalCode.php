<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

/**
 * Class StorePostalCode.
 */
class StorePostalCode
{
    /**
     * @var StorePostalCodeId
     */
    private $id;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var PostalCode
     */
    private $postalCode;

    /**
     * StorePostalCode constructor.
     *
     * @param StoreId    $storeId
     * @param PostalCode $postalCode
     */
    public function __construct(StoreId $storeId, PostalCode $postalCode)
    {
        $this->id         = new StorePostalCodeId();
        $this->storeId    = $storeId;
        $this->postalCode = $postalCode;
    }

    /**
     * @return StorePostalCodeId
     */
    public function id(): StorePostalCodeId
    {
        return $this->id;
    }
    
    /**
     * @return PostalCode
     */
    public function postalCode(): PostalCode
    {
        return $this->postalCode;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }
}
