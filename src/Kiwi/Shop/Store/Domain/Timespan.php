<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

use KiwiLib\DateTime\DateTime;

/**
 * Class TimeSpan.
 */
class Timespan
{
    /**
     * @var TimespanId
     */
    private $id;

    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * TimeSpan constructor.
     *
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function __construct(DateTime $startTime, DateTime $endTime)
    {
        $this->startTime = $startTime;
        $this->endTime   = $endTime;
        $this->id        = new TimespanId();
    }

    /**
     * @return TimespanId
     */
    public function id(): TimespanId
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }
}
