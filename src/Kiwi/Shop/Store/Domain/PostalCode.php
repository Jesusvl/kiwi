<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

/**
 * Class PostalCode.
 */
class PostalCode
{
    /**
     * @var string
     */
    private $postalCode;

    /**
     * PostalCode constructor.
     *
     * @param string $postalCode
     */
    public function __construct(string $postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function postalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->postalCode();
    }
}
