<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

/**
 * Class StoreTimeSpan.
 */
class StoreTimespan
{
    /**
     * @var StoreTimespanId
     */
    private $id;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var TimespanId
     */
    private $timespanId;

    /**
     * @var bool
     */
    private $isClosed;

    /**
     * StoreTimeSpan constructor.
     *
     * @param StoreTimespanId $id
     * @param StoreId         $storeId
     * @param TimespanId      $timespanId
     * @param bool            $isClosed
     */
    public function __construct(StoreTimespanId $id, StoreId $storeId, TimespanId $timespanId, bool $isClosed)
    {
        $this->storeId    = $storeId;
        $this->timespanId = $timespanId;
        $this->id         = $id;
        $this->isClosed   = $isClosed;
    }

    /**
     * @return StoreTimespanId
     */
    public function id(): StoreTimespanId
    {
        return $this->id;
    }

    /**
     * @return StoreId
     */
    public function storeId(): StoreId
    {
        return $this->storeId;
    }

    /**
     * @return TimespanId
     */
    public function timespanId(): TimespanId
    {
        return $this->timespanId;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->isClosed;
    }
}
