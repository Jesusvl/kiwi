<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class TimeSpanId.
 */
class TimespanId extends Uuid
{
}
