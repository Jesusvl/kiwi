<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Domain;

use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class Store.
 */
class Store
{
    /**
     * @var StoreId
     */
    private $id;

    /**
     * @var StoreName
     */
    private $name;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var UserId|null
     */
    private $userId;

    /**
     * @var LocationId|null
     */
    private $locationId;

    /**
     * @var bool
     */
    private $isExpressEnabled;

    /**
     * Store constructor.
     *
     * @param StoreId         $id
     * @param StoreName       $name
     * @param MerchantId      $merchantId
     * @param null|LocationId $locationId
     * @param null|UserId     $userId
     */
    public function __construct(
        StoreId $id,
        StoreName $name,
        MerchantId $merchantId,
        ?LocationId $locationId,
        ?UserId $userId
    ) {
        $this->id               = $id;
        $this->name             = $name;
        $this->merchantId       = $merchantId;
        $this->locationId       = $locationId;
        $this->userId           = $userId;
        $this->isExpressEnabled = true;
    }

    /**
     * @return StoreId
     */
    public function id(): StoreId
    {
        return $this->id;
    }

    /**
     * @return StoreName
     */
    public function name(): StoreName
    {
        return $this->name;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return UserId|null
     */
    public function userId(): ?UserId
    {
        return $this->userId;
    }

    /**
     * @return LocationId|null
     */
    public function locationId(): ?LocationId
    {
        return $this->locationId;
    }

    /**
     * @return bool
     */
    public function isExpressEnabled(): bool
    {
        return $this->isExpressEnabled;
    }

    /**
     * @param LocationId $locationId
     */
    public function addLocation(LocationId $locationId): void
    {
        $this->locationId = $locationId;
    }

    /**
     * @param StoreName $name
     */
    public function changeName(StoreName $name): void
    {
        $this->name = $name;
    }

    public function disableExpress(): void
    {
        $this->isExpressEnabled = false;
    }

    public function enableExpress(): void
    {
        $this->isExpressEnabled = true;
    }
}
