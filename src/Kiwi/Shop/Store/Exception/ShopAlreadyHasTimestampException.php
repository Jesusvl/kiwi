<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Exception;

use DomainException;
use Throwable;

/**
 * Class ShopAlreadyHasTimestampException.
 */
class ShopAlreadyHasTimestampException extends DomainException
{
    /**
     * ShopAlreadyHasTimestampException constructor.
     *
     * @param Throwable|null $previous
     */
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('The shop has already this timestamp', 0, $previous);
    }
}
