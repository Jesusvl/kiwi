<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class StoreAlreadyContainsPostalCodeException.
 */
class StoreAlreadyContainsPostalCodeException extends DomainException
{
    /**
     * StoreAlreadyContainsPostalCodeException constructor.
     */
    public function __construct()
    {
        parent::__construct('Store already contains the postal code');
    }
}
