<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class StoreDoesNotContainsPostalCodeException.
 */
class StoreDoesNotContainsPostalCodeException extends DomainException
{
    /**
     * StoreDoesNotContainsPostalCodeException constructor.
     */
    public function __construct()
    {
        parent::__construct('Store does not contains the postal code');
    }
}
