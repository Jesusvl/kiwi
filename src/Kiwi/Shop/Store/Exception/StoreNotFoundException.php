<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Exception;

use Kiwi\Core\Exception\DataNotFoundDomainException;

/**
 * Class StoreNotFoundException.
 */
class StoreNotFoundException extends DataNotFoundDomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Store not found.');
    }
}
