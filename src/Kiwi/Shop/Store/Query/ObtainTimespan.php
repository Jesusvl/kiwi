<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query;

use KiwiLib\DateTime\DateTime;
use Kiwi\Core\Infrastructure\Query;

/**
 * Class ObtainTimespan.
 */
class ObtainTimespan implements Query
{
    /**
     * @var DateTime
     */
    private $startTime;

    /**
     * @var DateTime
     */
    private $endTime;

    /**
     * ObtainTimeStamp constructor.
     *
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function __construct(DateTime $startTime, DateTime $endTime)
    {
        $this->startTime = $startTime;
        $this->endTime   = $endTime;
    }

    /**
     * @return DateTime
     */
    public function endTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return DateTime
     */
    public function startTime(): DateTime
    {
        return $this->startTime;
    }
}
