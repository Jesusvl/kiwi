<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Domain\StoreName;
use Kiwi\Shop\Store\Domain\StoreParts;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindStoreByMerchantAndName.
 */
class FindStoreByMerchantAndName implements Query
{
    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var StoreName
     */
    private $storeName;

    /**
     * @var StoreParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindStoreByMerchantAndName constructor.
     *
     * @param string $userId
     * @param string $merchantId
     * @param string $storeName
     * @param array  $parts
     */
    public function __construct(string $userId, string $merchantId, string $storeName, array $parts)
    {
        $this->merchantId = new MerchantId($merchantId);
        $this->storeName  = new StoreName($storeName);
        $this->parts      = new StoreParts($parts);
        $this->userId     = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return StoreName
     */
    public function storeName(): StoreName
    {
        return $this->storeName;
    }

    /**
     * @return StoreParts
     */
    public function parts(): StoreParts
    {
        return $this->parts;
    }
}
