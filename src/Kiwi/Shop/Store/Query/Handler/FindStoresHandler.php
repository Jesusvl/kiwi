<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Query\FindStores;

/**
 * Class FindStoresHandler.
 */
class FindStoresHandler extends StoreQueryHandler
{
    /**
     * @param FindStores $query
     *
     * @return PaginatedDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindStores $query): PaginatedDocument
    {
        $user = $this->service()->checkUser($query->userId());
        if (!$user->isAdmin()) {
            throw new InsufficientPermissionsException();
        }

        return $this->readModel()->findAll($query->filter(), $query->parts());
    }
}
