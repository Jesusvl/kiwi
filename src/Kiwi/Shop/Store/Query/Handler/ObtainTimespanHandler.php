<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query\Handler;

use Kiwi\Core\Infrastructure\QueryHandler;
use Kiwi\Shop\Store\Document\BaseTimespanDocument;
use Kiwi\Shop\Store\Query\ObtainTimespan;
use Kiwi\Shop\Store\Service\TimespanService;

/**
 * Class ObtainTimespanHandler.
 */
class ObtainTimespanHandler implements QueryHandler
{
    /**
     * @var TimespanService
     */
    private $timespanService;

    /**
     * ObtainTimespanHandler constructor.
     *
     * @param TimespanService $timespanService
     */
    public function __construct(TimespanService $timespanService)
    {
        $this->timespanService = $timespanService;
    }

    /**
     * @param ObtainTimespan $query
     *
     * @return BaseTimespanDocument
     */
    public function __invoke(ObtainTimespan $query): BaseTimespanDocument
    {
        return $this->timespanService->fetch($query->startTime(), $query->endTime());
    }
}
