<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Shop\Store\Infrastructure\StoreReadModel;
use Kiwi\Shop\Store\Service\StoreService;

/**
 * Class StoreQueryHandler.
 */
class StoreQueryHandler implements CommandHandler
{
    /**
     * @var StoreService
     */
    private $service;

    /**
     * @var StoreReadModel
     */
    private $readModel;

    /**
     * StoreQueryHandler constructor.
     *
     * @param StoreService   $service
     * @param StoreReadModel $readModel
     */
    public function __construct(StoreService $service, StoreReadModel $readModel)
    {
        $this->service   = $service;
        $this->readModel = $readModel;
    }

    /**
     * @return StoreReadModel
     */
    public function readModel(): StoreReadModel
    {
        return $this->readModel;
    }

    /**
     * @return StoreService
     */
    public function service(): StoreService
    {
        return $this->service;
    }
}
