<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Shop\Store\Query\FindStore;

/**
 * Class FindStoreHandler.
 */
class FindStoreHandler extends StoreQueryHandler
{
    /**
     * @param FindStore $query
     *
     * @return StoreDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindStore $query): StoreDocument
    {
        $store = $this->readModel()->find($query->id(), $query->parts());
        $this->service()->checkUserCanReadStore($query->userId(), $store);

        return $store;
    }
}
