<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Shop\Store\Query\FindStoreByMerchantAndName;

/**
 * Class FindStoreByMerchantAndNameHandler.
 */
class FindStoreByMerchantAndNameHandler extends StoreQueryHandler
{
    /**
     * @param FindStoreByMerchantAndName $query
     *
     * @return StoreDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindStoreByMerchantAndName $query): StoreDocument
    {
        $store = $this->readModel()->findByNameAndMerchant($query->storeName(), $query->merchantId(), $query->parts());
        $this->service()->checkUserCanReadStore($query->userId(), $store);

        return $store;
    }
}
