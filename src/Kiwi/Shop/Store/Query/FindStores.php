<?php

declare(strict_types=1);

namespace Kiwi\Shop\Store\Query;

use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Store\Domain\StoreParts;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindStores.
 */
class FindStores implements Query
{
    /**
     * @var Pagination
     */
    private $filter;

    /**
     * @var StoreParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUsers constructor.
     *
     * @param string $userId
     * @param int    $page
     * @param int    $pageSize
     * @param array  $parts
     */
    public function __construct(string $userId, int $page, int $pageSize, array $parts = [])
    {
        $this->filter = new Pagination($page, $pageSize);
        $this->parts  = new StoreParts($parts);
        $this->userId = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return Pagination
     */
    public function filter(): Pagination
    {
        return $this->filter;
    }

    /**
     * @return StoreParts
     */
    public function parts(): StoreParts
    {
        return $this->parts;
    }
}
