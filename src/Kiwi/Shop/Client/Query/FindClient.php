<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Client\Domain\ClientParts;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindClient.
 */
class FindClient implements Query
{
    /**
     * @var ClientId
     */
    private $id;

    /**
     * @var ClientParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindClient constructor.
     *
     * @param string   $userId
     * @param string   $id
     * @param string[] $parts
     */
    public function __construct(string $userId, string $id, array $parts = [])
    {
        $this->userId = new UserId($userId);
        $this->id     = new ClientId($id);
        $this->parts  = new ClientParts($parts);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return ClientId
     */
    public function id(): ClientId
    {
        return $this->id;
    }

    /**
     * @return ClientParts
     */
    public function parts(): ClientParts
    {
        return $this->parts;
    }
}
