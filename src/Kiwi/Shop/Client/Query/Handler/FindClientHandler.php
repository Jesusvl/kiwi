<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Query\FindClient;

/**
 * Class FindClientHandler.
 */
class FindClientHandler extends ClientQueryHandler
{
    /**
     * @param FindClient $query
     *
     * @return ClientDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindClient $query): ClientDocument
    {
        $client = $this->readModel()->find($query->id(), $query->parts());
        $this->service()->checkUserCanReadClient($query->userId(), $client);

        return $client;
    }
}
