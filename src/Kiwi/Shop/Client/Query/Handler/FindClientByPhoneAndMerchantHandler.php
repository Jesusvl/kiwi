<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Query\FindClientByPhoneAndMerchant;

/**
 * Class FindClientByPhoneAndMerchantHandler.
 */
class FindClientByPhoneAndMerchantHandler extends ClientQueryHandler
{
    /**
     * @param FindClientByPhoneAndMerchant $query
     *
     * @return ClientDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindClientByPhoneAndMerchant $query): ClientDocument
    {
        $client = $this->readModel()->findByPhoneAndMerchant($query->phone(), $query->merchantId(), $query->parts());
        $this->service()->checkUserCanReadClient($query->userId(), $client);

        return $client;
    }
}
