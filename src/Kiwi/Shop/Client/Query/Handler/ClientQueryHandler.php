<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query\Handler;

use Kiwi\Core\Infrastructure\QueryHandler;
use Kiwi\Shop\Client\Infrastructure\ClientReadModel;
use Kiwi\Shop\Client\Service\ClientService;

/**
 * Class ClientQueryHandler.
 */
abstract class ClientQueryHandler implements QueryHandler
{
    /**
     * @var ClientReadModel
     */
    private $readModel;

    /**
     * @var ClientService
     */
    private $service;

    /**
     * ClientQueryHandler constructor.
     *
     * @param ClientReadModel $readModel
     * @param ClientService   $service
     */
    public function __construct(ClientReadModel $readModel, ClientService $service)
    {
        $this->readModel = $readModel;
        $this->service   = $service;
    }

    /**
     * @return ClientReadModel
     */
    protected function readModel(): ClientReadModel
    {
        return $this->readModel;
    }

    /**
     * @return ClientService
     */
    protected function service(): ClientService
    {
        return $this->service;
    }
}
