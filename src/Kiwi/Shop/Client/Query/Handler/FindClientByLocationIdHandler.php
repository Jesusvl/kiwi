<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Query\FindClientByLocationId;

/**
 * Class FindClientByLocationIdHandler.
 */
class FindClientByLocationIdHandler extends ClientQueryHandler
{
    /**
     * @param FindClientByLocationId $query
     *
     * @return ClientDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindClientByLocationId $query): ClientDocument
    {
        $client = $this->readModel()->findByLocation($query->locationId(), $query->parts());
        $this->service()->checkUserCanReadClient($query->userId(), $client);

        return $client;
    }
}
