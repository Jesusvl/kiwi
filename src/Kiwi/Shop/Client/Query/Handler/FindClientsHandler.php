<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Query\FindClients;

/**
 * Class FindClientsHandler.
 */
class FindClientsHandler extends ClientQueryHandler
{
    /**
     * @param FindClients $query
     *
     * @return PaginatedDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindClients $query): PaginatedDocument
    {
        return $this->service()->findAll($query->userId(), $query->filter(), $query->parts());
    }
}
