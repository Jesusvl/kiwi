<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query;

use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Client\Domain\ClientParts;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindClients.
 */
class FindClients implements Query
{
    /**
     * @var Pagination
     */
    private $filter;

    /**
     * @var ClientParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUsers constructor.
     *
     * @param string $userId
     * @param int    $page
     * @param int    $pageSize
     * @param array  $parts
     */
    public function __construct(string $userId, int $page, int $pageSize, array $parts = [])
    {
        $this->userId = new UserId($userId);
        $this->filter = new Pagination($page, $pageSize);
        $this->parts  = new ClientParts($parts);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return Pagination
     */
    public function filter(): Pagination
    {
        return $this->filter;
    }

    /**
     * @return ClientParts
     */
    public function parts(): ClientParts
    {
        return $this->parts;
    }
}
