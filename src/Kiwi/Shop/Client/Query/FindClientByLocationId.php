<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Client\Domain\ClientParts;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindClientByLocationId.
 */
class FindClientByLocationId implements Query
{
    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * @var ClientParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindClientByLocationId constructor.
     *
     * @param string   $userId
     * @param string   $locationId
     * @param string[] $parts
     */
    public function __construct(string $userId, string $locationId, array $parts = [])
    {
        $this->userId     = new UserId($userId);
        $this->locationId = new LocationId($locationId);
        $this->parts      = new ClientParts($parts);
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }

    /**
     * @return ClientParts
     */
    public function parts(): ClientParts
    {
        return $this->parts;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
