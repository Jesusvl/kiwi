<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Client\Domain\ClientParts;
use Kiwi\Shop\Client\Domain\ClientPhone;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindClientByPhone.
 */
class FindClientByPhoneAndMerchant implements Query
{
    /**
     * @var ClientPhone
     */
    private $phone;

    /**
     * @var ClientParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * FindClientByPhone constructor.
     *
     * @param string $userId
     * @param string $phone
     * @param string $merchantId
     * @param array  $parts
     */
    public function __construct(string $userId, string $phone, string $merchantId, array $parts = [])
    {
        $this->userId     = new UserId($userId);
        $this->phone      = new ClientPhone($phone);
        $this->parts      = new ClientParts($parts);
        $this->merchantId = new MerchantId($merchantId);
    }

    /**
     * @return ClientPhone
     */
    public function phone(): ClientPhone
    {
        return $this->phone;
    }

    /**
     * @return ClientParts
     */
    public function parts(): ClientParts
    {
        return $this->parts;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }
}
