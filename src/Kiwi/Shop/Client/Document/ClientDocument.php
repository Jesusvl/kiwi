<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Document;

use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Merchant\Document\MerchantDocument;

/**
 * Class ClientDocument.
 */
class ClientDocument extends BaseClientDocument
{
    /**
     * @var LocationDocument
     */
    private $location;

    /**
     * @var MerchantDocument
     */
    private $merchant;

    /**
     * ClientDocument constructor.
     *
     * @param Client|null      $client
     * @param LocationDocument $location
     * @param MerchantDocument $merchant
     */
    public function __construct(?Client $client, LocationDocument $location, MerchantDocument $merchant)
    {
        parent::__construct($client);
        $this->location = $location;
        $this->merchant = $merchant;
    }

    /**
     * @return LocationDocument
     */
    public function location(): LocationDocument
    {
        return $this->location;
    }

    /**
     * @return MerchantDocument
     */
    public function merchant(): MerchantDocument
    {
        return $this->merchant;
    }

    /**
     * @return array
     */
    public function toScalar(): ?array
    {
        return $this->mergeDocumentArray(parent::toScalar(), [
            'location' => $this->location()->toScalar(),
            'merchant' => $this->merchant()->toScalar()
        ]);
    }

    /**
     * @return ClientDocument
     */
    public static function empty(): self
    {
        return new ClientDocument(null, new LocationDocument(null), new MerchantDocument(null));
    }
}
