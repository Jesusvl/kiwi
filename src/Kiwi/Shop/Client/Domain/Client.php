<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Domain;

use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Merchant\Domain\MerchantId;

/**
 * Class Client.
 */
class Client
{
    /**
     * @var ClientId
     */
    private $id;

    /**
     * @var ClientCode
     */
    private $code;

    /**
     * @var ClientName
     */
    private $fullName;

    /**
     * @var ClientPhone
     */
    private $phone;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * Client constructor.
     *
     * @param ClientId    $id
     * @param ClientCode  $code
     * @param ClientName  $fullName
     * @param ClientPhone $phone
     * @param MerchantId  $merchantId
     * @param LocationId  $locationId
     */
    public function __construct(
        ClientId $id,
        ClientCode $code,
        ClientName $fullName,
        ClientPhone $phone,
        MerchantId $merchantId,
        LocationId $locationId
    ) {
        $this->id         = $id;
        $this->code       = $code;
        $this->fullName   = $fullName;
        $this->phone      = $phone;
        $this->merchantId = $merchantId;
        $this->locationId = $locationId;
    }

    /**
     * @return ClientId
     */
    public function id(): ClientId
    {
        return $this->id;
    }

    /**
     * @return ClientCode
     */
    public function code(): ClientCode
    {
        return $this->code;
    }

    /**
     * @return ClientName
     */
    public function fullName(): ClientName
    {
        return $this->fullName;
    }

    /**
     * @return ClientPhone
     */
    public function phone(): ClientPhone
    {
        return $this->phone;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }
}
