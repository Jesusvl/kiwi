<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Domain;

/**
 * Class Code.
 */
class ClientCode
{
    /**
     * @var string
     */
    private $code;

    /**
     * ClientCode constructor.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->code();
    }
}
