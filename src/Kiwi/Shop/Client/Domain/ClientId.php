<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class ClientId.
 */
class ClientId extends Uuid
{
}
