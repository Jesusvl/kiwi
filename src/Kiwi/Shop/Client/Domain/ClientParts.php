<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Domain;

use Kiwi\Core\Domain\Parts;

/**
 * Class ClientParts.
 */
class ClientParts extends Parts
{
    public const LOCATION = 'location';
    public const MERCHANT = 'merchant';

    /**
     * ClientParts constructor.
     *
     * @param string[] $input
     */
    public function __construct(array $input = [])
    {
        parent::__construct(
            [self::LOCATION, self::MERCHANT],
            $input
        );
    }
}
