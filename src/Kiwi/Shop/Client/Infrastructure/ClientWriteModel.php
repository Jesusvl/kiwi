<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Infrastructure;

use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Location\Domain\LocationId;

/**
 * Interface ClientWriteModel.
 */
interface ClientWriteModel
{
    /**
     * @param ClientId $id
     *
     * @return Client|null
     */
    public function find(ClientId $id): ?Client;

    /**
     * @param Client $client
     */
    public function save(Client $client): void;

    /**
     * @param ClientId $id
     */
    public function delete(ClientId $id): void;

    /**
     * @param ClientId $id
     * @param LocationId $locationId
     */
    public function updateLocation(ClientId $id, LocationId $locationId): void;
}
