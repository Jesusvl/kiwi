<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Infrastructure;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Domain\ClientCode;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Client\Domain\ClientParts;
use Kiwi\Shop\Client\Domain\ClientPhone;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Domain\StoreId;

/**
 * Interface ClientReadModel.
 */
interface ClientReadModel
{
    /**
     * @param ClientId         $id
     * @param ClientParts|null $parts
     *
     * @return ClientDocument
     */
    public function find(ClientId $id, ?ClientParts $parts = null): ClientDocument;

    /**
     * @param Pagination       $filter
     * @param ClientParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter, ?ClientParts $parts = null): PaginatedDocument;

    /**
     * @param StoreId          $store
     * @param Pagination       $filter
     * @param ClientParts|null $parts
     *
     * @return PaginatedDocument */
    public function findAllByStore(
        StoreId $store,
        Pagination $filter,
        ?ClientParts $parts = null
    ): PaginatedDocument;

    /**
     * @param ClientCode       $code
     * @param ClientParts|null $parts
     *
     * @return ClientDocument
     */
    public function findByCode(ClientCode $code, ?ClientParts $parts = null): ClientDocument;

    /**
     * @param LocationId       $locationId
     * @param ClientParts|null $parts
     *
     * @return ClientDocument
     */
    public function findByLocation(LocationId $locationId, ?ClientParts $parts = null): ClientDocument;

    /**
     * @param ClientPhone      $phone
     * @param MerchantId       $merchant
     * @param ClientParts|null $parts
     *
     * @return ClientDocument
     */
    public function findByPhoneAndMerchant(
        ClientPhone $phone,
        MerchantId $merchant,
        ?ClientParts $parts = null
    ): ClientDocument;

    /**
     * @param MerchantId       $merchant
     * @param Pagination       $filter
     * @param ClientParts|null $parts
     *
     * @return mixed
     */
    public function findAllByMerchant(MerchantId $merchant, Pagination $filter, ?ClientParts $parts = null);
}
