<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Client\Domain\ClientCode;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Client\Domain\ClientName;
use Kiwi\Shop\Client\Domain\ClientPhone;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class CreateClient.
 */
class CreateClient implements Command
{
    /**
     * @var ClientId
     */
    private $id;

    /**
     * @var ClientCode
     */
    private $code;

    /**
     * @var ClientName
     */
    private $fullName;

    /**
     * @var ClientPhone
     */
    private $phone;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * @var UserId
     */
    private $userid;

    /**
     * CreateClient constructor.
     *
     * @param string $id
     * @param string $code
     * @param string $fullName
     * @param string $phone
     * @param string $merchantId
     * @param string $locationId
     * @param string $userid
     */
    public function __construct(
        string $userid,
        string $id,
        string $code,
        string $fullName,
        string $phone,
        string $merchantId,
        string $locationId
    ) {
        $this->id         = new ClientId($id);
        $this->code       = new ClientCode($code);
        $this->fullName   = new ClientName($fullName);
        $this->phone      = new ClientPhone($phone);
        $this->merchantId = new MerchantId($merchantId);
        $this->locationId = new LocationId($locationId);
        $this->userid     = new UserId($userid);
    }

    /**
     * @return UserId
     */
    public function userid(): UserId
    {
        return $this->userid;
    }
    
    /**
     * @return ClientId
     */
    public function id(): ClientId
    {
        return $this->id;
    }

    /**
     * @return ClientCode
     */
    public function code(): ClientCode
    {
        return $this->code;
    }

    /**
     * @return ClientName
     */
    public function fullName(): ClientName
    {
        return $this->fullName;
    }

    /**
     * @return ClientPhone
     */
    public function phone(): ClientPhone
    {
        return $this->phone;
    }

    /**
     * @return MerchantId
     */
    public function merchantId(): MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }
}
