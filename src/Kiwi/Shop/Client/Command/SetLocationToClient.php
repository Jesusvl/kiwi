<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class SetLocationToClient.
 */
class SetLocationToClient implements Command
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var ClientId
     */
    private $clientId;

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * SetLocationToClient constructor.
     *
     * @param string $userId
     * @param string $clientId
     * @param string $locationId
     */
    public function __construct(
        string $userId,
        string $clientId,
        string $locationId
    ) {
        $this->userId     = new UserId($userId);
        $this->clientId   = new ClientId($clientId);
        $this->locationId = new LocationId($locationId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return ClientId
     */
    public function clientId(): ClientId
    {
        return $this->clientId;
    }

    /**
     * @return LocationId
     */
    public function locationId(): LocationId
    {
        return $this->locationId;
    }
}
