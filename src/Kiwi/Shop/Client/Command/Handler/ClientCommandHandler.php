<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Shop\Client\Service\ClientService;

/**
 * Class ClientCommandHandler.
 */
abstract class ClientCommandHandler implements CommandHandler
{
    /**
     * @var ClientService
     */
    private $service;

    /**
     * ClientCommandHandler constructor.
     *
     * @param ClientService $service
     */
    public function __construct(ClientService $service)
    {
        $this->service = $service;
    }

    /**
     * @return ClientService
     */
    protected function service(): ClientService
    {
        return $this->service;
    }
}
