<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Command\CreateClient;
use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Client\Exception\DuplicatedClientCodeException;
use Kiwi\Shop\Client\Exception\DuplicatedClientLocationException;
use Kiwi\Shop\Client\Exception\DuplicatedClientPhoneException;

/**
 * Class CreateClientHandler.
 */
class CreateClientHandler extends ClientCommandHandler
{
    /**
     * @param CreateClient $command
     *
     * @throws InsufficientPermissionsException
     * @throws DuplicatedClientCodeException
     * @throws DuplicatedClientPhoneException
     * @throws DuplicatedClientLocationException
     */
    public function __invoke(CreateClient $command): void
    {
        $client = new Client(
            $command->id(),
            $command->code(),
            $command->fullName(),
            $command->phone(),
            $command->merchantId(),
            $command->locationId()
        );
        $this->service()->checkUserCanWriteClient($command->userid(), $client);
        $this->service()->save($client);
    }
}
