<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Command\SetLocationToClient;
use Kiwi\Shop\Client\Exception\ClientNotFoundException;
use Kiwi\Shop\Client\Exception\DuplicatedClientLocationException;

/**
 * Class SetLocationToClientHandler.
 */
class SetLocationToClientHandler extends ClientCommandHandler
{
    /**
     * @param SetLocationToClient $command
     *
     * @throws InsufficientPermissionsException
     * @throws ClientNotFoundException
     * @throws DuplicatedClientLocationException
     */
    public function __invoke(SetLocationToClient $command): void
    {
        $client = $this->service()->checkDomain($command->clientId());
        $this->service()->checkUserCanWriteClient($command->userId(), $client);
        $this->service()->updateLocation($client, $command->locationId());
    }
}
