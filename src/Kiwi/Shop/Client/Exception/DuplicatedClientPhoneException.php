<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class DuplicatedClientPhoneException.
 */
class DuplicatedClientPhoneException extends DomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Client phone alredy exists.');
    }
}
