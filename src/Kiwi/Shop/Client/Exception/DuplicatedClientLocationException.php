<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class DuplicatedClientLocationException.
 */
class DuplicatedClientLocationException extends DomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Client location alredy exists.');
    }
}
