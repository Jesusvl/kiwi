<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Exception;

use Kiwi\Core\Exception\DataNotFoundDomainException;

/**
 * Class ClientNotFoundException.
 */
class ClientNotFoundException extends DataNotFoundDomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Client not found.');
    }
}
