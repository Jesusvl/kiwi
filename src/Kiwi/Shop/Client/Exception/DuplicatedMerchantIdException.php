<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class DuplicatedMerchantIdException.
 */
class DuplicatedMerchantIdException extends DomainException
{
    public function __construct()
    {
        parent::__construct('Merchant already exists');
    }
}
