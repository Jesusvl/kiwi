<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class DuplicatedClientException.
 */
class DuplicatedClientCodeException extends DomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Client code alredy exists.');
    }
}
