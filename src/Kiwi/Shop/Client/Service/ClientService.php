<?php

declare(strict_types=1);

namespace Kiwi\Shop\Client\Service;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Client\Domain\ClientParts;
use Kiwi\Shop\Client\Exception\ClientNotFoundException;
use Kiwi\Shop\Client\Exception\DuplicatedClientCodeException;
use Kiwi\Shop\Client\Exception\DuplicatedClientLocationException;
use Kiwi\Shop\Client\Exception\DuplicatedClientPhoneException;
use Kiwi\Shop\Client\Infrastructure\ClientReadModel;
use Kiwi\Shop\Client\Infrastructure\ClientWriteModel;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Infrastructure\UserReadModel;

/**
 * Class ClientService.
 */
class ClientService
{
    /**
     * @var ClientWriteModel
     */
    private $clientWriteModel;

    /**
     * @var ClientReadModel
     */
    private $clientReadModel;

    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * ClientService constructor.
     *
     * @param ClientWriteModel $clientWriteModel
     * @param ClientReadModel  $clientReadModel
     * @param UserReadModel    $userReadModel
     */
    public function __construct(
        ClientWriteModel $clientWriteModel,
        ClientReadModel $clientReadModel,
        UserReadModel $userReadModel
    ) {
        $this->clientWriteModel = $clientWriteModel;
        $this->clientReadModel  = $clientReadModel;
        $this->userReadModel    = $userReadModel;
    }

    /**
     * @param ClientId $id
     *
     * @return Client
     *
     * @throws ClientNotFoundException
     */
    public function checkDomain(ClientId $id): Client
    {
        $client = $this->clientWriteModel->find($id);

        if ($client) {
            return $client;
        }

        throw new ClientNotFoundException();
    }

    /**
     * @param ClientId $id
     *
     * @return ClientDocument
     *
     * @throws ClientNotFoundException
     */
    public function checkDocument(ClientId $id): ClientDocument
    {
        $client = $this->clientReadModel->find($id);

        if ($client->isNotEmpty()) {
            return $client;
        }

        throw new ClientNotFoundException();
    }

    /**
     * @param Client $client
     *
     * @throws DuplicatedClientCodeException
     * @throws DuplicatedClientLocationException
     * @throws DuplicatedClientPhoneException
     */
    public function save(Client $client): void
    {
        /*if ($this->clientReadModel->findByCode($client->code())->isNotEmpty()) {
            throw new DuplicatedClientCodeException();
        }*/

        if ($this->clientReadModel->findByPhoneAndMerchant($client->phone(), $client->merchantId())->isNotEmpty()) {
            throw new DuplicatedClientPhoneException();
        }

        if ($this->clientReadModel->findByLocation($client->locationId())->isNotEmpty()) {
            throw new DuplicatedClientLocationException();
        }

        $this->clientWriteModel->save($client);
    }

    /**
     * @param Client     $client
     * @param LocationId $locationId
     *
     * @throws DuplicatedClientLocationException
     */
    public function updateLocation(Client $client, LocationId $locationId): void
    {
        if ($this->clientReadModel->findByLocation($locationId)->isNotEmpty()) {
            throw new DuplicatedClientLocationException();
        }

        $this->clientWriteModel->updateLocation($client->id(), $locationId);
    }

    /**
     * @param Client $client
     */
    public function delete(Client $client): void
    {
        $this->clientWriteModel->delete($client->id());
    }

    /**
     * @param UserId $userId
     * @param Client $client
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteClient(UserId $userId, Client $client): void
    {
        $user = $this->checkUser($userId);

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin()) {
            return;
        }

        if ($client->merchantId()->id() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId         $userId
     * @param ClientDocument $client
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadClient(UserId $userId, ClientDocument $client): void
    {
        $user = $this->checkUser($userId);

        if ($client->isEmpty()) {
            return;
        }

        if ($user->isAdmin() || ($user->isManager() && $client->merchantId() === $user->merchantId())) {
            return;
        }

        if ($user->isMerchantAdmin() && $client->merchantId() === $user->merchantId()) {
            return;
        }

        if ($user->isSad() && $client->merchantId() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }

    /**
     * @param UserId      $userId
     * @param Pagination  $filter
     * @param ClientParts $parts
     *
     * @return PaginatedDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function findAll(UserId $userId, Pagination $filter, ClientParts $parts): PaginatedDocument
    {
        $user = $this->checkUser($userId);

        if ($user->isAdmin()) {
            return $this->clientReadModel->findAll($filter, $parts);
        }

        if ($user->isManager()) {
            return $this
                ->clientReadModel
                ->findAllByMerchant(new MerchantId($user->merchantId()), $filter, $parts);
        }

        if ($user->isSad()) {
            return $this
                ->clientReadModel
                ->findAllByStore(new StoreId($user->storeId()), $filter, $parts);
        }

        throw new InsufficientPermissionsException();
    }
}
