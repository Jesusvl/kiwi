<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Service;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Exception\LocationNotFoundException;
use Kiwi\Shop\Location\Infrastructure\LocationReadModel;
use Kiwi\Shop\Location\Infrastructure\LocationWriteModel;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Infrastructure\UserReadModel;

/**
 * Class LocationService.
 */
class LocationService
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var LocationWriteModel
     */
    private $locationWriteModel;

    /**
     * @var LocationReadModel
     */
    private $locationReadModel;

    /**
     * LocationService constructor.
     *
     * @param LocationWriteModel $locationWriteModel
     * @param LocationReadModel  $locationReadModel
     * @param UserReadModel      $userReadModel
     */
    public function __construct(
        LocationWriteModel $locationWriteModel,
        LocationReadModel $locationReadModel,
        UserReadModel $userReadModel
    ) {
        $this->locationWriteModel = $locationWriteModel;
        $this->locationReadModel  = $locationReadModel;
        $this->userReadModel      = $userReadModel;
    }

    /**
     * @param LocationId $id
     *
     * @return Location
     *
     * @throws LocationNotFoundException
     */
    public function checkDomain(LocationId $id): Location
    {
        $location = $this->locationWriteModel->find($id);

        if ($location) {
            return $location;
        }

        throw new LocationNotFoundException();
    }

    /**
     * @param LocationId $id
     *
     * @return LocationDocument
     *
     * @throws LocationNotFoundException
     */
    public function checkDocument(LocationId $id): LocationDocument
    {
        $location = $this->locationReadModel->find($id);

        if ($location->isNotEmpty()) {
            return $location;
        }

        throw new LocationNotFoundException();
    }

    /**
     * @param Location $location
     */
    public function save(Location $location): void
    {
        $this->locationWriteModel->save($location);
    }

    /**
     * @param Location $location
     */
    public function update(Location $location): void
    {
        $this->locationWriteModel->update($location);
    }

    /**
     * @param Location $location
     */
    public function delete(Location $location): void
    {
        $this->locationWriteModel->delete($location->id());
    }

    /**
     * @param UserId   $userId
     * @param Location $location
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteLocation(UserId $userId, Location $location): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || $user->isManager() || $user->isSad()) {
            return;
        }

        if ($user->isMerchantAdmin()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId           $userId
     * @param LocationDocument $location
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadLocation(UserId $userId, LocationDocument $location): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || $user->isManager() || $user->isSad()) {
            return;
        }

        if ($user->isMerchantAdmin()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }
}
