<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\LocationAnnotation;
use Kiwi\Shop\Location\Domain\LocationCity;
use Kiwi\Shop\Location\Domain\LocationDoor;
use Kiwi\Shop\Location\Domain\LocationFloor;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Domain\LocationPostalCode;
use Kiwi\Shop\Location\Domain\LocationProvince;
use Kiwi\Shop\Location\Domain\LocationStair;
use Kiwi\Shop\Location\Domain\LocationStreet;
use Kiwi\Shop\Location\Domain\Longitude;

/**
 * Class UpdateLocation.
 */
class UpdateLocation implements Command
{
    /**
     * @var LocationId
     */
    private $id;

    /**
     * @var LocationStreet|null
     */
    private $name;

    /**
     * @var LocationStair|null
     */
    private $stairBuilding;

    /**
     * @var LocationFloor|null
     */
    private $floor;

    /**
     * @var LocationDoor|null
     */
    private $door;

    /**
     * @var LocationCity|null
     */
    private $city;

    /**
     * @var LocationProvince|null
     */
    private $province;

    /**
     * @var LocationPostalCode|null
     */
    private $postalCode;

    /**
     * @var bool|null
     */
    private $hasElevator;

    /**
     * @var bool|null
     */
    private $hasStepElevator;

    /**
     * @var LocationAnnotation|null
     */
    private $annotation;

    /**
     * @var Coordinates|null
     */
    private $coordinates;

    /**
     * UpdateLocation constructor.
     *
     * @param string      $id
     * @param string|null $name
     * @param string|null $stairBuilding
     * @param string|null $floor
     * @param string|null $door
     * @param string|null $city
     * @param string|null $province
     * @param string|null $postalCode
     * @param bool|null   $hasElevator
     * @param bool|null   $hasStepElevator
     * @param string|null $annotation
     * @param float|null  $latitude
     * @param float|null  $longitude
     */
    public function __construct(
        string $id,
        ?string $name,
        ?string $stairBuilding,
        ?string $floor,
        ?string $door,
        ?string $city,
        ?string $province,
        ?string $postalCode,
        ?bool $hasElevator,
        ?bool $hasStepElevator,
        ?string $annotation,
        ?float $latitude,
        ?float $longitude
    ) {
        $this->id              = new LocationId($id);
        $this->name            = $name ? new LocationStreet($name) : null;
        $this->stairBuilding   = $stairBuilding ? new LocationStair($stairBuilding) : null;
        $this->floor           = $floor ? new LocationFloor($floor) : null;
        $this->door            = $door ? new LocationDoor($door) : null;
        $this->city            = $city ? new LocationCity($city) : null;
        $this->province        = $province ? new LocationProvince($province) : null;
        $this->postalCode      = $postalCode ? new LocationPostalCode($postalCode) : null;
        $this->hasElevator     = $hasElevator;
        $this->hasStepElevator = $hasStepElevator;
        $this->annotation      = $annotation ? new LocationAnnotation($annotation) : null;
        $this->coordinates     = $latitude && $longitude ? new Coordinates(
            new Latitude($latitude),
            new Longitude($longitude)
        ) : null;
    }

    /**
     * @return LocationId
     */
    public function id(): LocationId
    {
        return $this->id;
    }

    /**
     * @return LocationStreet|null
     */
    public function name(): ?LocationStreet
    {
        return $this->name;
    }

    /**
     * @return LocationStair|null
     */
    public function stairBuilding(): ?LocationStair
    {
        return $this->stairBuilding;
    }

    /**
     * @return LocationFloor|null
     */
    public function floor(): ?LocationFloor
    {
        return $this->floor;
    }

    /**
     * @return LocationDoor|null
     */
    public function door(): ?LocationDoor
    {
        return $this->door;
    }

    /**
     * @return LocationCity|null
     */
    public function city(): ?LocationCity
    {
        return $this->city;
    }

    /**
     * @return LocationProvince|null
     */
    public function province(): ?LocationProvince
    {
        return $this->province;
    }

    /**
     * @return LocationPostalCode|null
     */
    public function postalCode(): ?LocationPostalCode
    {
        return $this->postalCode;
    }

    /**
     * @return bool|null
     */
    public function hasElevator(): ?bool
    {
        return $this->hasElevator;
    }

    /**
     * @return bool|null
     */
    public function hasStepElevator(): ?bool
    {
        return $this->hasStepElevator;
    }

    /**
     * @return LocationAnnotation|null
     */
    public function annotation(): ?LocationAnnotation
    {
        return $this->annotation;
    }

    /**
     * @return Coordinates|null
     */
    public function coordinates(): ?Coordinates
    {
        return $this->coordinates;
    }
}
