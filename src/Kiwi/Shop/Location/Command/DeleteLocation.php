<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class DeleteLocation.
 */
class DeleteLocation implements Command
{
    /**
     * @var LocationId
     */
    private $id;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * DeleteLocation constructor.
     *
     * @param string $userId
     * @param string $id
     */
    public function __construct(string $userId, string $id)
    {
        $this->id     = new LocationId($id);
        $this->userId = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return LocationId
     */
    public function id(): LocationId
    {
        return $this->id;
    }
}
