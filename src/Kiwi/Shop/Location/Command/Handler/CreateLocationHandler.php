<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Location\Command\CreateLocation;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Event\OnCreateLocationEvent;

/**
 * Class CreateLocationHandler.
 */
class CreateLocationHandler extends LocationCommandHandler
{
    /**
     * @param CreateLocation $command
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(CreateLocation $command): void
    {
        $location = new Location(
            $command->id(),
            $command->name(),
            $command->stairBuilding(),
            $command->floor(),
            $command->door(),
            $command->city(),
            $command->province(),
            $command->postalCode(),
            $command->hasElevator(),
            $command->hasStepElevator(),
            $command->coordinates(),
            $command->annotation()
        );

        $this->service()->checkUserCanWriteLocation($command->userId(), $location);
        $this->service()->save($location);

        if (!$location->isCompleted()) {
            $this->eventManager()->dispatch(new OnCreateLocationEvent($location->id()));
        }
    }
}
