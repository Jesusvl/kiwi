<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Shop\Location\Command\UpdateLocation;
use Kiwi\Shop\Location\Domain\IncompleteLocation;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Infrastructure\LocationWriteModel;

/**
 * Class UpdateLocationHandler.
 *
 * @todo as others handlers
 */
class UpdateLocationHandler implements CommandHandler
{
    /**
     * @var LocationWriteModel
     */
    private $writeModel;

    /**
     * UpdateLocationHandler constructor.
     *
     * @param LocationWriteModel $writeModel
     */
    public function __construct(LocationWriteModel $writeModel)
    {
        $this->writeModel = $writeModel;
    }

    /**
     * @param UpdateLocation $command
     */
    public function __invoke(UpdateLocation $command): void
    {
        $location = new IncompleteLocation(
            $command->id(),
            $command->name(),
            $command->stairBuilding(),
            $command->floor(),
            $command->door(),
            $command->city(),
            $command->province(),
            $command->postalCode(),
            $command->hasElevator(),
            $command->hasStepElevator(),
            $command->annotation(),
            $command->coordinates()
        );

        $this->writeModel->updateOld($location);

        unset($locations, $location, $cleanLocation);
    }
}
