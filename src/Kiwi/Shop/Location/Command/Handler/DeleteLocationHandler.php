<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Location\Command\DeleteLocation;
use Kiwi\Shop\Location\Exception\LocationNotFoundException;

/**
 * Class DeleteLocationHandler.
 */
class DeleteLocationHandler extends LocationCommandHandler
{
    /**
     * @param DeleteLocation $command
     *
     * @throws LocationNotFoundException
     * @throws InsufficientPermissionsException
     */
    public function __invoke(DeleteLocation $command): void
    {
        $location = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteLocation($command->userId(), $location);
        $this->service()->delete($location);
    }
}
