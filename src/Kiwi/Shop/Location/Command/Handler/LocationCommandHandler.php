<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Shop\Location\Service\LocationService;

/**
 * Class LocationCommandHandler.
 */
abstract class LocationCommandHandler implements CommandHandler
{
    /**
     * @var LocationService
     */
    private $service;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * LocationCommandHandler constructor.
     *
     * @param LocationService $service
     * @param EventManager    $eventManager
     */
    public function __construct(LocationService $service, EventManager $eventManager)
    {
        $this->service      = $service;
        $this->eventManager = $eventManager;
    }

    /**
     * @return LocationService
     */
    protected function service(): LocationService
    {
        return $this->service;
    }

    /**
     * @return EventManager
     */
    protected function eventManager(): EventManager
    {
        return $this->eventManager;
    }
}
