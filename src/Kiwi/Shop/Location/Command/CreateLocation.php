<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\LocationAnnotation;
use Kiwi\Shop\Location\Domain\LocationCity;
use Kiwi\Shop\Location\Domain\LocationDoor;
use Kiwi\Shop\Location\Domain\LocationFloor;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Domain\LocationPostalCode;
use Kiwi\Shop\Location\Domain\LocationProvince;
use Kiwi\Shop\Location\Domain\LocationStair;
use Kiwi\Shop\Location\Domain\LocationStreet;
use Kiwi\Shop\Location\Domain\Longitude;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class SetLocation.
 */
class CreateLocation implements Command
{
    /**
     * @var LocationId
     */
    private $id;

    /**
     * @var LocationStreet
     */
    private $name;

    /**
     * @var LocationStair
     */
    private $stairBuilding;

    /**
     * @var LocationFloor
     */
    private $floor;

    /**
     * @var LocationDoor
     */
    private $door;

    /**
     * @var LocationCity
     */
    private $city;

    /**
     * @var LocationProvince
     */
    private $province;

    /**
     * @var LocationPostalCode
     */
    private $postalCode;

    /**
     * @var bool
     */
    private $hasElevator;

    /**
     * @var bool
     */
    private $hasStepElevator;

    /**
     * @var LocationAnnotation
     */
    private $annotation;

    /**
     * @var Coordinates
     */
    private $coordinates;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * CreateLocation constructor.
     *
     * @param string $userId
     * @param string $id
     * @param string $name
     * @param string $stairBuilding
     * @param string $floor
     * @param string $door
     * @param string $city
     * @param string $province
     * @param string $postalCode
     * @param bool   $hasElevator
     * @param bool   $hasStepElevator
     * @param string $annotation
     * @param float  $latitude
     * @param float  $longitude
     */
    public function __construct(
        string $userId,
        string $id,
        string $name,
        string $stairBuilding,
        string $floor,
        string $door,
        string $city,
        string $province,
        string $postalCode,
        bool $hasElevator,
        bool $hasStepElevator,
        string $annotation,
        float $latitude,
        float $longitude
    ) {
        $this->id              = new LocationId($id);
        $this->name            = new LocationStreet($name);
        $this->stairBuilding   = new LocationStair($stairBuilding);
        $this->floor           = new LocationFloor($floor);
        $this->door            = new LocationDoor($door);
        $this->city            = new LocationCity($city);
        $this->province        = new LocationProvince($province);
        $this->postalCode      = new LocationPostalCode($postalCode);
        $this->hasElevator     = $hasElevator;
        $this->hasStepElevator = $hasStepElevator;
        $this->annotation      = new LocationAnnotation($annotation);
        $this->coordinates     = new Coordinates(
            new Latitude($latitude),
            new Longitude($longitude)
        );
        $this->userId          = new UserId($userId);
    }

    /**
     * @return LocationId
     */
    public function id(): LocationId
    {
        return $this->id;
    }

    /**
     * @return LocationStreet
     */
    public function name(): LocationStreet
    {
        return $this->name;
    }

    /**
     * @return LocationStair
     */
    public function stairBuilding(): LocationStair
    {
        return $this->stairBuilding;
    }

    /**
     * @return LocationFloor
     */
    public function floor(): LocationFloor
    {
        return $this->floor;
    }

    /**
     * @return LocationDoor
     */
    public function door(): LocationDoor
    {
        return $this->door;
    }

    /**
     * @return LocationCity
     */
    public function city(): LocationCity
    {
        return $this->city;
    }

    /**
     * @return LocationProvince
     */
    public function province(): LocationProvince
    {
        return $this->province;
    }

    /**
     * @return LocationPostalCode
     */
    public function postalCode(): LocationPostalCode
    {
        return $this->postalCode;
    }

    /**
     * @return bool
     */
    public function hasElevator(): bool
    {
        return $this->hasElevator;
    }

    /**
     * @return bool
     */
    public function hasStepElevator(): bool
    {
        return $this->hasStepElevator;
    }

    /**
     * @return LocationAnnotation
     */
    public function annotation(): LocationAnnotation
    {
        return $this->annotation;
    }

    /**
     * @return Coordinates
     */
    public function coordinates(): Coordinates
    {
        return $this->coordinates;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
