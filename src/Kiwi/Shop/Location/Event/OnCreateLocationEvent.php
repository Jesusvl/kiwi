<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Event;

use Kiwi\Core\Infrastructure\Event;
use Kiwi\Shop\Location\Domain\LocationId;

/**
 * Class OnCreateLocationEvent.
 */
class OnCreateLocationEvent implements Event
{
    public const NAME = 'OnCreateLocation';

    /**
     * @var LocationId
     */
    private $locationId;

    /**
     * ClientCreatedEvent constructor.
     *
     * @param LocationId $locationId
     */
    public function __construct(LocationId $locationId)
    {
        $this->locationId = $locationId;
    }

    /**
     * @return string
     */
    public function locationId(): string
    {
        return $this->locationId->id();
    }

    /**
     * @return string
     */
    public static function name(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return \serialize([
            $this->locationId->id()
        ]);
    }

    /**
     * @param string $message
     *
     * @return self
     */
    public static function deserialize(string $message): self
    {
        [$locationId] = \unserialize($message, ['allowed_classes' => false]);

        return new self(new LocationId($locationId));
    }
}
