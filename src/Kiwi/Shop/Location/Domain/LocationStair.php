<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class LocationStair.
 */
class LocationStair
{
    /**
     * @var string
     */
    private $name;

    /**
     * LocationStair constructor.
     *
     * @param string $stair
     */
    public function __construct(string $stair)
    {
        $this->name = $stair;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }
}
