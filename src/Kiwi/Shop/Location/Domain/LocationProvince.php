<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class LocationProvince.
 */
class LocationProvince
{
    /**
     * @var string
     */
    private $province;

    /**
     * LocationProvince constructor.
     *
     * @param string $province
     */
    public function __construct(string $province)
    {
        $this->province = $province;
    }

    /**
     * @return string
     */
    public function province(): string
    {
        return $this->province;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->province();
    }
}
