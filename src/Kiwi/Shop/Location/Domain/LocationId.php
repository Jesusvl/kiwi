<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class LocationId.
 */
class LocationId extends Uuid
{
}
