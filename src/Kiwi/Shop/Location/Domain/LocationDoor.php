<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class LocationDoor.
 */
class LocationDoor
{
    /**
     * @var string
     */
    private $door;

    /**
     * LocationDoor constructor.
     *
     * @param string $door
     */
    public function __construct(string $door)
    {
        $this->door = $door;
    }

    /**
     * @return string
     */
    public function door(): string
    {
        return $this->door;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->door();
    }
}
