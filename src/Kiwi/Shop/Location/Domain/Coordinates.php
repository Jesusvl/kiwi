<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * ValueObejct Coordinate.
 */
class Coordinates
{
    /**
     * @var Latitude
     */
    private $latitude;

    /**
     * @var Longitude
     */
    private $longitude;

    /**
     * Coordinates constructor.
     *
     * @param Latitude  $latitude
     * @param Longitude $longitude
     */
    public function __construct(Latitude $latitude, Longitude $longitude)
    {
        $this->latitude  = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return Latitude
     */
    public function latitude(): Latitude
    {
        return $this->latitude;
    }

    /**
     * @return Longitude
     */
    public function longitude(): Longitude
    {
        return $this->longitude;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return ($this->latitude->value() + $this->longitude->value()) >= 0.000001;
    }
}
