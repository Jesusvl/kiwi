<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class LocationCity.
 */
class LocationCity
{
    /**
     * @var string
     */
    private $city;

    /**
     * LocationCity constructor.
     *
     * @param string $city
     */
    public function __construct(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->city();
    }
}
