<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class Location.
 */
class Location
{
    /**
     * @var LocationId
     */
    private $id;

    /**
     * @var LocationStreet
     */
    private $name;

    /**
     * @var LocationStair
     */
    private $stairBuilding;

    /**
     * @var LocationFloor
     */
    private $floor;

    /**
     * @var LocationDoor
     */
    private $door;

    /**
     * @var LocationCity
     */
    private $city;

    /**
     * @var LocationProvince
     */
    private $province;

    /**
     * @var LocationPostalCode
     */
    private $postalCode;

    /**
     * @var bool
     */
    private $elevator;

    /**
     * @var Latitude
     */
    private $latitude;

    /**
     * @var Longitude
     */
    private $longitude;

    /**
     * @var bool
     */
    private $hasStepElevator;

    /**
     * @var LocationAnnotation
     */
    private $annotation;

    /**
     * @var Coordinates
     */
    private $coordinates;

    /**
     * @var bool
     */
    private $isCompleted;

    /**
     * SetLocationToClient constructor.
     *
     * @param LocationId         $id
     * @param LocationStreet     $name
     * @param LocationStair      $stairBuilding
     * @param LocationFloor      $floor
     * @param LocationDoor       $door
     * @param LocationCity       $city
     * @param LocationProvince   $province
     * @param LocationPostalCode $postalCode
     * @param bool               $elevator
     * @param bool               $hasStepElevator
     * @param Coordinates        $coordinates
     * @param LocationAnnotation $annotation
     */
    public function __construct(
        LocationId $id,
        LocationStreet $name,
        LocationStair $stairBuilding,
        LocationFloor $floor,
        LocationDoor $door,
        LocationCity $city,
        LocationProvince $province,
        LocationPostalCode $postalCode,
        bool $elevator,
        bool $hasStepElevator,
        Coordinates $coordinates,
        LocationAnnotation $annotation
    ) {
        $this->id              = $id;
        $this->name            = $name;
        $this->stairBuilding   = $stairBuilding;
        $this->floor           = $floor;
        $this->door            = $door;
        $this->city            = $city;
        $this->province        = $province;
        $this->postalCode      = $postalCode;
        $this->elevator        = $elevator;
        $this->hasStepElevator = $hasStepElevator;
        $this->latitude        = $coordinates->latitude();
        $this->longitude       = $coordinates->longitude();
        $this->isCompleted     = $coordinates->isCompleted();
        $this->coordinates     = $coordinates;
        $this->annotation      = $annotation;
    }

    /**
     * @return LocationId
     */
    public function id(): LocationId
    {
        return $this->id;
    }

    /**
     * @return LocationStreet
     */
    public function name(): LocationStreet
    {
        return $this->name;
    }

    /**
     * @return LocationStair
     */
    public function stairBuilding(): LocationStair
    {
        return $this->stairBuilding;
    }

    /**
     * @return LocationFloor
     */
    public function floor(): LocationFloor
    {
        return $this->floor;
    }

    /**
     * @return LocationDoor
     */
    public function door(): LocationDoor
    {
        return $this->door;
    }

    /**
     * @return LocationCity
     */
    public function city(): LocationCity
    {
        return $this->city;
    }

    /**
     * @return LocationProvince
     */
    public function province(): LocationProvince
    {
        return $this->province;
    }

    /**
     * @return LocationPostalCode
     */
    public function postalCode(): LocationPostalCode
    {
        return $this->postalCode;
    }

    /**
     * @return bool
     */
    public function hasElevator(): bool
    {
        return $this->elevator;
    }

    /**
     * @return bool
     */
    public function hasStepElevator(): bool
    {
        return $this->hasStepElevator;
    }

    /**
     * @return Coordinates
     */
    public function coordinates(): Coordinates
    {
        $this->coordinates = $this->coordinates ?? new Coordinates($this->latitude, $this->longitude);

        return $this->coordinates;
    }

    /**
     * @return Latitude
     */
    public function latitude(): Latitude
    {
        return $this->latitude;
    }

    /**
     * @return Longitude
     */
    public function longitude(): Longitude
    {
        return $this->longitude;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->isCompleted;
    }

    /**
     * @param Coordinates $coordinate
     */
    public function changeCoordinate(Coordinates $coordinate): void
    {
        $this->latitude    = $coordinate->latitude();
        $this->longitude   = $coordinate->longitude();
        $this->isCompleted = $coordinate->isCompleted();
        $this->coordinates = $coordinate;
    }

    /**
     * @return LocationAnnotation
     */
    public function annotation(): LocationAnnotation
    {
        return $this->annotation;
    }

    /**
     * @return string
     */
    public function prettyAddress(): string
    {
        return sprintf(
            '%s, %s %s',
            $this->name()->name(),
            $this->postalCode()->postalCode(),
            $this->city()->city()
        );
    }
}
