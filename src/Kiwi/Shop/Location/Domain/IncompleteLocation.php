<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class IncompleteLocation.
 */
class IncompleteLocation
{
    /**
     * @var LocationId
     */
    private $id;

    /**
     * @var LocationStreet|null
     */
    private $name;

    /**
     * @var LocationStair|null
     */
    private $stairBuilding;

    /**
     * @var LocationFloor|null
     */
    private $floor;

    /**
     * @var LocationDoor|null
     */
    private $door;

    /**
     * @var LocationCity|null
     */
    private $city;

    /**
     * @var LocationProvince|null
     */
    private $province;

    /**
     * @var LocationPostalCode|null
     */
    private $postalCode;

    /**
     * @var bool|null
     */
    private $elevator;

    /**
     * @var bool|null
     */
    private $hasStepElevator;

    /**
     * @var string|null
     */
    private $annotation;

    /**
     * @var Latitude|null
     */
    private $latitude;

    /**
     * @var Longitude|null
     */
    private $longitude;

    /**
     * @var Coordinates|null
     */
    private $coordinates;

    /**
     * @var bool|null
     */
    private $isCompleted;

    /**
     * UpdateLocation constructor.
     *
     * @param LocationId              $id
     * @param LocationStreet|null     $name
     * @param LocationStair|null      $stairBuilding
     * @param LocationFloor|null      $floor
     * @param LocationDoor|null       $door
     * @param LocationCity|null       $city
     * @param LocationProvince|null   $province
     * @param LocationPostalCode|null $postalCode
     * @param bool|null               $elevator
     * @param bool|null               $hasStepElevator
     * @param LocationAnnotation|null $annotation
     * @param Coordinates|null        $coordinates
     */
    public function __construct(
        LocationId $id,
        ?LocationStreet $name,
        ?LocationStair $stairBuilding,
        ?LocationFloor $floor,
        ?LocationDoor $door,
        ?LocationCity $city,
        ?LocationProvince $province,
        ?LocationPostalCode $postalCode,
        ?bool $elevator,
        ?bool $hasStepElevator,
        ?LocationAnnotation $annotation,
        ?Coordinates $coordinates
    ) {
        $this->id              = $id;
        $this->name            = $name;
        $this->stairBuilding   = $stairBuilding;
        $this->floor           = $floor;
        $this->door            = $door;
        $this->city            = $city;
        $this->province        = $province;
        $this->postalCode      = $postalCode;
        $this->elevator        = $elevator;
        $this->hasStepElevator = $hasStepElevator;
        $this->annotation      = $annotation;
        $this->coordinates     = $coordinates;
        $this->isCompleted     = $coordinates ? $coordinates->isCompleted() : null;
        $this->latitude        = $coordinates ? $coordinates->latitude() : null;
        $this->longitude       = $coordinates ? $coordinates->longitude() : null;
    }

    /**
     * @return LocationId
     */
    public function id(): LocationId
    {
        return $this->id;
    }

    /**
     * @return LocationStreet|null
     */
    public function name(): ?LocationStreet
    {
        return $this->name;
    }

    /**
     * @return LocationStair|null
     */
    public function stairBuilding(): ?LocationStair
    {
        return $this->stairBuilding;
    }

    /**
     * @return LocationFloor|null
     */
    public function floor(): ?LocationFloor
    {
        return $this->floor;
    }

    /**
     * @return LocationDoor|null
     */
    public function door(): ?LocationDoor
    {
        return $this->door;
    }

    /**
     * @return LocationCity|null
     */
    public function city(): ?LocationCity
    {
        return $this->city;
    }

    /**
     * @return LocationProvince|null
     */
    public function province(): ?LocationProvince
    {
        return $this->province;
    }

    /**
     * @return LocationPostalCode|null
     */
    public function postalCode(): ?LocationPostalCode
    {
        return $this->postalCode;
    }

    /**
     * @return bool|null
     */
    public function hasElevator(): ?bool
    {
        return $this->elevator;
    }

    /**
     * @return bool|null
     */
    public function hasStepElevator(): ?bool
    {
        return $this->hasStepElevator;
    }

    /**
     * @return LocationAnnotation|null
     */
    public function annotation(): ?LocationAnnotation
    {
        return $this->annotation;
    }

    /**
     * @return Latitude|null
     */
    public function latitude(): ?Latitude
    {
        return $this->latitude;
    }

    /**
     * @return Longitude|null
     */
    public function longitude(): ?Longitude
    {
        return $this->longitude;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->isCompleted;
    }

    /**
     * @return Coordinates|null
     */
    public function coordinates(): ?Coordinates
    {
        return $this->coordinates;
    }
}
