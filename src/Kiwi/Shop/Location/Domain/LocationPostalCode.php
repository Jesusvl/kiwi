<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class LocationPostalCode.
 */
class LocationPostalCode
{
    /**
     * @var string
     */
    private $postalCode;

    /**
     * LocationPostalCode constructor.
     *
     * @param string $postalCode
     */
    public function __construct(string $postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function postalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->postalCode();
    }
}
