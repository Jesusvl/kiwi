<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Domain;

/**
 * Class LocationAnnotation.
 */
class LocationAnnotation
{
    /**
     * @var string
     */
    private $message;

    /**
     * LocationAnnotation constructor.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->message();
    }
}
