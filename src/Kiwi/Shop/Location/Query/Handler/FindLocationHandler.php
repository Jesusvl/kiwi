<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Query\FindLocation;

/**
 * Class FindLocationHandler.
 */
class FindLocationHandler extends LocationQueryHandler
{
    /**
     * @param FindLocation $query
     *
     * @return LocationDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindLocation $query): LocationDocument
    {
        $location = $this->readModel()->find(new LocationId($query->id()));
        $this->service()->checkUserCanReadLocation($query->userId(), $location);

        return $location;
    }
}
