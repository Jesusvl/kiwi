<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Shop\Location\Query\FindInvalidLocations;

/**
 * Class FindInvalidLocationsHandler.
 */
class FindInvalidLocationsHandler extends LocationQueryHandler
{
    /**
     * @param FindInvalidLocations $query
     *
     * @return PaginatedDocument
     */
    public function __invoke(FindInvalidLocations $query): PaginatedDocument
    {
        $location = $this->readModel()->findInvalidLocations(new Pagination(
            $query->page(),
            $query->pageSize()
        ));

        return $location;
    }
}
