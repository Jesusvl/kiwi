<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Query\Handler;

use Kiwi\Core\Infrastructure\QueryHandler;
use Kiwi\Shop\Location\Infrastructure\LocationReadModel;
use Kiwi\Shop\Location\Service\LocationService;

/**
 * Class LocationQueryHandler.
 */
abstract class LocationQueryHandler implements QueryHandler
{
    /**
     * @var LocationService
     */
    private $service;

    /**
     * @var LocationReadModel
     */
    private $readModel;

    /**
     * LocationQueryHandler constructor.
     *
     * @param LocationService   $service
     * @param LocationReadModel $readModel
     */
    public function __construct(LocationService $service, LocationReadModel $readModel)
    {
        $this->service   = $service;
        $this->readModel = $readModel;
    }

    /**
     * @return LocationService
     */
    protected function service(): LocationService
    {
        return $this->service;
    }

    /**
     * @return LocationReadModel
     */
    public function readModel(): LocationReadModel
    {
        return $this->readModel;
    }
}
