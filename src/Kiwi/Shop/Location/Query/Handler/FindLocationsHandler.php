<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Location\Query\FindLocations;

/**
 * Class FindLocationsHandler.
 */
class FindLocationsHandler extends LocationQueryHandler
{
    /**
     * @param FindLocations $query
     *
     * @return PaginatedDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindLocations $query): PaginatedDocument
    {
        $this->service()->checkUser($query->userId());

        return $this->readModel()->findAll(new Pagination(
            $query->page(),
            $query->pageSize()
        ));
    }
}
