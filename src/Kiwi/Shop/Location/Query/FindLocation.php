<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindLocation.
 */
class FindLocation implements Query
{
    /**
     * @var string
     */
    private $locationId;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindLocation constructor.
     *
     * @param string $userId
     * @param string $locationId
     */
    public function __construct(string $userId, string $locationId)
    {
        $this->locationId = $locationId;
        $this->userId     = new UserId($userId);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->locationId;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
