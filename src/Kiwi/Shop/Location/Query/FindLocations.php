<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindLocations.
 */
class FindLocations implements Query
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUsers constructor.
     *
     * @param string $userId
     * @param int    $page
     * @param int    $pageSize
     */
    public function __construct(string $userId, int $page, int $pageSize)
    {
        $this->page     = $page;
        $this->pageSize = $pageSize;
        $this->userId   = new UserId($userId);
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function pageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
