<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Exception;

use Kiwi\Core\Exception\DataNotFoundDomainException;

/**
 * Class LocationNotFoundException.
 */
class LocationNotFoundException extends DataNotFoundDomainException
{
    /**
     * DuplicatedClientException constructor.
     */
    public function __construct()
    {
        parent::__construct('Location not found.');
    }
}
