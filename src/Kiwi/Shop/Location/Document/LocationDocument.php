<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Document;

use Kiwi\Core\Document\Document;
use Kiwi\Shop\Location\Domain\Location;

/**
 * Class LocationDocument.
 */
class LocationDocument extends Document
{
    /**
     * @var Location
     */
    private $location;

    /**
     * LocationDocument constructor.
     *
     * @param Location|null $location
     */
    public function __construct(?Location $location)
    {
        parent::__construct($location);
        $this->location = $location;
    }

    /**
     * @return LocationDocument
     */
    public static function empty(): self
    {
        return new self(null);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->location->id()->id();
    }

    /**
     * @return string
     */
    public function postalCode(): string
    {
        return $this->location->postalCode()->postalCode();
    }

    /**
     * @return string
     */
    public function province(): string
    {
        return $this->location->province()->province();
    }

    /**
     * @return string
     */
    public function city(): string
    {
        return $this->location->city()->city();
    }

    /**
     * @return string
     */
    public function door(): string
    {
        return $this->location->door()->door();
    }

    /**
     * @return string
     */
    public function floor(): string
    {
        return $this->location->floor()->floor();
    }

    /**
     * @return string
     */
    public function stairBuilding(): string
    {
        return $this->location->stairBuilding()->name();
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->location->name()->name();
    }

    /**
     * @return string
     */
    public function annotation(): string
    {
        return $this->location->annotation()->message();
    }

    /**
     * @return float
     */
    public function latitude(): float
    {
        return $this->location->latitude()->value();
    }

    /**
     * @return float
     */
    public function longitude(): float
    {
        return $this->location->longitude()->value();
    }

    /**
     * @return bool
     */
    public function hasStepElevator(): bool
    {
        return $this->location->hasStepElevator();
    }

    /**
     * @return bool
     */
    public function hasElevator(): bool
    {
        return $this->location->hasElevator();
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->location->isCompleted();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->location ? [
            'id'            => $this->id(),
            'name'          => $this->name(),
            'postalCode'    => $this->postalCode(),
            'city'          => $this->city(),
            'province'      => $this->province(),
            'floor'         => $this->floor(),
            'door'          => $this->door(),
            'stairBuilding' => $this->stairBuilding(),
            'latitude'      => $this->latitude(),
            'longitude'     => $this->longitude(),
            'hasElevator'   => $this->hasElevator(),
            'stepElevator'  => $this->hasStepElevator(),
            'annotation'    => $this->annotation(),
            'isCompleted'   => $this->isCompleted()
        ] : null;
    }
}
