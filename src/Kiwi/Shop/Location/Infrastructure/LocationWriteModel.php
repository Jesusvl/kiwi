<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Infrastructure;

use Kiwi\Shop\Location\Domain\IncompleteLocation;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Domain\LocationId;

/**
 * Interface LocationWriteModel.
 */
interface LocationWriteModel
{
    /**
     * @param LocationId $locationId
     *
     * @return Location|null
     */
    public function find(LocationId $locationId): ?Location;

    /**
     * @param Location $location
     */
    public function save(Location $location): void;

    /**
     * @param IncompleteLocation $location
     */
    public function updateOld(IncompleteLocation $location): void;

    /**
     * @param Location $location
     */
    public function update(Location $location): void;

    /**
     * @param LocationId $locationId
     */
    public function delete(LocationId $locationId): void;
}
