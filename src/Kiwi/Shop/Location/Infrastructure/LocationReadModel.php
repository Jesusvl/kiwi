<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Infrastructure;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Domain\LocationId;

/**
 * Interface LocationReadModel.
 */
interface LocationReadModel
{
    /**
     * @param LocationId $id
     *
     * @return LocationDocument
     */
    public function find(LocationId $id): LocationDocument;

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter): PaginatedDocument;

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findInvalidLocations(Pagination $filter): PaginatedDocument;
}
