<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Test\Mock;

use Kiwi\Core\Test\Mock;
use Kiwi\Shop\Location\Infrastructure\LocationWriteModel;

/**
 * Class LocationReadModelMock.
 *
 * @method shouldSave($location, $return)
 * @method shouldUpdateOld($location, $return)
 * @method shouldDelete($id, $return)
 */
class LocationRepositoryMock extends Mock
{
    /**
     * LocationReadModelMock constructor.
     */
    public function __construct()
    {
        parent::__construct(LocationWriteModel::class);
    }
}
