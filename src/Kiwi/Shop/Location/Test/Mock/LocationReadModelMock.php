<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Test\Mock;

use Kiwi\Core\Test\Mock;
use Kiwi\Shop\Location\Infrastructure\LocationReadModel;

/**
 * Class LocationReadModelMock.
 *
 * @method shouldFind($id, $return)
 * @method shouldFindAll($filter, $return)
 * @method shouldFindInvalidLocations($filter, $return)
 */
class LocationReadModelMock extends Mock
{
    /**
     * LocationReadModelMock constructor.
     */
    public function __construct()
    {
        parent::__construct(LocationReadModel::class);
    }
}
