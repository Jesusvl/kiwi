<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Test\Dummy;

use Faker\Factory;
use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\IncompleteLocation;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\LocationAnnotation;
use Kiwi\Shop\Location\Domain\LocationCity;
use Kiwi\Shop\Location\Domain\LocationDoor;
use Kiwi\Shop\Location\Domain\LocationFloor;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Domain\LocationPostalCode;
use Kiwi\Shop\Location\Domain\LocationProvince;
use Kiwi\Shop\Location\Domain\LocationStair;
use Kiwi\Shop\Location\Domain\LocationStreet;
use Kiwi\Shop\Location\Domain\Longitude;

/**
 * Class IncompleteLocationDummy.
 */
class IncompleteLocationDummy
{
    /**
     * @param string $id
     *
     * @return IncompleteLocation
     */
    public static function random(string $id): IncompleteLocation
    {
        return new IncompleteLocation(
            new LocationId($id),
            \random_int(0, 1) ? null : new LocationStreet(Factory::create()->streetAddress),
            \random_int(0, 1) ? null : new LocationStair(Factory::create()->word),
            \random_int(0, 1) ? null : new LocationFloor(Factory::create()->randomLetter),
            \random_int(0, 1) ? null : new LocationDoor(Factory::create()->randomLetter),
            \random_int(0, 1) ? null : new LocationCity(Factory::create()->city),
            \random_int(0, 1) ? null : new LocationProvince(Factory::create()->city),
            \random_int(0, 1) ? null : new LocationPostalCode(Factory::create()->postcode),
            Factory::create()->boolean,
            Factory::create()->boolean,
            \random_int(0, 1) ? null : new LocationAnnotation(Factory::create()->sentence),
            \random_int(0, 1) ? null : new Coordinates(
                new Latitude(Factory::create()->latitude),
                new Longitude(Factory::create()->longitude)
            )
        );
    }
}
