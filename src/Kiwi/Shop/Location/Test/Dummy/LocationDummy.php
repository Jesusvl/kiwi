<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Test\Dummy;

use Faker\Factory;
use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Domain\LocationAnnotation;
use Kiwi\Shop\Location\Domain\LocationCity;
use Kiwi\Shop\Location\Domain\LocationDoor;
use Kiwi\Shop\Location\Domain\LocationFloor;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Domain\LocationPostalCode;
use Kiwi\Shop\Location\Domain\LocationProvince;
use Kiwi\Shop\Location\Domain\LocationStair;
use Kiwi\Shop\Location\Domain\LocationStreet;
use Kiwi\Shop\Location\Domain\Longitude;
use KiwiInfrastructure\Shop\Location\Doctrine\Type\LocationLatitudeType;

/**
 * Class LocationDummy.
 */
class LocationDummy
{
    /**
     * @param string $id
     *
     * @return Location
     */
    public static function random(string $id): Location
    {
        return new Location(
            new LocationId($id),
            new LocationStreet(Factory::create()->streetAddress),
            new LocationStair(Factory::create()->word),
            new LocationFloor(Factory::create()->randomLetter),
            new LocationDoor(Factory::create()->randomLetter),
            new LocationCity(Factory::create()->city),
            new LocationProvince(Factory::create()->city),
            new LocationPostalCode(Factory::create()->postcode),
            Factory::create()->boolean,
            Factory::create()->boolean,
            new Coordinates(
                new Latitude(Factory::create()->latitude),
                new Longitude(Factory::create()->longitude)
            ),
            new LocationAnnotation(Factory::create()->sentence)
        );
    }
}
