<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Test;

use Kiwi\Shop\Location\Test\Mock\LocationReadModelMock;
use Kiwi\Shop\Location\Test\Mock\LocationRepositoryMock;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

/**
 * Class LocationUnitTest.
 */
class LocationUnitTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @var LocationReadModelMock
     */
    protected $readModel;

    /**
     * @var LocationRepositoryMock
     */
    protected $repository;

    protected function setUp(): void
    {
        $this->readModel  = new LocationReadModelMock();
        $this->repository = new LocationRepositoryMock();
    }
}
