<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Listener;

use Kiwi\Core\Exception\DataNotFoundDomainException;
use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Map\Map\Document\CoordinatesDocument;
use Kiwi\Map\Map\Query\FetchCoordinates;
use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Domain\Longitude;
use Kiwi\Shop\Location\Event\OnCreateLocationEvent;
use Kiwi\Shop\Location\Infrastructure\LocationWriteModel;

/**
 * Class FetchLocationCoordinates.
 */
class FetchLocationCoordinates implements AsyncListener
{
    /**
     * @var LocationWriteModel
     */
    private $locationRepository;

    /**
     * @var Bus
     */
    private $bus;

    /**
     * FetchLocationCoordinates constructor.
     *
     * @param LocationWriteModel $locationRepository
     * @param Bus                $bus
     */
    public function __construct(
        LocationWriteModel $locationRepository,
        Bus $bus
    ) {
        $this->locationRepository = $locationRepository;
        $this->bus                = $bus;
    }

    /**
     * @param Event|OnCreateLocationEvent $event
     */
    public function handle(Event $event): void
    {
        $location = $this->locationRepository->find(new LocationId($event->locationId()));

        if (!$location) {
            throw new DataNotFoundDomainException('Location not found');
        }

        /** @var CoordinatesDocument $coordinates */
        $coordinates = $this->bus->dispatchQuery(new FetchCoordinates($location->prettyAddress()));

        $location->changeCoordinate(new Coordinates(
            new Latitude($coordinates->latitude()),
            new Longitude($coordinates->longitude())
        ));

        $this->locationRepository->update($location);
    }

    /**
     * @return string
     */
    public function listensTo(): string
    {
        return OnCreateLocationEvent::NAME;
    }
}
