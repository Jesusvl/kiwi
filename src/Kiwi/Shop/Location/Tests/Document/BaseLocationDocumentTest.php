<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Tests\Document;

use Kiwi\Core\Utils\Uuid;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Test\Dummy\LocationDummy;
use Kiwi\Shop\Location\Test\LocationUnitTest;

/**
 * Class BaseLocationDocumentTest.
 *
 * @covers \Kiwi\Shop\Location\Document\LocationDocument
 */
class BaseLocationDocumentTest extends LocationUnitTest
{
    public function testShouldHaveValidData(): void
    {
        $location = LocationDummy::random(Uuid::new());
        $document = new LocationDocument($location);

        self::assertEquals([
            'id'            => $location->id()->id(),
            'name'          => $location->name()->name(),
            'postalCode'    => $location->postalCode()->postalCode(),
            'city'          => $location->city()->city(),
            'province'      => $location->province()->province(),
            'floor'         => $location->floor()->floor(),
            'door'          => $location->door()->door(),
            'stairBuilding' => $location->stairBuilding()->name(),
            'latitude'      => $location->latitude()->value(),
            'longitude'     => $location->longitude()->value(),
            'hasElevator'   => $location->hasElevator(),
            'stepElevator'  => $location->hasStepElevator(),
            'annotation'    => $location->annotation()->message(),
            'isCompleted'   => $location->isCompleted()
        ], $document->toScalar());

        $document = new LocationDocument(null);
        self::assertNull($document->toScalar());
    }
}
