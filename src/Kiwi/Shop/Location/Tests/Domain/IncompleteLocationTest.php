<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Tests\Domain;

use Faker\Factory;
use Kiwi\Core\Utils\Uuid;
use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\IncompleteLocation;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\LocationAnnotation;
use Kiwi\Shop\Location\Domain\LocationCity;
use Kiwi\Shop\Location\Domain\LocationDoor;
use Kiwi\Shop\Location\Domain\LocationFloor;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Domain\LocationPostalCode;
use Kiwi\Shop\Location\Domain\LocationProvince;
use Kiwi\Shop\Location\Domain\LocationStair;
use Kiwi\Shop\Location\Domain\LocationStreet;
use Kiwi\Shop\Location\Domain\Longitude;
use Kiwi\Shop\Location\Test\LocationUnitTest;

/**
 * Class IncompleteLocationTest.
 *
 * @covers \Kiwi\Shop\Location\Domain\IncompleteLocation
 */
class IncompleteLocationTest extends LocationUnitTest
{
    public function testShouldHaveValidData(): void
    {
        $id           = Uuid::new();
        $name         = Factory::create()->address;
        $stair        = Factory::create()->word;
        $floor        = Factory::create()->randomLetter;
        $door         = Factory::create()->randomLetter;
        $city         = Factory::create()->city;
        $province     = Factory::create()->city;
        $postalCode   = Factory::create()->postcode;
        $elevator     = Factory::create()->boolean;
        $stepElevator = Factory::create()->boolean;
        $latitude     = Factory::create()->latitude;
        $longitude    = Factory::create()->longitude;
        $annotation   = Factory::create()->sentence;

        $location = new IncompleteLocation(
            new LocationId($id),
            new LocationStreet($name),
            new LocationStair($stair),
            new LocationFloor($floor),
            new LocationDoor($door),
            new LocationCity($city),
            new LocationProvince($province),
            new LocationPostalCode($postalCode),
            $elevator,
            $stepElevator,
            new LocationAnnotation($annotation),
            new Coordinates(new Latitude($latitude), new Longitude($longitude))
        );
        self::assertEquals($id, $location->id());
        self::assertEquals($name, $location->name());
        self::assertEquals($stair, $location->stairBuilding());
        self::assertEquals($floor, $location->floor());
        self::assertEquals($door, $location->door());
        self::assertEquals($city, $location->city());
        self::assertEquals($province, $location->province());
        self::assertEquals($postalCode, $location->postalCode());
        self::assertEquals($elevator, $location->hasElevator());
        self::assertEquals($stepElevator, $location->hasStepElevator());
        self::assertEquals($latitude, $location->latitude()->value());
        self::assertEquals($longitude, $location->longitude()->value());
        self::assertEquals($annotation, $location->annotation());
    }

    public function testShouldAcceptNulls(): void
    {
        $id       = Uuid::new();
        $location = new IncompleteLocation(
            new LocationId($id),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );

        self::assertEquals($id, $location->id());
        self::assertNull($location->name());
        self::assertNull($location->stairBuilding());
        self::assertNull($location->floor());
        self::assertNull($location->door());
        self::assertNull($location->city());
        self::assertNull($location->province());
        self::assertNull($location->postalCode());
        self::assertNull($location->hasElevator());
        self::assertNull($location->hasStepElevator());
        self::assertNull($location->coordinates());
        self::assertNull($location->annotation());
    }
}
