<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Tests\Domain;

use Faker\Factory;
use Kiwi\Core\Utils\Uuid;
use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Domain\LocationAnnotation;
use Kiwi\Shop\Location\Domain\LocationCity;
use Kiwi\Shop\Location\Domain\LocationDoor;
use Kiwi\Shop\Location\Domain\LocationFloor;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Domain\LocationPostalCode;
use Kiwi\Shop\Location\Domain\LocationProvince;
use Kiwi\Shop\Location\Domain\LocationStair;
use Kiwi\Shop\Location\Domain\LocationStreet;
use Kiwi\Shop\Location\Domain\Longitude;
use Kiwi\Shop\Location\Test\Dummy\LocationDummy;
use Kiwi\Shop\Location\Test\LocationUnitTest;

/**
 * Class LocationTest.
 *
 * @covers \Kiwi\Shop\Location\Domain\Location
 */
class LocationTest extends LocationUnitTest
{
    public function testShouldHaveValidData(): void
    {
        $id           = Uuid::new();
        $name         = Factory::create()->address;
        $stair        = Factory::create()->word;
        $floor        = Factory::create()->randomLetter;
        $door         = Factory::create()->randomLetter;
        $city         = Factory::create()->city;
        $province     = Factory::create()->city;
        $postalCode   = Factory::create()->postcode;
        $elevator     = Factory::create()->boolean;
        $stepElevator = Factory::create()->boolean;
        $latitude     = 1;
        $longitude    = 2;
        $annotation   = Factory::create()->sentence;

        $location = new Location(
            new LocationId($id),
            new LocationStreet($name),
            new LocationStair($stair),
            new LocationFloor($floor),
            new LocationDoor($door),
            new LocationCity($city),
            new LocationProvince($province),
            new LocationPostalCode($postalCode),
            $elevator,
            $stepElevator,
            new Coordinates(new Latitude($latitude), new Longitude($longitude)),
            new LocationAnnotation($annotation)
        );
        self::assertEquals($id, $location->id());
        self::assertEquals($name, $location->name());
        self::assertEquals($stair, $location->stairBuilding());
        self::assertEquals($floor, $location->floor());
        self::assertEquals($door, $location->door());
        self::assertEquals($city, $location->city());
        self::assertEquals($province, $location->province());
        self::assertEquals($postalCode, $location->postalCode());
        self::assertEquals($elevator, $location->hasElevator());
        self::assertEquals($stepElevator, $location->hasStepElevator());
        self::assertEquals($latitude, $location->latitude()->value());
        self::assertEquals($longitude, $location->longitude()->value());
        self::assertEquals($annotation, $location->annotation());
        self::assertTrue($location->isCompleted());
    }

    public function testShouldChangeCoordinates(): void
    {
        $location = LocationDummy::random(Uuid::new());

        $latitude  = 0;
        $longitude = 0;
        $location->changeCoordinate(new Coordinates(new Latitude($latitude), new Longitude($longitude)));
        self::assertEquals($latitude, $location->latitude()->value());
        self::assertEquals($longitude, $location->longitude()->value());
        self::assertFalse($location->isCompleted());

        $latitude  = 2;
        $longitude = 1;
        $location->changeCoordinate(new Coordinates(new Latitude($latitude), new Longitude($longitude)));
        self::assertEquals($latitude, $location->latitude()->value());
        self::assertEquals($longitude, $location->longitude()->value());
        self::assertTrue($location->isCompleted());
    }
}
