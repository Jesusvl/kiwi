<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Tests\Domain;

use Kiwi\Core\Utils\Uuid;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Test\LocationUnitTest;

/**
 * Class LocationIdTest.
 *
 * @covers \Kiwi\Shop\Location\Domain\LocationId
 */
class LocationIdTest extends LocationUnitTest
{
    public function testShouldHaveValidData(): void
    {
        $id         = Uuid::new();
        $locationId = new LocationId($id);
        self::assertEquals($id, $locationId->id());
        self::assertEquals($id, (string) $locationId);
    }
}
