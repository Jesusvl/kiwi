<?php

declare(strict_types=1);

namespace Kiwi\Shop\Location\Tests\Domain;

use Kiwi\Shop\Location\Domain\Coordinates;
use Kiwi\Shop\Location\Domain\Latitude;
use Kiwi\Shop\Location\Domain\Longitude;
use Kiwi\Shop\Location\Test\LocationUnitTest;

/**
 * Class CoordinateTest.
 *
 * @covers \Kiwi\Shop\Location\Domain\Coordinates
 */
class CoordinateTest extends LocationUnitTest
{
    public function testShouldHaveValidData(): void
    {
        $latitude   = 4.14;
        $longitude  = 2.21;
        $coordinate = new Coordinates(new Latitude($latitude), new Longitude($longitude));
        self::assertEquals($latitude, $coordinate->latitude()->value());
        self::assertEquals($longitude, $coordinate->longitude()->value());
        self::assertTrue($coordinate->isCompleted());
    }

    public function testShouldNotBeCompleted(): void
    {
        $latitude   = 0.000000;
        $longitude  = 0.000000;
        $coordinate = new Coordinates(new Latitude($latitude), new Longitude($longitude));
        self::assertFalse($coordinate->isCompleted());
    }
}
