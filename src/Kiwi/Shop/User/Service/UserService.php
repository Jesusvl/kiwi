<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Service;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\User;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Exception\DuplicatedPhoneException;
use Kiwi\Shop\User\Exception\DuplicatedUsernameException;
use Kiwi\Shop\User\Exception\UserNotFoundException;
use Kiwi\Shop\User\Infrastructure\UserReadModel;
use Kiwi\Shop\User\Infrastructure\UserWriteModel;

/**
 * Class UserService.
 */
class UserService
{
    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * @var UserWriteModel
     */
    private $userWriteModel;

    /**
     * UserService constructor.
     *
     * @param UserWriteModel $userWriteModel
     * @param UserReadModel  $userReadModel
     */
    public function __construct(
        UserWriteModel $userWriteModel,
        UserReadModel $userReadModel
    ) {
        $this->userWriteModel = $userWriteModel;
        $this->userReadModel  = $userReadModel;
    }

    /**
     * @param UserId $id
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function checkDomain(UserId $id): User
    {
        $user = $this->userWriteModel->find($id);

        if ($user) {
            return $user;
        }

        throw new UserNotFoundException();
    }

    /**
     * @param UserId $id
     *
     * @return UserDocument
     *
     * @throws UserNotFoundException
     */
    public function checkDocument(UserId $id): UserDocument
    {
        $user = $this->userReadModel->find($id, new UserParts());

        if ($user->isNotEmpty()) {
            return $user;
        }

        throw new UserNotFoundException();
    }

    /**
     * @param User $user
     *
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function save(User $user): void
    {
        if ($this->userReadModel->findByUsername($user->username())->isNotEmpty()) {
            throw new DuplicatedUsernameException();
        }

        if ($this->userReadModel->findByPhone($user->phone())->isNotEmpty()) {
            throw new DuplicatedPhoneException();
        }

        $this->userWriteModel->save($user);
    }

    /**
     * @param User $user
     *
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     * @throws UserNotFoundException
     */
    public function update(User $user): void
    {
        $tmpUser = $this->userReadModel->findByUsername($user->username());
        if ($tmpUser->isEmpty()) {
            throw new UserNotFoundException();
        }

        if ($tmpUser->id() !== $user->id()->id()) {
            throw new DuplicatedUsernameException();
        }

        $tmpUser = $this->userReadModel->findByPhone($user->phone());
        if ($tmpUser->isEmpty()) {
            throw new UserNotFoundException();
        }

        if ($tmpUser->id() !== $user->id()->id()) {
            throw new DuplicatedPhoneException();
        }

        $this->userWriteModel->update($user);
    }

    /**
     * @param UserId $userId
     * @param User   $userTarget
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteUser(UserId $userId, User $userTarget): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || $user->id() === $userTarget->id()) {
            return;
        }

        if ($user->isMerchantAdmin() && $userTarget->merchantId()->id() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId       $userId
     * @param UserDocument $userTarget
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadUser(UserId $userId, UserDocument $userTarget): void
    {
        $user = $this->userReadModel->find($userId, new UserParts());

        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        if ($user->isAdmin() || $user->id() === $userTarget->id()) {
            return;
        }

        if ($user->isMerchantAdmin() && $userTarget->merchantId() === $user->merchantId()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }
}
