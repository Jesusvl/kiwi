<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Event;

use Kiwi\Core\Infrastructure\Event;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserName;
use Kiwi\Shop\User\Domain\UserPhone;
use Kiwi\Shop\User\Domain\UserRole;
use Kiwi\Shop\User\Domain\UserUsername;

/**
 * Class OnCreateUserEvent.
 */
class OnCreateUserEvent implements Event
{
    public const NAME = 'OnCreateUser';

    /**
     * @var UserId
     */
    private $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserUsername
     */
    private $username;

    /**
     * @var UserRole
     */
    private $role;

    /**
     * @var UserPhone
     */
    private $phone;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @var MerchantId
     */
    private $merchantId;

    /**
     * OnCreateUserEvent constructor.
     *
     * @param UserId       $id
     * @param UserName     $name
     * @param UserUsername $username
     * @param UserRole     $role
     * @param UserPhone    $phone
     * @param StoreId      $storeId
     * @param MerchantId   $merchantId
     */
    public function __construct(
        UserId $id,
        UserName $name,
        UserUsername $username,
        UserRole $role,
        UserPhone $phone,
        StoreId $storeId,
        MerchantId $merchantId
    ) {
        $this->id         = $id;
        $this->name       = $name;
        $this->username   = $username;
        $this->role       = $role;
        $this->phone      = $phone;
        $this->storeId    = $storeId;
        $this->merchantId = $merchantId;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id->id();
    }

    /**
     * @return string
     */
    public function merchantId(): string
    {
        return $this->merchantId->id();
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username->name();
    }

    /**
     * @return string
     */
    public function role(): string
    {
        return $this->role->name();
    }

    /**
     * @return string
     */
    public function phone(): string
    {
        return $this->phone->number();
    }

    /**
     * @return string
     */
    public function storeId(): string
    {
        return $this->storeId->id();
    }

    /**
     * @return string
     */
    public function fullName(): string
    {
        return $this->name->name();
    }

    /**
     * @return string
     */
    public static function name(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->id->id(),
            $this->name->name(),
            $this->username->name(),
            $this->role->name(),
            $this->phone->number(),
            $this->storeId->id(),
            $this->merchantId->id()
        ]);
    }

    /**
     * @param string $message
     *
     * @return OnCreateUserEvent
     */
    public static function deserialize(string $message): OnCreateUserEvent
    {
        [
            $id,
            $name,
            $username,
            $role,
            $phone,
            $store,
            $merchant
        ] = unserialize($message, ['allowed_classes' => false]);
        return new self(
            new UserId($id),
            new UserName($name),
            new UserUsername($username),
            new UserRole($role),
            new UserPhone($phone),
            new StoreId($store),
            new MerchantId($merchant)
        );
    }
}
