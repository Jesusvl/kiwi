<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Domain;

use Kiwi\Core\Domain\Uuid;

/**
 * Class UserId.
 */
class UserId extends Uuid
{
}
