<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Command;

use Kiwi\Core\Infrastructure\Command;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserName;
use Kiwi\Shop\User\Domain\UserPhone;
use Kiwi\Shop\User\Domain\UserRole;
use Kiwi\Shop\User\Domain\UserUsername;

/**
 * Class UpdateUser.
 */
class UpdateUser implements Command
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var UserId
     */
    private $id;

    /**
     * @var null|StoreId
     */
    private $storeId;

    /**
     * @var null|MerchantId
     */
    private $merchantId;

    /**
     * @var UserUsername
     */
    private $username;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserRole
     */
    private $role;

    /**
     * @var UserPhone
     */
    private $phone;

    /**
     * CreateUser constructor.
     *
     * @param string      $userId the user who wnats to update the user
     * @param string      $id
     * @param null|string $storeId
     * @param null|string $merchantId
     * @param string      $username
     * @param string      $name
     * @param string      $role
     * @param string      $phone
     */
    public function __construct(
        string $userId,
        string $id,
        ?string $storeId,
        ?string $merchantId,
        string $username,
        string $name,
        string $role,
        string $phone
    ) {
        $this->userId     = new UserId($userId);
        $this->id         = new UserId($id);
        $this->storeId    = new StoreId($storeId);
        $this->merchantId = new MerchantId($merchantId);
        $this->username   = new UserUsername($username);
        $this->name       = new UserName($name);
        $this->role       = new UserRole($role);
        $this->phone      = new UserPhone($phone);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return UserId
     */
    public function id(): UserId
    {
        return $this->id;
    }

    /**
     * @return StoreId|null
     */
    public function storeId(): ?StoreId
    {
        return $this->storeId;
    }

    /**
     * @return MerchantId|null
     */
    public function merchantId(): ?MerchantId
    {
        return $this->merchantId;
    }

    /**
     * @return UserUsername
     */
    public function username(): UserUsername
    {
        return $this->username;
    }

    /**
     * @return UserName
     */
    public function name(): UserName
    {
        return $this->name;
    }

    /**
     * @return UserRole
     */
    public function role(): UserRole
    {
        return $this->role;
    }

    /**
     * @return UserPhone
     */
    public function phone(): UserPhone
    {
        return $this->phone;
    }
}
