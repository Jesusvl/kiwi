<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Shop\User\Service\UserService;

/**
 * Class UserCommandHandler.
 */
abstract class UserCommandHandler implements CommandHandler
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * UserCommandHandler constructor.
     *
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @return UserService
     */
    public function service(): UserService
    {
        return $this->service;
    }
}
