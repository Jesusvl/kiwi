<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\User\Command\UpdateUser;
use Kiwi\Shop\User\Domain\User;
use Kiwi\Shop\User\Exception\DuplicatedPhoneException;
use Kiwi\Shop\User\Exception\DuplicatedUsernameException;
use Kiwi\Shop\User\Exception\UserNotFoundException;

/**
 * Class UpdateUserHandler.
 */
class UpdateUserHandler extends UserCommandHandler
{
    /**
     * @param UpdateUser $command
     *
     * @throws InsufficientPermissionsException
     * @throws UserNotFoundException
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function __invoke(UpdateUser $command): void
    {
        $user = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteUser($command->userId(), $user);

        $this->service()->update(new User(
            $command->id(),
            $command->storeId(),
            $command->merchantId(),
            $command->username(),
            $command->name(),
            $command->role(),
            $command->phone()
        ));
    }
}
