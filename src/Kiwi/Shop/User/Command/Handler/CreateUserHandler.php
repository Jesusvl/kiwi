<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Command\Handler;

use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Shop\User\Command\CreateUser;
use Kiwi\Shop\User\Domain\User;
use Kiwi\Shop\User\Exception\DuplicatedPhoneException;
use Kiwi\Shop\User\Exception\DuplicatedUsernameException;
use Kiwi\Shop\User\Infrastructure\UserWriteModel;

/**
 * Class CreateUserHandler.
 */
class CreateUserHandler extends UserCommandHandler
{
    /**
     * @param CreateUser $user
     *
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function __invoke(CreateUser $user): void
    {
        $this->service()->save(new User(
            $user->id(),
            $user->storeId(),
            $user->merchantId(),
            $user->username(),
            $user->name(),
            $user->role(),
            $user->phone()
        ));
    }
}
