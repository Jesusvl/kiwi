<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Command\Handler;

use Kiwi\Core\Exception\DomainException;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Core\Infrastructure\CommandHandler;
use Kiwi\Shop\User\Command\AssignMerchant;
use Kiwi\Shop\User\Exception\DuplicatedPhoneException;
use Kiwi\Shop\User\Exception\DuplicatedUsernameException;
use Kiwi\Shop\User\Exception\UserNotFoundException;
use Kiwi\Shop\User\Infrastructure\UserWriteModel;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Class AssignMerchantHandler.
 */
class AssignMerchantHandler extends UserCommandHandler
{
    /**
     * @param AssignMerchant $command
     *
     * @throws InsufficientPermissionsException
     * @throws UserNotFoundException
     * @throws DuplicatedPhoneException
     * @throws DuplicatedUsernameException
     */
    public function __invoke(AssignMerchant $command): void
    {
        $user = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteUser($command->userId(), $user);
        $user->assignMerchant($command->merchantId());
        $this->service()->update($user);
    }
}
