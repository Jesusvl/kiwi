<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Infrastructure;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Domain\UserPhone;
use Kiwi\Shop\User\Domain\UserUsername;

/**
 * Interface UserReadModel.
 */
interface UserReadModel
{
    /**
     * @param UserId         $id
     * @param UserParts|null $parts
     *
     * @return UserDocument
     */
    public function find(UserId $id, ?UserParts $parts = null): UserDocument;

    /**
     * @param UserUsername $username
     * @param UserParts    $parts
     *
     * @return UserDocument
     */
    public function findByUsername(UserUsername $username, ?UserParts $parts = null): UserDocument;

    /**
     * @param UserPhone      $phone
     * @param UserParts|null $parts
     *
     * @return UserDocument
     */
    public function findByPhone(UserPhone $phone, ?UserParts $parts = null): UserDocument;

    /**
     * @param Pagination     $filter
     * @param UserParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter, ?UserParts $parts = null): PaginatedDocument;
}
