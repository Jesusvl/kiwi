<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\User\Query\FindUsers;

/**
 * Class FindUsersHandler.
 */
class FindUsersHandler extends UserQueryHandler
{
    /**
     * @param FindUsers $query
     *
     * @return PaginatedDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindUsers $query): PaginatedDocument
    {
        $user = $this->service()->checkUser($query->userId());
        if (!$user->isAdmin()) {
            throw new InsufficientPermissionsException();
        }

        return $this->readModel()->findAll($query->filter(), $query->parts());
    }
}
