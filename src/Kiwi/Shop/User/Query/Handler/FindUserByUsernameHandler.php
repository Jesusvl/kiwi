<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Query\FindUserByUsername;

/**
 * Class FindUserByUsernameHandler.
 */
class FindUserByUsernameHandler extends UserQueryHandler
{
    /**
     * @param FindUserByUsername $query
     *
     * @return UserDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindUserByUsername $query): UserDocument
    {
        $user = $this->readModel()->findByUsername($query->username(), $query->parts());

        $this->service()->checkUserCanReadUser($query->userId(), $user);

        return $user;
    }
}
