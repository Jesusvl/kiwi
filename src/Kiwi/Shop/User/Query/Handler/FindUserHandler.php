<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Query\FindUser;

/**
 * Class FindUserHandler.
 */
class FindUserHandler extends UserQueryHandler
{
    /**
     * @param FindUser $query
     *
     * @return UserDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindUser $query): UserDocument
    {
        $user = $this->readModel()->find($query->id(), $query->parts());
        $this->service()->checkUserCanReadUser($query->userId(), $user);

        return $user;
    }
}
