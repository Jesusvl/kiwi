<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Domain\UserUsername;

/**
 * Class FindUserByUsername.
 */
class FindUserByUsername implements Query
{
    /**
     * @var UserUsername
     */
    private $username;

    /**
     * @var UserParts
     */
    private $parts;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUser constructor.
     *
     * @param string   $userId
     * @param string   $username
     * @param string[] $fields
     */
    public function __construct(string $userId, string $username, array $fields = [])
    {
        $this->parts    = new UserParts($fields);
        $this->username = new UserUsername($username);
        $this->userId   = new UserId($userId);
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return UserUsername
     */
    public function username(): UserUsername
    {
        return $this->username;
    }

    /**
     * @return UserParts
     */
    public function parts(): UserParts
    {
        return $this->parts;
    }
}
