<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Document;

use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Store\Document\BaseStoreDocument;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Shop\User\Domain\User;

/**
 * Class UserDocument.
 */
class UserDocument extends BaseUserDocument
{
    /**
     * @var BaseStoreDocument
     */
    private $store;

    /**
     * @var MerchantDocument
     */
    private $merchant;

    /**
     * UserWithStoreDocument constructor.
     *
     * @param User|null         $user
     * @param BaseStoreDocument $store
     * @param MerchantDocument  $merchant
     */
    public function __construct(?User $user, BaseStoreDocument $store, MerchantDocument $merchant)
    {
        parent::__construct($user);
        $this->store    = $store;
        $this->merchant = $merchant;
    }

    /**
     * @return UserDocument
     */
    public static function empty(): self
    {
        return new self(null, StoreDocument::empty(), MerchantDocument::empty());
    }

    /**
     * @return BaseStoreDocument
     */
    public function store(): BaseStoreDocument
    {
        return $this->store;
    }

    /**
     * @return MerchantDocument
     */
    public function merchant(): MerchantDocument
    {
        return $this->merchant;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->mergeDocumentArray(parent::toScalar(), [
            'store'    => $this->store()->toScalar(),
            'merchant' => $this->merchant()->toScalar()
        ]);
    }
}
