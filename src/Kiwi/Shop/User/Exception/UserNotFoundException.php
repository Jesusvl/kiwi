<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Exception;

use Kiwi\Core\Exception\DataNotFoundDomainException;

/**
 * Class UserNotFoundException.
 */
class UserNotFoundException extends DataNotFoundDomainException
{
    public function __construct()
    {
        parent::__construct('User not found.');
    }
}
