<?php

declare(strict_types=1);

namespace Kiwi\Shop\User\Exception;

use Kiwi\Core\Exception\DomainException;

/**
 * Class DuplicatedPhoneException.
 */
class DuplicatedPhoneException extends DomainException
{
    public function __construct()
    {
        parent::__construct('User with this phone already exists.');
    }
}
