<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Merchant\Domain\MerchantName;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindMerchantByName.
 */
class FindMerchantByName implements Query
{
    /**
     * @var MerchantName
     */
    private $name;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindMerchantByName constructor.
     *
     * @param string $userId
     * @param string $name
     */
    public function __construct(string $userId, string $name)
    {
        $this->name   = new MerchantName($name);
        $this->userId = new UserId($userId);
    }

    /**
     * @return MerchantName
     */
    public function name(): MerchantName
    {
        return $this->name;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
