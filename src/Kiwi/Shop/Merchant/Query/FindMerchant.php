<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindMerchant.
 */
class FindMerchant implements Query
{
    /**
     * @var MerchantId
     */
    private $id;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindMerchant constructor.
     *
     * @param string $userId
     * @param string $id
     */
    public function __construct(string $userId, string $id)
    {
        $this->id     = new MerchantId($id);
        $this->userId = new UserId($userId);
    }

    /**
     * @return MerchantId
     */
    public function id(): MerchantId
    {
        return $this->id;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
