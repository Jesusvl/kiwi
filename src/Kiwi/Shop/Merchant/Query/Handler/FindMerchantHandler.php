<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Query\FindMerchant;

/**
 * Class FindMerchantHandler.
 */
class FindMerchantHandler extends MerchantQueryHandler
{
    /**
     * @param FindMerchant $query
     *
     * @return MerchantDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindMerchant $query): MerchantDocument
    {
        $merchant = $this->readModel()->find($query->id());
        $this->service()->checkUserCanReadMerchant($query->userId(), $merchant);

        return $merchant;
    }
}
