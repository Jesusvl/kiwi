<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Query\Handler;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Merchant\Query\FindMerchants;

/**
 * Class FindMerchantsHandler.
 */
class FindMerchantsHandler extends MerchantQueryHandler
{
    /**
     * @param FindMerchants $query
     *
     * @return PaginatedDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindMerchants $query): PaginatedDocument
    {
        $user = $this->service()->checkUser($query->userId());
        if (!$user->isAdmin()) {
            throw new InsufficientPermissionsException();
        }

        return $this->readModel()->findAll($query->filter());
    }
}
