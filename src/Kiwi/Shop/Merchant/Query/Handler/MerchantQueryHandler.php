<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Query\Handler;

use Kiwi\Shop\Merchant\Infrastructure\MerchantReadModel;
use Kiwi\Shop\Merchant\Service\MerchantService;

/**
 * Class MerchantQueryHandler.
 */
abstract class MerchantQueryHandler
{
    /**
     * @var MerchantService
     */
    private $service;

    /**
     * @var MerchantReadModel
     */
    private $readModel;

    /**
     * MerchantQueryHandler constructor.
     *
     * @param MerchantService   $service
     * @param MerchantReadModel $readModel
     */
    public function __construct(MerchantService $service, MerchantReadModel $readModel)
    {
        $this->service   = $service;
        $this->readModel = $readModel;
    }

    /**
     * @return MerchantService
     */
    protected function service(): MerchantService
    {
        return $this->service;
    }

    /**
     * @return MerchantReadModel
     */
    protected function readModel(): MerchantReadModel
    {
        return $this->readModel;
    }
}
