<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Query\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Query\FindMerchantByName;

/**
 * Class FindMerchantByNameHandler.
 */
class FindMerchantByNameHandler extends MerchantQueryHandler
{
    /**
     * @param FindMerchantByName $query
     *
     * @return MerchantDocument
     *
     * @throws InsufficientPermissionsException
     */
    public function __invoke(FindMerchantByName $query): MerchantDocument
    {
        $merchant = $this->readModel()->findByName($query->name());
        $this->service()->checkUserCanReadMerchant($query->userId(), $merchant);

        return $merchant;
    }
}
