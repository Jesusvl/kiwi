<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Query;

use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Infrastructure\Query;
use Kiwi\Shop\User\Domain\UserId;

/**
 * Class FindMerchants.
 */
class FindMerchants implements Query
{
    /**
     * @var Pagination
     */
    private $filter;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * FindUsers constructor.
     *
     * @param string $userId
     * @param int    $page
     * @param int    $pageSize
     */
    public function __construct(string $userId, int $page, int $pageSize)
    {
        $this->filter = new Pagination($page, $pageSize);
        $this->userId = new UserId($userId);
    }

    /**
     * @return Pagination
     */
    public function filter(): Pagination
    {
        return $this->filter;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }
}
