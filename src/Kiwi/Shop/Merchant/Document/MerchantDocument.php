<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Document;

use Kiwi\Core\Document\Document;
use Kiwi\Shop\Merchant\Domain\Merchant;

/**
 * Class MerchantDocument.
 */
class MerchantDocument extends Document
{
    /**
     * @var Merchant|null
     */
    private $merchant;

    /**
     * MerchantDocument constructor.
     *
     * @param Merchant|null $merchant
     */
    public function __construct(?Merchant $merchant)
    {
        parent::__construct($merchant);
        $this->merchant = $merchant;
    }

    /**
     * @return MerchantDocument
     */
    public static function empty(): self
    {
        return new self(null);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->merchant->id()->id();
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->merchant->name()->name();
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return $this->merchant
            ? [
                'id'   => $this->id(),
                'name' => $this->name()
            ] : null;
    }
}
