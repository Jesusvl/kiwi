<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Domain;

/**
 * Class MerchantName.
 */
class MerchantName
{
    /**
     * @var string
     */
    private $name;

    /**
     * ClientName constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }
}
