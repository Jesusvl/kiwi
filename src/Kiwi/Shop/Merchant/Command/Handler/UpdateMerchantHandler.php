<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Merchant\Command\UpdateMerchant;
use Kiwi\Shop\Merchant\Exception\MerchantNotFoundException;

/**
 * Class UpdateMerchantHandler.
 */
class UpdateMerchantHandler extends MerchantCommandHandler
{
    /**
     * @param UpdateMerchant $command
     *
     * @throws InsufficientPermissionsException
     * @throws MerchantNotFoundException
     */
    public function __invoke(UpdateMerchant $command): void
    {
        $merchant = $this->service()->checkDomain($command->id());
        $this->service()->checkUserCanWriteMerchant($command->userId(), $merchant);
        $merchant->rename($command->name());
        $this->service()->update($merchant);
    }
}
