<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Command\Handler;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Exception\DuplicatedMerchantIdException;
use Kiwi\Shop\Merchant\Command\CreateMerchant;
use Kiwi\Shop\Merchant\Domain\Merchant;

/**
 * Class CreateMerchantHandler.
 */
class CreateMerchantHandler extends MerchantCommandHandler
{
    /**
     * @param CreateMerchant $command
     *
     * @throws DuplicatedMerchantIdException
     * @throws InsufficientPermissionsException
     */
    public function __invoke(CreateMerchant $command): void
    {
        $merchant = new Merchant($command->id(), $command->name());
        $this->service()->checkUserCanWriteMerchant($command->userId(), $merchant);
        $this->service()->save($merchant);
    }
}
