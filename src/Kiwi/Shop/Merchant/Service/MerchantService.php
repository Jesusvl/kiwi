<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Service;

use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Shop\Client\Exception\DuplicatedMerchantIdException;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Merchant\Exception\MerchantNotFoundException;
use Kiwi\Shop\Merchant\Infrastructure\MerchantReadModel;
use Kiwi\Shop\Merchant\Infrastructure\MerchantWriteModel;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Infrastructure\UserReadModel;

/**
 * Class MerchantService.
 */
class MerchantService
{
    /**
     * @var MerchantReadModel
     */
    private $merchantReadModel;

    /**
     * @var MerchantWriteModel
     */
    private $merchantWriteModel;

    /**
     * @var UserReadModel
     */
    private $userReadModel;

    /**
     * MerchantService constructor.
     *
     * @param MerchantReadModel  $merchantReadModel
     * @param MerchantWriteModel $merchantWriteModel
     * @param UserReadModel      $userReadModel
     */
    public function __construct(
        MerchantReadModel $merchantReadModel,
        MerchantWriteModel $merchantWriteModel,
        UserReadModel $userReadModel
    ) {
        $this->merchantReadModel  = $merchantReadModel;
        $this->merchantWriteModel = $merchantWriteModel;
        $this->userReadModel      = $userReadModel;
    }

    /**
     * @param MerchantId $id
     *
     * @return Merchant
     *
     * @throws MerchantNotFoundException
     */
    public function checkDomain(MerchantId $id): Merchant
    {
        $merchant = $this->merchantWriteModel->find($id);

        if ($merchant) {
            return $merchant;
        }

        throw new MerchantNotFoundException();
    }

    /**
     * @param MerchantId $id
     *
     * @return MerchantDocument
     *
     * @throws MerchantNotFoundException
     */
    public function checkDocument(MerchantId $id): MerchantDocument
    {
        $merchant = $this->merchantReadModel->find($id);

        if ($merchant->isNotEmpty()) {
            return $merchant;
        }

        throw new MerchantNotFoundException();
    }

    /**
     * @param Merchant $merchant
     *
     * @throws DuplicatedMerchantIdException
     */
    public function save(Merchant $merchant): void
    {
        if ($this->merchantReadModel->find($merchant->id())) {
            throw new DuplicatedMerchantIdException();
        }
        $this->merchantWriteModel->save($merchant);
    }

    /**
     * @param Merchant $merchant
     */
    public function update(Merchant $merchant): void
    {
        $this->merchantWriteModel->save($merchant);
    }

    /**
     * @param UserId $userId
     *
     * @return UserDocument
     * @throws InsufficientPermissionsException
     */
    public function checkUser(UserId $userId): UserDocument
    {
        $user = $this->userReadModel->find($userId, new UserParts());
        if ($user->isEmpty()) {
            throw new InsufficientPermissionsException();
        }

        return $user;
    }

    /**
     * @param UserId   $userId
     * @param Merchant $merchant
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanWriteMerchant(UserId $userId, Merchant $merchant): void
    {
        $user = $this->checkUser($userId);

        if ($user->isAdmin()) {
            return;
        }

        if ($user->isManager() && $user->merchantId() === $merchant->id()->id()) {
            return;
        }

        if ($user->isMerchantAdmin() && $user->merchantId() === $merchant->id()->id()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }

    /**
     * @param UserId           $userId
     * @param MerchantDocument $merchant
     *
     * @throws InsufficientPermissionsException
     */
    public function checkUserCanReadMerchant(UserId $userId, MerchantDocument $merchant): void
    {
        $user = $this->checkUser($userId);
        if ($user->isAdmin() || ($user->isManager() && $user->merchantId() === $merchant->id())) {
            return;
        }

        if ($user->isSad() && $merchant->id() === $user->merchantId()) {
            return;
        }

        if ($user->isMerchantAdmin()) {
            return;
        }

        throw new InsufficientPermissionsException();
    }
}
