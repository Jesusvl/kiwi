<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Infrastructure;

use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Merchant\Domain\MerchantName;

/**
 * Interface MerchantReadModel.
 */
interface MerchantReadModel
{
    /**
     * @param MerchantId $id
     *
     * @return MerchantDocument
     */
    public function find(MerchantId $id): MerchantDocument;

    /**
     * @param MerchantName $name
     *
     * @return MerchantDocument|null
     */
    public function findByName(MerchantName $name): ?MerchantDocument;

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter): PaginatedDocument;
}
