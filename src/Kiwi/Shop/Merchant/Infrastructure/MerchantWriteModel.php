<?php

declare(strict_types=1);

namespace Kiwi\Shop\Merchant\Infrastructure;

use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Merchant\Domain\MerchantName;

/**
 * Interface MerchantWriteModel.
 */
interface MerchantWriteModel
{
    /**
     * @param MerchantId $id
     *
     * @return Merchant|null
     */
    public function find(MerchantId $id): ?Merchant;

    /**
     * @param Merchant $merchant
     */
    public function save(Merchant $merchant): void;

    /**
     * @param MerchantId   $id
     * @param MerchantName $name
     */
    public function update(MerchantId $id, MerchantName $name): void;
}
