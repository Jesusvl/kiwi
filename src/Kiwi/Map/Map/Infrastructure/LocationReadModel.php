<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Infrastructure;

use Kiwi\Map\Map\Document\CoordinatesDocument;
use Kiwi\Map\Map\Domain\Coordinates;
use Kiwi\Map\Map\Domain\Location;

/**
 * Interface LocationReadModel.
 */
interface LocationReadModel
{
    /**
     * @param Location $location
     *
     * @return CoordinatesDocument
     */
    public function findCoordinates(Location $location): CoordinatesDocument;
}
