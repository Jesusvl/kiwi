<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Document;

use Kiwi\Core\Document\Document;

/**
 * Class CoordinatesDocument.
 */
class CoordinatesDocument extends Document
{
    /**
     * @var float|null
     */
    private $latitude;

    /**
     * @var float|null
     */
    private $longitude;

    /**
     * CoordinatesDocument constructor.
     *
     * @param float|null $latitude
     * @param float|null $longitude
     */
    public function __construct(?float $latitude, ?float $longitude)
    {
        parent::__construct($this->latitude);
        $this->latitude  = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function longitude(): float
    {
        return $this->longitude ?? 0.0;
    }

    /**
     * @return float
     */
    public function latitude(): float
    {
        return $this->latitude ?? 0.0;
    }

    /**
     * @return array|null
     */
    public function toScalar(): ?array
    {
        return [
            'latitude'  => $this->latitude(),
            'longitude' => $this->longitude()
        ];
    }
}
