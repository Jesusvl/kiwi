<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Listener;

use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Shop\Location\Event\OnCreateLocationEvent;

/**
 * Class OnOrderDestinationCreated.
 */
class OnOrderDestinationCreated implements AsyncListener
{
    /**
     * @param Event|OnCreateLocationEvent $event
     */
    public function handle(Event $event): void
    {
        // TODO: Implement handle() method.
    }

    /**
     * @return string
     */
    public function listensTo(): string
    {
        return '';
    }
}
