<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Domain;

/**
 * Class Location.
 */
class Location
{
    /**
     * @var string
     */
    private $address;

    /**
     * Location constructor.
     *
     * @param string $address
     */
    public function __construct(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function address(): string
    {
        return $this->address;
    }
}
