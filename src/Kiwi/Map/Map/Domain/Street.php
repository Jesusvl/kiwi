<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Domain;

/**
 * Class Street.
 */
class Street
{
    /**
     * @var string
     */
    private $name;

    /**
     * Street constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
