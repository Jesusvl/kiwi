<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Domain;

/**
 * Class City.
 */
class City
{
    /**
     * @var string
     */
    private $name;

    /**
     * City constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
