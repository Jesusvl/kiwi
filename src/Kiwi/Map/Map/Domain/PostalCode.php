<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Domain;

/**
 * Class PostalCode.
 */
class PostalCode
{
    /**
     * @var string
     */
    private $number;

    /**
     * PostalCode constructor.
     *
     * @param string $number
     */
    public function __construct(string $number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function number(): string
    {
        return $this->number;
    }
}
