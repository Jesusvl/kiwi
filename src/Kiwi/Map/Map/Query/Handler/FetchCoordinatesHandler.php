<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Query\Handler;

use Kiwi\Core\Infrastructure\QueryHandler;
use Kiwi\Map\Map\Document\CoordinatesDocument;
use Kiwi\Map\Map\Infrastructure\LocationReadModel;
use Kiwi\Map\Map\Query\FetchCoordinates;

/**
 * Class FetchCoordinatesHandler.
 */
class FetchCoordinatesHandler implements QueryHandler
{
    /**
     * @var LocationReadModel
     */
    private $readModel;

    /**
     * FetchCoordinatesHandler constructor.
     *
     * @param LocationReadModel $readModel
     */
    public function __construct(LocationReadModel $readModel)
    {
        $this->readModel = $readModel;
    }

    /**
     * @param FetchCoordinates $query
     *
     * @return CoordinatesDocument
     */
    public function __invoke(FetchCoordinates $query): CoordinatesDocument
    {
        return $this->readModel->findCoordinates($query->location());
    }
}
