<?php

declare(strict_types=1);

namespace Kiwi\Map\Map\Query;

use Kiwi\Core\Infrastructure\Query;
use Kiwi\Map\Map\Domain\Location;

/**
 * Class FetchCoordinates.
 */
class FetchCoordinates implements Query
{
    /**
     * @var Location
     */
    private $location;

    /**
     * FetchCoordinates constructor.
     *
     * @param string $address
     */
    public function __construct(string $address)
    {
        $this->location = new Location($address);
    }

    /**
     * @return Location
     */
    public function location(): Location
    {
        return $this->location;
    }
}
