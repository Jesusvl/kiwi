<?php

declare(strict_types=1);

namespace KiwiLib\Json;

use DOMNode;
use DOMNodeList;
use DOMXPath;
use Exception;
use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathException;
use InvalidArgumentException;
use DOMDocument;

/**
 * Class JsonArray.
 */
class JsonArray
{
    /**
     * @var array
     */
    protected $jsonArray = [];

    /**
     * @var DOMDocument
     */
    protected $jsonXml;

    /**
     * JsonArray constructor.
     *
     * @param $jsonString
     */
    public function __construct($jsonString)
    {
        if (!is_string($jsonString)) {
            throw new InvalidArgumentException('$jsonString param must be a string.');
        }

        $jsonDecode = json_decode($jsonString, true);

        if (!is_array($jsonDecode)) {
            $jsonDecode = [$jsonDecode];
        }

        $this->jsonArray = $jsonDecode;

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid json: %s. System message: %s.',
                    $jsonString,
                    json_last_error_msg()
                ),
                json_last_error()
            );
        }
    }

    /**
     * @return DOMDocument
     */
    public function toXml(): DOMDocument
    {
        if ($this->jsonXml) {
            return $this->jsonXml;
        }

        $root      = 'root';
        $jsonArray = $this->jsonArray;
        if (count($jsonArray) === 1) {
            $value = reset($jsonArray);
            if (is_array($value)) {
                $root      = key($jsonArray);
                $jsonArray = $value;
            }
        }

        $dom               = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = false;
        $root              = $dom->createElement($root);
        $dom->appendChild($root);
        $this->arrayToXml($dom, $root, $jsonArray);
        $this->jsonXml = $dom;

        return $dom;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->jsonArray;
    }

    /**
     * @param $xpath
     *
     * @return DOMNodeList
     */
    public function filterByXPath($xpath): DOMNodeList
    {
        $path = new DOMXPath($this->toXml());

        return $path->query($xpath);
    }

    /**
     * @param $jsonPath
     *
     * @return mixed
     * @throws JSONPathException
     */
    public function filterByJsonPath($jsonPath)
    {
        return (new JSONPath($this->jsonArray))->find($jsonPath)->data();
    }

    /**
     * @return string
     */
    public function getXmlString(): string
    {
        return $this->toXml()->saveXML();
    }

    /**
     * @param array $needle
     *
     * @return mixed
     */
    public function containsArray(array $needle)
    {
        return (new ArrayContainsComparator($this->jsonArray))->containsArray($needle);
    }

    /**
     * @param DOMDocument $doc
     * @param DOMNode     $node
     * @param array       $array
     */
    private function arrayToXml(DOMDocument $doc, DOMNode $node, $array): void
    {
        foreach ($array as $key => $value) {
            if (is_numeric($key)) {
                $subNode = $doc->createElement($node->nodeName);
                $node->parentNode->appendChild($subNode);
            } else {
                try {
                    $subNode = $doc->createElement($key);
                } catch (Exception $e) {
                    $key     = $this->getValidTagNameForInvalidKey($key);
                    $subNode = $doc->createElement($key);
                }
                $node->appendChild($subNode);
            }
            if (is_array($value)) {
                $this->arrayToXml($doc, $subNode, $value);
            } else {
                $subNode->nodeValue = htmlspecialchars((string)$value);
            }
        }
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    private function getValidTagNameForInvalidKey($key)
    {
        static $map = [];
        if (!isset($map[$key])) {
            $tagName   = 'invalidTag' . (count($map) + 1);
            $map[$key] = $tagName;
        }

        return $map[$key];
    }
}
