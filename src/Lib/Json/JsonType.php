<?php

declare(strict_types=1);

namespace KiwiLib\Json;

use KiwiLib\Uuid\Uuid;

/**
 * Class JsonType.
 */
class JsonType
{
    /**
     * @var array
     */
    static protected $customFilters = [];

    /**
     * @var array
     */
    protected $jsonArray;

    /**
     * Creates instance of JsonType
     * Pass an array or `JsonArray` with data.
     * If non-associative array is passed - the very first element of it will be used for matching.
     *
     * @param $jsonArray array
     */
    public function __construct($jsonArray)
    {
        if ($jsonArray instanceof JsonArray) {
            $jsonArray = $jsonArray->toArray();
        }
        $this->jsonArray = $jsonArray;
    }

    /**
     * Adds custom filter to JsonType list.
     * You should specify a name and parameters of a filter.
     *
     * Example:
     *
     * ```php
     * <?php
     * JsonType::addCustomFilter('slug', function($value) {
     *     return strpos(' ', $value) !== false;
     * });
     * // => use it as 'string:slug'
     *
     * // add custom function to matcher with `len($val)` syntax
     * // parameter matching patterns should be valid regex and start with `/` char
     * JsonType::addCustomFilter('/len\((.*?)\)/', function($value, $len) {
     *   return strlen($value) == $len;
     * });
     * // use it as 'string:len(5)'
     * ?>
     * ```
     *
     * @param          $name
     * @param callable $callable
     */
    public static function addCustomFilter($name, callable $callable): void
    {
        static::$customFilters[$name] = $callable;
    }

    /**
     * Removes all custom filters
     */
    public static function cleanCustomFilters(): void
    {
        static::$customFilters = [];
    }

    /**
     * Checks data against passed JsonType.
     * If matching fails function returns a string with a message describing failure.
     * On success returns `true`.
     *
     * @param array $jsonType
     *
     * @return bool|string
     */
    public function matches(array $jsonType)
    {
        if (array_key_exists(0, $this->jsonArray) && is_array($this->jsonArray[0])) {
            // a list of items
            $msg = '';
            foreach ($this->jsonArray as $array) {
                $res = $this->typeComparison($array, $jsonType);
                if ($res !== true) {
                    $msg .= "\n" . $res;
                }
            }
            if ($msg) {
                return $msg;
            }

            return true;
        }

        return $this->typeComparison($this->jsonArray, $jsonType);
    }

    /**
     * @param $data
     * @param array $jsonType
     *
     * @return bool|string
     */
    protected function typeComparison($data, $jsonType)
    {
        foreach ($jsonType as $key => $type) {
            $matchTypes  = preg_split("#(?![^]\(]*\))\|#", $type);

            if (!array_key_exists($key, $data) && !in_array('opt', $matchTypes, true)) {
                return "Key `$key` doesn't exist in " . json_encode($data);
            }

            if (is_array($jsonType[$key])) {
                $message = $this->typeComparison($data[$key], $jsonType[$key]);
                if (is_string($message)) {
                    return $message;
                }
                continue;
            }

            $matched     = false;
            $currentType = strtolower(gettype($data[$key] ?? null));
            if ($currentType === 'double') {
                $currentType = 'float';
            }
            foreach ($matchTypes as $matchType) {
                $filters      = preg_split("#(?![^]\(]*\))\:#", $matchType);
                $expectedType = strtolower(trim(array_shift($filters)));

                if ($expectedType !== $currentType && !($expectedType === 'opt' && $currentType === 'null')) {
                    continue;
                }

                $matched = true;

                foreach ($filters as $filter) {
                    $matched = $matched && $this->matchFilter($filter, $data[$key]);
                }
                if ($matched) {
                    break;
                }
            }
            if (!$matched) {
                return sprintf("`$key: %s` is of type `$type`", var_export($data[$key] ?? null, true));
            }
        }

        return true;
    }

    /**
     * @param $filter
     * @param $value
     *
     * @return bool|false|int|mixed
     */
    protected function matchFilter($filter, $value)
    {
        $filter = trim($filter);
        if (strpos($filter, '!') === 0) {
            return !$this->matchFilter(substr($filter, 1), $value);
        }

        // apply custom filters
        foreach (static::$customFilters as $customFilter => $callable) {
            if (strpos($customFilter, '/') === 0 && preg_match($customFilter, $filter, $matches)) {
                array_shift($matches);

                return call_user_func_array($callable, array_merge([$value], $matches));
            }
            if ($customFilter === $filter) {
                return $callable($value);
            }
        }

        if (strpos($filter, '=') === 0) {
            return $value === substr($filter, 1);
        }
        if ($filter === 'guid') {
            return Uuid::validate($value);
        }
        if ($filter === 'url') {
            return filter_var($value, FILTER_VALIDATE_URL);
        }
        if ($filter === 'date') {
            return preg_match(
                '/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(\.\d+)?(?:Z|(\+|-)([\d|:]*))?$/',
                $value
            );
        }
        if ($filter === 'email') { // from http://emailregex.com/
            // @codingStandardsIgnoreStart
            return preg_match('/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z\d]+(?:-[a-z\d]+)*\.){1,126}){1,}(?:(?:[a-z][a-z\d]*)|(?:(?:xn--)[a-z\d]+))(?:-[a-z\d]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f\d]{1,4}(?::[a-f\d]{1,4}){7})|(?:(?!(?:.*[a-f\d][:\]]){7,})(?:[a-f\d]{1,4}(?::[a-f\d]{1,4}){0,5})?::(?:[a-f\d]{1,4}(?::[a-f\d]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f\d]{1,4}(?::[a-f\d]{1,4}){5}:)|(?:(?!(?:.*[a-f\d]:){5,})(?:[a-f\d]{1,4}(?::[a-f\d]{1,4}){0,3})?::(?:[a-f\d]{1,4}(?::[a-f\d]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][\d])|(?:1[\d]{2})|(?:[1-9]?[\d]))(?:\.(?:(?:25[0-5])|(?:2[0-4][\d])|(?:1[\d]{2})|(?:[1-9]?[\d]))){3}))\]))$/iD', $value);
            // @codingStandardsIgnoreEnd
        }
        if ($filter === 'empty') {
            return empty($value);
        }
        if (preg_match('~^regex\((.*?)\)$~', $filter, $matches)) {
            return preg_match($matches[1], $value);
        }
        if (preg_match('~^>([\d\.]+)$~', $filter, $matches)) {
            return (float)$value > (float)$matches[1];
        }
        if (preg_match('~^<([\d\.]+)$~', $filter, $matches)) {
            return (float)$value < (float)$matches[1];
        }

        return null;
    }
}
