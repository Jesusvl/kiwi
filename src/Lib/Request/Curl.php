<?php

declare(strict_types=1);

namespace KiwiLib\Request;

/**
 * Class Curl.
 */
class Curl
{
    /**
     * @param $url
     *
     * @return array
     */
    public static function jsonGet($url): array
    {
        $channel = \curl_init($url);
        \curl_setopt($channel, \CURLOPT_CUSTOMREQUEST, 'GET');
        \curl_setopt($channel, \CURLOPT_RETURNTRANSFER, true);

        $resp = \curl_exec($channel);

        return \json_decode($resp, true);
    }
}
