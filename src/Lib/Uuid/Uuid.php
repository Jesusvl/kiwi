<?php

declare(strict_types=1);

namespace KiwiLib\Uuid;

/**
 * Class Uuid.
 */
class Uuid
{
    /**
     * @return string
     */
    public static function new(): string
    {
        return \Ramsey\Uuid\Uuid::uuid4()->toString();
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    public static function validate(string $value): bool
    {
        return \Ramsey\Uuid\Uuid::isValid($value);
    }
}
