<?php

declare(strict_types=1);

namespace KiwiLib\Exception;

use RuntimeException;

/**
 * Class TODOException.
 */
class TODOException extends RuntimeException
{
    /**
     * TODOException constructor.
     *
     * @param string $message
     */
    public function __construct(string $message = 'Not Yet Implemented.')
    {
        parent::__construct($message);
    }
}
