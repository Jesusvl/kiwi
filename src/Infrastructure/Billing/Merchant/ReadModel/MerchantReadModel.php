<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Merchant\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Billing\Merchant\Document\BaseMerchantDocument;
use Kiwi\Billing\Merchant\Document\PaginatedMerchantDocument;
use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Merchant\Domain\MerchantName;
use Kiwi\Billing\Merchant\Infrastructure\MerchantReadModel as BaseMerchantReadModel;

/**
 * Class DoctrineMerchantRepository.
 */
class MerchantReadModel extends DoctrineReadModel implements BaseMerchantReadModel
{
    /**
     * @param MerchantId $id
     *
     * @return BaseMerchantDocument
     */
    public function find(MerchantId $id): BaseMerchantDocument
    {
        return new BaseMerchantDocument($this->manager->getRepository(Merchant::class)->find($id));
    }

    /**
     * @param Pagination $filter
     *
     * @return PaginatedMerchantDocument
     */
    public function findAll(Pagination $filter): PaginatedMerchantDocument
    {
        $query = $this->manager->createQueryBuilder()
            ->select('m')
            ->from(Merchant::class, 'm')
            ->setFirstResult(($filter->page() - 1) * $filter->pageSize())
            ->setMaxResults($filter->pageSize());

        $paginator = new Paginator($query);

        return new PaginatedMerchantDocument(
            $this->toBaseDocumentArray(BaseMerchantDocument::class, $paginator->getIterator()),
            $filter->page(),
            $filter->pageSize(),
            $paginator->count()
        );
    }

    /**
     * @param MerchantName $name
     *
     * @return BaseMerchantDocument
     */
    public function findByName(MerchantName $name): BaseMerchantDocument
    {
        return new BaseMerchantDocument(
            $this->manager->getRepository(Merchant::class)->findOneBy(['name' => $name])
        );
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        return BaseMerchantDocument::empty();
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
