<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Merchant\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Billing\Merchant\Domain\MerchantId;

/**
 * Class MerchantIdType.
 */
class MerchantIdType extends GuidType
{
    public const NAME = 'billing_merchant_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return MerchantId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?MerchantId
    {
        return null === $value ? null : new MerchantId($value);
    }

    /**
     * @param MerchantId|string|null $value
     * @param AbstractPlatform       $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
