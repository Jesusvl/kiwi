<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Merchant\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Merchant\Domain\MerchantName;
use Kiwi\Billing\Merchant\Infrastructure\MerchantWriteModel as BaseMerchantWriteModel;

/**
 * Class DoctrineMerchantRepository.
 */
class MerchantWriteModel extends DoctrineWriteModel implements BaseMerchantWriteModel
{
    /**
     * @param Merchant $merchant
     */
    public function save(Merchant $merchant): void
    {
        $this->persist($merchant);
    }

    /**
     * @param MerchantId $id
     *
     * @return Merchant|null
     */
    public function find(MerchantId $id): ?Merchant
    {
        return $this->manager->getRepository(Merchant::class)->find($id);
    }

    /**
     * @param MerchantId   $id
     * @param MerchantName $name
     */
    public function update(MerchantId $id, MerchantName $name): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->update(Merchant::class, 'm')
                ->set('m.name', $builder->expr()->literal($name))
                ->where('m.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->execute();
    }
}
