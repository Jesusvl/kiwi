<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Kiwi\Billing\Order\Domain\OrderRefrigeratedQuantity;

/**
 * Class OrderRefrigeratedQuantityType.
 */
class OrderRefrigeratedQuantityType extends IntegerType
{
    public const NAME = 'billing_order_refrigerated_quantity';

    /**
     * @param int|null      $value
     * @param AbstractPlatform $platform
     *
     * @return OrderRefrigeratedQuantity|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?OrderRefrigeratedQuantity
    {
        return null === $value ? null : new OrderRefrigeratedQuantity($value);
    }

    /**
     * @param OrderRefrigeratedQuantity|int|null $value
     * @param AbstractPlatform         $platform
     *
     * @return int|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        if (\is_numeric($value)) {
            return (int)$value;
        }

        return null === $value ? null : (int)(string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
