<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Billing\Order\Domain\OrderClientName;

/**
 * Class OrderClientNameType.
 */
class OrderClientNameType extends StringType
{
    public const NAME = 'billing_order_client_name';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return OrderClientName|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?OrderClientName
    {
        return null === $value ? null : new OrderClientName($value);
    }

    /**
     * @param OrderClientName|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
