<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Billing\Order\Domain\OrderId;

/**
 * Class OrderIdType.
 */
class OrderIdType extends GuidType
{
    public const NAME = 'billing_order_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return OrderId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?OrderId
    {
        return null === $value ? null : new OrderId($value);
    }

    /**
     * @param OrderId|string|null $value
     * @param AbstractPlatform       $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
