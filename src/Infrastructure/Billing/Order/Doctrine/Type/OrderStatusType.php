<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Billing\Order\Domain\OrderStatus;

/**
 * Class OrderStatusType.
 */
class OrderStatusType extends StringType
{
    public const NAME = 'billing_order_status';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return OrderStatus|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?OrderStatus
    {
        return null === $value ? null : new OrderStatus($value);
    }

    /**
     * @param OrderStatus|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
