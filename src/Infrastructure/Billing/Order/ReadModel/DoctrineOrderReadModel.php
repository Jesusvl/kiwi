<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Order\ReadModel;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Order\Document\BaseOrderDocument;
use Kiwi\Billing\Order\Document\OrderDocumentCollection;
use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Billing\Order\Domain\OrderId;
use Kiwi\Billing\Order\Domain\OrderParts;
use Kiwi\Billing\Store\Domain\Store;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Order\Infrastructure\OrderReadModel;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Order\Document\OrderDocument;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class OrderReadModel.
 */
class DoctrineOrderReadModel extends DoctrineReadModel implements OrderReadModel
{
    /**
     * @param OrderId $id
     * @param OrderParts $parts
     *
     * @return BaseOrderDocument
     */
    public function find(OrderId $id, ?OrderParts $parts = null): BaseOrderDocument
    {
        $parts = $parts ?? new OrderParts();

        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('o')
            ->from(Order::class, 'o')
            ->where('o.id = :oid')
            ->setParameter('oid', $id);

        $result = $queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($result);
        $return = $this->fetchDocument($id);
        $this->clearResult();

        return $return;
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @param MerchantId $merchantId
     * @param StoreId $storeId
     *
     * @return OrderDocumentCollection
     * @throws \Kiwi\Billing\Order\Exception\InvalidItemClassException
     */
    public function findOrdersByTimeAndStoreAndMerchant(
        \DateTime $start,
        \DateTime $end,
        MerchantId $merchantId,
        StoreId $storeId
    ): OrderDocumentCollection {

        $queryBuilder = $this->manager->createQueryBuilder();
        $query = $queryBuilder
            ->select('o')
            ->from(Order::class, 'o')
            ->join(Store::class, 'st', Join::WITH, 'st.id = o.storeId')
            ->join(Merchant::class, 'om', Join::WITH, 'om.id = st.merchantId')
            ->where('o.startTime >= :date_start')
            ->andWhere('o.endTime <= :date_end')
            ->andWhere('st.merchantId = :merchant_id')
            ->andWhere('st.id = :store_id')
            ->setParameter('date_start', $start->format(DATE_ATOM))
            ->setParameter('date_end', $end->format(DATE_ATOM))
            ->setParameter('merchant_id', $merchantId->id())
            ->setParameter('store_id', $storeId->id())
            ->getQuery();

            $result  = $query->getResult();

        return new OrderDocumentCollection($result);
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        return OrderDocument::empty();
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
