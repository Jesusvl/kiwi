<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Order\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Billing\Order\Domain\Order;
use Kiwi\Billing\Order\Domain\OrderId;
use Kiwi\Billing\Order\Infrastructure\OrderWriteModel as BaseOrderWriteModel;

/**
 * Class OrderWriteModel.
 */
class DoctrineOrderWriteModel extends DoctrineWriteModel implements BaseOrderWriteModel
{

    /**
     * @param OrderId $id
     *
     * @return Order|null
     */
    public function find(OrderId $id): ?Order
    {
        return $this->manager->getRepository(Order::class)->find($id);
    }

    /**
     * @param Order $order
     */
    public function save(Order $order): void
    {
        $this->persist($order);
    }

    /**
     * @param Order $order
     */
    public function update(Order $order): void
    {
        $this->persist($order);
    }
}
