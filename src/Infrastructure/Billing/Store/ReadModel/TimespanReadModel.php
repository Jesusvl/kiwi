<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Store\ReadModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Billing\Store\Document\BaseTimespanDocument;
use Kiwi\Billing\Store\Domain\Timespan;
use Kiwi\Billing\Store\Infrastructure\TimespanReadModel as BaseTimespanReadModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class DoctrineTimespanReadModel.
 */
class TimespanReadModel extends DoctrineWriteModel implements BaseTimespanReadModel
{
    /**
     * @param DateTime $startTime
     * @param DateTime $endTime
     *
     * @return BaseTimespanDocument
     */
    public function find(DateTime $startTime, DateTime $endTime): BaseTimespanDocument
    {
        return new BaseTimespanDocument(
            $this->manager->getRepository(Timespan::class)->findOneBy([
                'startTime' => $startTime,
                'endTime'   => $endTime
            ])
        );
    }
}
