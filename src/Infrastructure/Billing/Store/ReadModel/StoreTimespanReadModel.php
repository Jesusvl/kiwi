<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Store\ReadModel;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Kiwi\Billing\Store\Document\BaseStoreTimespanDocument;
use Kiwi\Billing\Store\Document\BaseTimespanDocument;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreTimespan;
use Kiwi\Billing\Store\Domain\Timespan;
use Kiwi\Billing\Store\Infrastructure\StoreTimespanReadModel as BaseStoreTimespanReadModel;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class StoreTimespanReadModel.
 */
class StoreTimespanReadModel extends DoctrineReadModel implements BaseStoreTimespanReadModel
{
    /**
     * @param StoreId  $storeId
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return BaseStoreTimespanDocument
     */
    public function findByStoreAndTimeRange(
        StoreId $storeId,
        DateTime $start,
        DateTime $end
    ): BaseStoreTimespanDocument {
        $queryBuilder = $this->manager->createQueryBuilder();
        /** @var array $result */
        $result = $queryBuilder
            ->select('st, t')
            ->from(StoreTimespan::class, 'st')
            ->leftJoin(Timespan::class, 't', Join::WITH, 'st.timespanId = t.id')
            ->where('st.storeId = :sid')
            ->andWhere('t.startTime = :start')
            ->andWhere('t.endTime = :end')
            ->setParameter('sid', $storeId)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();

        $storeTimeSpan = null;
        $timeSpan      = null;

        if (count($result)) {
            $this->mapArrayResult($result);
            $storeTimeSpan = $result[0];
            $timeSpan      = $this->fetchResultAAAAA($result[0]->timespanId());
        }

        return new BaseStoreTimespanDocument(
            $storeTimeSpan,
            new BaseTimespanDocument($timeSpan)
        );
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        // TODO: Implement createDocument() method.
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts): DoctrineReadModel
    {
        // TODO: Implement buildJoins() method.
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
