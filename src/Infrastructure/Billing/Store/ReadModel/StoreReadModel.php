<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Store\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Billing\Store\Document\BaseTimespanDocument;
use Kiwi\Billing\Store\Domain\Timespan;
use Kiwi\Billing\Store\Domain\TimespanId;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Domain\Location;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Billing\Merchant\Domain\MerchantId;
use Kiwi\Billing\Store\Document\BaseStoreTimespanDocument;
use Kiwi\Billing\Store\Document\PaginatedStoreDocument;
use Kiwi\Billing\Store\Document\StoreDocument;
use Kiwi\Billing\Store\Document\StoreTimespanDocumentBook;
use Kiwi\Billing\Store\Domain\Store;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Domain\StoreName;
use Kiwi\Billing\Store\Domain\StoreParts;
use Kiwi\Billing\Store\Domain\StoreTimespan;
use Kiwi\Billing\Store\Infrastructure\StoreReadModel as BaseStoreReadModel;

/**
 * Class StoreReadModel.
 */
class StoreReadModel extends DoctrineReadModel implements BaseStoreReadModel
{

    /**
     * @param StoreId         $id
     * @param StoreParts|null $parts
     *
     * @return StoreDocument
     */
    public function find(StoreId $id, ?StoreParts $parts = null): StoreDocument
    {
        $parts        = $parts ?? new StoreParts();
        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('s')
            ->from(Store::class, 's')
            ->where('s.id=:sid')
            ->setParameter('sid', $id);

        if ($parts->contains(StoreParts::LOCATION)) {
            $queryBuilder
                ->addSelect('sl')
                ->leftJoin(Location::class, 'sl', Join::WITH, 'sl.id = s.locationId');
        }

        if ($parts->contains(StoreParts::TIMESPANS)) {
            $queryBuilder
                ->addSelect('t, st')
                ->leftJoin(StoreTimespan::class, 'st', Join::WITH, 'st.storeId = s.id')
                ->leftJoin(Timespan::class, 't', Join::WITH, 'st.timespanId = t.id');
        }

        $result = $queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($result);
        $document = $this->fetchDocument($id);
        $this->clearResult();

        return $document;
    }

    /**
     * @param Pagination      $filter
     * @param StoreParts|null $parts
     *
     * @return PaginatedStoreDocument
     */
    public function findAll(Pagination $filter, ?StoreParts $parts = null): PaginatedStoreDocument
    {
        $parts        = $parts ?? new StoreParts();
        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('s')
            ->from(Store::class, 's');

        if ($parts->contains(StoreParts::LOCATION)) {
            $queryBuilder
                ->addSelect('sl')
                ->leftJoin(Location::class, 'sl', Join::WITH, 'sl.id = s.locationId');
        }

        if ($parts->contains(StoreParts::TIMESPANS)) {
            $queryBuilder
                ->addSelect('t')
                ->leftJoin(Timespan::class, 't', Join::WITH, 'st.timespanId = t.id');
        }

        $paginator = new Paginator($queryBuilder->getQuery());
        $this->mapResult($paginator->getIterator());

        $result = [];
        foreach ($paginator as $data) {
            if ($data instanceof Store) {
                $result[] = $this->fetchDocument($data->id());
            }
        }

        $this->clearResult();

        return new PaginatedStoreDocument(
            $result,
            $filter->page(),
            $filter->pageSize(),
            $paginator->count()
        );
    }

    /**
     * @param StoreId    $id
     * @param TimespanId $timespanId
     * @param StoreParts $parts
     *
     * @return StoreDocument
     */
    public function findByIdAndTimespan(
        StoreId $id,
        TimespanId $timespanId,
        ?StoreParts $parts = null
    ): StoreDocument {
        $parts        = $parts ?? new StoreParts();
        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('s, t')
            ->from(Store::class, 's')
            ->leftJoin(StoreTimespan::class, 'st', Join::WITH, 's.id = st.storeId')
            ->leftJoin(Timespan::class, 't', Join::WITH, 'st.timespanId = t.id')
            ->where('s.id=:sid')
            ->andWhere('t.id = :tid')
            ->setParameter('sid', $id)
            ->setParameter('tid', $timespanId);

        if ($parts->contains(StoreParts::LOCATION)) {
            $queryBuilder
                ->addSelect('sl')
                ->leftJoin(Location::class, 'sl', Join::WITH, 'sl.id = s.locationId');
        }

        if ($parts->contains(StoreParts::TIMESPANS)) {
            $queryBuilder
                ->addSelect('t2')
                ->leftJoin(Timespan::class, 't2', Join::WITH, 'st.timespanId = t2.id');
        }

        $result = $queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($result);
        $document = $this->fetchDocument($id);
        $this->clearResult();

        return $document;
    }

    /**
     * @param StoreName  $storeName
     * @param MerchantId $merchantId
     * @param StoreParts $parts
     *
     * @return StoreDocument
     */
    public function findByNameAndMerchant(
        StoreName $storeName,
        MerchantId $merchantId,
        StoreParts $parts
    ): StoreDocument {
        $parts        = $parts ?? new StoreParts();
        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('s')
            ->from(Store::class, 's')
            ->where('s.name=:name')
            ->andWhere('s.merchantId=:mid')
            ->setParameter('name', $storeName)
            ->setParameter('mid', $merchantId);

        if ($parts->contains(StoreParts::LOCATION)) {
            $queryBuilder
                ->addSelect('sl')
                ->leftJoin(Location::class, 'sl', Join::WITH, 'sl.id = s.locationId');
        }

        if ($parts->contains(StoreParts::TIMESPANS)) {
            $queryBuilder
                ->addSelect('t')
                ->leftJoin(Timespan::class, 't', Join::WITH, 'st.timespanId = t.id');
        }

        /** @var array $result */
        $result = $queryBuilder
            ->getQuery()
            ->getResult();

        $document = StoreDocument::empty();

        $this->mapArrayResult($result);
        foreach ($result as $item) {
            if ($item instanceof Store) {
                $document = $this->fetchDocument($item->id());
                break;
            }
        }
        $this->clearResult();

        return $document;
    }

    /**
     * @param StoreId $id
     *
     * @return StoreDocument
     */
    private function fetchDocument(StoreId $id): StoreDocument
    {
        /** @var Store $store */
        $store    = $this->fetchResult(Store::class, $id);
        $location = $store ? $this->fetchResult(Location::class, $store->locationId()) : null;

        $list = [];
        foreach ($this->results() as $item) {
            if ($item instanceof StoreTimespan) {
                $list[$item->storeId()->id()] = $list[$item->storeId()->id()] ?? [];
                $list[$item->storeId()->id()][] = new BaseStoreTimespanDocument(
                    $item,
                    new BaseTimespanDocument($this->fetchResult(Timespan::class, $item->timespanId()))
                );
            }
        }

        return new StoreDocument(
            $store,
            new LocationDocument($location),
            new StoreTimespanDocumentBook($list[$store ? $store->id()->id() : -1] ?? [])
        );
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        return StoreDocument::empty();
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
