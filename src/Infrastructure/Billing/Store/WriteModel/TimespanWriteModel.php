<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Store\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Billing\Store\Domain\Timespan;
use Kiwi\Billing\Store\Infrastructure\TimespanWriteModel as BaseTimespanWriteModel;

/**
 * Class DoctrineTimespanRepository.
 */
class TimespanWriteModel extends DoctrineWriteModel implements BaseTimespanWriteModel
{
    /**
     * @param Timespan $timespan
     */
    public function save(Timespan $timespan): void
    {
        $this->persist($timespan);
    }
}
