<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Store\WriteModel;

use Kiwi\Billing\Store\Domain\Store;
use Kiwi\Billing\Store\Domain\StoreId;
use Kiwi\Billing\Store\Infrastructure\StoreWriteModel as BaseStoreWriteModel;
use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;

/**
 * Class StoreWriteModel.
 */
class StoreWriteModel extends DoctrineWriteModel implements BaseStoreWriteModel
{
    /**
     * @param StoreId $storeId
     *
     * @return Store|null
     */
    public function find(StoreId $storeId): ?Store
    {
        return $this->manager->getRepository(Store::class)->find($storeId);
    }

    /**
     * @param Store $store
     */
    public function save(Store $store): void
    {
        $this->persist($store);
    }

    /**
     * @param Store $store
     */
    public function update(Store $store): void
    {
        $this->persist($store);
    }

    /**
     * @param StoreId $id
     */
    public function delete(StoreId $id): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->delete(Store::class, 's')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}
