<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Store\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Billing\Store\Domain\StoreTimespanId;

/**
 * Class StoreTimespanIdType.
 */
class StoreTimespanIdType extends GuidType
{
    public const NAME = 'billing_store_timespan_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return StoreTimespanId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?StoreTimespanId
    {
        return null === $value ? null : new StoreTimespanId($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
