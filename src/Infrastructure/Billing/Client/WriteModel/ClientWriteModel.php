<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Client\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Billing\Client\Domain\Client;
use Kiwi\Billing\Client\Domain\ClientId;
use Kiwi\Billing\Client\Infrastructure\ClientWriteModel as BaseClientWriteModel;

/**
 * Class ClientWriteModel.
 */
class ClientWriteModel extends DoctrineWriteModel implements BaseClientWriteModel
{
    /**
     * @param ClientId $id
     *
     * @return Client|null
     */
    public function find(ClientId $id): ?Client
    {
        return $this->manager->getRepository(Client::class)->find($id);
    }

    /**
     * @param Client $client
     */
    public function save(Client $client): void
    {
        $this->persist($client);
    }

    /**
     * @param ClientId $id
     */
    public function delete(ClientId $id): void
    {
        $this->manager->createQueryBuilder()
            ->delete(Client::class, 'c')
            ->where('d.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}
