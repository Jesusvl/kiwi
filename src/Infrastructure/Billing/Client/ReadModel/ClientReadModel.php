<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Client\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Billing\Client\Infrastructure\ClientReadModel as BaseClientReadModel;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Client\Document\ClientDocument;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class ClientReadModel.
 */
class ClientReadModel extends DoctrineReadModel implements BaseClientReadModel
{
    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        return ClientDocument::empty();
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
