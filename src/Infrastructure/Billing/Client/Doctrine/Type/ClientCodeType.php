<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Client\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Billing\Client\Domain\ClientCode;

/**
 * Class ClientCodeType.
 */
class ClientCodeType extends StringType
{
    public const NAME = 'billing_client_code';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return ClientCode|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?ClientCode
    {
        return null === $value ? null : new ClientCode($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
