<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\Client\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Billing\Client\Domain\ClientPhone;

/**
 * Class ClientPhoneType.
 */
class ClientPhoneType extends StringType
{
    public const NAME = 'billing_client_phone';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return ClientPhone|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?ClientPhone
    {
        return null === $value ? null : new ClientPhone($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
