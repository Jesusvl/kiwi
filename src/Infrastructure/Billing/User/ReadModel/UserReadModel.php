<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Billing\User\ReadModel;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Billing\Merchant\Document\BaseMerchantDocument;
use Kiwi\Billing\Merchant\Domain\Merchant;
use Kiwi\Billing\Store\Document\BaseStoreDocument;
use Kiwi\Billing\Store\Domain\Store;
use Kiwi\Billing\User\Document\PaginatedUserDocument;
use Kiwi\Billing\User\Document\UserDocument;
use Kiwi\Billing\User\Domain\User;
use Kiwi\Billing\User\Domain\UserId;
use Kiwi\Billing\User\Domain\UserParts;
use Kiwi\Billing\User\Domain\UserPhone;
use Kiwi\Billing\User\Domain\UserUsername;
use Kiwi\Billing\User\Infrastructure\UserReadModel as BaseUserReadModel;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class UserReadModel.
 */
class UserReadModel extends DoctrineReadModel implements BaseUserReadModel
{
    /**
     * @param UserId         $id
     * @param UserParts|null $parts
     *
     * @return UserDocument
     */
    public function find(UserId $id, ?UserParts $parts = null): UserDocument
    {
        $parts        = $parts ?? new UserParts();
        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.id=:uid')
            ->setParameter('uid', $id);

        if ($parts->contains(UserParts::MERCHANT)) {
            $queryBuilder
                ->addSelect('um')
                ->leftJoin(Merchant::class, 'um', Join::WITH, 'um.id = u.merchantId');
        }

        $result = $queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($result);
        $document = $this->fetchDocument($id);
        $this->clearResult();

        return $document;
    }

    /**
     * @param UserUsername   $username
     * @param UserParts|null $parts
     *
     * @return UserDocument
     */
    public function findByUsername(UserUsername $username, ?UserParts $parts = null): UserDocument
    {
        $parts        = $parts ?? new UserParts();
        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.username = :username')
            ->setParameter('username', $username);

        if ($parts->contains(UserParts::MERCHANT)) {
            $queryBuilder
                ->addSelect('um')
                ->leftJoin(Merchant::class, 'um', Join::WITH, 'um.id = u.merchantId');
        }

        $result = $queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($result);
        $document = UserDocument::empty();

        foreach ($this->results() as $item) {
            if ($item instanceof User) {
                $document = $this->fetchDocument($item->id());
                break;
            }
        }

        $this->clearResult();

        return $document;
    }

    /**
     * @param UserPhone      $phone
     * @param UserParts|null $parts
     *
     * @return UserDocument
     */
    public function findByPhone(UserPhone $phone, ?UserParts $parts = null): UserDocument
    {
        $parts        = $parts ?? new UserParts();
        $queryBuilder = $this->manager->createQueryBuilder();
        $queryBuilder
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.phone = :phone')
            ->setParameter('phone', $phone);


        if ($parts->contains(UserParts::MERCHANT)) {
            $queryBuilder
                ->addSelect('um')
                ->leftJoin(Merchant::class, 'um', Join::WITH, 'um.id = u.merchantId');
        }

        $result = $queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($result);
        $document = UserDocument::empty();

        foreach ($this->results() as $item) {
            if ($item instanceof User) {
                $document = $this->fetchDocument($item->id());
                break;
            }
        }

        $this->clearResult();

        return $document;
    }

    /**
     * @param Pagination     $filter
     * @param UserParts|null $parts
     *
     * @return PaginatedUserDocument
     */
    public function findAll(Pagination $filter, ?UserParts $parts = null): PaginatedUserDocument
    {
        $parts        = $parts ?? new UserParts();
        $queryBuilder = $this->manager->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->setFirstResult($filter->firstResult())
            ->setMaxResults($filter->pageSize());

        if ($parts->contains(UserParts::MERCHANT)) {
            $queryBuilder
                ->addSelect('um')
                ->leftJoin(Merchant::class, 'um', Join::WITH, 'um.id = u.merchantId');
        }

        $paginator = new Paginator($queryBuilder->getQuery());
        $this->mapResult($paginator->getIterator());
        $result = [];
        foreach ($paginator as $data) {
            if ($data instanceof User) {
                $result[] = $this->fetchDocument($data->id());
            }
        }
        $this->clearResult();

        return new PaginatedUserDocument(
            $result,
            $filter->page(),
            $filter->pageSize(),
            $paginator->count()
        );
    }

    /**
     * @param UserId $id
     *
     * @return UserDocument
     */
    private function fetchDocument(UserId $id): UserDocument
    {
        /** @var User $user */
        $user     = $this->fetchResult(User::class, $id);
        $merchant = $user ? $this->fetchResult(Merchant::class, $user->merchantId()) : null;
        $store    = $user ? $this->fetchResult(Store::class, $user->storeId()) : null;

        return new UserDocument(
            $user,
            new BaseStoreDocument($store),
            new BaseMerchantDocument($merchant)
        );
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        // TODO: Implement createDocument() method.
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts): DoctrineReadModel
    {
        // TODO: Implement buildJoins() method.
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
