<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Merchant\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Merchant\Domain\MerchantName;

/**
 * Class MerchantNameType.
 */
class MerchantNameType extends StringType
{
    public const NAME = 'merchant_name';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return MerchantName|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?MerchantName
    {
        return null === $value ? null : new MerchantName($value);
    }

    /**
     * @param MerchantName|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
