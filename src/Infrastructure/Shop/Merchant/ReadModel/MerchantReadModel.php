<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Merchant\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Merchant\Domain\MerchantName;
use Kiwi\Shop\Merchant\Infrastructure\MerchantReadModel as BaseMerchantReadModel;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class DoctrineMerchantRepository.
 */
class MerchantReadModel extends DoctrineReadModel implements BaseMerchantReadModel
{
    /**
     * @param MerchantId $id
     *
     * @return MerchantDocument|Document
     */
    public function find(MerchantId $id): MerchantDocument
    {
        return $this
            ->select(Merchant::class, $id)
            ->retrieveOne();
    }

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter): PaginatedDocument
    {
        return $this
            ->select(Merchant::class)
            ->retrievePaginated($filter);
    }

    /**
     * @param MerchantName $name
     *
     * @return MerchantDocument|Document
     */
    public function findByName(MerchantName $name): MerchantDocument
    {
        return $this
            ->select(Merchant::class)
            ->retrieveOneOrDefault(MerchantDocument::empty());
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        return new MerchantDocument($this->fetchResult(Merchant::class, $id));
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
