<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Shop\Store\Domain\Timespan;
use Kiwi\Shop\Store\Infrastructure\TimespanWriteModel as BaseTimespanWriteModel;

/**
 * Class DoctrineTimespanRepository.
 */
class TimespanWriteModel extends DoctrineWriteModel implements BaseTimespanWriteModel
{
    /**
     * @param Timespan $timespan
     */
    public function save(Timespan $timespan): void
    {
        $this->persist($timespan);
    }
}
