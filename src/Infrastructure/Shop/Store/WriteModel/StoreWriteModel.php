<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\WriteModel;

use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StorePostalCode;
use Kiwi\Shop\Store\Domain\StoreTimespan;
use Kiwi\Shop\Store\Domain\StoreTimespanId;
use Kiwi\Shop\Store\Domain\TimespanId;
use Kiwi\Shop\Store\Infrastructure\StoreWriteModel as BaseStoreWriteModel;
use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;

/**
 * Class StoreWriteModel.
 */
class StoreWriteModel extends DoctrineWriteModel implements BaseStoreWriteModel
{
    /**
     * @param StoreId $storeId
     *
     * @return Store|null
     */
    public function find(StoreId $storeId): ?Store
    {
        return $this->manager->getRepository(Store::class)->find($storeId);
    }

    /**
     * @param Store $store
     */
    public function save(Store $store): void
    {
        $this->persist($store);
    }

    /**
     * @param Store $store
     */
    public function update(Store $store): void
    {
        $this->persist($store);
    }

    /**
     * @param StoreId $id
     */
    public function delete(StoreId $id): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->delete(Store::class, 's')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }

    /**
     * @param StoreId    $id
     * @param TimespanId $timespanId
     */
    public function addTimestamp(StoreId $id, TimespanId $timespanId): void
    {
        $this->persist(new StoreTimespan(
            new StoreTimespanId(),
            $id,
            $timespanId,
            false
        ));
    }

    /**
     * @param StoreTimespanId $timespanId
     */
    public function openTimestamp(StoreTimespanId $timespanId): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->update(StoreTimespan::class, 'st')
            ->set('st.isClosed', ':isClosed')
            ->where('st.id = :id')
            ->setParameter('isClosed', false)
            ->setParameter('id', $timespanId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param StoreTimespanId $timespanId
     */
    public function closeTimestamp(StoreTimespanId $timespanId): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->update(StoreTimespan::class, 'st')
            ->set('st.isClosed', ':isClosed')
            ->where('st.id = :id')
            ->setParameter('isClosed', true)
            ->setParameter('id', $timespanId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param StoreId $id
     */
    public function enableExpress(StoreId $id): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->update(Store::class, 's')
            ->set('s.isExpressEnabled', ':enable')
            ->where('s.id = :id')
            ->setParameter('enable', true)
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }

    /**
     * @param StoreId $id
     */
    public function disableExpress(StoreId $id): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->update(Store::class, 's')
            ->set('s.isExpressEnabled', ':enable')
            ->where('s.id = :id')
            ->setParameter('enable', false)
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }

    /**
     * @param StorePostalCode $storePostalCode
     */
    public function addPostalCode(StorePostalCode $storePostalCode): void
    {
        $this->persist($storePostalCode);
    }

    /**
     * @param StorePostalCode $storePostalCode
     */
    public function removePostalCode(StorePostalCode $storePostalCode): void
    {
        $this->remove($storePostalCode);
    }
}
