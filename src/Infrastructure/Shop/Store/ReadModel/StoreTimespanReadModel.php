<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Store\Document\BaseStoreTimespanDocument;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StoreTimespan;
use Kiwi\Shop\Store\Domain\Timespan;
use Kiwi\Shop\Store\Infrastructure\StoreTimespanReadModel as BaseStoreTimespanReadModel;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class StoreTimespanReadModel.
 */
class StoreTimespanReadModel extends DoctrineReadModel implements BaseStoreTimespanReadModel
{
    /**
     * @param StoreId  $storeId
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return BaseStoreTimespanDocument|Document
     */
    public function findByStoreAndTimeRange(
        StoreId $storeId,
        DateTime $start,
        DateTime $end
    ): BaseStoreTimespanDocument {
        return $this
            ->select(StoreTimespan::class)
            ->join(Timespan::class, 'timespanId')
            ->andHas('startTime', $start)
            ->andHas('endTime', $end)
            ->retrieveOneOrDefault(BaseStoreTimespanDocument::empty());
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        /** @var StoreTimespan $storeTimespan */
        $storeTimespan = $this->fetchResult(StoreTimespan::class, $id);

        return new BaseStoreTimespanDocument(
            $storeTimespan,
            $this->fetchResult(Timespan::class, $storeTimespan->timespanId())
        );
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
