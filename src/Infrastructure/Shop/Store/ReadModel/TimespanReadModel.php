<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\ReadModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Shop\Store\Document\BaseTimespanDocument;
use Kiwi\Shop\Store\Domain\Timespan;
use Kiwi\Shop\Store\Infrastructure\TimespanReadModel as BaseTimespanReadModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class DoctrineTimespanReadModel.
 */
class TimespanReadModel extends DoctrineWriteModel implements BaseTimespanReadModel
{
    /**
     * @param DateTime $startTime
     * @param DateTime $endTime
     *
     * @return BaseTimespanDocument
     */
    public function find(DateTime $startTime, DateTime $endTime): BaseTimespanDocument
    {
        return new BaseTimespanDocument(
            $this->manager->getRepository(Timespan::class)->findOneBy([
                'startTime' => $startTime,
                'endTime'   => $endTime
            ])
        );
    }
}
