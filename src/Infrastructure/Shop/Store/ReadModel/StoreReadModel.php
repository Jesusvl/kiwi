<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\ReadModel;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\DocumentBook;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Document\BaseStoreTimespanDocument;
use Kiwi\Shop\Store\Document\BaseTimespanDocument;
use Kiwi\Shop\Store\Document\StoreDocument;
use Kiwi\Shop\Store\Document\StorePostalCodeDocument;
use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\Store\Domain\StoreName;
use Kiwi\Shop\Store\Domain\StoreParts;
use Kiwi\Shop\Store\Domain\StorePostalCode;
use Kiwi\Shop\Store\Domain\StoreTimespan;
use Kiwi\Shop\Store\Domain\Timespan;
use Kiwi\Shop\Store\Domain\TimespanId;
use Kiwi\Shop\Store\Infrastructure\StoreReadModel as BaseStoreReadModel;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class StoreReadModel.
 */
class StoreReadModel extends DoctrineReadModel implements BaseStoreReadModel
{
    /**
     * @var boolean
     */
    private $partialJoin = false;

    /**
     * @param StoreId         $id
     * @param StoreParts|null $parts
     *
     * @return StoreDocument|Document
     */
    public function find(StoreId $id, ?StoreParts $parts = null): StoreDocument
    {
        return $this
            ->select(Store::class, $id)
            ->buildJoins($parts)
            ->retrieveOne();
    }

    /**
     * @param Pagination      $filter
     * @param StoreParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter, ?StoreParts $parts = null): PaginatedDocument
    {
        return $this
            ->select(Store::class)
            ->buildJoins($parts)
            ->retrievePaginated($filter);
    }

    /**
     * @param StoreId    $id
     * @param TimespanId $timespanId
     * @param StoreParts $parts
     *
     * @return StoreDocument|Document
     */
    public function findByIdAndTimespan(
        StoreId $id,
        TimespanId $timespanId,
        ?StoreParts $parts = null
    ): StoreDocument {
        $this->partialJoin = true;

        $this
            ->select(Store::class, $id)
            ->buildJoins($parts);

        $this->queryBuilder
            ->addSelect('t')
            ->addSelect('st')
            ->leftJoin(StoreTimespan::class, 'st', Join::WITH, 'e.id = st.storeId')
            ->leftJoin(Timespan::class, 't', Join::WITH, 'st.timespanId = t.id')
            ->andWhere('e.id=:sid')
            ->andWhere('t.id=:tid')
            ->setParameter('sid', $id)
            ->setParameter('tid', $timespanId);

        return $this->retrieveOne();
    }

    /**
     * @param StoreName  $name
     * @param MerchantId $merchant
     * @param StoreParts $parts
     *
     * @return StoreDocument|Document
     */
    public function findByNameAndMerchant(
        StoreName $name,
        MerchantId $merchant,
        StoreParts $parts
    ): StoreDocument {
        return $this
            ->select(Store::class)
            ->andHas('name', $name)
            ->andHas('merchantId', $merchant)
            ->buildJoins($parts)
            ->retrieveOneOrDefault(StoreDocument::empty());
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        /** @var Store $store */
        $store    = $this->fetchResult(Store::class, $id);
        $location = $store ? $this->fetchResult(Location::class, $store->locationId()) : null;

        $timespans   = [];
        $postalCodes = [];

        foreach ($this->results() as $item) {
            if ($item instanceof StoreTimespan) {
                $timespans[$item->storeId()->id()]   = $timespans[$item->storeId()->id()] ?? [];
                $timespans[$item->storeId()->id()][] = new BaseStoreTimespanDocument(
                    $item,
                    new BaseTimespanDocument($this->fetchResult(Timespan::class, $item->timespanId()))
                );
            } elseif ($item instanceof StorePostalCode) {
                $postalCodes[$item->storeId()->id()]   = $postalCodes[$item->storeId()->id()] ?? [];
                $postalCodes[$item->storeId()->id()][] = new StorePostalCodeDocument($item);
            }
        }

        return new StoreDocument(
            $store,
            new LocationDocument($location),
            new DocumentBook($timespans[$store ? $store->id()->id() : -1] ?? []),
            new DocumentBook($postalCodes[$store ? $store->id()->id() : -1] ?? [])
        );
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        $parts = $parts ?? new StoreParts();

        $this->conditionalJoin($parts->contains(StoreParts::LOCATION), Location::class, 'locationId');

        if ($parts->contains(StoreParts::TIMESPANS)) {
            $this->queryBuilder->addSelect('t', 'st');
            if (!$this->partialJoin) {
                $this->queryBuilder->leftJoin(StoreTimespan::class, 'st', Join::WITH, 'e.id = st.storeId');
            }
            $this->queryBuilder->leftJoin(Timespan::class, 't', Join::WITH, 'st.timespanId = t.id');
        }

        if ($parts->contains(StoreParts::POSTAL_CODES)) {
            $this->queryBuilder
                ->addSelect('spc')
                ->leftJoin(StorePostalCode::class, 'spc', Join::WITH, 'spc.storeId = e.id');
        }

        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
