<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Shop\Store\Domain\StoreId;

/**
 * Class StoreIdType.
 */
class StoreIdType extends GuidType
{
    public const NAME = 'store_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return StoreId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?StoreId
    {
        return null === $value ? null : new StoreId($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
