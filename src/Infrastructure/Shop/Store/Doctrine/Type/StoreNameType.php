<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Store\Domain\StoreName;

/**
 * Class StoreNameType.
 */
class StoreNameType extends StringType
{
    public const NAME = 'store_name';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return StoreName|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?StoreName
    {
        return null === $value ? null : new StoreName($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
