<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Shop\Store\Domain\StorePostalCodeId;

/**
 * Class StorePostalCodeIdType.
 */
class StorePostalCodeIdType extends GuidType
{
    public const NAME = 'store_postal_code_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return StorePostalCodeId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?StorePostalCodeId
    {
        return null === $value ? null : new StorePostalCodeId($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
