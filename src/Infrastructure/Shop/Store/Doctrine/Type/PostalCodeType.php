<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Store\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Store\Domain\PostalCode;

/**
 * Class PostalCodeType.
 */
class PostalCodeType extends StringType
{
    public const NAME = 'postal_code';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return PostalCode|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?PostalCode
    {
        return null === $value ? null : new PostalCode($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
