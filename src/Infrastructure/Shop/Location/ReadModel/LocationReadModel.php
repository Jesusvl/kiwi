<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Infrastructure\LocationReadModel as BaseLocationReadModel;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class DoctrineLocationRepository.
 */
class LocationReadModel extends DoctrineReadModel implements BaseLocationReadModel
{
    /**
     * @param LocationId $id
     *
     * @return LocationDocument|Document
     */
    public function find(LocationId $id): LocationDocument
    {
        return $this
            ->select(Location::class, $id)
            ->retrieveOne();
    }

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter): PaginatedDocument
    {
        return $this
            ->select(Location::class)
            ->retrievePaginated($filter);
    }

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findInvalidLocations(Pagination $filter): PaginatedDocument
    {
        return $this
            ->select(Location::class)
            ->andHas('isCompleted', 'FALSE')
            ->retrievePaginated($filter);
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        return new LocationDocument($this->fetchResult(Location::class, $id));
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
