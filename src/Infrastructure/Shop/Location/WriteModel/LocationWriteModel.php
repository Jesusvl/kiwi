<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Shop\Location\Domain\IncompleteLocation;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Location\Infrastructure\LocationWriteModel as BaseLocationWriteModel;

/**
 * Class DoctrineLocationRepository.
 */
class LocationWriteModel extends DoctrineWriteModel implements BaseLocationWriteModel
{
    /**
     * @param LocationId $locationId
     *
     * @return Location|null
     */
    public function find(LocationId $locationId): ?Location
    {
        return $this->manager->getRepository(Location::class)->find($locationId);
    }

    /**
     * @param Location $location
     */
    public function save(Location $location): void
    {
        $this->persist($location);
    }

    /**
     * @param Location $location
     */
    public function update(Location $location): void
    {
        $this->persist($location);
    }

    /**
     * @param IncompleteLocation $location
     */
    public function updateOld(IncompleteLocation $location): void
    {
        $this->override(Location::class, $location, ['coordinates']);
    }

    /**
     * @param LocationId $locationId
     */
    public function delete(LocationId $locationId): void
    {
        $this->manager->createQueryBuilder()
                      ->delete(Location::class, 'l')
                      ->where('l.id = :id')
                      ->setParameter('id', $locationId)
                      ->getQuery()
                      ->execute();
    }
}
