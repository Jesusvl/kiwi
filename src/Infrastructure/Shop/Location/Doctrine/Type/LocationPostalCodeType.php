<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Location\Domain\LocationPostalCode;

/**
 * Class LocationPostalCodeType.
 */
class LocationPostalCodeType extends StringType
{
    public const NAME = 'location_postal_code';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return LocationPostalCode|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?LocationPostalCode
    {
        return null === $value ? null : new LocationPostalCode($value);
    }

    /**
     * @param LocationPostalCode|string|null $value
     * @param AbstractPlatform               $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
