<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Location\Domain\LocationStair;

/**
 * Class LocationStairType.
 */
class LocationStairType extends StringType
{
    public const NAME = 'location_stair';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return LocationStair|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?LocationStair
    {
        return null === $value ? null : new LocationStair($value);
    }

    /**
     * @param LocationStair|string|null $value
     * @param AbstractPlatform          $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
