<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\FloatType;
use Kiwi\Shop\Location\Domain\Longitude;

/**
 * Class LongitudeType.
 */
class LocationLongitudeType extends FloatType
{
    public const NAME = 'location_longitude';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return Longitude|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Longitude
    {
        return null === $value ? null : new Longitude((float)$value);
    }

    /**
     * @param Latitude|string|null $value
     * @param AbstractPlatform     $platform
     *
     * @return float|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?float
    {
        if (\is_numeric($value)) {
            return (float)$value;
        }

        return null === $value ? null : (float)(string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
