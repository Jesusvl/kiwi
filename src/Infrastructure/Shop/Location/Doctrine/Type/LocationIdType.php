<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Shop\Location\Domain\LocationId;

/**
 * Class LocationIdType.
 */
class LocationIdType extends GuidType
{
    public const NAME = 'location_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return LocationId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?LocationId
    {
        return null === $value ? null : new LocationId($value);
    }

    /**
     * @param LocationId|string|null $value
     * @param AbstractPlatform       $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
