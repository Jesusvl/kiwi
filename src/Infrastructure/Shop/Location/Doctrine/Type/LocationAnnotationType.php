<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;
use Kiwi\Shop\Location\Domain\LocationAnnotation;

/**
 * Class LocationAnnotationType.
 */
class LocationAnnotationType extends TextType
{
    public const NAME = 'location_annotation';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return LocationAnnotation|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?LocationAnnotation
    {
        return null === $value ? null : new LocationAnnotation($value);
    }

    /**
     * @param LocationAnnotation|string|null $value
     * @param AbstractPlatform               $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
