<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\FloatType;
use Kiwi\Shop\Location\Domain\Latitude;

/**
 * Class LocationLatitudeType.
 */
class LocationLatitudeType extends FloatType
{
    public const NAME = 'location_latitude';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return Latitude|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Latitude
    {
        return null === $value ? null : new Latitude((float)$value);
    }

    /**
     * @param Latitude|string|null $value
     * @param AbstractPlatform     $platform
     *
     * @return float|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?float
    {
        if (\is_numeric($value)) {
            return (float)$value;
        }

        return null === $value ? null : (float)(string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
