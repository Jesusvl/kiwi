<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Location\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Location\Domain\LocationDoor;

/**
 * Class LocationDoorType.
 */
class LocationDoorType extends StringType
{
    public const NAME = 'location_door';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return LocationDoor|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?LocationDoor
    {
        return null === $value ? null : new LocationDoor($value);
    }

    /**
     * @param LocationDoor|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
