<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Client\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Client\Infrastructure\ClientWriteModel as BaseClientWriteModel;
use Kiwi\Shop\Location\Domain\LocationId;

/**
 * Class ClientWriteModel.
 */
class ClientWriteModel extends DoctrineWriteModel implements BaseClientWriteModel
{
    /**
     * @param ClientId $id
     *
     * @return Client|null
     */
    public function find(ClientId $id): ?Client
    {
        return $this->manager->getRepository(Client::class)->find($id);
    }

    /**
     * @param Client $client
     */
    public function save(Client $client): void
    {
        $this->persist($client);
    }

    /**
     * @param ClientId $id
     */
    public function delete(ClientId $id): void
    {
        $this->manager->createQueryBuilder()
            ->delete(Client::class, 'c')
            ->where('d.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }

    /**
     * @param ClientId   $id
     * @param LocationId $locationId
     */
    public function updateLocation(ClientId $id, LocationId $locationId): void
    {
        $builder = $this->manager->createQueryBuilder();
        $builder->update(Client::class, 'c')
            ->set('c.locationId', $builder->expr()->literal($locationId))
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}
