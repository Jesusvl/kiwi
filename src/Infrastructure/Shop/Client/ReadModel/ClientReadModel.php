<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Client\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Client\Document\ClientDocument;
use Kiwi\Shop\Client\Domain\Client;
use Kiwi\Shop\Client\Domain\ClientCode;
use Kiwi\Shop\Client\Domain\ClientId;
use Kiwi\Shop\Client\Domain\ClientParts;
use Kiwi\Shop\Client\Domain\ClientPhone;
use Kiwi\Shop\Client\Infrastructure\ClientReadModel as BaseClientReadModel;
use Kiwi\Shop\Location\Document\LocationDocument;
use Kiwi\Shop\Location\Domain\Location;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Store\Domain\StoreId;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class ClientReadModel.
 */
class ClientReadModel extends DoctrineReadModel implements BaseClientReadModel
{
    /**
     * @param ClientId         $id
     * @param ClientParts|null $parts
     *
     * @return ClientDocument|Document
     */
    public function find(ClientId $id, ?ClientParts $parts = null): ClientDocument
    {
        return $this
            ->select(Client::class, $id)
            ->buildJoins($parts)
            ->retrieveOne();
    }

    /**
     * @param Pagination       $filter
     * @param ClientParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter, ?ClientParts $parts = null): PaginatedDocument
    {
        return $this
            ->select(Client::class)
            ->buildJoins($parts)
            ->retrievePaginated($filter);
    }

    /**
     * @param StoreId          $store
     * @param Pagination       $filter
     * @param ClientParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAllByStore(
        StoreId $store,
        Pagination $filter,
        ?ClientParts $parts = null
    ): PaginatedDocument {
        return $this
            ->select(Client::class)
            ->andHas('storeId', $store)
            ->buildJoins($parts)
            ->retrievePaginated($filter);
    }

    /**
     * @param MerchantId       $merchant
     * @param Pagination       $filter
     * @param ClientParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAllByMerchant(
        MerchantId $merchant,
        Pagination $filter,
        ?ClientParts $parts = null
    ): PaginatedDocument {
        return $this
            ->select(Client::class)
            ->andHas('merchantId', $merchant)
            ->buildJoins($parts)
            ->retrievePaginated($filter);
    }

    /**
     * @param ClientCode       $code
     * @param ClientParts|null $parts
     *
     * @return ClientDocument|Document
     */
    public function findByCode(ClientCode $code, ?ClientParts $parts = null): ClientDocument
    {
        return $this
            ->select(Client::class)
            ->andHas('code', $code)
            ->buildJoins($parts)
            ->retrieveOneOrDefault(ClientDocument::empty());
    }

    /**
     * @param LocationId       $locationId
     * @param ClientParts|null $parts
     *
     * @return ClientDocument|Document
     */
    public function findByLocation(LocationId $locationId, ?ClientParts $parts = null): ClientDocument
    {
        return $this
            ->select(Client::class)
            ->andHas('locationId', $locationId)
            ->buildJoins($parts)
            ->retrieveOneOrDefault(ClientDocument::empty());
    }

    /**
     * @param ClientPhone      $phone
     * @param MerchantId       $merchant
     * @param ClientParts|null $parts
     *
     * @return ClientDocument|Document
     */
    public function findByPhoneAndMerchant(
        ClientPhone $phone,
        MerchantId $merchant,
        ?ClientParts $parts = null
    ): ClientDocument {
        return $this
            ->select(Client::class)
            ->andHas('phone', $phone)
            ->andHas('merchantId', $merchant)
            ->buildJoins($parts)
            ->retrieveOneOrDefault(ClientDocument::empty());
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        /** @var Client $client */
        $client   = $this->fetchResult(Client::class, $id);
        $location = $client ? $this->fetchResult(Location::class, $client->locationId()) : null;
        $merchant = $client ? $this->fetchResult(Merchant::class, $client->merchantId()) : null;

        return new ClientDocument($client, new LocationDocument($location), new MerchantDocument($merchant));
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        $parts = $parts ?? new ClientParts();
        $this->conditionalJoin($parts->contains('location'), Location::class, 'locationId');
        $this->conditionalJoin($parts->contains('merchant'), Merchant::class, 'merchantId');

        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
