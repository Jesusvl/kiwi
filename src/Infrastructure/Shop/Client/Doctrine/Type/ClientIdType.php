<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Client\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Shop\Client\Domain\ClientId;

/**
 * Class ClientIdType.
 */
class ClientIdType extends GuidType
{
    public const NAME = 'client_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return ClientId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?ClientId
    {
        return null === $value ? null : new ClientId($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
