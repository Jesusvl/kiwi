<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Client\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Client\Domain\ClientName;

/**
 * Class ClientNameType.
 */
class ClientNameType extends StringType
{
    public const NAME = 'client_name';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return ClientName|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?ClientName
    {
        return null === $value ? null : new ClientName($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
