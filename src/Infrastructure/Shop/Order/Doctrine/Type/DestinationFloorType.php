<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Order\Domain\DestinationFloor;

/**
 * Class DestinationFloorType.
 */
class DestinationFloorType extends StringType
{
    public const NAME = 'destination_floor';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return DestinationFloor|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DestinationFloor
    {
        return null === $value ? null : new DestinationFloor($value);
    }

    /**
     * @param DestinationFloor|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
