<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Kiwi\Shop\Order\Domain\DestinationId;

/**
 * Class DestinationIdType.
 */
class DestinationIdType extends GuidType
{
    public const NAME = 'destination_id';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return DestinationId|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DestinationId
    {
        return null === $value ? null : new DestinationId($value);
    }

    /**
     * @param DestinationId|string|null $value
     * @param AbstractPlatform       $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
