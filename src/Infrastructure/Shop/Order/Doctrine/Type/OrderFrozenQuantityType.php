<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Kiwi\Shop\Order\Domain\OrderFrozenQuantity;

/**
 * Class OrderFrozenQuantityType.
 */
class OrderFrozenQuantityType extends IntegerType
{
    public const NAME = 'order_frozen_quantity';

    /**
     * @param int|null      $value
     * @param AbstractPlatform $platform
     *
     * @return OrderFrozenQuantity|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?OrderFrozenQuantity
    {
        return null === $value ? null : new OrderFrozenQuantity($value);
    }

    /**
     * @param OrderFrozenQuantity|int|null $value
     * @param AbstractPlatform         $platform
     *
     * @return int|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        if (\is_numeric($value)) {
            return (int)$value;
        }

        return null === $value ? null : (int)(string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
