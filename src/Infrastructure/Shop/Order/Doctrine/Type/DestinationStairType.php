<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Order\Domain\DestinationStair;

/**
 * Class DestinationStairType.
 */
class DestinationStairType extends StringType
{
    public const NAME = 'destination_stair';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return DestinationStair|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DestinationStair
    {
        return null === $value ? null : new DestinationStair($value);
    }

    /**
     * @param DestinationStair|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
