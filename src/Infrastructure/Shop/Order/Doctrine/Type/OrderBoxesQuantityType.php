<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Kiwi\Shop\Order\Domain\OrderBoxesQuantity;

/**
 * Class OrderBoxesQuantityType.
 */
class OrderBoxesQuantityType extends IntegerType
{
    public const NAME = 'order_boxes_quantity';

    /**
     * @param int|null      $value
     * @param AbstractPlatform $platform
     *
     * @return OrderBoxesQuantity|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?OrderBoxesQuantity
    {
        return null === $value ? null : new OrderBoxesQuantity($value);
    }

    /**
     * @param OrderBoxesQuantity|int|null $value
     * @param AbstractPlatform         $platform
     *
     * @return int|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?int
    {
        if (\is_numeric($value)) {
            return (int)$value;
        }

        return null === $value ? null : (int)(string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
