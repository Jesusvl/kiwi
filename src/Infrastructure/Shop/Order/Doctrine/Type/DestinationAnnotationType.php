<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\TextType;
use Kiwi\Shop\Order\Domain\DestinationAnnotation;

/**
 * Class DestinationAnnotationType.
 */
class DestinationAnnotationType extends TextType
{
    public const NAME = 'destination_annotation';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return DestinationAnnotation|nulls
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DestinationAnnotation
    {
        return null === $value ? null : new DestinationAnnotation($value);
    }

    /**
     * @param DestinationAnnotation|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
