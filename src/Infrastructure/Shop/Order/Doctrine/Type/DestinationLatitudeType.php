<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\FloatType;
use Kiwi\Shop\Order\Domain\DestinationLatitude;

/**
 * Class DestinaType.
 */
class DestinationLatitudeType extends FloatType
{
    public const NAME = 'destination_latitude';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return DestinationLatitude|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DestinationLatitude
    {
        return null === $value ? null : new DestinationLatitude((float)$value);
    }

    /**
     * @param string|null $value
     * @param AbstractPlatform     $platform
     *
     * @return float|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?float
    {
        if (\is_numeric($value)) {
            return (float)$value;
        }

        return null === $value ? null : (float)(string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
