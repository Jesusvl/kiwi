<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Order\Domain\OrderNumber;

/**
 * Class OrderNumberType.
 */
class OrderNumberType extends StringType
{
    public const NAME = 'order_number';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return OrderNumber|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?OrderNumber
    {
        return null === $value ? null : new OrderNumber($value);
    }

    /**
     * @param OrderNumber|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
