<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\Order\Domain\DestinationAddress;

/**
 * Class DestinationAddressType.
 */
class DestinationAddressType extends StringType
{
    public const NAME = 'destination_address';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return DestinationAddress|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?DestinationAddress
    {
        return null === $value ? null : new DestinationAddress($value);
    }

    /**
     * @param DestinationAddress|string|null $value
     * @param AbstractPlatform         $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
