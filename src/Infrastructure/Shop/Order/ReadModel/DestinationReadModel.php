<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Order\Document\OrderDestinationDocument;
use Kiwi\Shop\Order\Domain\Destination;
use Kiwi\Shop\Order\Infrastructure\DestinationReadModel as BaseDestinationReadModel;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class DestinationReadModel.
 */
class DestinationReadModel extends DoctrineReadModel implements BaseDestinationReadModel
{

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    public function findBadDestinations(Pagination $filter): PaginatedDocument
    {
        return $this
            ->select(Destination::class)
            ->andCondition('d.latitude > -0.00001')
            ->andCondition('d.latitude < 0.00001')
            ->andCondition('d.longitude > -0.00001')
            ->andCondition('d.longitude < 0.00001')
            ->retrievePaginated($filter);
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        return new OrderDestinationDocument($this->fetchResult(Destination::class, $id));
    }

    /**
     * @param Parts|null $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
