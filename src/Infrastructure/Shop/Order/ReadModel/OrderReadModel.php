<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Core\Exception\InvalidSortArgumentException;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Merchant\Domain\MerchantId;
use Kiwi\Shop\Order\Document\OrderDestinationDocument;
use Kiwi\Shop\Order\Document\OrderDocument;
use Kiwi\Shop\Order\Domain\Destination;
use Kiwi\Shop\Order\Domain\Order;
use Kiwi\Shop\Order\Domain\OrderFilter;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\Order\Domain\OrderParts;
use Kiwi\Shop\Order\Domain\OrderSort;
use Kiwi\Shop\Order\Domain\OrderStatus;
use Kiwi\Shop\Order\Infrastructure\OrderReadModel as BaseOrderReadModel;
use Kiwi\Shop\Store\Document\BaseStoreDocument;
use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\Store\Domain\StoreId;
use Kiwi\Shop\User\Document\BaseUserDocument;
use Kiwi\Shop\User\Domain\User;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;
use KiwiLib\DateTime\DateTime;

/**
 * Class OrderReadModel.
 */
class OrderReadModel extends DoctrineReadModel implements BaseOrderReadModel
{
    /**
     * @param OrderId        $id
     * @param OrderParts     $parts
     *
     * @param OrderSort|null $sortBy
     *
     * @return OrderDocument|Document
     *
     * @throws InvalidSortArgumentException
     */
    public function find(OrderId $id, ?OrderParts $parts = null, ?OrderSort $sortBy = null): OrderDocument
    {
        $sortBy = $sortBy ?? new OrderSort('startTime', 'ASC');

        return $this
            ->select(Order::class, $id)
            ->buildJoins($parts)
            ->orderBy($sortBy)
            ->retrieveOne();
    }

    /**
     * @param DateTime        $date
     * @param Pagination      $pagination
     * @param OrderFilter[]   $filters
     * @param OrderParts|null $parts
     * @param OrderSort|null  $sortBy
     *
     * @return PaginatedDocument
     *
     * @throws InvalidSortArgumentException
     */
    public function findByDate(
        DateTime $date,
        Pagination $pagination,
        array $filters,
        ?OrderParts $parts = null,
        ?OrderSort $sortBy = null
    ): PaginatedDocument {
        $sortBy = $sortBy ?? new OrderSort('startTime', 'ASC');

        return $this
            ->select(Order::class)
            ->buildJoins($parts)
            ->orderBy($sortBy)
            ->applyFilters($filters)
            ->addCondition('DATE_DIFF(e.startTime, :date) = 0', 'date', $date)
            ->retrievePaginated($pagination);
    }

    /**
     * @param DateTime        $date
     * @param StoreId         $store
     * @param Pagination      $pagination
     * @param Filter[]        $filters
     * @param OrderParts|null $parts
     * @param OrderSort|null  $sortBy
     *
     * @return PaginatedDocument
     *
     * @throws InvalidSortArgumentException
     */
    public function findByDateAndStore(
        DateTime $date,
        StoreId $store,
        Pagination $pagination,
        array $filters,
        ?OrderParts $parts = null,
        ?OrderSort $sortBy = null
    ): PaginatedDocument {
        $sortBy = $sortBy ?? new OrderSort('startTime', 'ASC');

        return $this
            ->select(Order::class)
            ->andHas('storeId', $store)
            ->buildJoins($parts)
            ->applyFilters($filters)
            ->orderBy($sortBy)
            ->addCondition('DATE_DIFF(e.startTime, :date) = 0', 'date', $date)
            ->retrievePaginated($pagination);
    }

    /**
     * @param DateTime        $date
     * @param MerchantId      $merchant
     * @param Pagination      $pagination
     * @param Filter[]        $filters
     * @param OrderParts|null $parts
     * @param OrderSort|null  $sortBy
     *
     * @return PaginatedDocument
     *
     * @throws InvalidSortArgumentException
     */
    public function findByDateAndMerchant(
        DateTime $date,
        MerchantId $merchant,
        Pagination $pagination,
        array $filters,
        ?OrderParts $parts = null,
        ?OrderSort $sortBy = null
    ): PaginatedDocument {
        $sortBy = $sortBy ?? new OrderSort('startTime', 'ASC');

        return $this
            ->select(Order::class)
            ->andHas('merchantId', $merchant)
            ->buildJoins($parts)
            ->applyFilters($filters)
            ->orderBy($sortBy)
            ->addCondition('DATE_DIFF(e.startTime, :date) = 0', 'date', $date)
            ->retrievePaginated($pagination);
    }

    /**
     * @param DateTime       $date
     * @param MerchantId     $merchant
     * @param Pagination     $pagination
     * @param Filter[]       $filters
     * @param OrderParts     $parts
     * @param OrderSort|null $sortBy
     *
     * @return PaginatedDocument
     *
     * @throws InvalidSortArgumentException
     */
    public function findNonDeliveredByDateAndMerchant(
        DateTime $date,
        MerchantId $merchant,
        Pagination $pagination,
        array $filters,
        OrderParts $parts,
        ?OrderSort $sortBy = null
    ): PaginatedDocument {
        $sortBy = $sortBy ?? new OrderSort('startTime', 'ASC');

        $qb = $this
            ->select(Order::class)
            ->andHas('merchantId', $merchant)
            ->queryBuilder;

        $qb
            ->andWhere('e.status != :delivered')
            ->andWhere('e.status != :cancelled')
            ->setParameter('delivered', OrderStatus::delivered())
            ->setParameter('cancelled', OrderStatus::canceled());

        return $this
            ->buildJoins($parts)
            ->applyFilters($filters)
            ->orderBy($sortBy)
            ->addCondition('DATE_DIFF(e.startTime, :date) = 0', 'date', $date)
            ->retrievePaginated($pagination);
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        $parts = $parts ?? new OrderParts();

        return $this
            ->conditionalJoin($parts->contains(OrderParts::DESTINATION), Destination::class, 'destinationId')
            ->conditionalJoin($parts->contains(OrderParts::STORE), Store::class, 'storeId')
            ->conditionalJoin($parts->contains(OrderParts::SHIPPER), User::class, 'shipperId')
            ->conditionalJoin($parts->contains(OrderParts::MERCHANT), Merchant::class, 'merchantId');
    }

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    protected function createDocument(Uuid $id): Document
    {
        /** @var Order $order */
        $order = $this->fetchResult(Order::class, $id);

        $destination = $order ? $this->fetchResult(Destination::class, $order->destinationId()) : null;
        $store = $order ? $this->fetchResult(Store::class, $order->storeId()) : null;
        $shipper = $order ? $this->fetchResult(User::class, $order->shipperId()) : null;
        $merchant = $order ? $this->fetchResult(Merchant::class, $order->merchantId()) : null;

        return new OrderDocument(
            $order,
            new OrderDestinationDocument($destination),
            new BaseStoreDocument($store),
            new BaseUserDocument($shipper),
            new MerchantDocument($merchant)
        );
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        switch ($filter->name()) {
            case 'startTime':
                $queryBuilder->andWhere("HOUR(e.startTime) {$filter->comparatorSymbol()} :cf_1")
                    ->setParameter('cf_1', $filter->value());

                return true;
            case 'endTime':

                $queryBuilder->andWhere("HOUR(e.endTime) {$filter->comparatorSymbol()} :cf_2")
                    ->setParameter('cf_2', $filter->value());

                return true;
            default:
                return false;
        }
    }
}
