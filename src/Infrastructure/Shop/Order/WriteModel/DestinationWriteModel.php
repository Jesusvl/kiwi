<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Shop\Location\Domain\LocationId;
use Kiwi\Shop\Order\Domain\Destination;
use Kiwi\Shop\Order\Domain\DestinationId;
use Kiwi\Shop\Order\Infrastructure\DestinationWriteModel as BaseDestinationWriteModel;

/**
 * Class DestinationReadModel.
 */
class DestinationWriteModel extends DoctrineWriteModel implements BaseDestinationWriteModel
{
    /**
     * @param DestinationId $id
     *
     * @return Destination|null
     */
    public function find(DestinationId $id): ?Destination
    {
        return $this->manager->getRepository(Destination::class)->find($id);
    }

    /**
     * @param Destination $destination
     */
    public function save(Destination $destination): void
    {
        $this->persist($destination);
    }

    /**
     * @param Destination $destination
     */
    public function update(Destination $destination): void
    {
        $this->persist($destination);
    }

    /**
     * @param LocationId $locationId
     *
     * @return Destination[]
     */
    public function findByLocation(LocationId $locationId): array
    {
        return $this->manager->getRepository(Destination::class)->findBy(['locationId' => $locationId]);
    }
}
