<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\Order\WriteModel;

use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;
use Kiwi\Shop\Order\Domain\Order;
use Kiwi\Shop\Order\Domain\OrderId;
use Kiwi\Shop\Order\Infrastructure\OrderWriteModel as BaseOrderWriteModel;

/**
 * Class OrderWriteModel.
 */
class OrderWriteModel extends DoctrineWriteModel implements BaseOrderWriteModel
{

    /**
     * @param OrderId $id
     *
     * @return Order|null
     */
    public function find(OrderId $id): ?Order
    {
        return $this->manager->getRepository(Order::class)->find($id);
    }

    /**
     * @param Order $order
     */
    public function save(Order $order): void
    {
        $this->persist($order);
    }

    /**
     * @param Order $order
     */
    public function update(Order $order): void
    {
        $this->persist($order);
    }
}
