<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\User\ReadModel;

use Doctrine\ORM\QueryBuilder;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Shop\Merchant\Document\MerchantDocument;
use Kiwi\Shop\Merchant\Domain\Merchant;
use Kiwi\Shop\Store\Document\BaseStoreDocument;
use Kiwi\Shop\Store\Domain\Store;
use Kiwi\Shop\User\Document\UserDocument;
use Kiwi\Shop\User\Domain\User;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Domain\UserParts;
use Kiwi\Shop\User\Domain\UserPhone;
use Kiwi\Shop\User\Domain\UserUsername;
use Kiwi\Shop\User\Infrastructure\UserReadModel as BaseUserReadModel;
use KiwiInfrastructure\Core\ReadModel\DoctrineReadModel;

/**
 * Class UserReadModel.
 */
class UserReadModel extends DoctrineReadModel implements BaseUserReadModel
{
    /**
     * @param UserId         $id
     * @param UserParts|null $parts
     *
     * @return UserDocument|Document
     */
    public function find(UserId $id, ?UserParts $parts = null): UserDocument
    {
        return $this
            ->select(User::class, $id)
            ->buildJoins($parts)
            ->retrieveOne();
    }

    /**
     * @param UserUsername   $username
     * @param UserParts|null $parts
     *
     * @return UserDocument|Document
     */
    public function findByUsername(UserUsername $username, ?UserParts $parts = null): UserDocument
    {
        return $this
            ->select(User::class)
            ->andHas('username', $username)
            ->buildJoins($parts)
            ->retrieveOneOrDefault(UserDocument::empty());
    }

    /**
     * @param UserPhone      $phone
     * @param UserParts|null $parts
     *
     * @return UserDocument|Document
     */
    public function findByPhone(UserPhone $phone, ?UserParts $parts = null): UserDocument
    {
        return $this
            ->select(User::class)
            ->andHas('phone', $phone)
            ->buildJoins($parts)
            ->retrieveOneOrDefault(UserDocument::empty());
    }

    /**
     * @param Pagination     $filter
     * @param UserParts|null $parts
     *
     * @return PaginatedDocument
     */
    public function findAll(Pagination $filter, ?UserParts $parts = null): PaginatedDocument
    {
        return $this
            ->select(User::class)
            ->buildJoins($parts)
            ->retrievePaginated($filter);
    }

    /**
     * @param Uuid $id
     *
     * @return Document|UserDocument
     */
    protected function createDocument(Uuid $id): Document
    {
        /** @var User $user */
        $user     = $this->fetchResult(User::class, $id);
        $merchant = $user ? $this->fetchResult(Merchant::class, $user->merchantId()) : null;
        $store    = $user ? $this->fetchResult(Store::class, $user->storeId()) : null;

        return new UserDocument(
            $user,
            new BaseStoreDocument($store),
            new MerchantDocument($merchant)
        );
    }

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    protected function buildJoins(?Parts $parts = null): DoctrineReadModel
    {
        $parts = $parts ?? new UserParts();

        return $this
            ->conditionalJoin($parts->contains(UserParts::STORE), Store::class, 'storeId')
            ->conditionalJoin($parts->contains(UserParts::MERCHANT), Merchant::class, 'merchantId');
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool
    {
        return false;
    }
}
