<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\User\WriteModel;

use Kiwi\Shop\User\Domain\User;
use Kiwi\Shop\User\Domain\UserId;
use Kiwi\Shop\User\Infrastructure\UserWriteModel as BaseUserWriteModel;
use KiwiInfrastructure\Core\WriteModel\DoctrineWriteModel;

/**
 * Class UserWriteModel.
 */
class UserWriteModel extends DoctrineWriteModel implements BaseUserWriteModel
{
    /**
     * @param UserId $id
     *
     * @return User|null
     */
    public function find(UserId $id): ?User
    {
        return $this->manager->getRepository(User::class)->find($id);
    }

    /**
     * @param User $user
     */
    public function save(User $user): void
    {
        $this->persist($user);
    }

    /**
     * @param User $user
     */
    public function update(User $user): void
    {
        $this->persist($user);
    }
}
