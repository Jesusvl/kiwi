<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\User\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\User\Domain\UserUsername;

/**
 * Class UserUsernameType.
 */
class UserUsernameType extends StringType
{
    public const NAME = 'kiwiuser_username';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return UserUsername|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?UserUsername
    {
        return null === $value ? null : new UserUsername($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
