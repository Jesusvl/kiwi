<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\User\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\User\Domain\UserRole;

/**
 * Class UserRoleType.
 */
class UserRoleType extends StringType
{
    public const NAME = 'kiwiuser_role';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return UserRole|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?UserRole
    {
        return null === $value ? null : new UserRole($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
