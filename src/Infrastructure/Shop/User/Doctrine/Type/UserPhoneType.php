<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Shop\User\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Kiwi\Shop\User\Domain\UserPhone;

/**
 * Class UserPhoneType.
 */
class UserPhoneType extends StringType
{
    public const NAME = 'kiwiuser_phone';

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return UserPhone|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?UserPhone
    {
        return null === $value ? null : new UserPhone($value);
    }

    /**
     * @param string|null      $value
     * @param AbstractPlatform $platform
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (\is_string($value)) {
            return $value;
        }

        return null === $value ? null : (string)$value;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
