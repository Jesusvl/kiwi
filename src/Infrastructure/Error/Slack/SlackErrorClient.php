<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Error\Slack;

use GuzzleHttp\Exception\ConnectException;
use Kiwi\Error\Error\Infrastructure\ErrorClient;
use Maknz\Slack\Client;
use Maknz\Slack\Message;

/**
 * Class SlackErrorClient.
 */
class SlackErrorClient implements ErrorClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $channel;

    /**
     * SlackErrorClient constructor.
     *
     * @param string $url
     * @param string $channel
     */
    public function __construct(string $url, string $channel)
    {
        $this->client  = new Client($url, [
            'link_names' => true
        ]);
        $this->channel = $channel;
    }

    /**
     * @param string $message
     */
    public function sendMessage(string $message): void
    {
        try {
            $slackMessage = new Message($this->client);
            $slackMessage->setIcon(':sweat:');
            $slackMessage->setUsername('kiwi-error-notifier');
            $slackMessage->setChannel($this->channel);
            $slackMessage->send($message);
        } catch (ConnectException $exception) {
        }
    }
}
