<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Error\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Kiwi\Core\Exception\DataNotFoundDomainException;
use Kiwi\Core\Exception\DomainException;
use Kiwi\Core\Exception\InfrastructureException;
use Kiwi\Core\Exception\InsufficientPermissionsException;
use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Error\Error\Event\OnErrorEvent;
use KiwiInfrastructure\Core\Http\Exception\HttpException as KiwiHttpException;
use KiwiLib\DateTime\DateTime;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Throwable;

/**
 * Class ExceptionSubscriber.
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var string
     */
    private $env;

    /**
     * ExceptionSubscriber constructor.
     *
     * @param EventManager           $eventManager
     * @param EntityManagerInterface $manager
     * @param ContainerInterface     $container
     */
    public function __construct(
        EventManager $eventManager,
        EntityManagerInterface $manager,
        ContainerInterface $container
    ) {
        $this->eventManager  = $eventManager;
        $this->container     = $container;
        $this->entityManager = $manager;
        $this->env           = $container->getParameter('kernel.environment');
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::EXCEPTION  => [
                ['processException', 10],
                ['notifyException', -10]
            ],
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::RESPONSE   => 'onKernelResponse'
        ];
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        if ($this->env !== 'test') {
            try {
                $this->entityManager->beginTransaction();
            } catch (Throwable $e) {
            }
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event): void
    {
        try {
            if ($this->env !== 'test') {
                $this->entityManager->commit();
            }
        } catch (Throwable $e) {
        }
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $this->processException($event);
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function processException(GetResponseForExceptionEvent $event): void
    {
        try {
            if ($this->env !== 'test') {
                $this->entityManager->rollback();
            }
        } catch (Throwable $e) {
        }


        if (preg_match('/^\/api\//', $event->getRequest()->getPathInfo())) {
            $exception = $event->getException();
            if ($exception instanceof KiwiHttpException) {
                if ($exception->getStatus() === JsonResponse::HTTP_INTERNAL_SERVER_ERROR) {
                    $event->setResponse($this->getGenericResponse($exception));
                } else {
                    $event->setResponse(new JsonResponse([
                        'error'  => $exception->getMessage(),
                        'status' => $exception->getStatus(),
                        'extras' => $exception->getExtras()
                    ], $exception->getStatus()));
                }
            } elseif ($exception instanceof HttpException) {
                if ($exception->getStatusCode() === JsonResponse::HTTP_INTERNAL_SERVER_ERROR) {
                    $event->setResponse($this->getGenericResponse($exception));
                } else {
                    $event->setResponse(new JsonResponse([
                        'error'  => $exception->getMessage(),
                        'status' => $exception->getStatusCode(),
                        'extras' => []
                    ], $exception->getStatusCode()));
                }
            } elseif ($exception instanceof InfrastructureException) {
                $event->setResponse($this->getGenericResponse($exception));
            } elseif ($exception instanceof DataNotFoundDomainException) {
                $event->setResponse(new JsonResponse([
                    'error'  => $exception->getMessage(),
                    'status' => JsonResponse::HTTP_NOT_FOUND
                ], JsonResponse::HTTP_NOT_FOUND));
            } elseif ($exception instanceof InsufficientPermissionsException) {
                $event->setResponse(new JsonResponse([
                    'error'  => $exception->getMessage(),
                    'status' => JsonResponse::HTTP_FORBIDDEN
                ], JsonResponse::HTTP_FORBIDDEN));
            } elseif ($exception instanceof DomainException) {
                $event->setResponse(new JsonResponse([
                    'error'  => $exception->getMessage(),
                    'status' => JsonResponse::HTTP_CONFLICT
                ], JsonResponse::HTTP_CONFLICT));
            } else {
                $event->setResponse($this->getGenericResponse($exception));
            }
        } else {
            $exception = $event->getException();

            if ($event->getException() instanceof AccessDeniedHttpException) {
                $event->setResponse(new RedirectResponse('/login', Response::HTTP_FOUND));
            } elseif ($event->getException() instanceof InsufficientPermissionsException) {
                $event->setResponse(new RedirectResponse('/login', Response::HTTP_FOUND));
            } elseif ($exception instanceof InfrastructureException) {
                $event->setException(
                    new HttpException(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, $exception->getMessage(), $exception)
                );
            } elseif ($exception instanceof DataNotFoundDomainException) {
                $event->setException(
                    new HttpException(JsonResponse::HTTP_NOT_FOUND, $exception->getMessage(), $exception)
                );
            } elseif ($exception instanceof DomainException) {
                $event->setException(
                    new HttpException(JsonResponse::HTTP_CONFLICT, $exception->getMessage(), $exception)
                );
            }
        }
    }

    /**
     * @param GetResponseForExceptionEvent $event
     *
     * @throws Exception
     */
    public function notifyException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();

        $mustSend = true;
        if ($exception instanceof HttpException) {
            $mustSend = $exception->getCode() === JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        if ($mustSend) {
            $file = $event->getException()->getFile();
            $line = $event->getException()->getLine();

            if (strpos($file, 'vendor')) {
                $trace = $event->getException()->getTrace();

                foreach ($trace as $step) {
                    if (($step['file'] ?? false) && strpos($step['file'], 'vendor') === false) {
                        $file = $step['file'];
                        $line = $step['line'];
                        break;
                    }
                }
            }
            $this->eventManager->dispatch(new OnErrorEvent(
                get_class($event->getException()),
                $event->getException()->getMessage(),
                $file,
                $line,
                new DateTime()
            ));
        }
    }

    /**
     * @param Throwable $exception
     *
     * @return JsonResponse
     */
    private function getGenericResponse(Throwable $exception): JsonResponse
    {
        $isProd = $this->container->getParameter('kernel.environment') === 'prod';
        $extras = [];

        if ($exception instanceof KiwiHttpException) {
            $extras = $exception->getExtras();
        }
        if ($isProd) {
            return new JsonResponse(array_merge([
                'message' => 'Internal Server Error.',
                'status'  => JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            ], $extras), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(array_merge([
            'message' => $exception->getMessage(),
            'status'  => JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
            'file'    => $exception->getFile(),
            'line'    => $exception->getLine(),
            'trace'   => $exception->getTrace()
        ], $extras), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }
}
