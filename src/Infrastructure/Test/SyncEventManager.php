<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Test;

use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Core\Infrastructure\Listener;

/**
 * Class SyncEventManager.
 */
class SyncEventManager implements EventManager
{
    /**
     * @var array
     */
    private $listenerMap = [];

    /**
     * @var array
     */
    private $asyncListenerMap = [];

    /**
     * @param Event $event
     */
    public function dispatch(Event $event): void
    {
        if (\count($this->asyncListenerMap[$event->name()] ?? [])) {
            $this->dispatchSync($event);

            return;
        }

        if (\count($this->listenerMap[$event->name()] ?? [])) {
            $this->dispatchSync($event);

            return;
        }
    }

    /**
     * @param Event $event
     */
    public function dispatchSync(Event $event): void
    {
        /** @var Listener[] $listeners */
        $listeners = $this->listenerMap[$event->name()];
        foreach ($listeners as $listener) {
            $listener->handle($event);
        }
    }

    /**
     * @param Event $event
     */
    public function dispatchAsync(Event $event): void
    {
        /** @var AsyncListener[] $asyncListeners */
        $asyncListeners = $this->asyncListenerMap[$event->name()];
        foreach ($asyncListeners as $listener) {
            $listener->handle($event);
        }
    }

    /**
     * @param Listener $listener
     */
    public function subscribe(Listener $listener): void
    {
        $this->listenerMap[$listener->listensTo()]   = $this->listenerMap[$listener->listensTo()] ?? [];
        $this->listenerMap[$listener->listensTo()][] = $listener;
    }

    /**
     * @param AsyncListener $listener
     */
    public function subscribeForAsync(AsyncListener $listener): void
    {
        $this->asyncListenerMap[$listener->listensTo()]   = $this->asyncListenerMap[$listener->listensTo()] ?? [];
        $this->asyncListenerMap[$listener->listensTo()][] = $listener;
    }
}
