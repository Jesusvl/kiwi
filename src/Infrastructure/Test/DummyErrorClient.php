<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Test;

use Kiwi\Error\Error\Infrastructure\ErrorClient;

/**
 * Class DummyErrorClient.
 */
class DummyErrorClient implements ErrorClient
{

    /**
     * @param string $message
     */
    public function sendMessage(string $message): void
    {
    }
}
