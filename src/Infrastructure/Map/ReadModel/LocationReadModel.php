<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Map\ReadModel;

use Kiwi\Map\Map\Document\CoordinatesDocument;
use Kiwi\Map\Map\Domain\Location;
use Kiwi\Map\Map\Infrastructure\LocationReadModel as BaseLocationReadModel;
use KiwiLib\Request\Curl;

/**
 * Class LocationReadModel.
 */
class LocationReadModel implements BaseLocationReadModel
{
    private const URL = 'https://maps.googleapis.com/maps/api/geocode/json?';

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $secret;

    /**
     * MapRepository constructor.
     *
     * @param string $token
     * @param string $secret
     */
    public function __construct(string $token, string $secret)
    {
        $this->token  = $token;
        $this->secret = $secret;
    }

    /**
     * @param Location $location
     *
     * @return CoordinatesDocument
     */
    public function findCoordinates(Location $location): CoordinatesDocument
    {
        $address = $location->address();
        $url     = self::URL;
        $address = \urlencode($address);
        $url     = "{$url}address={$address}";
        $this->buildSignedUrl($url);

        /** @var array $results */
        $results = Curl::jsonGet($url)['results'];

        foreach ($results as $result) {
            $countryCode    = '';
            $latitude       = (float)$result['geometry']['location']['lat'];
            $longitude      = (float)$result['geometry']['location']['lng'];

            /** @var array $components */
            $components = $result['address_components'];
            foreach ($components as $component) {
                switch ($component['types'][0]) {
                    case 'country':
                        $countryCode = $component['short_name'];
                        break;
                }
            }

            if ('ES' !== $countryCode) {
                continue;
            }

            return new CoordinatesDocument($latitude, $longitude);
        }

        return new CoordinatesDocument(null, null);
    }

    /**
     * @param string $url
     */
    private function buildSignedUrl(string &$url): void
    {
        $url .= "&client={$this->token}";

        $parsedUrl        = \parse_url($url);
        $urlToSign        = $parsedUrl['path'] . '?' . \str_replace(['|'], ['%7C'], $parsedUrl['query']);
        $decodedKey       = $this->decodeBase64UrlSafe($this->secret);
        $signature        = \hash_hmac('sha1', $urlToSign, $decodedKey, true);
        $encodedSignature = $this->encodeBase64UrlSafe($signature);

        $url = "{$url}&signature={$encodedSignature}";
    }

    /**
     * @param string $value
     *
     * @return string
     */
    private function encodeBase64UrlSafe(string $value): string
    {
        return \str_replace(['+', '/'], ['-', '_'], \base64_encode($value));
    }

    /**
     * @param string $value
     *
     * @return string
     */
    private function decodeBase64UrlSafe(string $value): string
    {
        return \base64_decode(\str_replace(['-', '_'], ['+', '/'], $value), true);
    }
}
