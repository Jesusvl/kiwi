<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Http\Exception;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class InternalServerErrorHttpException.
 */
class InternalServerErrorHttpException extends HttpException
{
    /**
     * InternalServerErrorHttpException constructor.
     *
     * @param string         $message
     * @param Throwable|null $exception
     */
    public function __construct(string $message, Throwable $exception = null)
    {
        if ('prod' === \getenv('APP_ENV')) {
            $message = 'Internal Server Error.';
        }
        parent::__construct(Response::HTTP_INTERNAL_SERVER_ERROR, $message, [], $exception);
    }
}
