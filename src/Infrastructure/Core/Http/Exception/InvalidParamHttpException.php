<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Http\Exception;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class InvalidParamHttpException.
 */
class InvalidParamHttpException extends HttpException
{
    /**
     * InvalidParamHttpException constructor.
     *
     * @param string         $name
     * @param array          $extras
     * @param Throwable|null $exception
     */
    public function __construct(string $name, array $extras = [], Throwable $exception = null)
    {
        $extras['name'] = $name;
        parent::__construct(Response::HTTP_CONFLICT, 'Invalid Param "%name%".', $extras, $exception);
    }
}
