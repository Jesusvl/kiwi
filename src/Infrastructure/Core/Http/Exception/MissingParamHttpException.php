<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Http\Exception;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class MissingParamHttpException.
 */
class MissingParamHttpException extends HttpException
{
    /**
     * ResourceNotFoundHttpException constructor.
     *
     * @param string         $name
     * @param Throwable|null $exception
     */
    public function __construct(string $name, Throwable $exception = null)
    {
        parent::__construct(Response::HTTP_BAD_REQUEST, 'Missing Param "%param%".', ['param' => $name], $exception);
    }
}
