<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Http\Exception;

use Error;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException as SymfonyHttpException;
use Throwable;

/**
 * Class HttpException.
 */
abstract class HttpException extends SymfonyHttpException
{
    /**
     * @var int
     */
    protected $status;

    /**
     * @var array
     */
    protected $extras;

    /**
     * HttpException constructor.
     *
     * @param int            $status
     * @param null|string    $message
     * @param array          $extras
     * @param null|Throwable $exception
     */
    public function __construct(int $status, ?string $message, array $extras = [], ?Throwable $exception = null)
    {
        $this->extras = $extras;
        $this->status = $status;
        $message      = $message ?? 'Exception has been thrown.';

        $diff = [];
        foreach ($extras as $key => $value) {
            $diff["%${key}%"] = $value;
        }
        $message = \strtr($message, $diff);

        if ($exception instanceof Error) {
            $exception = new Exception($exception->getMessage(), $exception->getCode(), $exception);
        }
        parent::__construct($status, $message, $exception);

        if ('prod' !== \getenv('APP_ENV') && '1' === \getenv('APP_DEBUG')) {
            $this->extras['exception'] = $this->toArray();
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getMessage();
    }

    /**
     * @return array
     */
    public function getExtras(): array
    {
        return $this->extras;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return array
     */
    protected function toArray(): array
    {
        $prev = $this->getPrevious();

        if ($prev) {
            $prev = $prev->getTrace();
        }

        return  [
            'message'  => $this->getMessage(),
            'code'     => $this->getCode(),
            'file'     => $this->getFile(),
            'line'     => $this->getLine(),
            'status'   => $this->getStatus(),
            'previous' => $prev
        ];
    }
}
