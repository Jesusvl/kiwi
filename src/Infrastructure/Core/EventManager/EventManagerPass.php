<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\EventManager;

use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Core\Infrastructure\Listener;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EventManagerPass.
 */
class EventManagerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(EventManager::class)) {
            return;
        }

        $listenerDefinition = $container->findDefinition(EventManager::class);
        $ids                = $container->getServiceIds();

        foreach ($ids as $id) {
            if (\class_exists($id)) {
                $interfaces = \class_implements($id);

                if ($interfaces && \in_array(Listener::class, $interfaces, true)) {
                    $listenerDefinition->addMethodCall('subscribe', [new Reference($id)]);
                }

                if ($interfaces && \in_array(AsyncListener::class, $interfaces, true)) {
                    $listenerDefinition->addMethodCall('subscribeForAsync', [new Reference($id)]);
                }
            }
        }
    }
}
