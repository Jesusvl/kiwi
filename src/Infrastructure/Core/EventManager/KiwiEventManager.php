<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\EventManager;

use Kiwi\Core\Infrastructure\AsyncListener;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Core\Infrastructure\Listener;
use Kiwi\Core\Infrastructure\Worker;

/**
 * Class KiwiEventManager.
 */
class KiwiEventManager implements EventManager
{
    /**
     * @var array
     */
    private $listenerMap = [];

    /**
     * @var array
     */
    private $asyncListenerMap = [];

    /**
     * @var Worker
     */
    private $worker;

    /**
     * KiwiEventManager constructor.
     *
     * @param Worker          $worker
     */
    public function __construct(Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * @param Event $event
     */
    public function dispatch(Event $event): void
    {
        foreach ($this->asyncListenerMap as $boundary => $boundaryMap) {
            if (count($boundaryMap[$event::name()] ?? [])) {
                $this->worker->publish($event, $boundary);
            }
        }

        if (\count($this->listenerMap[$event->name()] ?? [])) {
            $this->dispatchSync($event);

            return;
        }
    }

    /**
     * @param Event $event
     */
    public function dispatchSync(Event $event): void
    {
        /** @var Listener[] $listeners */
        $listeners = $this->listenerMap[$event::name()];
        foreach ($listeners as $listener) {
            $listener->handle($event);
        }
    }

    /**
     * @param Event $event
     */
    public function dispatchAsync(Event $event): void
    {
        $boundary = $this->getBoundary($event);

        $boundaryMap = $this->asyncListenerMap[$boundary];
        /** @var AsyncListener[] $listeners */
        $listeners = $boundaryMap[$event::name()] ?? [];

        foreach ($listeners as $listener) {
            $listener->handle($event);
        }
    }

    /**
     * @param Listener $listener
     */
    public function subscribe(Listener $listener): void
    {
        $this->listenerMap[$listener->listensTo()]   = $this->listenerMap[$listener->listensTo()] ?? [];
        $this->listenerMap[$listener->listensTo()][] = $listener;
    }

    /**
     * @param AsyncListener $listener
     */
    public function subscribeForAsync(AsyncListener $listener): void
    {
        $boundary = $this->getBoundary($listener);

        $this->asyncListenerMap[$boundary][$listener->listensTo()]   =
            $this->asyncListenerMap[$listener->listensTo()] ?? [];
        $this->asyncListenerMap[$boundary][$listener->listensTo()][] = $listener;
    }

    /**
     * @param Event|Listener|AsyncListener $object
     *
     * @return string
     */
    public function getBoundary($object): string
    {
        $names = explode('\\', get_class($object));
        $boundary = "{$names[0]}.{$names[1]}";
        $this->asyncListenerMap[$boundary] = $this->asyncListenerMap[$boundary] ?? [];
        $this->listenerMap[$boundary]      = $this->listenerMap[$boundary] ?? [];

        return $boundary;
    }
}
