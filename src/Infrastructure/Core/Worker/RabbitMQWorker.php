<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Worker;

use ErrorException;
use Kiwi\Core\Infrastructure\Event;
use Kiwi\Core\Infrastructure\EventManager;
use Kiwi\Core\Infrastructure\Worker;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;
use Throwable;

/**
 * Class RabbitMQ.
 */
class RabbitMQWorker implements Worker
{
    /**
     * @var AMQPStreamConnection
     */
    private $worker;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RabbitMQWorker|null
     */
    private $newWorker;

    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @var array
     */
    private $map;

    /**
     * RabbitMQWorker constructor.
     *
     * @param LoggerInterface $logger
     * @param string          $host
     * @param string          $port
     * @param string          $user
     * @param string          $pass
     * @param string          $vhost
     */
    public function __construct(
        LoggerInterface $logger,
        string $host,
        string $port,
        string $user,
        string $pass,
        string $vhost
    ) {
        $this->logger = $logger;
        $config       = Yaml::parseFile(__DIR__ . '/_config/workers.yaml');
        $this->map    = $config['workers'];
        try {
            $connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);
            $channel    = $connection->channel();
            //$channel->queue_bind('gos_websocket', 'gos_websocket_exchange');

            foreach (array_keys($this->map) as $name) {
                $channel->queue_declare("KIWI_QUEUE_$name", false, true, false, false);
                $channel->exchange_declare("KIWI_EXCHANGE_$name", 'direct', false, true, false);
                $channel->queue_bind("KIWI_QUEUE_$name", "KIWI_EXCHANGE_$name");
            }

            $this->worker     = $channel;
            $this->connection = $connection;
        } catch (ErrorException $e) {
            $this->logger->warning('Cannot connect to RabbitMQ', [$e->getMessage()]);
        }
    }

    /**
     * @param EventManager $eventManager
     */
    public function setEventManager(EventManager $eventManager): void
    {
        $this->eventManager = $eventManager;
    }

    /**
     * @param Event  $event
     * @param string $boundary
     */
    public function publish(Event $event, string $boundary): void
    {
        if ($this->newWorker) {
            $this->newWorker->publish($event, $boundary);

            return;
        }

        $classParts  = explode('\\', get_class($event));
        $boundarySrc = "{$classParts[0]}.{$classParts[1]}";

        $class = get_class($event);

        if ($boundarySrc !== $boundary) {
            if ($this->map[$boundary] ?? false) {
                if ($this->map[$boundary][$event::name()] ?? false) {
                    $class = $this->map[$boundary][$event::name()];
                } else {
                    return;
                }
            } else {
                return;
            }
        }

        $message = new AMQPMessage(
            \json_encode([$class, $event->serialize()]),
            ['content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
        );

        $this->worker->basic_publish($message, "KIWI_EXCHANGE_$boundary");
    }

    /**
     * @param string $consumer
     * @param string $boundary
     */
    public function consume(string $consumer, string $boundary): void
    {
        if (!$this->worker) {
            $this->logger->error('Cannot connect to RabbitMQ');

            return;
        }

        $this->worker->basic_consume(
            "KIWI_QUEUE_$boundary",
            $consumer,
            false,
            false,
            false,
            false,
            [$this, 'consumeMessage']
        );

        while (\count($this->worker->callbacks)) {
            $this->worker->wait();
        }
    }

    /**
     * @param AMQPMessage $message
     */
    public function consumeMessage(AMQPMessage $message): void
    {
        gc_collect_cycles();
        /** @var AMQPChannel $channel */
        $channel = $message->delivery_info['channel'];
        $decoded = \json_decode($message->body);

        try {
            if (!class_exists($decoded[0])) {
                $this->logger->error("Class '{$decoded[0]}' does not exist.");
                $channel->basic_ack($message->delivery_info['delivery_tag']);

                return;
            }

            /** @var Event $event */
            $event     = \call_user_func($decoded[0] . '::deserialize', $decoded[1]);
            $eventName = \call_user_func($decoded[0] . '::name');

            $this->logger->info("{$eventName} -> {$event->serialize()}", [\get_class($event)]);

            try {
                $this->eventManager->dispatchAsync($event);
            } catch (Throwable $e) {
                $this->logger->error("{$eventName}: {$e->getMessage()}");
            }

            $channel->basic_ack($message->delivery_info['delivery_tag']);
        } catch (ErrorException $error) {
            $channel->basic_ack($message->delivery_info['delivery_tag']);
            $channel->close();
            $this->connection->close();
        } catch (\Throwable $error) {
            $channel->basic_ack($message->delivery_info['delivery_tag']);
            $channel->close();
            $this->connection->close();
        }
    }
}
