<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Prooph;

use Kiwi\Core\Infrastructure\Bus;
use Kiwi\Core\Infrastructure\Command;
use Kiwi\Core\Infrastructure\Query;

/**
 * Class Bus.
 */
class ProophBus implements Bus
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * ProophBus constructor.
     *
     * @param QueryBus   $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus   = $queryBus;
        $this->commandBus = $commandBus;
    }

    /**
     * @param Query $query
     *
     * @return mixed
     */
    public function dispatchQuery(Query $query)
    {
        return $this->queryBus->dispatch($query);
    }

    /**
     * @param Command $command
     */
    public function dispatchCommand(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
