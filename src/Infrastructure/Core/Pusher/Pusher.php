<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Pusher;

use Gos\Bundle\WebSocketBundle\Pusher\PusherInterface;
use Kiwi\Core\Domain\Topic;
use Kiwi\Core\Domain\UserId;
use Kiwi\Core\Infrastructure\Socket;

/**
 * Class Pusher.
 */
class Pusher implements Socket
{
    /**
     * @var PusherInterface
     */
    private $pusher;

    /**
     * Pusher constructor.
     *
     * @param PusherInterface $pusher
     */
    public function __construct(PusherInterface $pusher)
    {
        $this->pusher = $pusher;
    }

    /**
     * @param Topic       $topic
     * @param array       $message
     * @param UserId|null $userId
     */
    public function push(Topic $topic, array $message, ?UserId $userId = null): void
    {
        $this->pusher->push([
            'data' => $message,
            'type' => $topic->type()
        ], $topic->name());
    }
}
