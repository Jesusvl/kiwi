<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\Pusher;

use Exception;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Gos\Bundle\WebSocketBundle\Server\App\Dispatcher\TopicDispatcher;
use Gos\Bundle\WebSocketBundle\Server\Exception\FirewallRejectionException;
use Gos\Bundle\WebSocketBundle\Topic\PushableTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\SecuredTopicInterface;
use Gos\Bundle\WebSocketBundle\Topic\TopicPeriodicTimerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;

/**
 * Class FixTopicDispatcher.
 */
class FixTopicDispatcher extends TopicDispatcher
{
    /**
     * @param string                   $calledMethod
     * @param ConnectionInterface|null $conn
     * @param Topic                    $topic
     * @param WampRequest              $request
     * @param null                     $payload
     * @param null                     $exclude
     * @param null                     $eligible
     * @param null                     $provider
     *
     * @return bool
     *
     * @throws Exception
     */
    public function dispatch(
        $calledMethod,
        ConnectionInterface $conn = null,
        Topic $topic,
        WampRequest $request,
        $payload = null,
        $exclude = null,
        $eligible = null,
        $provider = null
    ): bool {
        $dispatched = false;

        if ($topic) {
            foreach ((array)$request->getRoute()->getCallback() as $callback) {
                $appTopic = $this->topicRegistry->getTopic($callback);

                if ($appTopic instanceof SecuredTopicInterface) {
                    try {
                        $appTopic->secure($conn, $topic, $request, $payload, $exclude, $eligible, $provider);
                    } catch (FirewallRejectionException $e) {
                        if ($conn) {
                            $conn->callError(
                                $topic->getId(),
                                $topic,
                                sprintf('You are not authorized to perform this action: %s', $e->getMessage()),
                                [
                                    'topic'   => $topic,
                                    'request' => $request,
                                    'event'   => $calledMethod
                                ]
                            );

                            $conn->close();
                        }
                        $dispatched = false;

                        return $dispatched;
                    }
                }

                if ($appTopic instanceof TopicPeriodicTimerInterface) {
                    $appTopic->setPeriodicTimer($this->topicPeriodicTimer);

                    if (false === $this->topicPeriodicTimer->isRegistered($appTopic) && 0 !== count($topic)) {
                        $appTopic->registerPeriodicTimer($topic);
                    }
                }

                if ($calledMethod === static::UNSUBSCRIPTION && 0 === count($topic)) {
                    $this->topicPeriodicTimer->clearPeriodicTimer($appTopic);
                }

                if ($calledMethod === static::PUSH) {
                    if (!$appTopic instanceof PushableTopicInterface) {
                        throw new Exception(sprintf('Topic %s doesn\'t support push feature', $appTopic->getName()));
                    }

                    $appTopic->onPush($topic, $request, $payload, $provider);
                    $dispatched = true;
                } else {
                    try {
                        if (null !== $payload) { //its a publish call.
                            $appTopic->{$calledMethod}($conn, $topic, $request, $payload, $exclude, $eligible);
                        } else {
                            $appTopic->{$calledMethod}($conn, $topic, $request);
                        }

                        $dispatched = true;
                    } catch (Exception $e) {
                        $this->logger->error($e->getMessage(), [
                            'code'  => $e->getCode(),
                            'file'  => $e->getFile(),
                            'trace' => $e->getTraceAsString(),
                        ]);

                        if ($conn) {
                            $conn->callError($topic->getId(), $topic, $e->getMessage(), [
                                'topic'   => $topic,
                                'request' => $request,
                                'event'   => $calledMethod,
                            ]);
                        }
                        $dispatched = false;
                    }
                }
            }
        }

        return $dispatched;
    }
}
