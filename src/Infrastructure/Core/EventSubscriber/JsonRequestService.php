<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class JsonRequestService.
 */
class JsonRequestService implements EventSubscriberInterface
{
    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        $request = $event->getRequest();

        if ('application/json' === $request->headers->get('Content-Type')) {
            $data = \json_decode($request->getContent(), true);
            $request->request->replace(\is_array($data) ? $data : []);
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController'
        ];
    }
}
