<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\ReadModel;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Iterator;
use Kiwi\Core\Document\Document;
use Kiwi\Core\Document\DocumentBook;
use Kiwi\Core\Document\PaginatedDocument;
use Kiwi\Core\Domain\Filter;
use Kiwi\Core\Domain\Pagination;
use Kiwi\Core\Domain\Parts;
use Kiwi\Core\Domain\Sort;
use Kiwi\Core\Domain\Uuid;

/**
 * Class DoctrineReadModel.
 */
abstract class DoctrineReadModel
{
    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var array
     */
    private $lastResult;

    /**
     * @var array
     */
    private $lastResult2;

    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var string
     */
    private $fetchedClass;

    /**
     * @var int
     */
    private $joins;

    /**
     * @var int
     */
    private $attributes;

    /**
     * @var Uuid|null
     */
    private $retrieveId;

    /**
     * DoctrineRepository constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->lastResult = [];
        $this->lastResult2 = [];
        $this->joins = 0;
        $this->attributes = 0;
    }

    /**
     * @param Iterator $data
     */
    protected function mapResult(Iterator $data): void
    {
        foreach ($data as $item) {
            if ($item) {
                $this->lastResult2[\get_class($item)] = $this->lastResult2[\get_class($item)] ?? [];
                if (\is_string($item->id())) {
                    $this->lastResult[$item->id()] = $item;
                    $this->lastResult2[\get_class($item)][$item->id()] = $item;
                } else {
                    $this->lastResult[$item->id()->id()] = $item;
                    $this->lastResult2[\get_class($item)][$item->id()->id()] = $item;
                }
            }
        }
    }

    /**
     * @param array $data
     */
    protected function mapArrayResult(array $data): void
    {
        foreach ($data as $item) {
            if ($item) {
                $this->lastResult2[\get_class($item)] = $this->lastResult2[\get_class($item)] ?? [];
                $this->lastResult[$item->id()->id()] = $item;
                $this->lastResult2[\get_class($item)][$item->id()->id()] = $item;
            }
        }
    }

    /**
     * @param Uuid $id
     *
     * @deprecated
     *
     * @return mixed|null
     */
    public function fetchResultAAAAA(?Uuid $id)
    {
        if (!$id) {
            return null;
        }

        return $this->lastResult[$id->id()] ?? null;
    }

    /**
     * @param string $class
     * @param Uuid   $id
     *
     * @return mixed|null
     */
    public function fetchResult(string $class, ?Uuid $id)
    {
        if (!$id) {
            return null;
        }

        return $this->lastResult2[$class][$id->id()] ?? null;
    }

    /**
     * @return array
     */
    public function results(): array
    {
        return $this->lastResult;
    }

    protected function clearResult(): void
    {
        unset($this->lastResult, $this->lastResult2);
        $this->lastResult = [];
        $this->lastResult2 = [];
    }

    /**
     * @param string   $class
     * @param Iterator $iterator
     *
     * @return array
     */
    protected function toBaseDocumentArray(string $class, Iterator $iterator): array
    {
        $result = [];
        foreach ($iterator as $item) {
            $result[] = new $class($item);
        }

        return $result;
    }

    /**
     * @param string    $entity
     * @param Uuid|null $id
     *
     * @return DoctrineReadModel
     */
    protected function select(string $entity, ?Uuid $id = null): DoctrineReadModel
    {
        $this->fetchedClass = $entity;
        $this->retrieveId = $id;

        $qb = $this->manager->createQueryBuilder();
        $qb
            ->select('e')
            ->from($entity, 'e')
            ->where('1=1');

        if ($id) {
            $qb->where('e.id=:e_id')
                ->setParameter('e_id', $id);
        }

        $this->queryBuilder = $qb;

        return $this;
    }

    /**
     * @param string $entity
     * @param string $field
     * @param bool   $addSelect
     *
     * @return DoctrineReadModel
     */
    protected function join(string $entity, string $field, bool $addSelect = true): DoctrineReadModel
    {
        $this->joins++;
        $alias = "e{$this->joins}";
        $this->queryBuilder->leftJoin($entity, $alias, Join::WITH, "{$alias}.id = e.{$field}");

        if ($addSelect) {
            $this->queryBuilder->addSelect($alias);
        }

        return $this;
    }

    /**
     * @param bool   $condition
     * @param string $entity
     * @param string $field
     * @param bool   $addSelect
     *
     * @return DoctrineReadModel
     */
    protected function conditionalJoin(
        bool $condition,
        string $entity,
        string $field,
        bool $addSelect = true
    ): DoctrineReadModel {
        if ($condition) {
            return $this->join($entity, $field, $addSelect);
        }

        return $this;
    }

    /**
     * @param string $condition
     * @param string $field
     * @param        $value
     *
     * @return DoctrineReadModel
     */
    protected function addCondition(string $condition, string $field, $value): self
    {
        $this->queryBuilder
            ->andWhere($condition)
            ->setParameter($field, $value);

        return $this;
    }

    /**
     * @param Sort $sortBy
     *
     * @return DoctrineReadModel
     */
    protected function orderBy(?Sort $sortBy = null): DoctrineReadModel
    {
        if ($sortBy) {
            $this->queryBuilder->orderBy("e.{$sortBy->attribute()}", $sortBy->type());
        }

        return $this;
    }

    /**
     * @return Document
     */
    protected function retrieveOne(): Document
    {
        $result = $this->queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($result);
        $doc = $this->createDocument($this->retrieveId);
        $this->clearResult();

        return $doc;
    }

    /**
     * @param Document $document
     *
     * @return Document
     */
    public function retrieveOneOrDefault(Document $document): Document
    {
        /** @var Document[] $list */
        $list = $this->retrieveAll()->list();

        if (count($list)) {
            return $list[0];
        }

        return $document;
    }

    /**
     * @param string $attribute
     * @param        $value
     *
     * @return DoctrineReadModel
     */
    public function andHas(string $attribute, $value): self
    {
        $this->attributes++;
        $this->queryBuilder
            ->andWhere("e.{$attribute}=:attr_{$this->attributes}")
            ->setParameter("attr_{$this->attributes}", $value);

        return $this;
    }

    /**
     * @param string $condition
     *
     * @return DoctrineReadModel
     */
    public function andCondition(string $condition): self
    {
        $this->queryBuilder->andWhere($condition);

        return $this;
    }

    /**
     * @return DocumentBook
     */
    protected function retrieveAll(): DocumentBook
    {
        /** @var array $list */
        $list = $this->queryBuilder
            ->getQuery()
            ->getResult();

        $this->mapArrayResult($list);
        $result = [];

        foreach ($list as $data) {
            if (is_a($data, $this->fetchedClass)) {
                $result[] = $this->createDocument($data->id());
            }
        }
        $this->clearResult();

        return new DocumentBook($result);
    }

    /**
     * @param Pagination $filter
     *
     * @return PaginatedDocument
     */
    protected function retrievePaginated(Pagination $filter): PaginatedDocument
    {
        $this->queryBuilder
            ->setMaxResults($filter->pageSize())
            ->setFirstResult($filter->firstResult());

        $paginator = new Paginator($this->queryBuilder->getQuery());
        $this->mapResult($paginator->getIterator());
        $result = [];
        foreach ($paginator as $data) {
            if (is_a($data, $this->fetchedClass)) {
                $result[] = $this->createDocument($data->id());
            }
        }
        $this->clearResult();

        return new PaginatedDocument(
            $result,
            $filter->page(),
            $filter->pageSize(),
            $paginator->count()
        );
    }

    /**
     * @param Filter[] $filters
     *
     * @return DoctrineReadModel
     */
    public function applyFilters(array $filters): DoctrineReadModel
    {
        $matches = [
            'lte' => '<=',
            'lt' => '<',
            'eq' => '=',
            'neq' => '!=',
            'gt' => '>',
            'gte' => '>='
        ];

        foreach ($filters as $key => $filter) {
            if (!$this->applyCustomFilter($filter, $this->queryBuilder)) {
                $this->queryBuilder
                    ->andWhere("e.{$filter->name()} {$matches[$filter->comparator()]} :f_{$key}")
                    ->setParameter("f_{$key}", $filter->value());
            }
        }

        return $this;
    }

    /**
     * @param Filter       $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    abstract protected function applyCustomFilter(Filter $filter, QueryBuilder $queryBuilder): bool;

    /**
     * @param Uuid $id
     *
     * @return Document
     */
    abstract protected function createDocument(Uuid $id): Document;

    /**
     * @param Parts $parts
     *
     * @return DoctrineReadModel
     */
    abstract protected function buildJoins(?Parts $parts): DoctrineReadModel;
}
