<?php

declare(strict_types=1);

namespace KiwiInfrastructure\Core\WriteModel;

use Doctrine\ORM\EntityManagerInterface;
use Kiwi\Core\Domain\Uuid;
use Kiwi\Core\Exception\InfrastructureException;
use Kiwi\Shop\Client\Infrastructure\ClientReadModel;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DoctrineRepository.
 */
abstract class DoctrineWriteModel
{
    protected const PAGE_SIZE = 30;

    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var array
     */
    private $lastResult;

    /**
     * @var bool
     */
    static private $noCache = true;

    /**
     * DoctrineRepository constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager    = $manager;
        $this->lastResult = [];
    }

    public static function disableCache(): void
    {
        self::$noCache = true;
    }

    public static function enableCache(): void
    {
        self::$noCache = false;
    }

    /**
     * @param array $data
     */
    protected function mapResult(array $data): void
    {
        foreach ($data as $item) {
            if ($item) {
                if (\is_string($item->id())) {
                    $this->lastResult[$item->id()] = $item;
                } else {
                    $this->lastResult[$item->id()->id()] = $item;
                }
            }
        }
    }

    /**
     * @param array $data
     */
    protected function mapResultArray(array $data): void
    {
        foreach ($data as $item) {
            if ($item) {
                if (\is_string($item->id())) {
                    $this->lastResult[$item->id()] = $item;
                } else {
                    $this->lastResult[$item->id()->id()] = $item;
                }
            }
        }
    }

    /**
     * @param string|null $id
     *
     * @return mixed|null
     */
    protected function fetchResult(?string $id)
    {
        return $this->lastResult[$id] ?? null;
    }

    /**
     * @param Uuid|null $id
     *
     * @return mixed|null
     */
    protected function fetchResultById(Uuid $id)
    {
        return $this->lastResult[$id->id()];
    }

    protected function clearResult(): void
    {
        unset($this->lastResult);
        $this->lastResult = [];
    }

    /**
     * @param $entity
     */
    protected function persist($entity): void
    {
        $this->manager->merge($entity);
        $this->manager->flush();
        if (!self::$noCache) {
            $this->manager->clear();
        }
    }

    /**
     * @param $entity
     */
    protected function remove($entity): void
    {
        $this->manager->remove($entity);
        $this->manager->flush();
        if (self::$noCache) {
            $this->manager->clear();
        }
    }

    /**
     * @param string $class
     * @param        $object
     * @param array  $exclude
     *
     * @throws InfrastructureException
     */
    protected function override(string $class, $object, array $exclude = []): void
    {
        $queryBuilder = $this->manager->createQueryBuilder()
            ->update($class, 'e');

        try {
            $reflect = new ReflectionClass($object);
        } catch (ReflectionException $e) {
            throw new InfrastructureException($e);
        }
        $attrs = $reflect->getProperties();

        foreach ($attrs as $attr) {
            $name = $attr->getName();

            if ('id' === $name || 'id' === $name) {
                continue;
            }

            if (in_array($name, $exclude, true)) {
                continue;
            }

            $attr->setAccessible(true);
            $value = $attr->getValue($object);
            $attr->setAccessible(false);
            if (null !== $value) {
                $queryBuilder
                    ->set("e.${name}", ":{$name}")
                    ->setParameter($name, $value);
            }
        }
        $queryBuilder->where('e.id = :id')
            ->setParameter('id', $object->id())
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $class
     * @param array  $iterator
     *
     * @return array
     */
    protected function toBaseDocumentArray(string $class, array $iterator): array
    {
        $result = [];
        foreach ($iterator as $item) {
            $result[] = new $class($item);
        }

        return $result;
    }
}
