# kiwi

## Installation

### Local

brew install rabbitmq \
rabbitmqctl add_user root 1234 \
rabbitmqctl set_permissions -p / root ".*" ".*" ".*" \
rabbitmqctl set_user_tags root administrator \
brew services start rabbitmq \ 

rabbitmq-plugins enable rabbitmq_web_stomp

### Heroku
heroku ps:scale worker=1 \

### TODO
OrderLocation
Domains in infrastructure
No EventBusOnHandlers
BetterDocumentCreation

### Sentry
https://b17d8a22599c40a1b579f458d5d05a74:61dde1e8eea1462e8c0091e651c475cf@sentry.io/267808